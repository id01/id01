from bliss.controllers.counter import CalcCounterController
from bliss.config.beacon_object import BeaconObject
from bliss.common.counter import CalcCounter
from bliss.common.scans import ct
from bliss.config import settings
from bliss import current_session, setup_globals, global_map

import pdb
import numpy as np

class ID01CounterParameters(CalcCounter,BeaconObject):
    """
    Class instance to hold all parameters related to pseudoCounters
    """

    gain = BeaconObject.property_setting('gain',default=3)
    offset = BeaconObject.property_setting('offset',default=0)
    factor = BeaconObject.property_setting('factor',default=0)
    gain_range = BeaconObject.property_setting('range',default=0) 
    maxrate = BeaconObject.property_setting('maxrate',default=0)
    
                
    def __init__(self,name,config,controller,dim):
        """
        Initialise beacon objects 
        """
        CalcCounter.__init__(self,  name,
                                    controller,
                                    dim,
                                    unit=config.get('unit'),)
                                    
        BeaconObject.__init__(self, config,
                                    name=name,
                                    share_hardware=False)
                                    
        self.gain_dict = settings.HashObjSetting(f'{name}:gain_dict')

        self.init_gains()
        
        # setup hardware related parameters
        hardware_config = config.get('hardware')
        if hardware_config is not None:
            self.hardware_type = hardware_config.get('type')
            self.hardware_type_amp = hardware_config.get('type_amp')
            self.device = hardware_config.get('device')
            self.channels = hardware_config.get('channels')
            self.speed_channel = hardware_config.get('speed')
            
            # TODO set the coupling i.e. always grounded
            self.coupling_channel = hardware_config.get('coupling')
            self.coupling_default = hardware_config.get('coupling_default')
            if self.coupling_channel is not None:
                print('here',self.coupling_channel,self.coupling_default)
                pass
                # this works in the terminal 
                #ID01_pseudo_counter.counters.mon1.device.set("f0dout1_3",[0])
                #self.device.set(self.coupling_channel,[self.coupling_default])
        else:
            self.device = None
            self.channels = []
            
        #TODO need a gain set here from the config to ensure all devices
        # are in a good state
        #self.use_device_gain()
    
    def init_gains(self):
        """
        Check the gain dictionary has all keys found in the config file,
        if it does not exist apply the config file.
        """
        config_gains = self.config.get('gains',[])
        for cfg in config_gains:
            value = cfg.get('value',0)
            tmp_dict = self.gain_dict.get(value,{})
            tmp_dict.setdefault('offset',cfg.get('offset',0))
            tmp_dict.setdefault('factor', cfg.get('factor',0))
            tmp_dict.setdefault('range',cfg.get('range',0))
            self.gain_dict[value] = tmp_dict

    def apply_config(self):
        """
        reload gain_dict from config .yaml
        """
        config_gains = self.config.get('gains',[])
        for cfg in config_gains:
            value = cfg.get('value',0)
            tmp_dict = {}
            tmp_dict['offset'] = cfg.get('offset',0)
            tmp_dict['factor'] = cfg.get('factor',0)
            tmp_dict['range'] = cfg.get('range',0)
            self.gain_dict[value] = tmp_dict
    
    
    # not clear if the set/get are needed in a purely Bliss world
    # i.e.  multiple sessions

    def get_device_gain(self, ):
        """
        read the gain set in the device
        this was only really made for SPEC - which can update the gain 
        setting without Bliss knowing
        """
        # EBV
        if self.hardware_type=='ebv':
            tmp_dict = self.gain_dict[self.gain]
            self.gain_range = tmp_dict.get('range',0)
            tmp_value = self.device.diode_range_available.index(self.gain_range)+1 # +1 we live in python
        
        # FEMTO  - binary  
        elif self.hardware_type=='wago' and self.hardware_type_amp=='femto':
            if self.device.get(self.speed_channel)==1:
                offset = 1
            else:
                offset = 8
            st="0b"
            rev_channels = self.channels[::-1]
            for channel in rev_channels:
                #print(channel, self.device.get(channel))
                st+="%i"%self.device.get(channel)
            tmp_value = int(st,2)+offset
            
        # NOVELEC  - not binary!
        elif self.hardware_type=='wago' and self.hardware_type_amp=='novelec':
            #gain_map = {1:(0,0,1),2:(0,1,0),3:(1,0,0)}
            gain_map = {(0,0,1):1,(0,1,0):2,(1,0,0):3}
            output_bits = ()
            for channel in self.channels:
                #print(channel, self.device.get(channel))
                output_bits += (int(self.device.get(channel)),)
            #print(output_bits)
            try:
                tmp_value = gain_map[output_bits]
            except:
                print("could not map this gain: ", output_bits, "\n not found in: ",gain_map)
                tmp_value = gain_map[(0,0,1)]
                print("setting gain to minimum: ",tmp_value)
        else:
            tmp_value=0
        return tmp_value
        
    def check_device_gain(self,):
        if self.gain !=  self.get_device_gain():
            print("device gain: {} redis gain: {}".format(self.gain,self.get_device_gain()))
            print("take the device gain with self.use_device_gain")
            print("or just set a new gain with self.gain = value")
    
    def use_device_gain(self,):
        value = self.get_device_gain()
        self.gain = value
                
    @BeaconObject.property(default=1)
    def gain(self):
        return self.gain   
        
    @gain.setter
    def gain(self, value):
        tmp_dict = self.gain_dict[value]
        self.offset = tmp_dict.get('offset',0)
        self.factor = tmp_dict.get('factor',0)
        tmp_value = value
        
        if self.gain != self.get_device_gain():
            print("outside agent must have changed the gain of {} - BEWARE".format(self.name))        

        # ESRF beamviewers (EBV)
        if self.hardware_type=='ebv':
            self.gain_range = tmp_dict.get('range',0)
            #print(self.gain_range,tmp_dict)
            self.device.diode_range = self.gain_range
        
        # FEMTO
        elif self.hardware_type=='wago' and self.hardware_type_amp=='femto':
            #workaround for the femto 2 levels - binary!
            if self.speed_channel is not None:
                if value <=7:
                    # slow 1
                    self.device.set(self.speed_channel,1)
                    self.speed = 1
                    tmp_value -= 1
                else: # fast
                    self.device.set(self.speed_channel,0) 
                    self.speed = 0
                    # modify gain
                    tmp_value -= 8
                    
                    
            # set the gain based on the bit value of the gain
            def _get_bit(value) :
                for i in range(len(self.channels)):
                    yield value & 0x1
                    value >>= 1
                
            #set the gain in the wago
            if self.device:
                for channel,ch_val in zip(self.channels,_get_bit(tmp_value)):
                    #print(channel,ch_val)
                    self.device.set(channel,ch_val)

        # bv0 is not a standard EBV hence novelec
        #NOVELEC
        elif self.hardware_type=='wago' and self.hardware_type_amp=='novelec':
            #workaround for the novelec bits - not binary!
            gain_map = {1:[0,0,1],2:[0,1,0],3:[1,0,0]}       
                
            #set the gain in the wago
            if self.device:
                for channel,ch_val in zip(self.channels,gain_map[value]):
                    #print(channel,ch_val)
                    self.device.set(channel,ch_val)
            
    def set_offset(self, gain_id, newoffset):
        """
        set the offset
        value = gain no. to change
        newfactor = offset
        """
        tmp_gain = self.gain
        if tmp_gain != gain_id:
            print(f"WARNING: you are changing the offset for gain ({gain_id}) but {self.name} is set to gain ({tmp_gain})")
        tmp_dict = self.gain_dict[gain_id]
        tmp_dict["offset"] = np.array([newoffset])
        self.gain_dict[gain_id] = tmp_dict
        self.gain = gain_id
        self.gain = tmp_gain

    
    def set_factor(self, gain_id, newfactor):
        """
        set the scale factor
        gain_id = gain no. to change
        newfactor = scale factor
        """
        tmp_gain = self.gain
        if tmp_gain != gain_id:
            print(f"WARNING: you are changing the factor for gain ({gain_id}) but {self.name} is set to gain ({tmp_gain})")
        tmp_dict = self.gain_dict[gain_id]
        tmp_dict["factor"] = newfactor
        self.gain_dict[gain_id] = tmp_dict
        self.gain = gain_id
        self.gain = tmp_gain

    def find_offset(self, gain_id, exposure_time):
        """
        determine the offset for a  given gain value and exposure time
        offset defined in cts/sec and set
        """
        tmp_gain = self.gain
        if tmp_gain != gain_id:
            print(f"WARNING: you are finding the offset for gain ({gain_id}) but {self.name} is set to gain ({tmp_gain})") 
            print(f"setting gain {gain_id}...")
            self.gain = gain_id       
        scan_obj = ct(exposure_time)
        #time = scan_obj.get_data('acq_time')
        counts = scan_obj.get_data("ct_"+self.name)
        counts = scan_obj.get_data(self.name)[0]
        offset = counts/time
        self.set_offset(gain_id, float(offset[0]))
        self.gain = tmp_gain        
        
class ID01pseudoCounters(CalcCounterController,BeaconObject):
    """
    state = BeaconObject.property_setting('state',default=1)

    @BeaconObject.property(default=1)
    def state(self):
        return self.state   
        
    @state.setter
    def state(self, value):
        self.state = value
    """

    def __init__(self,name,config,register_counter=True):
        CalcCounterController.__init__(self,name,config,register_counter)
        BeaconObject.__init__(self,config,name,share_hardware=False)

        #fe_counters
        self.fe_counters = ["att2","bv0","bv1","bv2","opt1","opt2"]
        #ss_counters
        self.safshut_counters = ["mon1","mon2","sax1","exp1","exp2","bv3","bv4"]
    
    def _new_calc_counter(self,cnt_config,cnt_name,controller,dim):
        return ID01CounterParameters(cnt_name,cnt_config,controller,dim)
    
 
    def calc_function(self, input_dict):
        """
        the pseudocounter function is:  (counter/acq_time  -offset*acq_time ) * gain * units_scale
        """
        #simu_diode_value = input_dict['simu_diode']
        ureg = 1  # 1E-12  # conversion to coulomb SI unit
        #pdb.set_trace()
        acq_time = input_dict['time']
        return {self.tags[cnt.name] : (input_dict['ct_'+cnt.name] - (cnt.offset * acq_time)) * cnt.factor * ureg / acq_time
                for cnt in self.outputs}
                
    #def find_offset_all(self,exposure_time):
    #    """
    #    reset the offsets over a defined exposure time
    #    """
    #    scan_obj = ct(exposure_time)
    #    time = scan_obj.get_data('acq_time')
    #    tags2cntname = {v:k for k,v in self.tags.items()} # thanks Seb
    #    for cnt in self.outputs:
    #        key = tags2cntname["ct_"+cnt.name]
    #        counts = scan_obj.get_data(key)  
    #        offset = counts/time
    #        cnt.set_offset(cnt.gain,offset)
    
    def zero(self,*counter_name,exposure_time=1):
        """
        zero a specific counter in its current state (gain)
        """
        try:
            devs_b4_safshut = counter_name - self.safshut_counters
            if len(devs_b4_safshut)==0:
                current_session.env_dict["safshut"].close()
            else:
                current_session.env_dict["fe"].close()
        except:
            print("failed to close shutter - maybe shutdown")
        scan_obj = ct(exposure_time)
        time = scan_obj.get_data('acq_time')
        tags2cntname = {v:k for k,v in self.tags.items()} # thanks Seb
        #key = "ct_"+counter_name #tags2cntname["ct_"+counter_name]
        for cnt in self.outputs:
            if counter_name.count(cnt.name):
                key = tags2cntname["ct_"+cnt.name]
                counts = scan_obj.get_data(key)  
                offset = counts/time
                cnt.set_offset(cnt.gain,float(offset[0]))
                print(f"{cnt.name} offset set to {offset}")            
        try:
            if len(devs_b4_safshut)==0:
                current_session.env_dict["safshut"].open()
            else:
                current_session.env_dict["fe"].open()
        except:
            print("failed to open shutter - maybe shutdown")
    
    def zero_all(self,exposure_time=1):
        """
        zero all monitors for the current defined gain
        """
        # close the appropriate beam shutter TODO
        try:
            current_session.env_dict["fe"].close()
        except:
            print("failed to close shutter - maybe shutdown")
        scan_obj = ct(exposure_time)
        time = scan_obj.get_data('acq_time')
        tags2cntname = {v:k for k,v in self.tags.items()} # thanks Seb
        for cnt in self.outputs:
            try:
                key = tags2cntname["ct_"+cnt.name]
                counts = scan_obj.get_data(key) 
                offset = counts/time
                cnt.set_offset(cnt.gain,float(offset[0]))  
                print(f"{cnt.name} offset set to {offset}")
            except:
                print(f"hmm missing key: {key} for counter {cnt.name}")

        try:
            current_session.env_dict["fe"].open()
        except:
            print("failed to open shutter - maybe shutdown")


    def zero_all_gains(self,*counter_name,exposure_time=1):
        """
        Zero all gain for values for a monitor
        """
        
        # record original state
        counter_dict = {}
        gains_2_check = []
        
        for cnt in self.outputs:
            if counter_name.count(cnt.name):
                counter_dict[cnt.name] = cnt.gain
                gains_2_check += list(cnt.gain_dict.keys())

        # adjust all gains
        for gain in np.unique(gains_2_check):
            print(f"setting gain: {gain}")
            for cnt in self.outputs:
                if counter_name.count(cnt.name):
                    cnt.gain = int(gain)
            self.zero(*counter_name, exposure_time=exposure_time)

        # return to original state        
        for cnt in self.outputs:
            if counter_name.count(cnt.name):
                cnt.gain = int(counter_dict[cnt.name])
        

# TODO sensible debug code
# DONE beamviewer default gain is diffferrent from SPEC gains - not important just a scaling in the end
# TODO max rate + ct modification