import tango

class frameacc():
    def __init__(self,name,config):
        camera = config['camera']
        url = camera.proxy.getPluginDeviceNameFromType("frameaccumulation") 
        self._dev=tango.DeviceProxy(url)  
                    
    def Start(self):
        self._dev.Start()  
    
    def Stop(self):
        self._dev.Stop()
        
    def State(self):
        return self._dev.state()
