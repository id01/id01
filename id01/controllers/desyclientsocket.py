import weakref
from bliss.comm import tcp
from bliss.common.soft_axis import SoftAxis
from bliss.common.counter import SoftCounter
from bliss.common import protocols

class Axis:
    def __init__(self,cnt,name):
        self._cnt = weakref.proxy(cnt)
        self._name = name
        
    @property
    def position(self):
        reply = self._cnt._cnx.write_readline(f"wm {self._name}\n".encode())
        return float(reply)*1e6

    @position.setter
    def position(self,pos):
        pos /= 1e6
        return self._cnt._cnx.write_readline(f"mv {self._name} {pos}\n".encode())
    
class SocketAxisCtrl(protocols.CounterContainer):
    def __init__(self,name,config):
        self.name = name
        
        hostport = config.get("hostport")
        hostname,port = hostport.split(':')
        self._cnx = tcp.Socket(hostname, int(port), eol="\n")
        self._axes = dict()
        for axis_name in config.get('axes',[]):
            axis = Axis(self,axis_name)
            self._axes[axis_name] = axis
            soft_axis = SoftAxis(axis_name, axis)
            soft_axis._positioner = True # to get it in wa()
        self._counters = list()
        for cnt_name in config.get('counters',[]):
            axis = Axis(self,cnt_name)
            soft_cnt = SoftCounter(axis,value='position',name=cnt_name)
            self._counters.append(soft_cnt)
    @property
    def counters(self):
        return protocols.counter_namespace(self._counters)
