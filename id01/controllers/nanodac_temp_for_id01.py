# Yves Watier :  This is to debug an experiment in february 2023 
# Who are you ?
# you are not supposed to be here SHOO SHOO///

            
class nanoHack:
    def __init__(self,loop):
        self.loop = loop
    @property
    def ch1out(self):
        return self.loop.controller.hw_controller.send_cmd(f"Loop_1_OP_Ch1Out")

    @ch1out.setter
    def ch1out(self,value):
        return self.loop.controller.hw_controller.send_cmd(f"Loop_1_OP_ManualOutVal", value)

    @property
    def autoMode_Loop1(self):
        if self.loop.controller.hw_controller.send_cmd(f"Loop_1_Main_AutoMan") == 0 :
            return True
        else:
            return False

    @autoMode_Loop1.setter
    def autoMode_Loop1(self, value):
        if value:
            self.loop.controller.hw_controller.send_cmd(f"Loop_1_Main_AutoMan", 0)
        else:
            self.loop.controller.hw_controller.send_cmd(f"Loop_1_Main_AutoMan", 1)

