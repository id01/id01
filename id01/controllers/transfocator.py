import weakref
from bliss.controllers import transfocator
from bliss.controllers.wago import wago
from bliss import global_map

class Transfocator(transfocator.Transfocator):
    CNX_CACHE = weakref.WeakValueDictionary()
    def __init__(self,name,config):
        super().__init__(name,config)

        _wago = config["wago"]
        control_channel = config["control_channel"]
        status_channel = config["status_channel"]

        #alter the original mapping string with the one needed for the transfocator control
        mapping_str = _wago.modules_config.mapping_str.replace(control_channel,"ctrl")
        mapping_str = mapping_str.replace(status_channel,"stat")

        modules_config = transfocator.ModulesConfig(mapping_str,ignore_missing=True)
        comm = self.CNX_CACHE.get((self.wago_ip,self.wago_port))
        if comm is None:
            conf = {"modbustcp": {"url": f"{self.wago_ip}:{self.wago_port}"}}
            comm = wago.get_wago_comm(conf)
            self.CNX_CACHE[(self.wago_ip,self.wago_port)] = comm
            
        self.wago = wago.WagoController(comm, modules_config)
        #self.wago = wago.WagoController(_wago.client, modules_config)
        global_map.register(self, children_list=[self.wago])

    def in_all(self,):
        self.set_all(True)

    def out_all(self,):
        self.set_all(False)

       
