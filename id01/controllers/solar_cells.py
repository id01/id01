import os
import numpy
import gevent
import datetime
import h5py
from bliss.config import channels
from bliss.common import counter
from bliss.common.protocols import counter_namespace
from bliss.comm import gpib
from bliss.scanning import scan_saving

class TemperatureCtrl:
    def __init__(self,name,config):
        mult_gpib_parameters = config['multimeter']
        self.multimeter = gpib.Gpib(**mult_gpib_parameters)
        powersupply_gpib_parameters = config['powersupply']
        self.powersupply = gpib.Gpib(**powersupply_gpib_parameters)

        self._set_point = channels.Channel(f'{name}:set_point',default_value=25)

        self._current_temperature = channels.Channel(f'{name}:current_temperature',default_value=-1)
        self._current_resistor = channels.Channel(f'{name}:current_resistor',default_value=-1)
        self._current = channels.Channel(f'{name}:current',default_value=-1)

        self._current_temperature_counter = counter.SoftCounter(self,'current_temperature',
                                                                name=f'{name}:current_temperature')
        self._current_resistor_counter = counter.SoftCounter(self,'current_resistor',
                                                             name=f'{name}:current_resistor')
        self._current_counter = counter.SoftCounter(self,'current',
                                                    name=f'{name}:current')
        
        self._scan_saving = scan_saving.ESRFScanSaving(config['saving_session'])

    @property
    def counters(self):
        return counter_namespace([self._current_temperature_counter,
                                  self._current_resistor_counter,
                                  self._current_counter])
    @property
    def set_point(self):
        return self._set_point.value

    @set_point.setter
    def set_point(self,value):
        self._set_point.value = value

    @property
    def current_temperature(self):
        return self._current_temperature.value

    @current_temperature.setter
    def current_temperature(self,value):
        self._current_temperature.value = value

    @property
    def current_resistor(self):
        return self._current_resistor.value

    @current_resistor.setter
    def current_resistor(self,value):
        self._current_resistor.value = value

    @property
    def current(self):
        return self._current.value

    @current_resistor.setter
    def current(self,value):
        self._current.value = value

    def loop(self,Temp_chip,Res_T,A,B,file_name):
        tolerance = 0.05

        path = self._scan_saving.get_path()

        #---------- Creat HDF5 file ---------------------
        tn = str(datetime.datetime.now())
        date_time=(tn.split(" ")[0]+"-"+tn.split(" ")[1].split(":")[0]+"h"+tn.split(" ")[1].split(":")[1]+"m"+tn.split(" ")[1].split(":")[2][0:2]+"s-")
        file_name_save =  date_time + "Temperature_log-" + file_name +  ".hdf5"

        full_path_name = os.path.join(path,file_name_save)
        
        f = h5py.File(full_path_name, 'w', libver='latest')
        grp = f.create_group("Data")
        grp = f.create_group("Infos")
        arr=numpy.array([])
        dset = f.create_dataset("Data/Current", maxshape=(None,), data=arr)
        dset = f.create_dataset("Data/Elapsed time", maxshape=(None,), data=arr)
        dset = f.create_dataset("Data/Resistance", maxshape=(None,), data=arr)
        dset = f.create_dataset("Data/Temperature", maxshape=(None,), data=arr)
        dset = f.create_dataset("Data/Set_Temperature", maxshape=(None,), data=arr)
        dset = f.create_dataset("Data/DateTime", maxshape=(None,), data=arr)
        dset = f.create_dataset("Infos/Date_time", data = str(datetime.datetime.now()))
        f.close()

        def Temp_from_res(Resistance):
            a=R0*B
            b=R0*A
            c=R0-Resistance
            delta = (b*b)-4*a*c
            temp = (-b+numpy.sqrt(delta))/(2*a)
            temp_round = round(temp,2)
            return temp_round

        #---------- POWER SUPPLY ------------
        def set_6221(function):
            self.powersupply.write(function)

        def query_6221(function1):
            result = self.powersupply.write_readline(function1)
            return result
        #----------- MULTIMETER-------
        def set_2000(function2):
            self.multimeter.write(function2)

        def query_2000(function3):
            result = self.multimeter.write_readline(function3)
            return result
        ###------------------ conf power supply and  multimeter------------
        set_6221("*RST")
        set_6221("*CLS")
        set_6221("SOUR:FUNC CURR")
        set_6221("SOUR:CURR:COMP 12")
        set_6221("CURR 0")
        set_2000("*RST")
        set_2000("*CLS")
        set_2000('CONFigure:FRESistance')
        #set_2000('MEASure:RESistance?')

        #----------- PROTECTION TO HIGH TEMPERATURE ------------
        def set_current(value):
            if value >0.1:
                print("High temperature")
                set_6221("CURR 0")
                set_6221("OUTP OFF")
                raise RuntimeError("Interlocked by Current > 50 mA")
            else:
                pass
            return value

        # Calculate R0
        R0 = Res_T/ ((B*Temp_chip*Temp_chip)+ (A*Temp_chip)+1)
        # First measurement
        set_6221("OUTP ON")
        gevent.sleep(1)
        resistance = float(query_2000('MEAS:RES?'))
        #Share the current resistor
        self.current_resistor = resistance
        
        temp_1 = round(Temp_from_res(float(resistance)),2)
        #Share the current temperature
        self.current_temperature = temp_1
        
        t0 = datetime.datetime.now()
        Current_to_set_1 = 0.0001
        try:
            while True:
                SET_Temperature = self.set_point
                # ---------STOP CONTROL AND TURN OFF -------
                if SET_Temperature==0:
                    set_6221("OUTP OFF")
                    print("Finished by SET_Temperature = 0")
                    break

                if temp_1 > 200:
                    set_6221("OUTP OFF")
                    print("Interlocked by temperature > 100 °C")

                    break
                # ------------------SET CURRENT SETP--------------------------
                if float(abs(temp_1-SET_Temperature)/SET_Temperature) < 0.03:
                    step_current =0.0001/2
                elif float(abs(temp_1-SET_Temperature)/SET_Temperature) < 0.15:
                    step_current =0.0001
                elif float(abs(temp_1-SET_Temperature)/SET_Temperature) < 0.40:
                    step_current =0.0002
                else:
                    step_current = 0.0004

                #----------- UP or DOWN current ------------------ 
                if temp_1 < (SET_Temperature - tolerance): # UP
                    Current_to_set_1 = Current_to_set_1 +step_current
                elif temp_1 > (SET_Temperature + tolerance): #DOWN
                    Current_to_set_1 = Current_to_set_1 -step_current
                else:
                    Current_to_set_1 = Current_to_set_1 # KEEP THE SAME

                set_current(Current_to_set_1)

                if Current_to_set_1 < 0:
                    Current_to_set_1 = 0

                set_6221('CURR ' + str(Current_to_set_1))
                self.current = Current_to_set_1
                gevent.sleep(1)

                resistance = float(query_2000(':MEAS:RES?'))
                self.current_resistor= resistance
                
                time_now=datetime.datetime.now()
                temp_1=Temp_from_res(resistance)
                self.current_temperature = temp_1
                
                delta_time = datetime.datetime.now()-t0
                time_total = delta_time.total_seconds()

                print_text = "Temperature = " + str(Temp_from_res(resistance)) + " °C"
                set_point = f"Set point = {SET_Temperature}"
                print (set_point, print_text, end="\r")
                
                with h5py.File(full_path_name, 'a') as hf:
                    hf["Data/Current"].resize((hf["Data/Current"].shape[0] + 1), axis = 0)
                    hf["Data/Current"][-1:] = Current_to_set_1

                    hf["Data/Resistance"].resize((hf["Data/Resistance"].shape[0] + 1), axis = 0)
                    hf["Data/Resistance"][-1:] = resistance

                    hf["Data/Temperature"].resize((hf["Data/Temperature"].shape[0] + 1), axis = 0)
                    hf["Data/Temperature"][-1:] = temp_1

                    hf["Data/Set_Temperature"].resize((hf["Data/Set_Temperature"].shape[0] + 1), axis = 0)
                    hf["Data/Set_Temperature"][-1:] = SET_Temperature
                    # salve time
                    delta_time = datetime.datetime.now()-t0
                    time_total = delta_time.total_seconds()
                    hf["Data/Elapsed time"].resize((hf["Data/Elapsed time"].shape[0] + 1), axis = 0)
                    hf["Data/Elapsed time"][-1:] = time_total

                    hf["Data/DateTime"].resize((hf["Data/DateTime"].shape[0] + 1), axis = 0)
                    tn = str(datetime.datetime.now())
                    hf["Data/DateTime"][-1:] = str(tn[0:4]+tn[5:7]+tn[8:10]+tn[11:13]+tn[14:16]+tn[17:19])
            set_6221("OUTP OFF")            
        finally:
            set_6221("OUTP OFF")



class SolarCellsElec:
    def __init__(self,name,config):
        source_meter_gpib_parameters = config['source_meter']
        self.source_meter = gpib.Gpib(**source_meter_gpib_parameters)
        self._scan_saving = scan_saving.ESRFScanSaving(config['saving_session'])

        self._target_voltage = channels.Channel(f'{name}:target_voltage')
        # Rodrigo change here
        self._target_current = channels.Channel(f'{name}:target_current')


    @property
    def target_voltage(self):
        return self._target_voltage.value
    @target_voltage.setter
    def target_voltage(self,value):
        self._target_voltage.value = value

        # Rodrigo change here
    @property
    def target_current(self):
        return self._target_current.value
    @target_current.setter
    def target_current(self, value):
        self._target_current.value = value

        
    def JV_scan(self, V_init, V_fin, V_step, delay, file_name):

        # GPIB conection ID01 keihtley 6221
        #source_meter=gpib.Gpib("enet://gpibid01c", pad=16)  # PAD to be corrected

        def set_2450(function):
            self.source_meter.write(function)

        def query_2450(function1):
            result = self.source_meter.write_readline(function1)
            return result

        num_step = (abs(V_fin-V_init)/V_step)+1
        set_2450("*CLS")
        set_2450("*RST")
        set_2450("SOUR:FUNC VOLT")
        set_2450("SOUR:VOLT:RANG 2")
        set_2450("SENS:FUNC 'CURR'")
        set_2450("SENS:CURR:RANG 1")
        set_2450("SOUR:SWE:VOLT:LIN "+ str(V_init) + "," + str(V_fin) + "," + str(num_step) + "," + str(delay)+", 1")
        set_2450("INIT")
        set_2450("*WAI")
        print("Measuring JV")
        result = query_2450("TRAC:DATA? 1,"+ str(num_step) +", 'defbuffer1', SOUR, READ" )
        result_float = [float(x) for x in result.split(b",")]

        #time.sleep((num_step+1)*(delay+0.1))
        set_2450("OUTP 0")
        set_2450("SOUR:VOLT 0")

        voltage=[]
        current=[]
        for n in range(len(result_float)):
            if n % 2 == 0:
                voltage.append(result_float[n])
            else:
                current.append(result_float[n])

        path = self._scan_saving.get_path()

        
        tn = str(datetime.datetime.now())
        date_time=(tn.split(" ")[0]+"-"+tn.split(" ")[1].split(":")[0]+"h"+tn.split(" ")[1].split(":")[1]+"m"+tn.split(" ")[1].split(":")[2][0:2]+"s-")
        file_name_save = date_time + "JVscan-" + file_name +  ".hdf5"
        full_path_name = os.path.join(path,file_name_save)
        # Subgroups
        f = h5py.File(full_path_name, 'a')
        try:
            grp = f.create_group("JV_scan")
            grp = f.create_group("Infos")
            # Salve data
            dset = f.create_dataset("JV_scan/Current", data = numpy.array(current))
            dset = f.create_dataset("JV_scan/Voltage", data = numpy.array(voltage))
            volt_curr = [numpy.array(voltage),numpy.array(current)]
            dset = f.create_dataset("JV_scan/Voltage_Current", data = volt_curr)
            power = numpy.array(voltage)*numpy.array(current)*-1
            dset = f.create_dataset("JV_scan/Power", data = power)
            dset = f.create_dataset("Infos/V_init", data = float(V_init))
            dset = f.create_dataset("Infos/V_fin", data = float(V_fin))
            dset = f.create_dataset("Infos/V_step", data = float(V_step))
            dset = f.create_dataset("Infos/delay", data = float(delay))
            dset = f.create_dataset("Infos/num_step", data = float(num_step))
            dset = f.create_dataset("Infos/Date_time", data =tn)
            print("Measurement Finished")
        finally:
            f.close()

        
    def Curr_time(self, Delay, file_name):

        tn = str(datetime.datetime.now())
        date_time=(tn.split(" ")[0]+"-"+tn.split(" ")[1].split(":")[0]+"h"+tn.split(" ")[1].split(":")[1]+"m"+tn.split(" ")[1].split(":")[2][0:2]+"s-")
        file_name_save = date_time + "CurrTrack-" + file_name +  ".hdf5"
        path = self._scan_saving.get_path()
        full_path_name = os.path.join(path,file_name_save)
        
        f = h5py.File(full_path_name, 'w', libver='latest')
        grp = f.create_group("Data")
        grp = f.create_group("Infos")
        arr=numpy.array([])
        dset = f.create_dataset("Data/Current", maxshape=(None,), data=arr)
        dset = f.create_dataset("Data/Voltage", maxshape=(None,), data=arr)
        dset = f.create_dataset("Data/Elapsed time", maxshape=(None,), data=arr)
        dset = f.create_dataset("Data/DateTime", maxshape=(None,), data=arr)
        dset = f.create_dataset("Infos/Date_time", data = str(datetime.datetime.now()))
        f.close()


        def set_2450(function):
            self.source_meter.write(function)

        def query_2450(function1):
            result = self.source_meter.write_readline(function1)
            return result

        set_2450("*CLS")
        set_2450("*RST")
        set_2450("SOUR:FUNC VOLT")
        set_2450("SOUR:VOLT:RANG 1.2")
        set_2450("SENS:FUNC 'CURR'")
        set_2450("SENS:CURR:RANG 0.1")
        set_2450("SOUR:VOLT 0")

        try:
            t0 = datetime.datetime.now()
            set_2450("OUTP 1")
            while 1==1:
                Voltage = self.target_voltage
                if Voltage is None:
                    raise RuntimeError("Please set target voltage first")


                value = numpy.array([float(query_2450("READ?"))])

                #display
                print (f'Target voltage: {Voltage}, Measure current: {value}', end="\r")
                set_2450("SOUR:VOLT "+ str(Voltage ))
                with h5py.File(full_path_name, 'a') as hf:
                    #hf.swmr_mode = True
                    data_size = 0
                    try:
                        data_size = hf["Data/Current"].shape[0]
                    except KeyError:
                        pass

                    
                    hf["Data/Current"].resize((data_size + value.shape[0]), axis = 0)
                    hf["Data/Current"][-value.shape[0]:] = value
                    hf["Data/Voltage"].resize((data_size + 1), axis = 0)
                    hf["Data/Voltage"][-value.shape[0]:] = Voltage
                    # salve time
                    delta_time = datetime.datetime.now()-t0
                    time_total = delta_time.total_seconds()
                    hf["Data/Elapsed time"].resize((data_size + 1), axis = 0)
                    hf["Data/Elapsed time"][-1:] = time_total
                    hf["Data/DateTime"].resize((data_size + 1), axis = 0)
                    tn = str(datetime.datetime.now())
                    hf["Data/DateTime"][-1:] = str(tn[0:4]+tn[5:7]+tn[8:10]+tn[11:13]+tn[14:16]+tn[17:19])

                gevent.sleep(Delay)
        finally:
            set_2450("OUTP 0")
            print("Interrupted")


    def Volt_time(self, Delay, file_name):

        tn = str(datetime.datetime.now())
        date_time=(tn.split(" ")[0]+"-"+tn.split(" ")[1].split(":")[0]+"h"+tn.split(" ")[1].split(":")[1]+"m"+tn.split(" ")[1].split(":")[2][0:2]+"s-")
        file_name_save = date_time + "VoltTrack-" + file_name +  ".hdf5"
        path =  self._scan_saving.get_path()
        full_path_name =  os.path.join(path, file_name_save)

        f = h5py.File(full_path_name, 'w', libver='latest')
        grp = f.create_group("Data")
        grp = f.create_group("Infos")
        arr=numpy.array([])
        dset = f.create_dataset("Data/Current", maxshape=(None,), data=arr)
        dset = f.create_dataset("Data/Voltage", maxshape=(None,), data=arr)
        dset = f.create_dataset("Data/Elapsed time", maxshape=(None,), data=arr)
        dset = f.create_dataset("Data/DateTime", maxshape=(None,), data=arr)
        dset = f.create_dataset("Infos/Date_time", data = str(datetime.datetime.now()))
        f.close()

        def set_2450(function):
            self.source_meter.write(function)

        def query_2450(function1):
            result = self.source_meter.write_readline(function1)
            return result

        set_2450("*CLS")
        set_2450("*RST")
        set_2450("SOUR:FUNC CURR")
        set_2450("SOUR:CURR:RANG 1")
        set_2450("SENS:FUNC 'VOLT'")
        set_2450("SENS:VOLT:RANG 2")
        set_2450("SOUR:VOLT 0")

        try:
            t0 = datetime.datetime.now()
            set_2450("OUTP 1")
            while 1==1:
                Current = self.target_current
                if Current is None:
                    raise RuntimeError("Please set target current first") 
                value = numpy.array([float(query_2450("READ?"))])

                #display
                print(f'Target current: {Current}, Measure voltage: {value}', end = '\r')
                set_2450("SOUR:CURR "+ str(Current ))

                with h5py.File(full_path_name, 'a') as hf:
                    data_size = 0
                    try:
                        data_size = hf["Data/Current"].shape[0]
                    except KeyError:
                        pass

                    hf["Data/Current"].resize((data_size + 1), axis = 0)
                    hf["Data/Current"][-value.shape[0]:] = Current
                    hf["Data/Voltage"].resize((data_size + 1), axis = 0)
                    hf["Data/Voltage"][-value.shape[0]:] = value

                    # salve time
                    delta_time = datetime.datetime.now()-t0
                    time_total = delta_time.total_seconds()
                    hf["Data/Elapsed time"].resize((data_size + 1), axis = 0)
                    hf["Data/Elapsed time"][-1:] = time_total
                    hf["Data/DateTime"].resize((data_size + 1), axis = 0)
                    tn = str(datetime.datetime.now())
                    hf["Data/DateTime"][-1:] = str(tn[0:4]+tn[5:7]+tn[8:10]+tn[11:13]+tn[14:16]+tn[17:19])
                gevent.sleep(Delay)
        finally:
            set_2450("OUTP 0")
            print("Interrupted")
