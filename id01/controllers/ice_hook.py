# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2019 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""
test hook / cloned from ID31 motion hook for the carnac motors.
"""

import gevent
from bliss.common.hook import MotionHook
from bliss.common.logtools import *


class IceHook(MotionHook):
    """
    Motion hook specific for ID31 carnac motors.

    Configuration example:

    .. code-block:: yaml

        hooks:
          - name: carnac_hook
            class: IceHook
            module: motors.hooks
            plugin: bliss
        controllers:
          - name: ice313
            class: IcePAP
            host: iceid313
            plugin: emotion
            axes:
              - name: cncx
                motion_hooks:
                  - $carnac_hook
              - name: cncy
                motion_hooks:
                  - $carnac_hook
              - name: cncz
                motion_hooks:
                  - $carnac_hook
    """

    class SafetyError(Exception):
        pass

    def __init__(self, name, config):
        self.axes_roles = {}
        super(IceHook, self).__init__()

    def init(self):
        # store which axis has which
        # roles in the system
        for axis in self.axes.values():
            print(f"+++++++++++ axis[{axis.name}]")
            tags = axis.config.get('tags')
            if 'd1y' in tags:
                self.axes_roles[axis] = 'd1y'
            elif 'd2x' in tags:
                self.axes_roles[axis] = 'd2x'
            elif 'd2y' in tags:
                self.axes_roles[axis] = 'd2y'
            else:
                raise KeyError(f'[{axis.name}] detector motor needs a safety role')


    def pre_move(self, motion_list):
        
        for axis in self.axes.values():
            print(f"----------- axis[{axis.name}] pos[{axis.position}]")
        
        print(f"motion_list[{motion_list}]")
        for motion in motion_list:
            ax = motion.axis

            steps_per_unit = ax.steps_per_unit
            pos=ax.position
            utarget_pos = motion.user_target_pos
            print(f"pos[{pos}] utarget_pos[{utarget_pos}]")

            target_pos = motion.target_pos
            delta = motion.delta
            
            pos_end = target_pos/steps_per_unit
            pos_start = (target_pos - delta)/steps_per_unit
            print(f"start[{pos_start}] end[{pos_end}] ")
            
            tags = ax.config.get('tags')
            print(f"axis.name[{motion.axis.name}]")
            print(f"axis.position[{ax.position}]")
            print(f"axis.steps_per_unit[{ax.steps_per_unit}]")
            print(f"axis.sign[{ax.sign}]")
            print(f"backlash[{motion.backlash}]")
            print(f"delta[{motion.delta}]")
            print(f"type[{motion.type}]")
            print(f"tags[{tags}]")
            #print(dir(ax))
            #print(dir(motion))
        axes = [motion.axis for motion in motion_list]
        axes_names = ", ".join([axis.name for axis in axes])
        
        #target_pos = dict([(axis, axis.position()) for axis in self.axes_roles])
        
        #print(f"\n.... target_pos[{target_pos}]\n")
        print(f"\n.... pre_move axes_names[{axes_names}]\n")

    def post_move(self, motion_list):
        axes = [motion.axis for motion in motion_list]
        axes_names = ", ".join([axis.name for axis in axes])
        print(f"\n.... post_move axes_names[{axes_names}]\n")


    def _wait_ready(self, axes):
        with gevent.Timeout(1, RuntimeError("not all motors ready after timeout")):
            while True:
                ready = [axis for axis in axes if axis.hw_state.READY]
                if len(ready) == len(axes):
                    break
        log_debug(self, "All motors ready!")

    def Xpre_move(self, motion_list):
        axes = [motion.axis for motion in motion_list]
        axes_names = ", ".join([axis.name for axis in axes])
        log_debug(self, f"Start power ON for {axes_names}")
        tasks = [gevent.spawn(axis.controller.set_on, axis) for axis in axes]
        gevent.joinall(tasks, timeout=1, raise_error=True)
        log_debug(self, f"Finished power ON for {axes_names}")
        # we know empirically that the carnac takes ~1.3s to reply it is
        # ready after a power on
        gevent.sleep(1.2)
        self._wait_ready(axes)

    def Xpost_move(self, motion_list):
        axes = [motion.axis for motion in motion_list]
        axes_names = ", ".join([axis.name for axis in axes])
        log_debug(self, f"Start power OFF for {axes_names}")
        tasks = [gevent.spawn(axis.controller.set_off, axis) for axis in axes]
        gevent.joinall(tasks, timeout=1, raise_error=True)
        log_debug(self, f"Finished power OFF for {axes_names}")
        self._wait_ready(axes)
