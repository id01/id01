import re
from bliss.config.beacon_object import BeaconObject
from bliss.comm import util

class KL2500Led(BeaconObject):
    def __init__(self,name,config):
        super().__init__(config,name=name)
        self._cnx = util.get_comm(config,eol=b';')
        
    @property
    def identity(self):
        return self._cnx.write_readline(b"0ID?;").decode()
    
    @property
    def temperature(self):
        pattern = re.compile(r'0TX([0-9a-fA-F]{4})')
        str_value = self._cnx.write_readline(b'0TX?;')
        m = pattern.match(str_value.decode())
        if m:
            return int(m.group(1),base=16)*0.0625
        else:
            raise RuntimeError(f"Temperature error. return string is {str_value}")
        
    @BeaconObject.property
    def brightness(self):
        pattern = re.compile(r'0BR([0-9a-fA-F]{4})')
        str_value = self._cnx.write_readline(b'0BR?;')
        m = pattern.match(str_value.decode())
        if m:
            return int(m.group(1),base=16)/10.
        else:
            raise RuntimeError(f"Brightness error. return string is {str_value}")
        
    @brightness.setter
    def brightness(self,value):
        value = float(value)
        if 0 <= value <= 100:
            msg = b'0BR%04x' % int(value * 10)
            rx_msg = self._cnx.write_readline(msg + b';')
            if rx_msg != msg:
                raise RuntimeError(f"Brightness error send: {msg}, rx: {rx_msg}")
        else:
            raise TypeError("Brightness is a percent (between 0 and 100%)")    
    
    
