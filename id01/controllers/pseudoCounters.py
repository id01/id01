from bliss.controllers.counter import CalcCounterController
from bliss.config.beacon_object import BeaconObject
from bliss.common.counter import CalcCounter

class MyCntParameters(CalcCounter,BeaconObject):
    gain = BeaconObject.property_setting('gain',default=3)
    offset = BeaconObject.property_setting('offset',default=0)
    factor = BeaconObject.property_setting('factor',default=0)

    @gain.setter
    def gain(self, value):
        config_gains = self.config.get('gains',[])
        for cfg in config_gains:
            if cfg.get('value') == value:
                self.offset = cfg.get('offset',0)
                self.factor = cfg.get('factor',0)
            
    def __init__(self,name,config,controller,dim):
        CalcCounter.__init__(self,name,controller,dim)
        BeaconObject.__init__(self,config,name=name,share_hardware=False)
        

    

class Seb(CalcCounterController):
    def _new_calc_counter(self,cnt_config,cnt_name,controller,dim):
        return MyCntParameters(cnt_name,cnt_config,controller,dim)
    
 
    def calc_function(self, input_dict):
        simu_diode_value = input_dict['simu_diode']
        acq_time = input_dict['time']
        return {self.tags[cnt.name] : (simu_diode_value + (cnt.offset * acq_time)) * cnt.gain
                for cnt in self.outputs}
    
