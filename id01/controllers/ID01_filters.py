from bliss.config.beacon_object import BeaconObject
from bliss.config import settings
import click
import numpy as np

class ID01Filters(BeaconObject):
    """
    Class instance to hold all parameters related to pseudoCounters
    """

    filtersetname = BeaconObject.property_setting('filtersetname',default= "")
    #filtersetObj = BeaconObject.property_setting('filtersetObj',default= None)
    
                
    def __init__(self,name,config):
        """
        Initialise beacon objects 
        """
                                    
        BeaconObject.__init__(self, config,
                                    name=name,
                                    share_hardware=False)
                                    
        filterset_dict = self.get_filterset_dict()

        #try:
        #    self.filtersetObj = filterset_dict[self.filtersetname]
        #except:
        #    print("oops unset")
        # default first in list
        self.filtersetObj = filterset_dict[list(filterset_dict.keys())[0]]
        self.filtersetname # this forces the filtersetObj for some reason the code in the try: above doesnt work

    def get_filterset_dict(self):
        self.filterset_dict = dict()
        for i in self.config["filter_boxes"]:
            self.filterset_dict[i["box_name"].name]=i["box_name"]
        return self.filterset_dict


    @BeaconObject.property(default=1)
    def filtersetname(self):
        return self.filtersetname   
        
    @filtersetname.setter
    def filtersetname(self, value):
        filterset_list = list(self.get_filterset_dict().keys())
        if filterset_list.count(value)==0:
            print(f"The defined filterset '{value}' is not in the defined filter boxes list: {filterset_list}")
        else:
            # check all filters are out before changing
            if self.filtersetObj.filter !=0:
                ans = click.confirm(f"Do you want to take out all of the filters for {self.filtersetname} before switching to {value}?") 
                if ans:
                    # TODO force fast shutter closed for safety
                    self.filtersetObj.filter = 0          
            self.filtersetObj = self.filterset_dict[value]
 
    def switch(self,value):
        self.filtersetname = value    
    
    def show_available_filters(self,):
        print(self.filterset_dict.keys())

    

