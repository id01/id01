from bliss.comm import util
from bliss.controllers.counter import SamplingCounterController
from bliss.common.counter import SamplingCounter
from numpy import double

class K2636(SamplingCounterController):
    def __init__(self,name,config):
        super().__init__(name)
        if config.get('tcp'):
            self._cnx = util.get_comm(config,port=5025)
        else:
            self._cnx = util.get_comm(config)
        #export counters :-(
        for cnt_name in ['voltage','amp']:
            SamplingCounter(cnt_name,self)

        #self.user_init()

        

    def reset_machine(self,):
        self._cnx.write(b'smua.reset()\r\n')

    def OUTPUT_ON(self,):
        self._cnx.write(b'smua.source.output = smua.OUTPUT_ON\r\n')

    def OUTPUT_OFF(self,):
        self._cnx.write(b'smua.source.output = smua.OUTPUT_OFF\r\n')

    def OUTPUT_I(self,):
        self._cnx.write(b'smua.source.func = smua.OUTPUT_DCAMPS\r\n')

    def OUTPUT_V(self,):
        self._cnx.write(b'smua.source.func = smua.OUTPUT_DCVOLTS\r\n')

    def autorange_V_ON(self,):
        self._cnx.write(b'smua.measure.autorangev = smua.AUTORANGE_ON\r\n')

    def autorange_V_OFF(self,):
        self._cnx.write(b'smua.measure.autorangev = smua.AUTORANGE_OFF\r\n')

    def autorange_source_I_ON(self,):
        self._cnx.write(b'smua.source.autorangei = smua.AUTORANGE_ON\r\n')
    def autorange_source_I_OFF(self,):
        self._cnx.write(b'smua.source.autorangei = smua.AUTORANGE_OFF\r\n')

    @property
    def idn(self):
        return self._cnx.write_readline(b'*IDN?\n')

    @property
    def voltage_chanA(self):
        return float(self._cnx.write_readline('print(smua.source.levelv)\r\n'))
    @voltage_chanA.setter
    def voltage_chanA(self,value):
        self._cnx.write('smua.source.levelv = %f\r\n' % value)

    @property
    def limit_volt_chanA(self):
        return float(self._cnx.write_readline('print(smua.source.limitv)\r\n'))
    @limit_volt_chanA.setter
    def limit_volt_chanA(self,value):
        self._cnx.write('smua.source.limitv = %f\r\n' % value)

    @property
    def amp_chanA(self):
        return double(self._cnx.write_readline('print(smua.source.leveli)\r\n'))
    @amp_chanA.setter
    def amp_chanA(self,value):
        self._cnx.write('smua.source.leveli = %.16f\r\n' % value)

    @property
    def measurerange_V_chanA(self):
        return double(self._cnx.write_readline('print(smua.measure.rangev)\r\n'))
    @measurerange_V_chanA.setter
    def measurerange_V_chanA(self,value):
        self._cnx.write('smua.measure.rangev = %f\r\n' % value)

    @property
    def sourcerange_I_chanA(self):
        return double(self._cnx.write_readline('print(smua.source.rangei)\r\n'))
    @sourcerange_I_chanA.setter
    def sourcerange_I_chanA(self,value):
        self._cnx.write('smua.source.rangei = %f\r\n' % value)

    @property
    def lowrange_I_chanA(self):
        return double(self._cnx.write_readline('print(smua.source.lowrangei)\r\n'))
    @lowrange_I_chanA.setter
    def lowrange_I_chanA(self,value):
        self._cnx.write('smua.source.lowrangei = %.12f\r\n' % value)

    def read_voltage_chanA(self):
        return float(self._cnx.write_readline('print(smua.measure.v())\r\n'))

    def read_amp_chanA(self):
        return float(self._cnx.write_readline('print(smua.measure.i())\r\n'))


    def read(self,cnt):
        if cnt.name.find('amp') > -1:
            return self.read_amp_chanA()
        else:
            return self.read_voltage_chanA()


    def user_init(self,):
        # comment: all commands that need to be run
        self.reset_machine()
        self.OUTPUT_I()
        self.limit_volt_chanA=1
        self.autorange_V_ON()
     #   self.sourcerange_I_chanA=1e-9
        self.autorange_source_I_ON()
        self.amp_chanA=1e-9
        pass