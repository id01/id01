# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2019 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.


import struct
import functools
import collections

import gevent.lock

from bliss.comm.util import get_comm
from bliss.common.measurement import SamplingCounter
from bliss.common import mapping
from bliss.common.logtools import LogMixin



class TestcntError(Exception):
    pass




def debug_it(f):
    name = f.__name__

    @functools.wraps(f)
    def wrapper(self, *args, **kwargs):
        self._logger.debug("[start] %s()", name)
        r = f(self, *args, **kwargs)
        self._logger.debug("[end] %s() -> %r", name, r)
        return r

    return wrapper


def _only_dev_type(device, cmd, dev_type=None):
    dev = device.init_info.type
    if dev != dev_type:
        raise TestcntError(
            "Command {0} only valid for {1} (device is {2})".format(
                cmd.name, dev_type, dev
            )
        )

class TestcntCounter(SamplingCounter):
    def __init__(self, name, controller, p201, channel):
        SamplingCounter.__init__(self, name, controller)
        self.p201 = p201
        self.channel = channel
        self.ctp201 = None
        for cnt in self.p201.counters: 
            cnt_name = cnt.name
            if self.channel == cnt_name:
                self.ctp201 = cnt                                                                                                      

    def read(self):
#        return getattr(self.controller, self.channel)
        if self.ctp201 == None:
            return -1

        return self.ctp201.channel



class Testcnt:

    def __init__(self, name, config_tree):

        self.name = name
        self.controller = None

        self.counters = {}
        

        # Create counters
        for counter_config in config_tree["counters"]:
            counter_name = counter_config["counter_name"]
            if hasattr(self, counter_name):
                #self._logger.error(
                print(
                    "Skipped counter %r (controller already "
                    "has a member with that name)",
                    counter_name,
                )
                continue
            channel = counter_config.get("channel", "ch0")
            p201 = counter_config.get("p201", None)
            print("+++++", name, channel, p201)
            counter = TestcntCounter(counter_name, self, p201, channel)
            counter.channel = channel
            counter.p201 = p201
            self.counters[counter_name] = counter
            setattr(self, counter_name, counter)     




