import enum
import json
import requests
from bliss.common.counter import SamplingCounter
from bliss.controllers.counter import SamplingCounterController

VALVES_STATUS = enum.Enum("VALVES_STATUS","CLOSE OPEN",start=0)

class Counter(SamplingCounter):
    def __init__(self,counter_name,controller,table_type,table_name,index):
        super().__init__(counter_name,controller)
        self.table_type = table_type
        self.table_name = table_name
        self.index = index

class CounterSettable(Counter):
    def __init__(self,counter_name,controller,table_type,table_name,index,max_value):
        super().__init__(counter_name,controller,table_type,table_name,index)
        self._max_value = max_value
        
    def set(self,value):
        if value > self._max_value:
            raise RuntimeError(f"Value ({value}) above {self._max_value}")
        
        user = self._counter_controller._user
        password = self._counter_controller._password
        hostname = self._counter_controller._hostname
        url = _GasPanel.base_url.format(user=user,
                                   password=password,
                                   hostname=hostname)
        url += _GasPanel.mid_url + _GasPanel.tables_url
        url += f"/{self.table_type}/{self.table_name}/{self.index}"
        r = requests.post(url,data=json.dumps({'value':value}))
        return r.ok
    
    
class CounterIO(SamplingCounter):
    def __init__(self,counter_name,controller,
                 io_type,io_name):
        super().__init__(counter_name,controller)
        self.io_type = io_type
        self.io_name = io_name
        
    def _build_url(self):
        base_url = self._counter_controller.base_url
        user = self._counter_controller._user
        password = self._counter_controller._password
        hostname = self._counter_controller._hostname

        url = base_url.format(user=user,password=password,hostname=hostname)
        url += self._counter_controller.mid_url + self._counter_controller.ios_url
        url += f"/{self.io_type}/{self.io_name}"
        return url
    
    def read(self):
        url = self._build_url()
        r = requests.get(url)
        return r.json()['value']

    def set(self,value):
        url = self._build_url()
        r = requests.post(url,data=json.dumps({"value":value}))
        return r.ok
        
    
class _GasPanel(SamplingCounterController):
    base_url = "http://{user}:{password}@{hostname}"
    mid_url = "/api/v1/device/strategy"
    tables_url = "/tables"
    ios_url = "/ios"
    
    def __init__(self,name,config):
        super().__init__(name,register_counters=False)
        self._hostname = config['hostname']
        self._user = config['user']
        self._password = config['password']

        for counter_config in config.get('counters',list()):
            counter_type = counter_config.get('counter_type')
            counter_name = counter_config['counter_name']
            if counter_type == "io":
                counter_io = CounterIO(counter_name,self,
                                       counter_config['io_type'],
                                       counter_config['io_name'])
            else:
                table_type = counter_config['table_type']
                table_name = counter_config['table_name']
                index = counter_config['index']
                settable = counter_config.get('settable',False)
                if settable:
                    c = CounterSettable(counter_name,self,table_type,
                                        table_name,index,
                                        counter_config['max_value'])
                else:
                    c = Counter(counter_name,self,table_type,table_name,index)

                
    def read_all(self,*counters):
        tables = dict()
        for cnt in counters:
            if isinstance(cnt,Counter):
                tables[(cnt.table_type,cnt.table_name)] = None
            
        for table_type,table_name in tables:
            url = self.base_url.format(user=self._user,
                                       password=self._password,
                                       hostname=self._hostname)
            url += self.mid_url + self.tables_url
            url += f"/{table_type}/{table_name}"
            r = requests.get(url)
            tables[(table_type,table_name)] = r.json()

        data = []
        for cnt in counters:
            if isinstance(cnt,CounterIO):
                data.append(cnt.read())
            else:
                values = tables[(cnt.table_type,cnt.table_name)]
                data.append(values[cnt.index])

        return data
                                   
        
def GasPanel(name,config):
    
    hostname = config['hostname']
    user = config['user']
    password = config['password']
    
    class Valves:
        def __init__(self,table_type,table_name,index):
            self.table_type = table_type
            self.table_name = table_name
            self.index = index

        def _build_url(self,index=None):
            url = _GasPanel.base_url.format(user=user,
                                            password=password,
                                            hostname=hostname)
            url += _GasPanel.mid_url + _GasPanel.tables_url
            url += f"/{self.table_type}/{self.table_name}"
            if index is not None:
                url += f"/{index}"
                
            return url
        
        def open(self):
            url = self._build_url(index=self.index)
            r = requests.post(url,data=json.dumps({"value":1}))
            return r.ok
            
        def close(self):
            url = self._build_url(index=self.index)
            r = requests.post(url,data=json.dumps({"value":0}))
            return r.ok
        
        def status(self,table_value = None):
            if table_value == None:
                url = self._build_url()
                r = requests.get(url)
                table_value = r.json()

            return VALVES_STATUS(table_value[self.index])

    gas_panel = _GasPanel(name,config)
    
    for cfg in config.get('valves',list()):
        v = Valves(cfg['table_type'],cfg['table_name'],cfg['index'])

        setattr(gas_panel,cfg['valve_name'],v)
        
    return gas_panel
