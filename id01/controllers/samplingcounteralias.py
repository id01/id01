#========================================================================
#========================================================================
# 2020/01/21 rh
# OBSOLETE only for reference / patch made by Linus
#
# Linus patch
#~ nano3:~/local/bliss.git % git status; git rev-parse HEAD 
#~ On branch expression_based_calc_counter
#~ ad5bf5110a5f211edfc98b0b80f3c1de5a836826
#
# working merged
#~ nano3:~/local/bliss.git % git status; git rev-parse HEAD 
#~ On branch nd287
#~ f889a22c4cb962c82ab88d7581f455b8690b7dcd
#========================================================================
#========================================================================




import inspect

from bliss.common.counter import SoftCounter
from bliss.common.counter import SamplingCounter
from bliss.controllers import pepu

"""
- class: SamplingCounterAlias
  controller: $pepu_dcm3
  counters:
    - name: atto1
      counter_name: IN1
    - name: atto2
      counter_name: IN4
"""

class SamplingCounterAlias(SoftCounter):
    def __init__(self, name, config):
        
        cntcontroller      = config.get_inherited("controller")
        counter_name    = config.get("counter_name")
        controller_type = config.get("type")
        
        steps_per_unit = config.get("steps_per_unit")
        if steps_per_unit is None:
            steps_per_unit = 1.0
        offset = config.get("offset")
        if offset is None:
            offset = 0.0
            
        counters = cntcontroller.counters

        #print(f"--- counter_name[{counter_name}] controller_type[{controller_type}] counters[{counters}]")
        real_counter = getattr(counters, counter_name)
        #~ if isinstance(cntcontroller, pepu.PEPU):
            #~ real_counter = getattr(real_counter, "channel")
        #~ 
        def get_value(self):
            breakpoint()
            #~ if hasattr(real_counter, "read"):
                #~ value = real_counter.read()
            #~ elif hasattr(real_counter, "value"):
                #~ if inspect.ismethod(real_counter.value):
                    #~ value = real_counter.value()
                #~ else:
                    #~ value = real_counter.value
            #~ else:
                #~ raise NotImplemented
            
            return  value/steps_per_unit+offset
        
        #breakpoint()

        SoftCounter.__init__(self, real_counter, name=name, apply=get_value)
        
        self.cntcontroller = cntcontroller
        self.offset = offset
        self.steps_per_unit = steps_per_unit


class SingleChannelCalcCounter(SamplingCounter):
    def __init__(self, name, config):
        
        Counter.__init__(self, name)
        
        # Get source counter object
        self.source_controller = config.get_inherited("controller")
        self.source_cnt_name = config.get("source cnt")
        self.source_acq_name = config.get("source acq")
        if self.source_acq_name is None:
            self.source_acq_name = self.source_cnt_name
        counters = self._controller.counters
        self.source_cnt = getattr(counters, self.source_cnt_name)
        if isinstance(self._controller, pepu.PEPU):
            self.xource_cnt = getattr(self.source_cnt, "channel")        
        
        # get calculation parameters
        self.steps_per_unit = config.get("steps_per_unit")
        if self.steps_per_unit is None:
            self.steps_per_unit = 1.0
        self.offset = config.get("offset")
        if self.offset is None:
            self.offset = 0.0
        
    def get_acq_device(self, cont_acq):
        
        acq_cont_name = "%s_cont"%self.name
        CalcAcquisitionDevice(acq_cont_name, (cont_acq,),
                              self.get_acq_value, bragg_pepu_calc.acq_channels)
        
    def calc_value(self, value):
        return self.steps_per_unit * value + self.offset
            
    def get_cnt_value(self):
        if hasattr(self.source_cnt, "read"):
            value = self.source_cnt.read()
        elif hasattr(self.source_cnt, "value"):
            if inspect.ismethod(self.source_cnt.value):
                value = self.source_cnt.value()
            else:
                value = self.source_cnt.value
        else:
            raise NotImplemented
            
        return self.calc_value(value)
    
    def get_acq_value(self, sender, data_dict):
        data = data_dict.get(self.source_acq_name)
        
        if data is None:
            return()
            
        if len(data) == 0:
            return ()
        
        step_data = self.calc_value(data)

        return {self._dest_name : step_data}

    @property
    def acq_channels(self):
        return [AcquisitionChannel(self.name, np.float,())]

