from bliss import setup_globals as SG
import time

## we added a trigger signal for the potentiostat - this was copied from ID31 configuration
# a workaround has been found in EClab to chain potential variation to make macros possible.

wcid01eh1 = SG.wcid01eh1
class Potentiostat:

    def __init__(self, name):
        self.name = name

    @property
    def potential(self):
        return -1*wcid01eh1.get('vmon')
    @property
    def current(self):
        return -1*wcid01eh1.get('imon')


def addcounter(group,poten):
    from bliss.common.counter import SoftCounter
    pot = poten('pot1y')    
    E = SoftCounter(pot,'potential')
    I = SoftCounter(pot,'current')
    E.unit = 'V'
    I.unit = 'mA'
    group.add(E)
    group.add(I)
    return E,I
    

def trigger_wait():
    while True:
        if wcid01eh1.get('adc5') > 3:
            break
        time.sleep(0.5)

def trigger_out():
    try:
        wcid01eh1.set('vctl',2)
        time.sleep(1)
    finally:
        wcid01eh1.set('vctl',0)
'''    
def GoWAXS():
    umv(cncz,122)
    umv(wbsy,7.66)
    ACTIVE_MG.disable("*de*")
    ACTIVE_MG.enable("*p3*")
    ACTIVE_MG.disable("*bpm*")
    ACTIVE_MG.disable("*mpx*")
    att(3)

def GoSAXS():
    umv(cncz,400)
    umv(wbsy,17.6)
    ACTIVE_MG.enable("*de*")
    ACTIVE_MG.disable("*p3*")
    ACTIVE_MG.disable("*bpm*")
    ACTIVE_MG.disable("*mpx*")
    #dark(de,3)
    att(3)
'''
"""
def GoWAXS():
    umv(mu,0,gam,0)
    umv(dy,510)
    umv(cncx,-385,cncz,144,wbsy,11.84,wbsz,-0.25)
    dlt.set_detector_active(False)
    ACTIVE_MG.disable("*de*")
    ACTIVE_MG.enable("*p3*")
    ACTIVE_MG.disable("*mpx*")
    umv(mu,0.05)
    att(31)
    
def GoXRR():
    umv(cncz,500,wbsy,0)
    umv(dy,19.9715)
    dlt.set_detector_active(True)      
    ACTIVE_MG.disable("*de*")
    ACTIVE_MG.disable("*p3*")
    ACTIVE_MG.enable("*mpx*")
    ACTIVE_MG.disable("*mpx2*")
    att(31)
    
def GoSAXS():
    umv(mu,0,gam,0)
    umv(dy,510)
    umv(cncx,584,cncz,500,wbsy,0)
    umv(fty,2.08)
    ACTIVE_MG.enable("*de*")
    ACTIVE_MG.disable("*p3*")
    ACTIVE_MG.disable("*bpm*")
    ACTIVE_MG.disable("*mpx*")
    #dark(de,3)
    umv(mu,0.05)
    att(31)

def zscan():
    # WAXS
    GoWAXS()
    ascan(saz2,16.3,16.5,40,1)
    # SAXS
    GoSAXS()
    ascan(saz2,16.3,16.5,40,3)    
        

def measure():
    cycle = 0
    while True:
        cycle = cycle +1
        print("scanning cycle: ", cycle)
        zscan()
        timesleep(300)

def full_macro_scan_withXRR(number_of_cycles):
    print('Moving to high dose spot 1 ')
    umv(saz2,  12.4718, phi, -0.7375, sax, -2.6316, th, 93.23, say, 0.447, chi, -0.057) #Moving to WAXS 1 (spot 1)
    i = 1
    while(i<number_of_cycles):
        att(7)
        print("Cycle number: {:2d}  of {:2d} ".format(i+1, number_of_cycles))
        if i % 6 == 0:
            umv(saz2,  12.468, phi, -0.6975, sax, -3, th, 90, say, 0.25, chi, -0.057)#Moving to WAXS 2 (spot 2)
            print('Low dose spot 1')
            sct(1)
            #
        if i % 24 == 0:
            umv(saz2, 12.657, sax, -3.7, phi, -0.401, chi, -0.394, th, 97.6, say,1.1)#Moving to WAXS 2 (spot 2)
            print('Super Low dose spot 2')
            sct(1)
            #umv(saz2,10.1749, chi,0.1,say,0.2,th,5)#Moving to WAXS 1 (spot 1)
        if i % 24== 0:
            print('Starting XRR!')
            print('DO NOT INTERRUPT! Starting to move detectors!')
            GoXRR()
            umv(saz2,12.662,phi,-0.682,sax,-4.7,th,85, say, 1.1, chi, -0.59)#Moving to XRR (spot 3)
            print('XRR Scan Starting')
            XRR(0.3)
            print('DO NOT INTERRUPT! Starting to move detectors!')
            GoWAXS()
            #umv(saz2, 12.659, sax, -4.2, say, 1.1, phi, -0.354, chi, 0.406, th, 92.8) #Moving to WAXS 1 (spot 1)
            att(9)
            
        umv(saz2,  12.4718, phi, -0.7375, sax, -2.6316, th, 93.23, say, 0.447, chi, -0.057)#Moving to WAXS 1 (spot 1)
        print('High dose spot 1')
        sct(2)
        print('High dose spot 2')
        umv(saz2,  12.468, phi, -0.6975, sax, -3.5, th, 87, say, 0.25, chi, -0.057)#Moving to WAXS 3 (spot 3)
        sct(2)
        #print('High do')
        #att(11)
        #umv(saz2,  10.4773, phi, -0.2, sax, -3.01, th, 2.945, say, 1.1, chi, -0.0941)#Moving to WAXS 4 (spot 4)
        #sct(2)
        i=i+1
        timesleep(5)
    print("Macro done after {:4d}  cycles".format(number_of_cycles))

def waxs_macro_fast_scan(sleep_time):
    while(1): 
        print('Doing fast scans')
        sct(1)
        timesleep(sleep_time)


def waxs_macro_scan():
    print('Moving to spot 1')
    umv(saz2,15.933,phi,-0.186,sax,-5.022)#Moving to WAXS 1
    i = 0
    while(1):
        print("Cycle number:", i)
        if i % 5 == 0:
            umv(saz2,15.933,phi,-0.186,sax,-4.522)#Moving to WAXS 2
            print('Low dose spot 2')
            sct(1)
            umv(saz2,15.933,phi,-0.186,sax,-5.022)#Moving to WAXS 1            
        print('High dose spot 1')
        sct(1)
        i = i+1
        timesleep(60)

def timesleep(sl):
    for remaining in range(sl,1,-1):
        print("{:2d} seconds of sleep remaining.".format(remaining),end='\r')
        sleep(1)
    print('Done sleeping after {:2d} seconds. Moving on!'.format(sl))
        
def XRR(mu_high):
    #umv(csvo,-3.33869)
    att(17)
    a2scan(mu,0.03,0.05,gam,0.03,0.05,10,1)    
    att(12)
    a2scan(mu,0.05,0.07,gam,0.05,0.07,5,1)    
    #umv(csvo,-3.53869)
    att(10)
    a2scan(mu,0.07,0.09,gam,0.07,0.09,5,1)
    att(9)
    a2scan(mu,0.09,0.11,gam,0.09,0.11,5,1)
    att(8)
    a2scan(mu,0.11,0.13,gam,0.11,0.13,5,1)
    att(8)
    a2scan(mu,0.13,mu_high,gam,0.13,mu_high,int((mu_high-0.13)/0.005),1)
    att(31)

def XRR_test():
        print('Starting XRR!')
        print('DO NOT INTERRUPT! Starting to move detectors!')
        GoXRR()
        umv(saz2,12.662,phi,-0.682,sax,-4.7,th,85, say, 1.1, chi, -0.59)#Moving to XRR (spot 3)
        print('XRR Scan Starting')
        XRR(0.3)
        print('DO NOT INTERRUPT! Starting to move detectors!')
        GoWAXS()
        #umv(saz2, 12.659, sax, -4.2, say, 1.1, phi, -0.354, chi, 0.406, th, 92.8) #Moving to WAXS 1 (spot 1)
        att(9)

def full_macro_scan(number_of_cycles):
    print('Moving to high dose spot 1 ')
    umv(saz2, 12.659, sax, -4.2, say, 1.1, phi, -0.364, chi, 0.406, th, 92.8) #Moving to WAXS 1 (spot 1)
    i = 0
    while(i<number_of_cycles):
        print("Cycle number: {:2d}  of {:2d} ".format(i+1, number_of_cycles))
        if i % 6 == 0:
            umv(saz2, 12.672, sax, -4.9, phi, -0.684, chi, -0.573, th, 85.3, say, 1.5)#Moving to WAXS 2 (spot 5)
            print('Low dose spot 1')
            sct(1)
        if i % 24 == 0:
            umv(saz2, 12.657, sax, -3.7, phi, -0.401, chi, -0.394, th, 97.6, say,1.1)#Moving to WAXS 2 (spot 2)
            print('Super Low dose spot 2')
            sct(1)
        umv(saz2, 12.659, sax, -4.2, say, 1.1, phi, -0.364, chi, 0.406, th, 92.8) #Moving to WAXS 1 (spot 1)
        print('High dose spot 1')
        sct(2)
        print('High dose spot 2')
        umv(saz2, 12.665, sax, -5.1, phi, -0.717, chi, -0.741, th, 86.8, say, 1.5)#Moving to WAXS 3 (spot 4)
        sct(2)
        i=i+1
        timesleep(5)
    print("Macro done after {:4d}  cycles".format(number_of_cycles))        
    """    
    
    
    
    
