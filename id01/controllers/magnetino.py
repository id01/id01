## controller from Dmitry Karpov for his magnetino setup
#
# we set this up using lid013 computer in the hutch - we created an 
#  xmlrpc server on that computer and talk to it with an xmlrpc client in bliss.
#  see magnetino object

import serial
import time
import xmlrpc.client


class Magnet():
    """
    this class was not used
    but the functions are the same
    """
    def __init__(self, name, config):
        self.port = config.get("port",8000)
        self.boud_rate = config.get("boud_rate",11520)
        self.startMarker = '<'
        self.endMarker = '>'
        self.dataStarted = False
        self.dataBuf = ""
        self.messageComplete = False

        
        self.ser = serial.Serial(self.port, self.boud_rate, rtscts=True)
        self.ser.timeout = 0
        if(self.ser.isOpen() == False):
            self.ser.open()

        print("Waiting for Arduino to reset")
        msg = ""
        while msg.find("Arduino is ready") == -1:
            msg = self.recvLikeArduino()
            if not (msg == 'XXX'): 
                print(msg)
        
        
    def check_connection(self):
        # checks if the communication is established                
        if(self.ser.isOpen() == False):
            self.ser.open()
            self.connected = True
            print('connection re-established')

    def close_connection(self):
        # close the connection to serial port
        self.ser.close()
        print('connection closed')

    def sendToArduino(self, stringToSend):
        stringWithMarkers = (self.startMarker)
        stringWithMarkers += stringToSend
        stringWithMarkers += (self.endMarker)

        self.ser.write(stringWithMarkers.encode('utf-8'))

    def recvLikeArduino(self):
        if self.ser.inWaiting() > 0 and self.messageComplete == False:
            x = self.ser.read().decode("utf-8")
            
            if self.dataStarted == True:
                if x != self.endMarker:
                    self.dataBuf = self.dataBuf + x
                else:
                    self.dataStarted = False
                    self.messageComplete = True
            elif x == self.startMarker:
                self.dataBuf = ''
                self.dataStarted = True
        
        if (self.messageComplete == True):
            self.messageComplete = False
            return self.dataBuf
        else:
            return "XXX" 
    
    def open_bridge(self, bridgeID):
        self.check_connection()
        self.sendToArduino("M" + bridgeID + "O")
        
    def close_bridge(self, bridgeID):
        self.check_connection()
        self.sendToArduino("M" + bridgeID + "C")
        
    def set_voltage(self, voltage_level):
        self.check_connection()
        if int(voltage_level) <=255 and int(voltage_level) >= 0:
            self.sendToArduino("L{0:03d}".format(int(voltage_level)))
        else:
            print('Error: wrong voltage level given. Use values between 0 and 255.')
        
        
    def close_all_bridges(self):
        self.close_bridge('1P'); self.close_bridge('1N');
        self.close_bridge('2P'); self.close_bridge('2N');
        self.close_bridge('3P'); self.close_bridge('3N');
        self.close_bridge('4P'); self.close_bridge('4N');
        self.close_bridge('5P'); self.close_bridge('5N');
        self.close_bridge('6P'); self.close_bridge('6N');
        self.close_bridge('7P'); self.close_bridge('7N');
        self.close_bridge('8P'); self.close_bridge('8N');
        
    def open_all_positive_bridges(self):
        self.open_bridge('1P'); self.open_bridge('2P');
        self.open_bridge('3P'); self.open_bridge('4P');
        self.open_bridge('5P'); self.open_bridge('6P');
        self.open_bridge('7P'); self.open_bridge('8P');
        
    def open_all_negative_bridges(self):
        self.open_bridge('1N'); self.open_bridge('2N');
        self.open_bridge('3N'); self.open_bridge('4N');
        self.open_bridge('5N'); self.open_bridge('6N');
        self.open_bridge('7N'); self.open_bridge('8N');
        
    def initialize_sample(self, numOfRuns):
        print('Initializing the sample...')
        self.close_all_bridges()
        self.set_voltage(250)
        for i in range(numOfRuns):
            self.open_bridge('1P'); time.sleep(0.1);
            self.close_all_bridges()
            self.open_bridge('2P'); time.sleep(0.1);
            self.close_all_bridges()
            self.open_bridge('5P'); time.sleep(0.1);
            self.close_all_bridges()
            self.set_voltage(0)
        print('DONE!')
        
        
class MagnetinoXMLRPC():
    def __init__(self,name,config):
        self.controller = xmlrpc.client.ServerProxy("http://lid013:9000/")
        self.controller.set_voltage(255) 

#magnetino = Magnet()
#magnetino.close_all_bridges()
#magnetino.set_voltage(255)
# magnetino.open_all_positive_bridges()
# magnetino.open_bridge('8P')
