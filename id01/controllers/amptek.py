import struct
import weakref
import numpy
import gevent
from bliss.comm import util
from bliss.controllers.counter import CounterController
from bliss.common.counter import Counter
from bliss.scanning.chain import AcquisitionSlave

class Amptek:
    class SCA:
        def __init__(self,ctrl,index):
            self.__index = index
            self.__ctl = weakref.proxy(ctrl)
            self._check_exist()

        @property
        def threshold(self):
            reply = self.__ctl._text_configuartion_get(b"SCAI=%d;SCAL;SCAH;" % self.__index)
            low_value,high_value = None,None
            for token,value in reply:
                if token == b'SCAI':
                    assert int(value) == self.__index
                elif token == b'SCAL':
                    low_value = int(value)
                elif token == b'SCAH':
                    high_value = int(value)
            return low_value,high_value
        
        @threshold.setter
        def threshold(self,low_high_value):
            self.__ctl._text_configuartion_set(b"SCAI=%d;SCAL=%d;SCAH=%d;" % (self.__index,*low_high_value))

        @property
        def output_level(self):
            """
            return the output level (OF{F}|HI{GH}|LO{W})
            """
            reply = self.__ctl._text_configuartion_get(b"SCAI=%d;SCAO;" % self.__index)
            return reply[1][1]
        
        def _check_exist(self):
            reply = self.__ctl._text_configuartion_get(b"SCAI=%d;" % self.__index)
            token,value = reply[0]
            assert token == b"SCAI"
            if value == b"??":
                raise RuntimeError("This SCA doesn't exist")
            
    def __init__(self,name,config):
        self._name = name
        self._cnx = util.get_comm(config,port=10001)
        self._netfind = util.get_comm(config,port=3040)
        self._lock = gevent.lock.RLock()

        self._counters_container = _AmptekCounter(self)

    @property
    def name(self):
        return self._name
    
    @property
    def counters(self):
        return self._counters_container.counters
    
    def identity(self):
        seq = numpy.random.randint(0,0xffff)
        msg = struct.pack(">BBHBB",0,0,seq,0xfa,0xfa)
        self._netfind.write(msg)
        reply = self._netfind.raw_read()
        connection_status = int(reply[1])
        cnx_status = {0:"Interface is open (unconnected)",
                      1:"Interface is connected (sharing is allowed)",
                      2:"Interface is connected (sharing is not allowed)",
                      3:"Interface is locked",
                      4:"Interface is unavailable because USB is connected"}
        
        ev1 = reply[4:8]
        ev2 = reply[8:12]
        seconds = reply[12:14]

        ev1_day,ev1_hours,ev1_min = struct.unpack(">HBB",ev1)
        ev2_day,ev2_hours,ev2_min = struct.unpack(">HBB",ev2)
        ev1_sec,ev2_sec = struct.unpack(">BB",seconds)
        variable_str = reply[32:].split(b'\x00')
        model,ev1_desc,ev2_desc = variable_str[0],variable_str[2],variable_str[3]

        ev1_time = b'%d day(s) %02d:%02d:%02d' % (ev1_day,ev1_hours,ev1_min,ev1_sec)
        ev2_time = b'%d day(s) %02d:%02d:%02d' % (ev2_day,ev2_hours,ev2_min,ev2_sec)
        return {'model':model.decode(),ev1_desc.decode():ev1_time.decode(),
                ev2_desc.decode():ev2_time.decode(),"connection status":cnx_status[connection_status]}
    
    @property
    def gain(self):
        """
        Analog gain index
        """
        reply = self._text_configuartion_get(b"GAIA;")
        tocken,value = reply[0]
        assert tocken == b'GAIA'
        return int(value)
    @gain.setter
    def gain(self,value):
        self._text_configuartion_set(b"GAIA=%d;" % value)

    def get_sca(self,index):
        return self.SCA(self,index)
    
    def clear_spectrum(self):
        self._send_cmd(0xf0,1)
        
    def read_spectrum(self):
        reply = self._send_cmd(2,1)
        return self._decode_spectrum(reply)

    def read_and_clear_spectrum(self):
        reply = self._send_cmd(2,2)
        return self._decode_spectrum(reply)

    def _text_configuartion_get(self,ascii_cmd):
        reply = self._send_cmd(0x20,3,ascii_cmd)
        data = reply['data']
        return [x.split(b'=') for x in data.split(b';') if x]

    def _text_configuartion_set(self,ascii_cmd):
        self._send_cmd(0x20,2,ascii_cmd)
        
    def _decode_spectrum(self,reply):
        data = numpy.frombuffer(reply['data'],dtype=numpy.uint8)
        fst_uint8, mid_uint8, lst_uint8 = numpy.reshape(data,(data.shape[0]//3,3)).astype(numpy.uint16).T
        data = numpy.zeros(len(fst_uint8),dtype=numpy.uint32)
        data[:] = fst_uint8 + mid_uint8 * 256 + lst_uint8 * 65536
        return data

    def _send_cmd(self,pid1,pid2,data=b''):
        data_len = len(data)
        msg = struct.pack('>BBBB',0xf5,0xfa,pid1,pid2)
        check_sum = sum(msg) + sum(data)
        check_sum += data_len
        check_sum = 0x10000 - (check_sum & 0xffff)
        msg += struct.pack('>H',data_len)
        msg += data
        msg += struct.pack('>H',check_sum)
        #print(msg)
        with self._lock:
            self._cnx.write(msg)
            reply = self._cnx.raw_read()
            #print('reply',reply)
            pattern = '>BBBBH'
            struct_size = struct.calcsize(pattern)
            while 1:
                sync1,sync2,pid1,pid2,msg_size = struct.unpack(pattern,reply[:struct_size])
                data = reply[struct_size:struct_size+msg_size]
                if len(data) < msg_size:
                    reply += self._cnx.raw_read()
                else:
                    break
            if pid1 == 0xff:    # error
                if pid2 == 0 or pid1 == 0x0c: # Acknowledge packet: “OK” (not an error)
                    return
                
                pid2_enum_error  = {1:'Sync Error',
                                    2:'PID Error',
                                    3:'LEN Error',
                                    4:'Checksum Error',
                                    5:'Bad Parameter:',
                                    6:'Bad Hex Record',
                                    7:'Unrecognized Command:',
                                    8:'FPGA Error',
                                    9:'CP2201 Not Found',
                                    0x0a:'Scope Data Not Available',
                                    0x0b:'PC5 Not Present:',
                                    0x0e:'I2C Error',
                                    0x10:'Feature not supported by this FPGA version',
                                    0x11:'Calibration data not present',
                                    
                }
                raise RuntimeError(b'%s %s' % (pid2_enum_error.get(pid2,'Unknown Error').encode(),data))
            
                                    
            #don't check checksum :-(
            assert sync1 == 0xf5
            assert sync2 == 0xfa
            return {'pid1':pid1,'pid2':pid2,'data':data}
        
class _oneDCounter(Counter):
    def __init__(self,*args,**kargs):
        super().__init__(*args,**kargs)
        self._shape = 1024
    @property
    def shape(self):
        return (self._shape,)                                                    

class _AmptekCounter(CounterController):
    def __init__(self,amptek):
        super().__init__(amptek.name,register_counters=True)

        self._spectrum_cnt = _oneDCounter('spectrum',self)
        self._amptek = amptek

    def get_acquisition_object(self,acq_params,ctrl_params,parent_acq_params):
        return _AmptekAcquisitonSlave(self._amptek,count_time=acq_params.get('count_time',1))

    def get_default_chain_parameters(self,*args):
        return args[0]

class _AmptekAcquisitonSlave(AcquisitionSlave):
    def __init__(self,amptek,npoints=1,count_time=1):
        self._amptek = amptek
        self._count_time = count_time
        super().__init__(amptek._counters_container,npoints=npoints,
                         trigger_type=AcquisitionSlave.SOFTWARE,
                         prepare_once=False,start_once=False)

    def prepare(self):
        pass

    def start(self):
        pass

    def stop(self):
        pass

    def trigger(self):
        self._amptek.clear_spectrum()
        gevent.sleep(self._count_time)
        spectrum = self._amptek.read_spectrum()
        self._amptek._counters_container._spectrum_cnt._shape = len(spectrum)
        return self.channels.update({'spectrum':spectrum})
