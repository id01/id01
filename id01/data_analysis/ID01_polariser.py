from id01.process.h5utils import openScan
import matplotlib.pyplot as plt
import numpy as np
from scipy import interpolate
from scipy.signal import find_peaks

from bliss import current_session, setup_globals
#from bliss.common.scans import sct

from bliss.common.scans import *
from bliss.shell.standard import mv, mvr, umv, umvr
#from bliss.scanning.scan_tools import *




#filename = "/data/visitor/hc4963/id01/TbIG_001/TbIG_001_0001/TbIG_001_0001.h5"
#scan_no = 26

def polariser_crossing_points(  filename, 
                                scan_no, 
                                v_counter = "mon1", 
                                h_counter = "mon2", 
                                mot_pol = "pol_r", 
                                v_min=None, 
                                v_max=None, 
                                h_min=None, 
                                h_max = None):
    """
    find the crossing points for the two scans to identify the locations for specific polarisations of light
    """
    plt.figure()

    scan = openScan(filename, scan_no)

    y0 = data_v_diode = scan.getCounter(v_counter)
    y1 = data_h_diode = scan.getCounter(h_counter)
    
    print(f"v_min = {data_v_diode.min()}, v_max = {data_v_diode.max()}")
    print(f"h_min = {data_h_diode.min()}, h_max = {data_h_diode.max()}")
    
    x = data_pol_r = scan.getCounter(mot_pol)
    
    if v_min == None:
        y0min = v_min = y0.min()
    else:
        y0min = v_min
    if v_max == None:
        y0max = v_max = y0.max()
    else:
        y0max = v_max
    
    y0s = (y0-y0min)/(y0max-y0min) 

    if h_min == None:
        y1min = h_min = y1.min()
    else:
        y1min = h_min
        
    if h_max == None:
        y1max = h_max = y1.max()
    else:
        y1max = h_max
    
    y1s = (y1-y1min)/(y1max-y1min)     
    
    #plt.plot(x,y0s,label = "vertical")
    #plt.plot(x,y1s, label = "horizontal")
    #plt.legend()
    #plt.show()
    #plt.figure()
    
    #code from I16 diamond
    C1 = y0s
    C2 = y1s
    xval = x 
    
    ival = np.linspace(np.min(xval), np.max(xval), 100*len(xval))
    iC1 = np.interp(ival, xval, C1)
    iC2 = np.interp(ival, xval, C2)
    
    diff = np.abs((iC1)/2 - (iC2)/2)


    fig_size = [8,8]

    fig, ax1 = plt.subplots(figsize=fig_size)
    plt.plot(xval, C1, 'b-', lw=2, label='C1')
    plt.plot(xval, C2, 'r-', lw=2, label='C2')

    plt.plot(ival, diff, 'k-', lw=0.5, label='|C1+C3-C2-C4|')
    #labels(ttl, xvar, 'QBPM6', legend=True)

    #mon = data_.datacol("exp2")
    #ax2 = ax1.twinx()
    #plt.plot(xval, mon, 'g:', lw=2, label='mon')
    #labels(None,None,'ic1monitor')
    #ax2.tick_params(axis='y', labelcolor='g')
    #ax2.set_ylabel('mon',color='g')

    # new method 29/2/2020
    # find smallest differences furthest appart
    npoints = 0
    percentile = 0
    while npoints < 2:
        percentile += 1
        minthresh = np.percentile(diff, percentile)
        minxvals = ival[ diff < minthresh ]
        npoints = len(minxvals)
    neg = minxvals[0]
    pos = minxvals[-1]

    avmid = (pos+neg)/2
    negoff = neg-avmid
    posoff = pos-avmid
    #midpoint = xval[np.argmin(mon)]
    #print(ttl)
    #print('avmid')
    #print('Estimated Midpoint: %s=%7.3f'%(xvar,midpoint))
    print('   Lower intercept: %8.4f (%+6.4f)'%(neg,negoff))
    print('   Upper intercept: %8.4f (%+6.4f)'%(pos,posoff))
    #print('   Actual midpoint: %s=%8.4f'%(xvar,avmid))

         

    plt.sca(ax1)
    plt.axvline(neg, c='k', lw=0.5)
    plt.axvline(pos, c='k', lw=0.5)

    plt.show()

#polariser_crossing_points(filename, scan_no, v_max=3350)   



def polariser_crossing_points_auto( filename, scan_no, 
                                    polarisation = "neg", 
                                    v_counter = "mon1", 
                                    h_counter = "mon2", 
                                    mot_pol = "pol_r", 
                                    v_min=None, 
                                    v_max=None, 
                                    h_min=None, 
                                    h_max = None, 
                                    verbose_plot = False, 
                                    optimise_mot_pol = True,
                                    sigma_offset = 150, ):
    """
    find the crossing points for the two scans to identify the locations for specific polarisations of light
    polarisation = "neg","pos","pi","sigma" are options
    v_counter = vertical polarisation counter name
    h_counter = horizontal polarisation counter name
    mot_pol = polariser rotation axis motor name
    v_min, v_max = user defined cut off for vertical plots
    h_min, h_max = user defined cut off for horizontal plots
    verbosr_plot = show plots (user interaction required)
    optimise_mot_pol = move to target polarisation and optimise to the counter value
    sigma_offset = offset from "neg" peak to find sigma, basically far from the Bragg can be arbitrarily large
    
    """
    
    #pol_mot = current_session.env_dict[mot_pol]
    #target_countrate = 0
    

    #scan_bliss = dscan(pol_r,-100,100,100,0.1)
    #breakpoint()
    # load from hdf5
    #scan = openScan(scan_bliss.scan_info("filename"), scan_bliss.scan_info("scan_nb"))

    from silx.io import h5py_utils

    @h5py_utils.retry(retry_timeout=120, retry_period=2)
    def read_data():
        scan = openScan(filename, scan_no)
        data_v_diode = scan.getCounter(v_counter)[...]
        data_h_diode = scan.getCounter(h_counter)[...]
        data_pol_r = scan.getCounter(mot_pol)[...]
        return data_v_diode, data_h_diode, data_pol_r

    data_v_diode, data_h_diode, data_pol_r = read_data()

    y0 = data_v_diode
    y1 = data_h_diode
    
    print(f"v_min = {data_v_diode.min()}, v_max = {data_v_diode.max()}")
    print(f"h_min = {data_h_diode.min()}, h_max = {data_h_diode.max()}")
    
    x = data_pol_r
    
    if v_min == None:
        y0min = v_min = y0.min()
    else:
        y0min = v_min
    if v_max == None:
        y0max = v_max = y0.max()
    else:
        y0max = v_max
    
    y0s = (y0-y0min)/(y0max-y0min) 

    if h_min == None:
        y1min = h_min = y1.min()
    else:
        y1min = h_min
        
    if h_max == None:
        y1max = h_max = y1.max()
    else:
        y1max = h_max
    
    y1s = (y1-y1min)/(y1max-y1min)     
    
    #plt.plot(x,y0s,label = "vertical")
    #plt.plot(x,y1s, label = "horizontal")
    #plt.legend()
    #plt.show()
    #plt.figure()
    
    #code from I16 diamond
    C1 = y0s
    C2 = y1s
    xval = x 
    
    #breakpoint()
    
    ival = np.linspace(np.min(xval), np.max(xval), 100*len(xval))
    iC1 = np.interp(ival, xval, C1)
    iC2 = np.interp(ival, xval, C2)
    
    diff = np.abs((iC1)/2 - (iC2)/2)

    #labels(ttl, xvar, 'QBPM6', legend=True)

    #mon = data_.datacol("exp2")
    #ax2 = ax1.twinx()
    #plt.plot(xval, mon, 'g:', lw=2, label='mon')
    #labels(None,None,'ic1monitor')
    #ax2.tick_params(axis='y', labelcolor='g')
    #ax2.set_ylabel('mon',color='g')
    # new method 29/2/2020
    # find smallest differences furthest appart
    npoints = 0
    percentile = 0
    while npoints < 2:
        percentile += 1
        minthresh = np.percentile(diff, percentile)
        minxvals = ival[ diff < minthresh ]
        npoints = len(minxvals)
    neg = minxvals[0]
    pos = minxvals[-1]

    avmid = (pos+neg)/2
    negoff = neg-avmid
    posoff = pos-avmid
    #midpoint = xval[np.argmin(mon)]
    #print(ttl)
    #print('avmid')
    print('Estimated Midpoint: %7.3f'%(avmid))
    print('   Lower intercept: %8.4f (%+6.4f)'%(neg,negoff))
    print('   Upper intercept: %8.4f (%+6.4f)'%(pos,posoff))
    #print('   Actual midpoint: %s=%8.4f'%(xvar,avmipositive
    #breakpoint()

    #split the plot in two based on the midpoint - easier to find the peaks
    
    #counter value at peak
    peak_loc = find_peaks(y1[xval<avmid])[0][0]
    peak_neg_val = y1[peak_loc]
    peak_neg_xval =xval[peak_loc]
    peak_loc = find_peaks(y1[xval>avmid])[0][0]
    peak_pos_val = y1[peak_loc+(y1[xval<avmid].shape[0])]
    peak_pos_xval = xval[peak_loc+(y1[xval<avmid].shape[0])]

    #interpolate original scan with defined positions
    interp_func_v = interpolate.interp1d(x,y0)
    vneg_cr = interp_func_v(neg)
    vpos_cr = interp_func_v(pos)

    vpi_cr = interp_func_v(peak_pos_xval)
    #vsigma_cr = interp_func_v(neg+124)

    interp_func_h = interpolate.interp1d(x,y1)
    hneg_cr = interp_func_h(neg)
    hpos_cr = interp_func_h(pos)

    hpi_cr = interp_func_h(peak_pos_xval)
    #hsigma_cr = interp_func_h(neg+124)

    #plot pi sigma on graph

    if verbose_plot:
        fig_size = [8,8]

        fig, ax1 = plt.subplots(figsize=fig_size)
        plt.plot(xval, C1, 'b-', lw=2, label='C1')
        plt.plot(xval, C2, 'r-', lw=2, label='C2')

        plt.plot(ival, diff, 'k-', lw=0.5, label='|C1+C3-C2-C4|')
        plt.legend(loc="best")
        plt.sca(ax1)
        plt.axvline(neg, c='k', lw=0.5)
        plt.axvline(pos, c='k', lw=0.5)
        plt.axvline(peak_pos_xval, c='k', lw=0.5)
        plt.axvline(neg+sigma_offset, c='k', lw=0.5)
        plt.show()
    
        plt.plot(x,y0,label = "vertical")
        plt.plot(x,y1, label = "horizontal")
        plt.axvline(neg, c='k', lw=0.5)
        plt.axvline(pos, c='k', lw=0.5)
        plt.axvline(peak_pos_xval, c='k', lw=0.5)
        plt.axvline(neg+sigma_offset, c='k', lw=0.5)
        plt.show()

    print("vertical (neg,pos,pi,sigma):",vneg_cr,vpos_cr,vpi_cr)#,vsigma_cr)
    print("horizontal (neg,pos,pi,sigma):",hneg_cr,hpos_cr,hpi_cr)#,hsigma_cr)

    if optimise_mot_pol:
        pol_mot = current_session.env_dict[mot_pol]
        v_counter_obj = current_session.env_dict["ID01_pseudo_counter"].counters.mon1 #"ct_"+v_counter]
        if polarisation == "neg":
            umv(pol_mot,neg)
            optimise_mot2countervalue(pol_mot,v_counter_obj,vneg_cr)
        
        if polarisation == "pos":
            umv(pol_mot,pos)
            optimise_mot2countervalue(pol_mot,v_counter_obj,vpos_cr)

        if polarisation == "pi":
            umv(pol_mot,peak_pos_xval)
            optimise_mot2countervalue(pol_mot,v_counter_obj,vpi_cr)

        # dont care about optimising for sigma
        if polarisation == "sigma":
            umv(pol_mot,neg+sigma_offset)
    


def optimise_mot2countervalue(  mot,
                                counter,
                                target_cnt_val,
                                step_size = 0.5,
                                exposure = 0.1):
    """
    move a motor to a target counter position
    """
    data = ct(exposure,counter)
    last_cnt_val = data.get_data(counter.name)[0]
    #breakpoint()
    move_pos = True
    while move_pos:
        umvr(mot,step_size)
        print("step positive")
        data = ct(exposure,counter)
        curr_cnt_val = data.get_data(counter.name)[0]
        if np.abs(target_cnt_val-curr_cnt_val) >= np.abs(target_cnt_val - last_cnt_val):
            move_pos = False
        
        last_cnt_val = curr_cnt_val

    move_neg = True
    while move_neg:
        umvr(mot,-step_size)
        print("step negative")
        data = ct(exposure,counter)
        curr_cnt_val = data.get_data(counter.name)[0]
        if np.abs(target_cnt_val-curr_cnt_val) >= np.abs(target_cnt_val - last_cnt_val):
            move_neg = False
            umvr(mot,step_size) # one step back
        
        last_cnt_val = curr_cnt_val  

    print(f"optimised count rate : {curr_cnt_val} vs {target_cnt_val} target count rate")
        
def optimise_motor_on_counter():
    """
    TODO if does not exist
    """
    pass

#scan_bliss = dscan(pol_r,-100,100,100,0.1)
#polariser_crossing_points_auto(scan_bliss.scan_info["filename"], scan_bliss.scan_info["scan_nb"])    
