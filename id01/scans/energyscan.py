import numpy as np
from bliss.comm.spec import connection  
from bliss.common.scans.ct import sct
import h5py
import pylab as plt
from bliss import current_session, setup_globals

from bliss.common.standard import sync


def getCurrentMononrj():
    bl_server = connection.SpecConnection("nano3:blserver")

    from gevent.event import Event
    e = Event()
    def cbk(*args):
        e.set()
    r = bl_server.send_msg_cmd_with_return("mono_getL()")
    e.wait(1.) # need to wait for optics session to respond
    sync() # do I really need that? It seem so.... Hope it's fine
    wavelength = r.getValue()*1e-9
    current_mononrj = 12.39842/(wavelength*1e10)
    return current_mononrj

def energyAbsoluteScan(energy_start, energy_end, nb_points, counting_time=1):

    current_mononrj = getCurrentMononrj()
    
    optics = connection.SpecConnection('nano3:optics') 
        
    energy_list = np.linspace(energy_start, energy_end, nb_points)
    
    for n,energy in enumerate(energy_list):
        print("umv mononrj {}".format(energy))
        optics.send_msg_cmd("umv mononrj {}".format(energy))
        a = sct(counting_time)
      
    optics.send_msg_cmd("umv mononrj {}".format(current_mononrj))  
    
    first_scan = int(a.scan_number)-len(energy_list)+1
    last_scan = int(a.scan_number)
    print("First scan number : {}".format(first_scan))
    print("Last scan number : {}".format(last_scan))

    return
    
def plotEnergyScan(first_scan, last_scan,
                   filename=None,
                   roi_name='mpx1x4_mpx4int'):
    if filename is None: 
        globals_dict = current_session.env_dict
        filename = globals_dict["SCAN_SAVING"].filename
        
    scan_list = np.arange(first_scan, last_scan+1)
    counter = np.zeros(len(scan_list))

    h = 6.62e-34
    c = 3e8
    e = 1.6e-19
    energy = np.zeros(len(scan_list))
    for n,scan in enumerate(scan_list):
        with h5py.File(filename,'r') as h5:
            counter[n] += h5['{}.1/measurement/{}'.format(scan,roi_name)][()]
            energy[n] = (12.39842/(h5['{}.1/instrument/monochromator/WaveLength'.format(scan)][()]*1e10))


    plt.figure(figsize=(10,5))
    plt.plot(energy, counter, 'o-')
    plt.xlabel('Energy (keV)', fontsize=15)
    plt.ylabel(roi_name, fontsize=15)
    plt.show()
    return
