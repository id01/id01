from bliss.common import plot
from bliss import global_map

def get_selected_counters():
    counters_name = set(plot.get_plotted_counters())
    counters = []
    for cnt in global_map.get_counters_iter():
        if cnt.fullname in counters_name:
            counters.append(cnt)
    return counters
