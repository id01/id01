# -*- coding: utf-8 -*-

# ID01 BLISS git repo
#       authors:
#         Steven Leake, leake@esrf.fr
#
#       Created at: 11. Apr 14:30:00 CET 2023
#
#----------------------------------------------------------------------
# Description: 
#    utility functions for ID01
#
#
# TODO:
#   accumulation mode is yet to be added  - needs to use Atrig in MUSST
#
#
# FUNCTIONS:
#   afscan
#   dfscan
#   afscan2d
#   dfscan2d
#----------------------------------------------------------------------
from bliss import setup_globals
from bliss.common.scans import *
from bliss.scanning.scan_tools import *
from bliss import setup_globals
from bliss.shell.standard import umv 
from bliss import current_session

#from bliss.common.cleanup import cleanup, axis as cleanup_axis


def afscan(motObj, start, stop, nPts, exposure_time = None, debug = False):
    """
    absolute fscan  
    velocity = 0.5 degrees per second
    """
    detObjs_names = get_active_detector_names()
    print("detectors found:", detObjs_names)

    if len(detObjs_names) == 0:
        print("No active detector found")
        return
    elif len(detObjs_names) == 1:
        detObj = current_session.env_dict[detObjs_names[0]]
    elif len(detObjs_names) > 1:
        print("more than one detector in measurement group - desole not supported yet")
        return
    
    fscan = setup_globals.fscan_eh.get_runner("fscan")
    step = (stop-start)/nPts 
    velocity_limit = motObj.velocity_high_limit

    if exposure_time is None:
        # use default
        exposure_time = detObj.accumulation.max_expo_time

    requested_velocity = 1./((1./step)*exposure_time)
    
    if requested_velocity > velocity_limit:
        print("Too fast .... velocity requested: %.3f"%requested_velocity)
        print("suggested minimum npts = %i"%((((stop-start)/velocity_limit)/exposure_time)))
        
    else:
        if debug:
            print(motObj,start,step,nPts,exposure_time)
        else:
            fscan(motObj,start,step,nPts,exposure_time)     
      
def dfscan(motObj, start , stop, nPts, exposure_time = None, debug = False):
    """
    relative fscan
    """

    detObjs_names = get_active_detector_names()
    print("detectors found:", detObjs_names)

    if len(detObjs_names) == 0:
        print("No active detector found")
        return
    elif len(detObjs_names) == 1:
        detObj = current_session.env_dict[detObjs_names[0]]
    elif len(detObjs_names) > 1:
        print("more than one detector in measurement group - desole not supported yet")
        return

    ref_motObj_position = motObj.position
    fscan = setup_globals.fscan_eh.get_runner("fscan")
    step = (stop-start)/nPts 
    
    velocity_limit = motObj.velocity_high_limit

    if exposure_time is None:
        exposure_time = detObj.accumulation.max_expo_time

    requested_velocity = 1./((1./step)*exposure_time)
    if requested_velocity > velocity_limit:
        print("Too fast .... velocity requested: %.3f"%requested_velocity)
        print("suggested minimum npts = %i"%(((stop-start)/velocity_limit)/exposure_time))
        
    else:
        if debug:
            print(motObj,motObj.position+start,step,nPts,exposure_time)
        else:
            fscan(motObj,motObj.position+start,step,nPts,exposure_time) 
    print(f"moving motor {motObj.name} back to start position: {ref_motObj_position}")
    umv(motObj,ref_motObj_position)

def afscan2d(motObj0, start0 , stop0, nPts0, motObj1, start1 , stop1, nPts1, exposure_time = None, debug = False):
    """
    absolute fscan2d  
    velocity = 0.5 degrees per second
    motObj0 = slow motor
    motObj1 = fast motor
    """
    detObjs_names = get_active_detector_names()
    print("detectors found:", detObjs_names)

    if len(detObjs_names) == 0:
        print("No active detector found")
        return
    elif len(detObjs_names) == 1:
        detObj = current_session.env_dict[detObjs_names[0]]
    elif len(detObjs_names) > 1:
        print("more than one detector in measurement group - desole not supported yet")
        return
    
    fscan2d = setup_globals.fscan_eh.get_runner("fscan2d")
    step0 = (stop0-start0)/nPts0 
    step1 = (stop1-start1)/nPts1 

    velocity_limit = motObj1.velocity_high_limit

    if exposure_time is None:
        exposure_time = detObj.accumulation.max_expo_time

    requested_velocity = 1./((1./step1)*exposure_time)
    
    if requested_velocity > velocity_limit:
        print("Too fast .... velocity requested: %.3f"%requested_velocity)
        print("suggested minimum npts = %i"%((((stop1-start1)/velocity_limit)/exposure_time)))
        
    else:
        if debug:
            print(motObj0,start0,step0,nPts0,motObj1,start1,step1,nPts1,exposure_time)
        else:
            fscan2d(motObj0,start0,step0,nPts0,motObj1,start1,step1,nPts1,exposure_time) 

def dfscan2d(motObj0, start0 , stop0, nPts0, motObj1, start1 , stop1, nPts1, exposure_time = None, debug = False):
    """
    relative fscan2d
    """

    #fscan2d = fscan_eh.get_runner("fscan2d")

    detObjs_names = get_active_detector_names()
    print("detectors found:", detObjs_names)

    if len(detObjs_names) == 0:
        print("No active detector found")
        return
    elif len(detObjs_names) == 1:
        detObj = current_session.env_dict[detObjs_names[0]]
    elif len(detObjs_names) > 1:
        print("more than one detector in measurement group - desole not supported yet")
        return

    ref_motObj_position0 = motObj0.position
    ref_motObj_position1 = motObj1.position
    fscan2d = setup_globals.fscan_eh.get_runner("fscan2d")
    step0 = (stop0-start0)/nPts0 
    step1 = (stop1-start1)/nPts1 
    
    velocity_limit = motObj1.velocity_high_limit

    if exposure_time is None:
        exposure_time = detObj.accumulation.max_expo_time

    requested_velocity = 1./((1./step1)*exposure_time)
    if requested_velocity > velocity_limit:
        print("Too fast .... velocity requested: %.3f"%requested_velocity)
        print("suggested minimum npts = %i"%(((stop1-start1)/velocity_limit)/exposure_time))
        
    else:
        if debug:
            print(motObj0,motObj0.position+start0,step0,nPts0,motObj1,motObj1.position+start1,step1,nPts1,exposure_time)
        else:
            fscan2d(motObj0,motObj0.position+start0,step0,nPts0,motObj1,motObj1.position+start1,step1,nPts1,exposure_time) 

    print(f"moving motor {motObj0.name} back to start position: {ref_motObj_position0}")
    print(f"moving motor {motObj1.name} back to start position: {ref_motObj_position1}")
    umv(motObj0,ref_motObj_position0)
    umv(motObj1,ref_motObj_position1)

def get_active_detector_names():
    """
    search active measurement group for all detectors with ...:image counter.
    """
    det_list = []
    for item in setup_globals.ACTIVE_MG.enabled:
        if item.count("image")>0:
            det_list.append(item.split(":")[0])

    return det_list
