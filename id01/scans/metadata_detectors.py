#########################################
# Metadata definitions for all ID01 detectors
#  to use them initialise an object with one of the classes
#   >> see the init scan_meta.set gets a fill function which pulls all important
#      data from the sessions and adds it in the acquitisition chain
#########################################

# TODO metadata for  all dteectors except eiger2m_bigpipe
# TODO how to toggle only the live detectors or is this taken care of byt the MG?

from gevent.event import Event
from bliss.scanning import scan_meta
from bliss.config.beacon_object import BeaconObject
from bliss.comm.spec import connection
import datetime


class MetaData_Detector_eiger2M_dummy(BeaconObject):
    Title = BeaconObject.property_setting('Title',default='')
    Dummy = BeaconObject.property_setting('Dummy',default=-1)
    DDummy = BeaconObject.property_setting('DDummy',default=0.1)
    Center_1 = BeaconObject.property_setting('Center_1',default=0)
    Center_2 = BeaconObject.property_setting('Center_2',default=0)
    DetectorRotation_1 = BeaconObject.property_setting('DetectorRotation_1',default=0)
    DetectorRotation_2 = BeaconObject.property_setting('DetectorRotation_2',default=0)
    DetectorRotation_3 = BeaconObject.property_setting('DetectorRotation_3',default=0)
    #DetectorName = BeaconObject.property_setting('DetectorName',default='')
    DetboxOffset = BeaconObject.property_setting('DetboxOffset',default=0.4736)  # standard offset of detector in saxs tube
    SampleOffset = BeaconObject.property_setting('SampleOffset',default=0.300)   # distance of multisample holder on rotation circle
    
    def __init__(self,name,config):
        super().__init__(config,name,share_hardware=False)
        user_meta = scan_meta.get_user_scan_meta()        
        self.bl_server = connection.SpecConnection('nano3:blserver')

        self.sxdetx = config.get('sxdetx')

        def _fill_meta(scan):
            return_dict = {
                'Title': self.Title,
                'HMStartTime' : datetime.datetime.utcnow().isoformat(),
                'Dummy' : self.Dummy,
                'DDummy' : self.DDummy,
                'Center_1' : self.Center_1,
                'Center_2' : self.Center_2,
                'DetectorRotation_1' : self.DetectorRotation_1,
                'DetectorRotation_2' : self.DetectorRotation_2,
                'DetectorRotation_3' : self.DetectorRotation_3,
                'SampleDistance' : self._get_eiger_distance_in_tube(),
                'DetectorPosition' : self.sxdetx.dial*0.001,
                'WaveLength' : self.get_wavelength(),
                'DetboxOffset' : self.DetboxOffset,
                'SampleOffset' : self.SampleOffset,
                'ProjectionType' : 'Saxs',
                'RasterOrientation' : 1,
                }
            return {f'{self.DetectorName}/header' :return_dict}
        user_meta.instrument.set(f'{name}:meta',_fill_meta)
        
    def get_wavelength(self):
        """ Return WaveLength in meter """
        e = Event()
        def cbk(*args):
            e.set()
            
        r = self.bl_server.send_msg_cmd_with_return("mono_getL()",cbk)
        e.wait(5*60.)
        return r.getValue()*1e-9

    def _get_eiger_distance_in_tube(self):
        """ Return SampleDistance in meter """
        return self.sxdetx.dial*0.001+self.DetboxOffset+self.SampleOffset

class MetaData_Detector_eiger2M_bigpipe(BeaconObject):
    Description = BeaconObject.property_setting('Description',default='')
    dim_i = BeaconObject.property_setting('dim_i',default=1030)
    dim_j = BeaconObject.property_setting('dim_j',default=2167)
    dim_k = BeaconObject.property_setting('dim_k',default=0)
    local_name = BeaconObject.property_setting('local_name',default="eiger2M")
    x_pixel_size = BeaconObject.property_setting('x_pixel_size',default=75E-6)
    y_pixel_size = BeaconObject.property_setting('y_pixel_size',default=75E-6)
    calibration_date = BeaconObject.property_setting('calibration_date',default="") #ISO 8601 standard
    layout = BeaconObject.property_setting('layout',default="area")
    beam_center_x = BeaconObject.property_setting('beam_center_x',default=0)
    beam_center_y = BeaconObject.property_setting('beam_center_y',default=0)
    flat_field = BeaconObject.property_setting('flat_field',default=False)
    flat_field_applied = BeaconObject.property_setting('flat_field_applied',default=False)
    pixel_mask = BeaconObject.property_setting('pixel_mask',default="") 
    pixel_mask_applied = BeaconObject.property_setting('pixel_mask_applied',default=False)  
    countrate_correction_applied = BeaconObject.property_setting('countrate_correction_applied',default=False)  
    saturation_value = BeaconObject.property_setting('saturation_value',default=0)  
    threshold_energy = BeaconObject.property_setting('threshold_energy',default=0)  
    transformation = BeaconObject.property_setting('transformation',default="")  
    sensor_thickness = BeaconObject.property_setting('sensor_thickness',default=500E-6)  
    sensor_material = BeaconObject.property_setting('sensor_material',default="Silicon")  
    bit_depth_readout = BeaconObject.property_setting('bit_depth_readout',default=0)  
    distance = BeaconObject.property_setting('distance',default=0)  
    component_index = BeaconObject.property_setting('component_index',default=0)  
    
    def __init__(self,name,config):
        
        super().__init__(config,name,share_hardware=False)
        user_meta = scan_meta.get_user_scan_meta()        
        self.DetCalib = config.get("DetCalib")
        def _fill_meta(scan):
            #TODO check ACTIVE_MG for detObjs and adjust the return dict accordingly 
            return_dict = {
                'Description': self.Description,
                'dim_i' : self.DetCalib.det_obj.image.width,
                'dim_j' : self.DetCalib.det_obj.image.height,
                #'dim_k' : scan.scan_info["npoints"], # TODO fails for sxdm no npoints defined
                'local_name' : self.DetCalib.det_obj.name,
                'x_pixel_size' : self.x_pixel_size,
                'y_pixel_size' : self.y_pixel_size,
                'calibration_date' : self.calibration_date,
                'layout' : self.layout,
                'beam_center_x' : self.DetCalib.beam_center_x,  # mod with det_calib
                'beam_center_y' : self.DetCalib.beam_center_y,  # mod with det_calib
                'flat_field' : self.DetCalib.det_obj.processing.flatfield,  # TODO should be an array
                'flat_field_applied' : self.DetCalib.det_obj.processing.use_flatfield,
                'pixel_mask' : self.DetCalib.mask, # TODO should be an array   https://manual.nexusformat.org/classes/base_classes/NXdetector.html
                'pixel_mask_applied' : self.DetCalib.det_obj.processing.use_mask,
                'countrate_correction_applied': self.countrate_correction_applied,
                'saturation_value' : self.DetCalib.det_obj.accumulation.nb_frames*(2**int(self.DetCalib.det_obj.camera.pixel_depth)),
                'threshold_energy' : self.DetCalib.det_obj.camera.threshold_energy/1000,  ## in kev TODO check units
                'transformation' : self.transformation,
                'sensor_thickness' : self.sensor_thickness,
                'sensor_material' : self.sensor_material,
                'bit_depth_readout' : int(self.DetCalib.det_obj.camera.pixel_depth), 
                'distance' : self.DetCalib.distance,  # mod with det_calib
                'component_index' : self.component_index,
                }
            return {f'{self.DetCalib.det_obj.name}' :return_dict}
        user_meta.instrument.set(f'{name}:meta',_fill_meta)
        

class MetaData_Detector_eiger2M_huber(BeaconObject):
    Description = BeaconObject.property_setting('Description',default='')
    dim_i = BeaconObject.property_setting('dim_i',default=1030)
    dim_j = BeaconObject.property_setting('dim_j',default=2167)
    dim_k = BeaconObject.property_setting('dim_k',default=0)
    local_name = BeaconObject.property_setting('local_name',default="eiger2M")
    x_pixel_size = BeaconObject.property_setting('x_pixel_size',default=75E-6)
    y_pixel_size = BeaconObject.property_setting('y_pixel_size',default=75E-6)
    calibration_date = BeaconObject.property_setting('calibration_date',default="2017-11-29") #ISO 8601 standard
    layout = BeaconObject.property_setting('layout',default="area")
    beam_center_x = BeaconObject.property_setting('beam_center_x',default=0)
    beam_center_y = BeaconObject.property_setting('beam_center_y',default=0)
    flat_field = BeaconObject.property_setting('flat_field',default=False)
    flat_field_applied = BeaconObject.property_setting('flat_field_applied',default=False)
    pixel_mask = BeaconObject.property_setting('pixel_mask',default="") 
    pixel_mask_applied = BeaconObject.property_setting('pixel_mask_applied',default=False)  
    countrate_correction_applied = BeaconObject.property_setting('countrate_correction_applied',default=False)  
    saturation_value = BeaconObject.property_setting('saturation_value',default=0)  
    threshold_energy = BeaconObject.property_setting('threshold_energy',default=0)  
    transformation = BeaconObject.property_setting('transformation',default="")  
    sensor_thickness = BeaconObject.property_setting('sensor_thickness',default=500E-6)  
    sensor_material = BeaconObject.property_setting('sensor_material',default="Silicon")  
    bit_depth_readout = BeaconObject.property_setting('bit_depth_readout',default=0)  
    distance = BeaconObject.property_setting('distance',default=0)  
    component_index = BeaconObject.property_setting('component_index',default=0)  
    
    def __init__(self,name,config):
        
        super().__init__(config,name,share_hardware=False)
        user_meta = scan_meta.get_user_scan_meta()        

        
        def _fill_meta(scan):
            #TODO check ACTIVE_MG for detObjs and adjust the return dict accordingly 
            return_dict = {
                'Description': self.Description,
                'dim_i' : self.DetectorName.image.width,
                'dim_j' : self.DetectorName.image.height,
                #'dim_k' : scan.scan_info["npoints"],
                'local_name' : self.DetectorName.name,
                'x_pixel_size' : self.x_pixel_size,
                'y_pixel_size' : self.y_pixel_size,
                'calibration_date' : self.calibration_date,
                'layout' : self.layout,
                'beam_center_x' : self.beam_center_x,  # mod with det_calib
                'beam_center_y' : self.beam_center_y,  # mod with det_calib
                'flat_field' : self.DetectorName.processing.flatfield,  # TODO should be an array
                'flat_field_applied' : self.DetectorName.processing.use_flatfield,
                'pixel_mask' : self.get_detector_mask(),   # TODO should be an array   https://manual.nexusformat.org/classes/base_classes/NXdetector.html
                'pixel_mask_applied' : self.DetectorName.processing.use_mask,
                'countrate_correction_applied': self.countrate_correction_applied,
                'saturation_value' : self.DetectorName.accumulation.nb_frames*(2**int(self.DetectorName.camera.pixel_depth)),
                'threshold_energy' : self.DetectorName.camera.threshold_energy/1000,  ## in kev TODO check units
                'transformation' : self.transformation,
                'sensor_thickness' : self.sensor_thickness,
                'sensor_material' : self.sensor_material,
                'bit_depth_readout' : int(self.DetectorName.camera.pixel_depth), 
                'distance' : self.distance,  # mod with det_calib
                'component_index' : self.component_index,
                }
            return {f'{self.DetectorName.name}' :return_dict}
        user_meta.instrument.set(f'{name}:meta',_fill_meta)

    def get_detector_mask(self,):
        exec(f"mask = self.metadata_{self.DetectorName}.mask")
        return mask
        


class MetaData_Detector_mpx1x4(BeaconObject):
    Description = BeaconObject.property_setting('Description',default='')
    dim_i = BeaconObject.property_setting('dim_i',default=516)
    dim_j = BeaconObject.property_setting('dim_j',default=516)
    dim_k = BeaconObject.property_setting('dim_k',default=0)
    local_name = BeaconObject.property_setting('local_name',default="mpx1x4")
    x_pixel_size = BeaconObject.property_setting('x_pixel_size',default=55E-6)
    y_pixel_size = BeaconObject.property_setting('y_pixel_size',default=55E-6)
    calibration_date = BeaconObject.property_setting('calibration_date',default="2017-11-29") #ISO 8601 standard
    layout = BeaconObject.property_setting('layout',default="area")
    beam_center_x = BeaconObject.property_setting('beam_center_x',default=0)
    beam_center_y = BeaconObject.property_setting('beam_center_y',default=0)
    flat_field = BeaconObject.property_setting('flat_field',default=False)
    flat_field_applied = BeaconObject.property_setting('flat_field_applied',default=False)
    pixel_mask = BeaconObject.property_setting('pixel_mask',default=None) 
    pixel_mask_applied = BeaconObject.property_setting('pixel_mask_applied',default=False)  
    countrate_correction_applied = BeaconObject.property_setting('countrate_correction_applied',default=False)  
    saturation_value = BeaconObject.property_setting('saturation_value',default=11810)  
    threshold_energy = BeaconObject.property_setting('threshold_energy',default=0)  
    transformation = BeaconObject.property_setting('transformation',default="")  
    sensor_thickness = BeaconObject.property_setting('sensor_thickness',default=500E-6)  
    sensor_material = BeaconObject.property_setting('sensor_material',default="Silicon")  
    bit_depth_readout = BeaconObject.property_setting('bit_depth_readout',default=0)  
    distance = BeaconObject.property_setting('distance',default=0)  
    component_index = BeaconObject.property_setting('component_index',default=0)  
    
    def __init__(self,name,config):     

        super().__init__(config,name,share_hardware=False)

        user_meta = scan_meta.get_user_scan_meta()   
        self.DetCalib = config.get("DetCalib")
        
        def _fill_meta(scan):
            return_dict = {
                'Description': self.Description,
                'dim_i' : self.DetCalib.det_obj.image.width,
                'dim_j' : self.DetCalib.det_obj.image.height,
                #'dim_k' : scan.scan_info["npoints"], # TODO fails for sxdm no npoints defined
                'local_name' : self.DetCalib.det_obj.name,
                'x_pixel_size' : self.x_pixel_size,
                'y_pixel_size' : self.y_pixel_size,
                'calibration_date' : self.calibration_date,
                'layout' : self.layout,
                'beam_center_x' : self.DetCalib.beam_center_x,  # mod with det_calib
                'beam_center_y' : self.DetCalib.beam_center_y,  # mod with det_calib
                'flat_field' : self.DetCalib.det_obj.processing.flatfield,  # TODO should be an array
                'flat_field_applied' : self.DetCalib.det_obj.processing.use_flatfield,
                'pixel_mask' : self.DetCalib.mask, # TODO should be an array   https://manual.nexusformat.org/classes/base_classes/NXdetector.html
                'pixel_mask_applied' : self.DetCalib.det_obj.processing.use_mask,
                'countrate_correction_applied': self.countrate_correction_applied,
                'saturation_value' : self.DetCalib.det_obj.accumulation.nb_frames*11810,
                'threshold_energy' : self.DetCalib.det_obj.camera.energy_threshold,
                'transformation' : self.transformation,
                'sensor_thickness' : self.sensor_thickness,
                'sensor_material' : self.sensor_material,
                #'bit_depth_readout' : self.bit_depth_readout,  # maxipix where?
                'distance' : self.DetCalib.distance,  # mod with det_calib
                'component_index' : self.component_index,
                }
            return {f'{self.DetCalib.det_obj.name}' :return_dict}

        user_meta.instrument.set(f'{name}:meta',_fill_meta)
        
class MetaData_Detector_mpxgaas(BeaconObject):
    Description = BeaconObject.property_setting('Description',default='')
    dim_i = BeaconObject.property_setting('dim_i',default=516)
    dim_j = BeaconObject.property_setting('dim_j',default=516)
    dim_k = BeaconObject.property_setting('dim_k',default=0)
    local_name = BeaconObject.property_setting('local_name',default="mpxgaas")
    x_pixel_size = BeaconObject.property_setting('x_pixel_size',default=55E-6)
    y_pixel_size = BeaconObject.property_setting('y_pixel_size',default=55E-6)
    calibration_date = BeaconObject.property_setting('calibration_date',default="2017-11-29") #ISO 8601 standard
    layout = BeaconObject.property_setting('layout',default="area")
    beam_center_x = BeaconObject.property_setting('beam_center_x',default=0)
    beam_center_y = BeaconObject.property_setting('beam_center_y',default=0)
    flat_field = BeaconObject.property_setting('flat_field',default=False)
    flat_field_applied = BeaconObject.property_setting('flat_field_applied',default=False)
    pixel_mask = BeaconObject.property_setting('pixel_mask',default=None) 
    pixel_mask_applied = BeaconObject.property_setting('pixel_mask_applied',default=False)  
    countrate_correction_applied = BeaconObject.property_setting('countrate_correction_applied',default=False)  
    saturation_value = BeaconObject.property_setting('saturation_value',default=11810)  
    threshold_energy = BeaconObject.property_setting('threshold_energy',default=0)  
    transformation = BeaconObject.property_setting('transformation',default="")  
    sensor_thickness = BeaconObject.property_setting('sensor_thickness',default=500E-6)  
    sensor_material = BeaconObject.property_setting('sensor_material',default="Silicon")  
    bit_depth_readout = BeaconObject.property_setting('bit_depth_readout',default=0)  
    distance = BeaconObject.property_setting('distance',default=0)  
    component_index = BeaconObject.property_setting('component_index',default=0)  
    
    def __init__(self,name,config):     

        super().__init__(config,name,share_hardware=False)
        
        user_meta = scan_meta.get_user_scan_meta()   
        self.DetCalib = config.get("DetCalib")
        
        def _fill_meta(scan):
            return_dict = {
                'Description': self.Description,
                'dim_i' : self.DetCalib.det_obj.image.width,
                'dim_j' : self.DetCalib.det_obj.image.height,
                #'dim_k' : scan.scan_info["npoints"], # TODO fails for sxdm no npoints defined
                'local_name' : self.DetCalib.det_obj.name,
                'x_pixel_size' : self.x_pixel_size,
                'y_pixel_size' : self.y_pixel_size,
                'calibration_date' : self.calibration_date,
                'layout' : self.layout,
                'beam_center_x' : self.DetCalib.beam_center_x,  # mod with det_calib
                'beam_center_y' : self.DetCalib.beam_center_y,  # mod with det_calib
                'flat_field' : self.DetCalib.det_obj.processing.flatfield,  # TODO should be an array
                'flat_field_applied' : self.DetCalib.det_obj.processing.use_flatfield,
                'pixel_mask' : self.DetCalib.mask, # TODO should be an array   https://manual.nexusformat.org/classes/base_classes/NXdetector.html
                'pixel_mask_applied' : self.DetCalib.det_obj.processing.use_mask,
                'countrate_correction_applied': self.countrate_correction_applied,
                'saturation_value' : self.DetCalib.det_obj.accumulation.nb_frames*11810,
                'threshold_energy' : self.DetCalib.det_obj.camera.energy_threshold,
                'transformation' : self.transformation,
                'sensor_thickness' : self.sensor_thickness,
                'sensor_material' : self.sensor_material,
                #'bit_depth_readout' : self.bit_depth_readout,  # maxipix where?
                'distance' : self.DetCalib.distance,  # mod with det_calib
                'component_index' : self.component_index,
                }
            return {f'{self.DetCalib.det_obj.name}' :return_dict}

        user_meta.instrument.set(f'{name}:meta',_fill_meta)
    
class MetaData_Detector_mpx1x1(BeaconObject):
    Description = BeaconObject.property_setting('Description',default='')
    dim_i = BeaconObject.property_setting('dim_i',default=516)
    dim_j = BeaconObject.property_setting('dim_j',default=516)
    dim_k = BeaconObject.property_setting('dim_k',default=0)
    local_name = BeaconObject.property_setting('local_name',default="mpx1x4")
    x_pixel_size = BeaconObject.property_setting('x_pixel_size',default=55E-6)
    y_pixel_size = BeaconObject.property_setting('y_pixel_size',default=55E-6)
    calibration_date = BeaconObject.property_setting('calibration_date',default="2017-11-29") #ISO 8601 standard
    layout = BeaconObject.property_setting('layout',default="area")
    beam_center_x = BeaconObject.property_setting('beam_center_x',default=0)
    beam_center_y = BeaconObject.property_setting('beam_center_y',default=0)
    flat_field = BeaconObject.property_setting('flat_field',default=False)
    flat_field_applied = BeaconObject.property_setting('flat_field_applied',default=False)
    pixel_mask = BeaconObject.property_setting('pixel_mask',default="") 
    pixel_mask_applied = BeaconObject.property_setting('pixel_mask_applied',default=False)  
    countrate_correction_applied = BeaconObject.property_setting('countrate_correction_applied',default=False)  
    saturation_value = BeaconObject.property_setting('saturation_value',default=11810)  
    threshold_energy = BeaconObject.property_setting('threshold_energy',default=0)  
    transformation = BeaconObject.property_setting('transformation',default="")  
    sensor_thickness = BeaconObject.property_setting('sensor_thickness',default=500E-6)  
    sensor_material = BeaconObject.property_setting('sensor_material',default="Silicon")  
    bit_depth_readout = BeaconObject.property_setting('bit_depth_readout',default=0)  
    distance = BeaconObject.property_setting('distance',default=0)  
    component_index = BeaconObject.property_setting('component_index',default=0)  
    
    def __init__(self,name,config):     

        super().__init__(config,name,share_hardware=False)
        
        user_meta = scan_meta.get_user_scan_meta()   
        self.DetCalib = config.get("DetCalib")
        
        def _fill_meta(scan):
            return_dict = {
                'Description': self.Description,
                'dim_i' : self.DetCalib.det_obj.image.width,
                'dim_j' : self.DetCalib.det_obj.image.height,
                #'dim_k' : scan.scan_info["npoints"], # TODO fails for sxdm no npoints defined
                'local_name' : self.DetCalib.det_obj.name,
                'x_pixel_size' : self.x_pixel_size,
                'y_pixel_size' : self.y_pixel_size,
                'calibration_date' : self.calibration_date,
                'layout' : self.layout,
                'beam_center_x' : self.DetCalib.beam_center_x,  # mod with det_calib
                'beam_center_y' : self.DetCalib.beam_center_y,  # mod with det_calib
                'flat_field' : self.DetCalib.det_obj.processing.flatfield,  # TODO should be an array
                'flat_field_applied' : self.DetCalib.det_obj.processing.use_flatfield,
                'pixel_mask' : self.DetCalib.mask, # TODO should be an array   https://manual.nexusformat.org/classes/base_classes/NXdetector.html
                'pixel_mask_applied' : self.DetCalib.det_obj.processing.use_mask,
                'countrate_correction_applied': self.countrate_correction_applied,
                'saturation_value' : self.DetCalib.det_obj.accumulation.nb_frames*11810,
                'threshold_energy' : self.DetCalib.det_obj.camera.energy_threshold,
                'transformation' : self.transformation,
                'sensor_thickness' : self.sensor_thickness,
                'sensor_material' : self.sensor_material,
                #'bit_depth_readout' : self.bit_depth_readout,  # maxipix where?
                'distance' : self.DetCalib.distance,  # mod with det_calib
                'component_index' : self.component_index,
                }
            return {f'{self.DetCalib.det_obj.name}' :return_dict}

        user_meta.instrument.set(f'{name}:meta',_fill_meta)
        

"""
class MetaData_Detector_mpxgaas(BeaconObject):
    Title = BeaconObject.property_setting('Title',default='')
    Dummy = BeaconObject.property_setting('Dummy',default=-1)
    DDummy = BeaconObject.property_setting('DDummy',default=0.1)
    Center_1 = BeaconObject.property_setting('Center_1',default=0)
    Center_2 = BeaconObject.property_setting('Center_2',default=0)
    DetectorRotation_1 = BeaconObject.property_setting('DetectorRotation_1',default=0)
    DetectorRotation_2 = BeaconObject.property_setting('DetectorRotation_2',default=0)
    DetectorRotation_3 = BeaconObject.property_setting('DetectorRotation_3',default=0)
    #DetectorName = BeaconObject.property_setting('DetectorName',default='')
    DetboxOffset = BeaconObject.property_setting('DetboxOffset',default=0.4736)  # standard offset of detector in saxs tube
    SampleOffset = BeaconObject.property_setting('SampleOffset',default=0.300)   # distance of multisample holder on rotation circle
    
    def __init__(self,name,config):
        super().__init__(config,name,share_hardware=False)
        user_meta = scan_meta.get_user_scan_meta()        
        self.bl_server = connection.SpecConnection('nano3:blserver')

        self.sxdetx = config.get('sxdetx')

        def _fill_meta(scan):
            return_dict = {
                'Title': self.Title,
                'HMStartTime' : datetime.datetime.utcnow().isoformat(),
                'Dummy' : self.Dummy,
                'DDummy' : self.DDummy,
                'Center_1' : self.Center_1,
                'Center_2' : self.Center_2,
                'DetectorRotation_1' : self.DetectorRotation_1,
                'DetectorRotation_2' : self.DetectorRotation_2,
                'DetectorRotation_3' : self.DetectorRotation_3,
                'SampleDistance' : self._get_eiger_distance_in_tube(),
                'DetectorPosition' : self.sxdetx.dial*0.001,
                'WaveLength' : self.get_wavelength(),
                'DetboxOffset' : self.DetboxOffset,
                'SampleOffset' : self.SampleOffset,
                'ProjectionType' : 'Saxs',
                'RasterOrientation' : 1,
                }
            return {f'{self.DetectorName}/header' :return_dict}
        user_meta.instrument.set(f'{name}:meta',_fill_meta)
        
    def get_wavelength(self):
        e = Event()
        def cbk(*args):
            e.set()
            
        r = self.bl_server.send_msg_cmd_with_return("mono_getL()",cbk)
        e.wait(5*60.)
        return r.getValue()*1e-9

    def _get_eiger_distance_in_tube(self):
        return self.sxdetx.dial*0.001+self.DetboxOffset+self.SampleOffset
"""



class MetaData_Detector_mpx1x4_id13_si_0(BeaconObject):
    Description = BeaconObject.property_setting('Description',default='')
    dim_i = BeaconObject.property_setting('dim_i',default=516)
    dim_j = BeaconObject.property_setting('dim_j',default=516)
    dim_k = BeaconObject.property_setting('dim_k',default=0)
    local_name = BeaconObject.property_setting('local_name',default="mpx1x4")
    x_pixel_size = BeaconObject.property_setting('x_pixel_size',default=55E-6)
    y_pixel_size = BeaconObject.property_setting('y_pixel_size',default=55E-6)
    calibration_date = BeaconObject.property_setting('calibration_date',default="2017-11-29") #ISO 8601 standard
    layout = BeaconObject.property_setting('layout',default="area")
    beam_center_x = BeaconObject.property_setting('beam_center_x',default=0)
    beam_center_y = BeaconObject.property_setting('beam_center_y',default=0)
    flat_field = BeaconObject.property_setting('flat_field',default=False)
    flat_field_applied = BeaconObject.property_setting('flat_field_applied',default=False)
    pixel_mask = BeaconObject.property_setting('pixel_mask',default=None) 
    pixel_mask_applied = BeaconObject.property_setting('pixel_mask_applied',default=False)  
    countrate_correction_applied = BeaconObject.property_setting('countrate_correction_applied',default=False)  
    saturation_value = BeaconObject.property_setting('saturation_value',default=11810)  
    threshold_energy = BeaconObject.property_setting('threshold_energy',default=0)  
    transformation = BeaconObject.property_setting('transformation',default="")  
    sensor_thickness = BeaconObject.property_setting('sensor_thickness',default=500E-6)  
    sensor_material = BeaconObject.property_setting('sensor_material',default="Silicon")  
    bit_depth_readout = BeaconObject.property_setting('bit_depth_readout',default=0)  
    distance = BeaconObject.property_setting('distance',default=0)  
    component_index = BeaconObject.property_setting('component_index',default=0)  
    
    def __init__(self,name,config):     

        super().__init__(config,name,share_hardware=False)
        
        user_meta = scan_meta.get_user_scan_meta()   
        self.DetCalib = config.get("DetCalib")
        
        def _fill_meta(scan):
            return_dict = {
                'Description': self.Description,
                'dim_i' : self.DetCalib.det_obj.image.width,
                'dim_j' : self.DetCalib.det_obj.image.height,
                #'dim_k' : scan.scan_info["npoints"], # TODO fails for sxdm no npoints defined
                'local_name' : self.DetCalib.det_obj.name,
                'x_pixel_size' : self.x_pixel_size,
                'y_pixel_size' : self.y_pixel_size,
                'calibration_date' : self.calibration_date,
                'layout' : self.layout,
                'beam_center_x' : self.DetCalib.beam_center_x,  # mod with det_calib
                'beam_center_y' : self.DetCalib.beam_center_y,  # mod with det_calib
                'flat_field' : self.DetCalib.det_obj.processing.flatfield,  # TODO should be an array
                'flat_field_applied' : self.DetCalib.det_obj.processing.use_flatfield,
                'pixel_mask' : self.DetCalib.mask, # TODO should be an array   https://manual.nexusformat.org/classes/base_classes/NXdetector.html
                'pixel_mask_applied' : self.DetCalib.det_obj.processing.use_mask,
                'countrate_correction_applied': self.countrate_correction_applied,
                'saturation_value' : self.DetCalib.det_obj.accumulation.nb_frames*11810,
                'threshold_energy' : self.DetCalib.det_obj.camera.energy_threshold,
                'transformation' : self.transformation,
                'sensor_thickness' : self.sensor_thickness,
                'sensor_material' : self.sensor_material,
                #'bit_depth_readout' : self.bit_depth_readout,  # maxipix where?
                'distance' : self.DetCalib.distance,  # mod with det_calib
                'component_index' : self.component_index,
                }
            return {f'{self.DetCalib.det_obj.name}' :return_dict}

        user_meta.instrument.set(f'{name}:meta',_fill_meta)

class MetaData_Detector_mpx1x4_id13_si_1(BeaconObject):
    Description = BeaconObject.property_setting('Description',default='')
    dim_i = BeaconObject.property_setting('dim_i',default=516)
    dim_j = BeaconObject.property_setting('dim_j',default=516)
    dim_k = BeaconObject.property_setting('dim_k',default=0)
    local_name = BeaconObject.property_setting('local_name',default="mpx1x4")
    x_pixel_size = BeaconObject.property_setting('x_pixel_size',default=55E-6)
    y_pixel_size = BeaconObject.property_setting('y_pixel_size',default=55E-6)
    calibration_date = BeaconObject.property_setting('calibration_date',default="2017-11-29") #ISO 8601 standard
    layout = BeaconObject.property_setting('layout',default="area")
    beam_center_x = BeaconObject.property_setting('beam_center_x',default=0)
    beam_center_y = BeaconObject.property_setting('beam_center_y',default=0)
    flat_field = BeaconObject.property_setting('flat_field',default=False)
    flat_field_applied = BeaconObject.property_setting('flat_field_applied',default=False)
    pixel_mask = BeaconObject.property_setting('pixel_mask',default=None) 
    pixel_mask_applied = BeaconObject.property_setting('pixel_mask_applied',default=False)  
    countrate_correction_applied = BeaconObject.property_setting('countrate_correction_applied',default=False)  
    saturation_value = BeaconObject.property_setting('saturation_value',default=11810)  
    threshold_energy = BeaconObject.property_setting('threshold_energy',default=0)  
    transformation = BeaconObject.property_setting('transformation',default="")  
    sensor_thickness = BeaconObject.property_setting('sensor_thickness',default=500E-6)  
    sensor_material = BeaconObject.property_setting('sensor_material',default="Silicon")  
    bit_depth_readout = BeaconObject.property_setting('bit_depth_readout',default=0)  
    distance = BeaconObject.property_setting('distance',default=0)  
    component_index = BeaconObject.property_setting('component_index',default=0)  
    
    def __init__(self,name,config):     

        super().__init__(config,name,share_hardware=False)
        
        user_meta = scan_meta.get_user_scan_meta()   
        self.DetCalib = config.get("DetCalib")
        
        def _fill_meta(scan):
            return_dict = {
                'Description': self.Description,
                'dim_i' : self.DetCalib.det_obj.image.width,
                'dim_j' : self.DetCalib.det_obj.image.height,
                #'dim_k' : scan.scan_info["npoints"], # TODO fails for sxdm no npoints defined
                'local_name' : self.DetCalib.det_obj.name,
                'x_pixel_size' : self.x_pixel_size,
                'y_pixel_size' : self.y_pixel_size,
                'calibration_date' : self.calibration_date,
                'layout' : self.layout,
                'beam_center_x' : self.DetCalib.beam_center_x,  # mod with det_calib
                'beam_center_y' : self.DetCalib.beam_center_y,  # mod with det_calib
                'flat_field' : self.DetCalib.det_obj.processing.flatfield,  # TODO should be an array
                'flat_field_applied' : self.DetCalib.det_obj.processing.use_flatfield,
                'pixel_mask' : self.DetCalib.mask, # TODO should be an array   https://manual.nexusformat.org/classes/base_classes/NXdetector.html
                'pixel_mask_applied' : self.DetCalib.det_obj.processing.use_mask,
                'countrate_correction_applied': self.countrate_correction_applied,
                'saturation_value' : self.DetCalib.det_obj.accumulation.nb_frames*11810,
                'threshold_energy' : self.DetCalib.det_obj.camera.energy_threshold,
                'transformation' : self.transformation,
                'sensor_thickness' : self.sensor_thickness,
                'sensor_material' : self.sensor_material,
                #'bit_depth_readout' : self.bit_depth_readout,  # maxipix where?
                'distance' : self.DetCalib.distance,  # mod with det_calib
                'component_index' : self.component_index,
                }
            return {f'{self.DetCalib.det_obj.name}' :return_dict}

        user_meta.instrument.set(f'{name}:meta',_fill_meta)

class MetaData_Detector_mpx1x4_id13_gaas(BeaconObject):
    Description = BeaconObject.property_setting('Description',default='')
    dim_i = BeaconObject.property_setting('dim_i',default=516)
    dim_j = BeaconObject.property_setting('dim_j',default=516)
    dim_k = BeaconObject.property_setting('dim_k',default=0)
    local_name = BeaconObject.property_setting('local_name',default="mpx1x4")
    x_pixel_size = BeaconObject.property_setting('x_pixel_size',default=55E-6)
    y_pixel_size = BeaconObject.property_setting('y_pixel_size',default=55E-6)
    calibration_date = BeaconObject.property_setting('calibration_date',default="2017-11-29") #ISO 8601 standard
    layout = BeaconObject.property_setting('layout',default="area")
    beam_center_x = BeaconObject.property_setting('beam_center_x',default=0)
    beam_center_y = BeaconObject.property_setting('beam_center_y',default=0)
    flat_field = BeaconObject.property_setting('flat_field',default=False)
    flat_field_applied = BeaconObject.property_setting('flat_field_applied',default=False)
    pixel_mask = BeaconObject.property_setting('pixel_mask',default=None) 
    pixel_mask_applied = BeaconObject.property_setting('pixel_mask_applied',default=False)  
    countrate_correction_applied = BeaconObject.property_setting('countrate_correction_applied',default=False)  
    saturation_value = BeaconObject.property_setting('saturation_value',default=11810)  
    threshold_energy = BeaconObject.property_setting('threshold_energy',default=0)  
    transformation = BeaconObject.property_setting('transformation',default="")  
    sensor_thickness = BeaconObject.property_setting('sensor_thickness',default=500E-6)  
    sensor_material = BeaconObject.property_setting('sensor_material',default="GaAs")  
    bit_depth_readout = BeaconObject.property_setting('bit_depth_readout',default=0)  
    distance = BeaconObject.property_setting('distance',default=0)  
    component_index = BeaconObject.property_setting('component_index',default=0)  
    
    def __init__(self,name,config):     

        super().__init__(config,name,share_hardware=False)
        
        user_meta = scan_meta.get_user_scan_meta()   
        self.DetCalib = config.get("DetCalib")
        
        def _fill_meta(scan):
            return_dict = {
                'Description': self.Description,
                'dim_i' : self.DetCalib.det_obj.image.width,
                'dim_j' : self.DetCalib.det_obj.image.height,
                #'dim_k' : scan.scan_info["npoints"], # TODO fails for sxdm no npoints defined
                'local_name' : self.DetCalib.det_obj.name,
                'x_pixel_size' : self.x_pixel_size,
                'y_pixel_size' : self.y_pixel_size,
                'calibration_date' : self.calibration_date,
                'layout' : self.layout,
                'beam_center_x' : self.DetCalib.beam_center_x,  # mod with det_calib
                'beam_center_y' : self.DetCalib.beam_center_y,  # mod with det_calib
                'flat_field' : self.DetCalib.det_obj.processing.flatfield,  # TODO should be an array
                'flat_field_applied' : self.DetCalib.det_obj.processing.use_flatfield,
                'pixel_mask' : self.DetCalib.mask, # TODO should be an array   https://manual.nexusformat.org/classes/base_classes/NXdetector.html
                'pixel_mask_applied' : self.DetCalib.det_obj.processing.use_mask,
                'countrate_correction_applied': self.countrate_correction_applied,
                'saturation_value' : self.DetCalib.det_obj.accumulation.nb_frames*11810,
                'threshold_energy' : self.DetCalib.det_obj.camera.energy_threshold,
                'transformation' : self.transformation,
                'sensor_thickness' : self.sensor_thickness,
                'sensor_material' : self.sensor_material,
                #'bit_depth_readout' : self.bit_depth_readout,  # maxipix where?
                'distance' : self.DetCalib.distance,  # mod with det_calib
                'component_index' : self.component_index,
                }
            return {f'{self.DetCalib.det_obj.name}' :return_dict}

        user_meta.instrument.set(f'{name}:meta',_fill_meta)


class MetaData_Detector_andor(BeaconObject):
    Title = BeaconObject.property_setting('Title',default='')
    Dummy = BeaconObject.property_setting('Dummy',default=-1)
    DDummy = BeaconObject.property_setting('DDummy',default=0.1)
    Center_1 = BeaconObject.property_setting('Center_1',default=0)
    Center_2 = BeaconObject.property_setting('Center_2',default=0)
    DetectorRotation_1 = BeaconObject.property_setting('DetectorRotation_1',default=0)
    DetectorRotation_2 = BeaconObject.property_setting('DetectorRotation_2',default=0)
    DetectorRotation_3 = BeaconObject.property_setting('DetectorRotation_3',default=0)
    #DetectorName = BeaconObject.property_setting('DetectorName',default='')
    DetboxOffset = BeaconObject.property_setting('DetboxOffset',default=0.4736)  # standard offset of detector in saxs tube
    SampleOffset = BeaconObject.property_setting('SampleOffset',default=0.300)   # distance of multisample holder on rotation circle
    
    def __init__(self,name,config):
        super().__init__(config,name,share_hardware=False)
        user_meta = scan_meta.get_user_scan_meta()        
        #self.bl_server = connection.SpecConnection('nano3:blserver')

        self.DetCalib = config.get("DetCalib")

        self.sxdetx = config.get('sxdetx')

        def _fill_meta(scan):
            return_dict = {
                'Title': self.Title,
                'HMStartTime' : datetime.datetime.utcnow().isoformat(),
                'Dummy' : self.Dummy,
                'DDummy' : self.DDummy,
                'Center_1' : self.Center_1,
                'Center_2' : self.Center_2,
                'DetectorRotation_1' : self.DetectorRotation_1,
                'DetectorRotation_2' : self.DetectorRotation_2,
                'DetectorRotation_3' : self.DetectorRotation_3,
                'SampleDistance' : self._get_eiger_distance_in_tube(),
                'DetectorPosition' : self.sxdetx.dial*0.001,
                'WaveLength' : self.get_wavelength(),
                'DetboxOffset' : self.DetboxOffset,
                'SampleOffset' : self.SampleOffset,
                'ProjectionType' : 'Saxs',
                'RasterOrientation' : 1,
                }
            return {f'{self.DetCalib.det_obj.name}/header' :return_dict}
        user_meta.instrument.set(f'{name}:meta',_fill_meta)
        
    #def get_wavelength(self):
    #    """ Return WaveLength in meter """
    #    e = Event()
    #    def cbk(*args):
    #        e.set()
    #        
    #    r = self.bl_server.send_msg_cmd_with_return("mono_getL()",cbk)
    #    e.wait(5*60.)
    #    return r.getValue()*1e-9

    def _get_eiger_distance_in_tube(self):
        """ Return SampleDistance in meter """
        return self.sxdetx.dial*0.001+self.DetboxOffset+self.SampleOffset


class MetaData_Detector_simcam1(BeaconObject):
    Description = BeaconObject.property_setting('Description',default='')
    dim_i = BeaconObject.property_setting('dim_i',default=1024)
    dim_j = BeaconObject.property_setting('dim_j',default=1024)
    dim_k = BeaconObject.property_setting('dim_k',default=0)
    local_name = BeaconObject.property_setting('local_name',default="mpx1x4")
    x_pixel_size = BeaconObject.property_setting('x_pixel_size',default=55E-6)
    y_pixel_size = BeaconObject.property_setting('y_pixel_size',default=55E-6)
    calibration_date = BeaconObject.property_setting('calibration_date',default="2017-11-29") #ISO 8601 standard
    layout = BeaconObject.property_setting('layout',default="area")
    beam_center_x = BeaconObject.property_setting('beam_center_x',default=0)
    beam_center_y = BeaconObject.property_setting('beam_center_y',default=0)
    flat_field = BeaconObject.property_setting('flat_field',default=False)
    flat_field_applied = BeaconObject.property_setting('flat_field_applied',default=False)
    pixel_mask = BeaconObject.property_setting('pixel_mask',default=None) 
    pixel_mask_applied = BeaconObject.property_setting('pixel_mask_applied',default=False)  
    countrate_correction_applied = BeaconObject.property_setting('countrate_correction_applied',default=False)  
    saturation_value = BeaconObject.property_setting('saturation_value',default=11810)  
    threshold_energy = BeaconObject.property_setting('threshold_energy',default=0)  
    transformation = BeaconObject.property_setting('transformation',default="")  
    sensor_thickness = BeaconObject.property_setting('sensor_thickness',default=500E-6)  
    sensor_material = BeaconObject.property_setting('sensor_material',default="Silicon")  
    bit_depth_readout = BeaconObject.property_setting('bit_depth_readout',default=0)  
    distance = BeaconObject.property_setting('distance',default=0)  
    component_index = BeaconObject.property_setting('component_index',default=0)  
    
    def __init__(self,name,config):     

        super().__init__(config,name,share_hardware=False)
        
        user_meta = scan_meta.get_user_scan_meta()   
        self.DetCalib = config.get("DetCalib")
        
        def _fill_meta(scan):
            return_dict = {
                'Description': self.Description,
                'dim_i' : self.DetCalib.det_obj.image.width,
                'dim_j' : self.DetCalib.det_obj.image.height,
                #'dim_k' : scan.scan_info["npoints"], # TODO fails for sxdm no npoints defined
                'local_name' : self.DetCalib.det_obj.name,
                'x_pixel_size' : self.x_pixel_size,
                'y_pixel_size' : self.y_pixel_size,
                'calibration_date' : self.calibration_date,
                'layout' : self.layout,
                'beam_center_x' : self.DetCalib.beam_center_x,  # mod with det_calib
                'beam_center_y' : self.DetCalib.beam_center_y,  # mod with det_calib
                'flat_field' : self.DetCalib.det_obj.processing.flatfield,  # TODO should be an array
                'flat_field_applied' : self.DetCalib.det_obj.processing.use_flatfield,
                'pixel_mask' : self.DetCalib.mask, # TODO should be an array   https://manual.nexusformat.org/classes/base_classes/NXdetector.html
                'pixel_mask_applied' : self.DetCalib.det_obj.processing.use_mask,
                'countrate_correction_applied': self.countrate_correction_applied,
                #'saturation_value' : self.DetCalib.det_obj.accumulation.nb_frames,
                'threshold_energy' : self.DetCalib.det_obj.camera.energy_threshold,
                'transformation' : self.transformation,
                'sensor_thickness' : self.sensor_thickness,
                'sensor_material' : self.sensor_material,
                #'bit_depth_readout' : self.bit_depth_readout,  # maxipix where?
                'distance' : self.DetCalib.distance,  # mod with det_calib
                'component_index' : self.component_index,
                }
            return {f'{self.DetCalib.det_obj.name}' :return_dict}

        user_meta.instrument.set(f'{name}:meta',_fill_meta)

#  /NXentry/NXinstrument/NXcrystal/wavelength


class MetaData_id01(BeaconObject):
    Title = BeaconObject.property_setting('Title',default='All metadata not associated to a specific object yet')
    Dummy = BeaconObject.property_setting('Dummy',default=-1)
    WaveLength = BeaconObject.property_setting('WaveLength',default=0)
    Energy = BeaconObject.property_setting('Energy',default=0)
    
    def __init__(self,name,config):
        super().__init__(config,name,share_hardware=False)
        user_meta = scan_meta.get_user_scan_meta()        
        #self.bl_server = connection.SpecConnection('nano3:blserver')


        def _fill_meta(scan):
            return_dict = {
                'Title': self.Title,
                'HMStartTime' : datetime.datetime.utcnow().isoformat(),
                'WaveLength' : self.get_wavelength(),
                'Energy' : self.energy.position*1000,
                }
            return {f'monochromator' :return_dict}
        user_meta.instrument.set(f'monochromator:meta',_fill_meta)
        
    # def get_wavelength(self):
    #     """ Return WaveLength in meter """
    #     e = Event()
    #     def cbk(*args):
    #         e.set()
            
    #     r = self.bl_server.send_msg_cmd_with_return("mono_getL()",cbk)
    #     e.wait(5*60.)
    #     return r.getValue()*1e-9

    def get_wavelength(self):
        return 12.39842/self.energy.position *1e-10