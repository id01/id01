from bliss.scanning.scan import ScanPreset


class FScanMux(ScanPreset):
    def __init__(self,multi,musst,eiger=None,mpx=None,nrj=None, nrj_tolerance=0.9):
        self._multi = multi
        self._musst = musst
        self._eiger = eiger
        self._mpx = mpx
        self._nrj = nrj
        self._nrj_tolerance = nrj_tolerance
        
    def prepare(self, scan):
        self._multi.switch("P201_GATE_IN","MUSST")
        #self._multi.switch("2D_DETECTOR","MUSST_1_BTRIG")
        self._multi.switch("2D_DETECTOR","MUSST_1_ATRIG")
         # force first input as encoder due to SPEC
        self._musst.putget("chcfg ch1 enc")
        self._musst.putget("chcfg ch2 enc")  # SL added to be checked

        #check if the detector is in acquisition chain
        
        nrj_val = self._nrj.position
        if self._eiger is not None:    
            nodes = scan.acq_chain.get_node_from_devices(self._eiger)
            if nodes:
                #self._eiger.acquisition.mode = 'ACCUMULATION'

                if self._eiger.camera.threshold_energy/1000 < (nrj_val*self._nrj_tolerance)/2 or self._eiger.camera.threshold_energy/1000 > (nrj_val*(2-self._nrj_tolerance))/2:
                    print(f"WARNING: {self._eiger.name}:{self._eiger.camera.threshold_energy/1000} is not close to half the beam energy {nrj_val}keV")
                
        if self._mpx is not None:    
            nodes = scan.acq_chain.get_node_from_devices(self._mpx)
            if nodes:
                #self._mpx.acquisition.mode = 'ACCUMULATION'
                #breakpoint()
                #if self._mpx.acquisition.mode == 'SINGLE':
                #    self._multi.switch("2D_DETECTOR","MUSST_1_BTRIG")
                #elif self._mpx.acquisition.mode =='ACCUMULATION':        
                #    self._multi.switch("2D_DETECTOR","MUSST_1_ATRIG")
                    
                    

                if self._mpx.camera.energy_threshold < (nrj_val*self._nrj_tolerance)/2 or self._mpx.camera.energy_threshold > (nrj_val*(2-self._nrj_tolerance))/2:
                    print(f"WARNING: {self._mpx.name}:{self._mpx.camera.energy_threshold} is not close to half the beam energy {nrj_val}keV")

    def set_mpx(self,mpx):
        self._mpx = mpx

    def start(self, chain):
        self._multi.switch("FS_CHANNEL","OPEN")
        
    def stop(self,chain):
        self._multi.switch("FS_CHANNEL","CLOSE")




class FScanMuxMPXONLY(ScanPreset):
    def __init__(self,multi,musst,mpx):
        self._multi = multi
        self._musst = musst
        self._mpx = mpx
        
    def prepare(self, scan):
        self._multi.switch("P201GATEIN","MUSST")
        self._multi.switch("MAXIPIX_CHANNEL","MUSST_1_BTRIG")
         # force first input as encoder due to SPEC
        self._musst.putget("chcfg ch1 enc")
        self._musst.putget("chcfg ch2 enc")  # SL added to be checked

    
        
    def start(self, chain):
        self._multi.switch("FS_CHANNEL","OPEN")
        
    def stop(self,chain):
        self._multi.switch("FS_CHANNEL","CLOSE")

