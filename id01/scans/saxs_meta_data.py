from gevent.event import Event
from bliss.scanning import scan_meta
from bliss.config.beacon_object import BeaconObject
from bliss.comm.spec import connection
import datetime




class MetaDetectorData(BeaconObject):
    Title = BeaconObject.property_setting('Title',default='')
    Dummy = BeaconObject.property_setting('Dummy',default=-1)
    DDummy = BeaconObject.property_setting('DDummy',default=0.1)
    Center_1 = BeaconObject.property_setting('Center_1',default=0)
    Center_2 = BeaconObject.property_setting('Center_2',default=0)
    DetectorRotation_1 = BeaconObject.property_setting('DetectorRotation_1',default=0)
    DetectorRotation_2 = BeaconObject.property_setting('DetectorRotation_2',default=0)
    DetectorRotation_3 = BeaconObject.property_setting('DetectorRotation_3',default=0)
    DetectorName = BeaconObject.property_setting('DetectorName',default='')
    DetboxOffset = BeaconObject.property_setting('DetboxOffset',default=0.4736)  # standard offset of detector in saxs tube
    SampleOffset = BeaconObject.property_setting('SampleOffset',default=0.300)   # distance of multisample holder on rotation circle
    
    def __init__(self,name,config):
        super().__init__(config,name,share_hardware=False)
        user_meta = scan_meta.get_user_scan_meta()        
        self.bl_server = connection.SpecConnection('nano3:blserver')

        self.sxdetx = config.get('sxdetx')

        def _fill_meta(scan):
            return_dict = {
                'Title': self.Title,
                'HMStartTime' : datetime.datetime.utcnow().isoformat(),
                'Dummy' : self.Dummy,
                'DDummy' : self.DDummy,
                'Center_1' : self.Center_1,
                'Center_2' : self.Center_2,
                'DetectorRotation_1' : self.DetectorRotation_1,
                'DetectorRotation_2' : self.DetectorRotation_2,
                'DetectorRotation_3' : self.DetectorRotation_3,
                'SampleDistance' : self._get_eiger_distance_in_tube(),
                'DetectorPosition' : self.sxdetx.dial*0.001,
                'WaveLength' : self.get_wavelength(),
                'DetboxOffset' : self.DetboxOffset,
                'SampleOffset' : self.SampleOffset,
                'ProjectionType' : 'Saxs',
                'RasterOrientation' : 1,
                }
            return {f'{self.DetectorName}/header' :return_dict}
        user_meta.instrument.set(f'{name}:meta',_fill_meta)
        
    def get_wavelength(self):
        """ Return WaveLength in meter """
        e = Event()
        def cbk(*args):
            e.set()
            
        r = self.bl_server.send_msg_cmd_with_return("mono_getL()",cbk)
        e.wait(5*60.)
        return r.getValue()*1e-9

    def _get_eiger_distance_in_tube(self):
        """ Return SampleDistance in meter """
        return self.sxdetx.dial*0.001+self.DetboxOffset+self.SampleOffset
        
