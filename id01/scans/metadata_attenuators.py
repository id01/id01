## All attenuator objects on the beamline

from gevent.event import Event
from bliss.scanning import scan_meta
from bliss.config.beacon_object import BeaconObject
from bliss.shell.standard import newsample, newdataset, newproposal
import click

#from bliss.comm.spec import connection
import datetime

# you can set the Beacon settings with obj.settings.set(dict)

class MetaData_Be_valve(BeaconObject):
    Type = BeaconObject.property_setting('type',default="Be")
    Thickness = BeaconObject.property_setting('thickness',default=300e-6)
    Status = BeaconObject.property_setting('status',default="OUT")
    Distance = BeaconObject.property_setting('distance',default=114)
    AttenuatorID = BeaconObject.property_setting('attentuatorID',default=1)

    def __init__(self,name,config):
        super().__init__(config,name,share_hardware=False)
        user_meta = scan_meta.get_user_scan_meta()     
        print(name)

        def _fill_meta(scan):
            return_dict = {
                'type': self.Type, 
                'thickness' : self.get_thickness(),
                'status' : self.get_status(), 
                'distance' : self.Distance,      
                }
            return {f'attenuator{self.get_attenuatorID():02}' :return_dict}

        user_meta.instrument.set(f'attenuator{self.get_attenuatorID():02}:meta',_fill_meta)  

    def get_status(self,):
        if self.config["valve"].is_closed:
            self.Status = "IN"
        else:
            self.Status = "OUT"

        return self.Status
    
    def get_thickness(self,):
        return self.Thickness
    
    def get_attenuatorID(self,):
        return self.AttenuatorID

    
class MetaData_ebv2(BeaconObject):
    Type = BeaconObject.property_setting('type',default="Sapphire")
    Thickness = BeaconObject.property_setting('thickness',default=100e-6)
    Status = BeaconObject.property_setting('status',default="OUT")
    Distance = BeaconObject.property_setting('distance',default=38.176)
    AttenuatorID = BeaconObject.property_setting('attentuatorID',default=9)

    def __init__(self,name,config):
        super().__init__(config,name,share_hardware=False)
        user_meta = scan_meta.get_user_scan_meta()     
        
        def _fill_meta(scan):
            return_dict = {
                'type': self.Type,
                'thickness' : self.get_thickness(),
                'status' : self.get_status(), 
                'distance' : self.Distance,      
                }
            return {f'attenuator{self.get_attenuatorID():02}' :return_dict}

        user_meta.instrument.set(f'attenuator{self.get_attenuatorID():02}:meta',_fill_meta)  

    def get_status(self,):
        self.Status = self.config["beamviewer"].screen_status
        if self.Status is not "IN" and self.Status is not "OUT":
            self.Status = "OUT"

        return self.Status
    
    def get_thickness(self,):
        return self.Thickness

    def get_attenuatorID(self,):
        return self.AttenuatorID


class MetaData_filterHandler(BeaconObject):
    TypeA = BeaconObject.property_setting('typeA',default="Be")
    ThicknessA = BeaconObject.property_setting('thicknessA',default=300e-6)
    StatusA = BeaconObject.property_setting('statusA',default="OUT")
    DistanceA = BeaconObject.property_setting('distanceA',default=114)
    AttenuatorIDA = BeaconObject.property_setting('attentuatorIDA',default=1)

    TypeB = BeaconObject.property_setting('typeB',default="Be")
    ThicknessB = BeaconObject.property_setting('thicknessB',default=300e-6)
    StatusB = BeaconObject.property_setting('statusB',default="OUT")
    DistanceB = BeaconObject.property_setting('distanceB',default=114)
    AttenuatorIDB = BeaconObject.property_setting('attentuatorIDB',default=1)

    TypeC = BeaconObject.property_setting('typeC',default="Be")
    ThicknessC = BeaconObject.property_setting('thicknessC',default=300e-6)
    StatusC = BeaconObject.property_setting('statusC',default="OUT")
    DistanceC = BeaconObject.property_setting('distanceC',default=114)
    AttenuatorIDC = BeaconObject.property_setting('attentuatorIDC',default=1)

    TypeD = BeaconObject.property_setting('typeD',default="Be")
    ThicknessD = BeaconObject.property_setting('thicknessD',default=300e-6)
    StatusD = BeaconObject.property_setting('statusD',default="OUT")
    DistanceD = BeaconObject.property_setting('distanceD',default=114)
    AttenuatorIDD = BeaconObject.property_setting('attentuatorIDD',default=1)


    def __init__(self,name,config):
        super().__init__(config,name,share_hardware=False)
        user_meta = scan_meta.get_user_scan_meta()     
        print(name)

        def _fill_metaA(scan):
            return_dict = {
                'type': self.get_typeA(0), 
                'thickness' : self.get_thicknessA(0),
                'status' : self.get_statusA(0), 
                'distance' : self.DistanceA,      
                }
            return {f'attenuator{self.get_attenuatorIDA(0):02}' :return_dict}

        user_meta.instrument.set(f'attenuator{self.get_attenuatorIDA(0):02}:meta',_fill_metaA)  

        def _fill_metaB(scan):
            return_dict = {
                'type': self.get_typeB(1), 
                'thickness' : self.get_thicknessB(1),
                'status' : self.get_statusB(1), 
                'distance' : self.DistanceB,      
                }
            return {f'attenuator{self.get_attenuatorIDB(1):02}' :return_dict}

        user_meta.instrument.set(f'attenuator{self.get_attenuatorIDB(1):02}:meta',_fill_metaB) 

        def _fill_metaC(scan):
            return_dict = {
                'type': self.get_typeC(2), 
                'thickness' : self.get_thicknessC(2),
                'status' : self.get_statusC(2), 
                'distance' : self.DistanceC,      
                }
            return {f'attenuator{self.get_attenuatorIDC(2):02}' :return_dict}

        user_meta.instrument.set(f'attenuator{self.get_attenuatorIDC(2):02}:meta',_fill_metaC) 

        def _fill_metaD(scan):
            return_dict = {
                'type': self.get_typeD(3), 
                'thickness' : self.get_thicknessD(3),
                'status' : self.get_statusD(3), 
                'distance' : self.DistanceD,      
                }
            return {f'attenuator{self.get_attenuatorIDD(3):02}' :return_dict}

        user_meta.instrument.set(f'attenuator{self.get_attenuatorIDD(3):02}:meta',_fill_metaD) 


    def update_metaA(self,position):
        binary_filters = format(self.config["filterHandler"].filtersetObj.position,"04b")
        conf_filters = self.config["filterHandler"].filtersetObj._config_filters

        for filter in conf_filters:
            if filter["position"]==position:
                filter_is_used = binary_filters[filter["position"]]
                self.AttenuatorIDA = self.AttenuatorIDA+filter["position"]
                self.TypeA = filter["material"]
                self.ThicknessA = filter["thickness"]
                if int(filter_is_used):
                    self.StatusA= "IN"            
                else:
                    self.StatusA= "OUT"    
        return self.StatusA

    def get_typeA(self,position):
        self.update_metaA(position)
        return self.TypeA
        
    def get_statusA(self,position):
        self.update_metaA(position)
        return self.StatusA
    
    def get_thicknessA(self,position):
        self.update_metaA(position)
        return self.ThicknessA
    
    def get_attenuatorIDA(self,position):
        self.update_metaA(position)
        return self.AttenuatorIDA

    def update_metaB(self,position):
        binary_filters = format(self.config["filterHandler"].filtersetObj.position,"04b")
        conf_filters = self.config["filterHandler"].filtersetObj._config_filters

        for filter in conf_filters:
            if filter["position"]==position:
                filter_is_used = binary_filters[filter["position"]]
                self.AttenuatorIDB = self.AttenuatorIDA+filter["position"]
                self.TypeB = filter["material"]
                self.ThicknessB = filter["thickness"]
                if int(filter_is_used):
                    self.StatusB= "IN"            
                else:
                    self.StatusB= "OUT"    
        return self.StatusB

    def get_typeB(self,position):
        self.update_metaB(position)
        return self.TypeB
        
    def get_statusB(self,position):
        self.update_metaB(position)
        return self.StatusB
    
    def get_thicknessB(self,position):
        self.update_metaB(position)
        return self.ThicknessB
    
    def get_attenuatorIDB(self,position):
        self.update_metaB(position)
        return self.AttenuatorIDB

    def update_metaC(self,position):
        binary_filters = format(self.config["filterHandler"].filtersetObj.position,"04b")
        conf_filters = self.config["filterHandler"].filtersetObj._config_filters

        for filter in conf_filters:
            if filter["position"]==position:
                filter_is_used = binary_filters[filter["position"]]
                self.AttenuatorIDC = self.AttenuatorIDA+filter["position"]
                self.TypeC = filter["material"]
                self.ThicknessC = filter["thickness"]
                if int(filter_is_used):
                    self.StatusC= "IN"            
                else:
                    self.StatusC= "OUT"    
        return self.StatusC

    def get_typeC(self,position):
        self.update_metaC(position)
        return self.TypeC
        
    def get_statusC(self,position):
        self.update_metaC(position)
        return self.StatusC
    
    def get_thicknessC(self,position):
        self.update_metaC(position)
        return self.ThicknessC
    
    def get_attenuatorIDC(self,position):
        self.update_metaC(position)
        return self.AttenuatorIDC
    
    def update_metaD(self,position):
        binary_filters = format(self.config["filterHandler"].filtersetObj.position,"04b")
        conf_filters = self.config["filterHandler"].filtersetObj._config_filters

        for filter in conf_filters:
            if filter["position"]==position:
                filter_is_used = binary_filters[filter["position"]]
                self.AttenuatorIDD = self.AttenuatorIDA+filter["position"]
                self.TypeD = filter["material"]
                self.ThicknessD = filter["thickness"]
                if int(filter_is_used):
                    self.StatusD= "IN"            
                else:
                    self.StatusD= "OUT"    
        return self.StatusD

    def get_typeD(self,position):
        self.update_metaD(position)
        return self.TypeD
        
    def get_statusD(self,position):
        self.update_metaD(position)
        return self.StatusD
    
    def get_thicknessD(self,position):
        self.update_metaD(position)
        return self.ThicknessD
    
    def get_attenuatorIDD(self,position):
        self.update_metaD(position)
        return self.AttenuatorIDD
