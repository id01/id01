from bliss.scanning.scan import ScanPreset

class SXDM_Shutter_Preset(ScanPreset):
    def __init__(self,multiplexer):
        self._multiplexer = multiplexer
        
    def start(self,scan):
        self._multiplexer.switch('FS_CHANNEL', 'OPEN')
    def stop(self,scan):
        self._multiplexer.switch('FS_CHANNEL', 'CLOSE')
