from bliss.config import settings
from bliss.scanning.chain import ChainPreset

_FS_SHUTTER_SETTINGS = settings.SimpleSetting('fast_shutter:open_mode',
                                              default_value='P201')
_MULTIPLEXER = None

class StepAccumulationMux(ChainPreset):
    def __init__(self,multi,eiger=None,mpx=None,nrj=None, nrj_tolerance=0.9):
        super().__init__()
        self._multi = multi
        self._eiger = eiger
        self._mpx = mpx
        self._nrj = nrj
        self._nrj_tolerance = nrj_tolerance
 
    def prepare(self, chain):
        self._multi.switch('ITRIG_MUSST_1', 'P201')
        self._multi.switch('2D_DETECTOR', 'MUSST_1_BTRIG')
        self._multi.switch("P201_GATE_IN","P201")
        shutter_mode = _FS_SHUTTER_SETTINGS.get()
        self._multi.switch("FS_CHANNEL",shutter_mode)

        nrj_val = self._nrj.position

        if self._eiger is not None:
            if self._eiger.camera.threshold_energy/1000 < (nrj_val*self._nrj_tolerance)/2 or self._eiger.camera.threshold_energy/1000 > (nrj_val*(2-self._nrj_tolerance))/2:
                print(f"WARNING: {self._eiger.name}:{self._eiger.camera.threshold_energy/1000} is not close to half the beam energy {nrj_val}keV")

        if self._mpx is not None:    
            if self._mpx.camera.energy_threshold < (nrj_val*self._nrj_tolerance)/2 or self._mpx.camera.energy_threshold > (nrj_val*(2-self._nrj_tolerance))/2:
                print(f"WARNING: {self._mpx.name}:{self._mpx.camera.energy_threshold} is not close to half the beam energy {nrj_val}keV")


def fshut_mode_setup(multi):
    global _MULTIPLEXER
    _MULTIPLEXER = multi
    
def fshut_mode_force_close():
    _MULTIPLEXER.switch("FS_CHANNEL","CLOSE")
    _FS_SHUTTER_SETTINGS.set("CLOSE")     # hash this out requested by EB - forces use of limatake - not good for counters 

def fshut_mode_force_open():
    _MULTIPLEXER.switch("FS_CHANNEL","OPEN")
    _FS_SHUTTER_SETTINGS.set("OPEN")    # hash this out requested by EB - forces use of limatake - not good for counters 

def fshut_mode_p201():
    _MULTIPLEXER.switch("FS_CHANNEL","P201")
    _FS_SHUTTER_SETTINGS.clear() # use the default
    
    
