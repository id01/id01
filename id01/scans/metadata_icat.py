#########################################
# Metadata definitions for BCDI
#########################################

from gevent.event import Event
from bliss.scanning import scan_meta
from bliss.config.beacon_object import BeaconObject
from bliss.shell.standard import newsample, newdataset, newproposal,enddataset
from bliss import setup_globals as SG
import click

#from bliss.comm.spec import connection
import datetime


def ID01newsample(sample_name = None, description = None, query_user = True):
    # TODO add test against A-form defined samples
    if sample_name is None:
        sample_name = input("Please provide a sample name:")
    #if query_user:
    #    ans = click.confirm(f"\n sample: \'{sample_name}\' is OK? y/n")
    #else:
    #    ans = True  

    if description is None:
        description = input("Please provide a description of your sample:")

    newsample(sample_name,description)

def ID01newdataset(technique_name = None, verbose= False):
    
    techniques = ["BCDI", "PTYCHO", "SXDM", "ALIGNMENT","FLUO"]

    while not techniques.count(technique_name):
        print("available techniques: ")
        [print("\t"+x) for x in techniques]
        technique_name = input("Please provide a valid technique name :")
    if verbose:
        try:
            print(SG.SCAN_SAVING.dataset.existing.definition)
        except:
            print(".. no technique defined ..")

    if not SG.SCAN_SAVING.dataset.is_closed:
        ID01enddataset()

    if verbose:
        print(f"setting: {technique_name}")
    newdataset(technique_name)
    SG.SCAN_SAVING.dataset.add_techniques(str.lower(technique_name))
    
    if verbose:
        print(SG.SCAN_SAVING.dataset.existing.definition)


def ID01enddataset(technique_name = None, verbose=False):
    print("CLOSING dataset...")
    enddataset()
    techniques = ["BCDI", "PTYCHO", "SXDM", "ALIGNMENT","FLUO"]
    SG.SCAN_SAVING.dataset.remove_techniques(*techniques)
    if verbose:
        try:
            print(SG.SCAN_SAVING.dataset.existing.definition)
        except:
            print(".. no technique defined ..")

def ID01newproposal(proposal_name = None):
    # TODO add test for avaiable experiments
    newproposal(proposal_name)

def setID01metadata():
    
    # so we want to iterate through all components touhing the beam and set up metadata definitions
    pass


class MetaDataDummy(BeaconObject):
    Title = BeaconObject.property_setting('Title',default='')
    Dummy = BeaconObject.property_setting('Dummy',default=-1)

    def __init__(self,name,config):
        super().__init__(config,name,share_hardware=False)
        user_meta = scan_meta.get_user_scan_meta()     

        def _fill_meta(scan):
            return_dict = {
                'Title': self.Title,
                'HMStartTime' : datetime.datetime.utcnow().isoformat(),
                'Dummy' : self.Dummy,        
                }
            print("executed this...")
            print(f'{self.DetectorName.name}/header')
            return {f'{self.DetectorName.name}/header' :return_dict}
        print(f'{name}:meta')
        user_meta.instrument.set(f'{name}:meta',_fill_meta)  