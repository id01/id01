#########################################
# Metadata definitions for microscopes
#  to use them initialise an object with one of the classes
#   >> see the init scan_meta.set gets a fill function which pulls all important
#      data from the sessions and adds it in the acquitisition chain
#  
#   TODO:  switch the several masters in the scan, one being the microscope who
#          operates a loopscan(1)
#########################################


from gevent.event import Event
from bliss.scanning import scan_meta
from bliss.config.beacon_object import BeaconObject
from bliss.comm.spec import connection
import id01.scripts.image as image

import datetime


      

class MetaData_Eta_Microscope(BeaconObject):
    Description = BeaconObject.property_setting('Description',default='eta microscope')
    status = BeaconObject.property_setting('status',default="OUT")
    image_start = BeaconObject.property_setting('image_start',default=None)
    center_of_rotation = BeaconObject.property_setting('center_of_rotation',default=None)
    
    def __init__(self,name,config):
        
        super().__init__(config,name,share_hardware=False)
        user_meta = scan_meta.get_user_scan_meta()        

        
        def _fill_meta(scan):
            #TODO check ACTIVE_MG for detObjs and adjust the return dict accordingly 
            return_dict = {
                'Description': self.Description,
                'status' : "IN", 
                'image_start' : self.get_microscope_image(),
                'center_of_rotation' : self.get_COR(),
                }
            return {f'eta_microscope' :return_dict}
        user_meta.instrument.set(f'{name}:meta',_fill_meta)
        

    def get_microscope_image(self, url = "http://vidid011.esrf.fr/jpg/1/image.jpg", navg=10, enhance=True):
        """
        return a microscope image from a URL
        """
        im = image.url2array(url, navg=navg)
        

        # TODO stretch shouldnt work!
        if enhance:
            self._stretch = 5., 95.
            im = image.stretch_contrast(im, *self._stretch)
            
        return im

    def get_COR(self, ):
        """
        return the COR for the microscope
        TODO
        """
        return (0,0)
