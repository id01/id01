from pprint import pprint
import requests
import getpass
import time
import os


def get_headers(token):
    return {"X-SLURM-USER-NAME": getpass.getuser(), "X-SLURM-USER-TOKEN": token}


def async_job(url, token, script, std_out = None, std_err = None, timeout=None):
    job_name = "test"
    # API: https://slurm.schedmd.com/rest_api.html#v0.0.37_job_properties
    
    ##MPI
    job_recipe = {
        "script": script,
        "job": {
            "name": job_name,
            "current_working_directory": f"/home/esrf/{getpass.getuser()}/slurm_logs",
            "standard_input": "/dev/null",
            "standard_output": f"/home/esrf/{getpass.getuser()}/slurm_logs/{job_name}.%j.out",
            "standard_error": f"/home/esrf/{getpass.getuser()}/slurm_logs/{job_name}.%j.err",
            "environment": {"MYVAR": "myvalue"},
            "partition": "p9gpu",
            "nodes":"2",
            "tasks":4,
            "gres":"gpu:2",
            "cpus_per_task":"8",
            "memory_per_cpu":"4G",
            #"output":"%x-%j.out",
        },
    }
    
    ##NON-MPI
    """
    job_recipe = {
        "script": script,
        "job": {
            "name": job_name,
            "current_working_directory": f"/home/esrf/{getpass.getuser()}/slurm_logs",
            "standard_input": "/dev/null",
            "standard_output": f"/home/esrf/{getpass.getuser()}/slurm_logs/{job_name}.%j.out",
            "standard_error": f"/home/esrf/{getpass.getuser()}/slurm_logs/{job_name}.%j.err",
            "environment": {"MYVAR": "myvalue"},
            "partition": "p9gpu",
            "tasks":1,
            "gres":"gpu:1",
            "memory_per_cpu":"4G",
            #"output":"%x-%j.out",
        },
    }
    """
    if std_out is not None:
        job_recipe["job"]["standard_output"] = os.path.join(std_out,f"{job_name}.%j.out")
    if std_err is not None:
        job_recipe["job"]["standard_error"] = os.path.join(std_err,f"{job_name}.%j.err")    
        
    response = requests.post(
        url + "/slurm/v0.0.37/job/submit",
        json=job_recipe,
        timeout=timeout,
        headers=get_headers(token),
    )
    response.raise_for_status()
    return response.json()


def job_status(url, token, job_id, timeout=None):
    response = requests.get(
        url + f"/slurm/v0.0.37/job/{job_id}",
        timeout=timeout,
        headers=get_headers(token),
    )
    response.raise_for_status()
    return response.json()


def get_jobs(url, token, user_name=None, myjobs=True, timeout=None):
    response = requests.get(
        url + "/slurm/v0.0.37/jobs", timeout=timeout, headers=get_headers(token)
    )
    response.raise_for_status()
    jobs = dict()
    if myjobs:
        user_name = getpass.getuser()
    for job in response.json()["jobs"]:
        if user_name and job["user_name"] != user_name:
            continue
        adict = jobs.setdefault(job["user_name"], dict())
        adict = adict.setdefault(job["partition"], dict())
        adict.setdefault(job["name"], 0)
        adict[job["name"]] += 1
    return jobs


def handle_logs(filename):
    for _ in range(30):
        if os.path.exists(filename):
            break
        time.sleep(0.1)
    if not os.path.exists(filename):
        print(f"{filename} is missing")
        return
    with open(filename) as f:
        lines = list(f)
    os.unlink(filename)
    print("".join(lines))


def sync_job(url, token, script):
    response = async_job(url, token, script)
    print("JOB:")
    pprint(response)
    job_id = response["job_id"]

    job_state = None
    while job_state not in ("FAILED", "COMPLETED"):
        response = job_status(url, token, job_id)
        job = response["jobs"][0]
        job_state = job["job_state"]
        time.sleep(0.1)

    print("\nSTDOUT")
    handle_logs(job["standard_output"].replace("%j", str(job_id)))
    print("\nSTDERR")
    handle_logs(job["standard_error"].replace("%j", str(job_id)))

    print()
    print(job_state)


if __name__ == "__main__":
    # Get a token with:
    #
    #  scontrol token lifespan=99999

    token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2NDM5NzY1ODYsImlhdCI6MTY0Mzg3NjU4Nywic3VuIjoiZGVub2xmIn0.l-Y6UBHcKDM6va9PHpHgBHUPPCuLUnhVKi6XI7OebHE"
    token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2NDQ0MTk2NjQsImlhdCI6MTY0NDQxNjA2NCwic3VuIjoiZGVub2xmIn0.9_sLH8qCGTxAngUBs5v_CSptxYf9KESnNyuV1ENTWqk"
    url = "http://slurm-api.esrf.fr:6820"

    script = """#!/bin/bash
export
echo 'I am from the REST API'
"""

    sync_job(url, token, script)

    # pprint(get_jobs(url, token))
