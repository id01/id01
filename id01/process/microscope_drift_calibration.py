# -*- coding: utf-8 -*-

# ID01 BLISS git repo
#       authors:
#         Steven Leake, leake@esrf.fr
#
#       Created at: 15. February 14:50:00 CET 2022
#
#----------------------------------------------------------------------
# Description: 
#    migrated functions from id01sware
#    get_relative_shift
#    get_value_from_LUT
#
# TODO:
#   TODO all other functions if useful
#----------------------------------------------------------------------

# camview_drift calib
#   rotate some axis using SPEC - take microscope images at each step
#   log the data
#   analyse the shift 
#   !!!! no checking be careful what you send to spec !!!!
#   TODO
#      Make all functions generic
#      Could parallelise the operation but tolerable for now.

import pylab as pl
import numpy as np
import time
import os
import h5py as h5
import pdb
from scipy.ndimage.fourier import fourier_shift
from scipy import interpolate
import scipy.ndimage
import math
#from moviepy.editor import ImageSequenceClip
import matplotlib.pyplot as plt

from skimage.registration import phase_cross_correlation
from silx.image import sift

#from id01lib import image
#from id01lib.process import microscope
#from id01lib.process import SpecClientWrapper

from bliss.shell.standard import umvr 
from id01.scripts.ID01_utils import shexa_set_pose
import id01.scripts.image as image
import id01.process.microscope as microscope


def generate_drift_calib_fn(outdir, suffix):
    """
    Generate the output filename with a timestamp prefix
    """
    now = time.gmtime()
    timestamp = "%04.i%02.i%02.i_%02.i%02.i%02.i"%(now.tm_year,now.tm_mon,now.tm_mday,now.tm_hour,now.tm_min,now.tm_sec) \
                + suffix
    output_directory = outdir \
                       + timestamp \
                       + '/'
    try:
        os.mkdir(output_directory)
        print("... saving to (%s) ..."%output_directory)
    except:
        print("... failed to write (%s) ..."%output_directory)

    outfn = output_directory \
            +"%s.h5"%(timestamp)
            
    return outfn


def analyse_axis(mot_name, mot_stroke, spec, image_url, outfn):
    """
    Take an image at each position of a given axis
    """
    outf = h5.File(outfn,"a")

    for ii,mot_pos in enumerate(mot_stroke):
        spec.send_sc("umv %s %.3f"%(mot_name, mot_pos))
        print(mot_name, mot_pos)
        img = image.url2array(image_url, 10)
        outf["%.3f"%mot_pos]=img #[100:-100,100:-100]
        '''
        pl.figure(1)
        pl.clf()
        pl.imshow(img)#[100:-100,100:-100])
        pl.savefig(output_directory+"%s/camview_%04.i_%04.i.png"%(timestamp,ii,jj))
        '''
    outf.close()
    
def analyse_axis_plus_focus(mot_name, mot_stroke, mot_focus_name, mot_focus_stroke, spec, image_url, outfn):
    """
    Take an image at each position of a given axis, optimise focus before each exposure 
    """

    outf=h5.File(outfn,"a")

    for ii,mot_pos in enumerate(mot_stroke):
        spec.send_sc("umv %s %.3f"%(mot_name, mot_pos))
        print(mot_name, mot_pos)
        # check the optimal mot_focus_name at each position - use contrast to optimise this
        for jj,mot_focus_pos in enumerate(mot_focus_stroke):
            spec.send_sc("umv %s %.3f"%(mot_focus_name, mot_focus_pos))
            img = image.url2array(image_url, 10)
            outf["%s_%.3f_%s_%.3f"%(mot_name, mot_pos, mot_focus_name, mot_focus_pos)] = img #[100:-100,100:-100]
            '''
            pl.figure(1)
            pl.clf()
            pl.imshow(img)#[100:-100,100:-100])
            pl.savefig(output_directory+"%s/camview_%04.i_%04.i.png"%(timestamp,ii,jj))
            '''
    outf.close()

def calib_microscope( spec, outfn, image_url, mot1name="pix", mot1pos=np.array([1,99]), mot1end = 50.0, mot2name="piy", mot2pos=np.array([1,99]), mot2end = 50.0 ):
    """
    Calibrate the microscope in pixels per micron 
    """

    outf = h5.File(outfn,"a")

    spec.send_sc("umv %s %i"%(mot1name, mot1pos[0]))
    spec.send_sc("sleep(0.5)")

    img = image.url2array(image_url, 10)
    outf["mot1_min"] = img
    outf["mot1_name"] = mot1name
    outf["mot1_pos"] = mot1pos

    spec.send_sc("umv %s %i"%(mot1name, mot1pos[1]))
    spec.send_sc("sleep(0.5)")

    img = image.url2array(image_url, 10)
    outf["mot1_max"] = img

    spec.send_sc("umv %s %i"%(mot2name, mot2pos[0]))
    spec.send_sc("sleep(0.5)")

    img = image.url2array(image_url, 10)
    outf["mot2_min"] = img
    outf["mot2_name"] = mot2name
    outf["mot2_pos"] = mot2pos

    spec.send_sc("umv %s %i"%(mot2name, mot2pos[1]))
    spec.send_sc("sleep(0.5)")

    img = image.url2array(image_url, 10)
    outf["mot2_max"] = img
    outf.close()
    spec.send_sc("umv %s %i %s %i"%(mot1name, mot1end, mot2name, mot2end))

    #spec.send_sc("umv %s %i %s %i"%(mot1name, mot1pos.sum()/2, mot2name, mot2pos.sum()/2))

def microscope_pixels_2_microns(outfn):
    """
    Calculate pixels per degree for the microscope image
    """
    outf = h5.File(outfn,"a")
    mot1min = outf["mot1_min"][70:-70,130:-130]
    mot1max = outf["mot1_max"][70:-70,130:-130]
    mot2min = outf["mot2_min"][70:-70,130:-130]
    mot2max = outf["mot2_max"][70:-70,130:-130]

    deltamot1 = image_offset(mot1min, mot1max)
    deltamot2 = image_offset(mot2min, mot2max)
    
    print(deltamot1,deltamot2)

    outf["mot1_calib"] = deltamot1/(outf["mot1_pos"][1]-outf["mot1_pos"][0])  #pixels per micron
    outf["mot2_calib"] = deltamot2/(outf["mot2_pos"][1]-outf["mot2_pos"][0])  #pixels per micron
    outf.close()


def image_offset(refIm,Im):
    """
    Find the pixelwise offset between two images
    """
    devicetype = "GPU"
    sa = sift.LinearAlign(refIm, devicetype=devicetype)
    res = sa.align(Im, shift_only=True, return_all=True,double_check=False, relative=False, orsa=False)
    results = dict(align=res)
    return results["align"]["offset"][::-1]

def calc_coords(offset,mot1_calib,mot2_calib):
    """
    convert the shift in pixels to microns
    """
    A = np.array([[mot1_calib[0], mot2_calib[0]], [mot1_calib[1], mot2_calib[1]]])
    D = np.linalg.inv(A)
    offset_microns = np.dot(D,offset)
    return offset_microns
    
def calc_drift(fn, outfn, mot1_calib, mot2_calib, ref_key = "0.000"):
    """
    Calculate the drift in pixels
    """
    with h5.File(fn,"r") as data:
        with h5.File(outfn,'a') as outputarr:
            ref_image = data[ref_key].value[70:-70,130:-130]

            output=[]
            outputshiftpixels=[]
            
            i = 0
            rawdata = fn.split('/')[-1]+"_raw"
            shiftdata = fn.split('/')[-1]+"_shift"
            outputarr.create_dataset(rawdata, (len(data.keys()),ref_image.shape[0],ref_image.shape[1]),maxshape=(None,ref_image.shape[0],ref_image.shape[1]))
            outputarr.create_dataset(shiftdata, (len(data.keys()),ref_image.shape[0],ref_image.shape[1]),maxshape=(None,ref_image.shape[0],ref_image.shape[1]))
            
            # loop through all images in hdf5 file
            for key in data.keys():
                image = data[key].value[70:-70,130:-130]
                offset = image_offset(ref_image, image)
                offset_microns = calc_coords(offset, mot1_calib, mot2_calib)
                output.append([float(key),offset_microns[0],offset_microns[1]])
                outputshiftpixels.append([float(key),offset[0],offset[1]])
                #(((offset[0]/deltapix)**2).sum())**0.5,(((offset[1]/deltapiy)**2).sum())**0.5])
                #print(offset)
                outputarr[rawdata][i,:,:] = image
                outputarr[shiftdata][i,:,:] = abs(np.fft.ifft2((fourier_shift(np.fft.fft2(image),shift=[-offset[1],-offset[0]]))))
                #print("shift_left",image_offset(ref_image,abs(np.fft.ifft2((fourier_shift(np.fft.fft2(image),shift=[-offset[1],-offset[0]]))))))
                i+=1
                                
            # save the data	
            outputarr[fn.split('/')[-1]]=np.array(output)
            outputarr[fn.split('/')[-1]+"pixels"]=np.array(outputshiftpixels)


def plot_drifts(fn, mot_name, calib_mot1_name, calib_mot2_name):
    """
    plot the drifts as a function of the target axis
    """
    with h5.File(fn,"r") as data:
        pl.figure()
        pl.subplot(1,2,1)
        for key in data.keys():
            if key.endswith(".h5"):
                pl.plot(data[key].value[:,0],data[key].value[:,2],"x")
           
        pl.xlabel("%s (degrees)"%mot_name)
        pl.ylabel("%s shift (microns)"%calib_mot2_name)
           
        pl.subplot(1,2,2)
        for key in data.keys():
            if key.endswith(".h5"):
                pl.plot(data[key].value[:,0],data[key].value[:,1],"x")
           
        pl.xlabel("%s (degrees)"%mot_name)
        pl.ylabel("%s shift (microns)"%calib_mot1_name)
        pl.savefig(fn.split('.')[0] 
                   + "shift.pdf")
        pl.clf()
        pl.close()
    
def generate_LUT(path, prefix, outfn, suffix="_results20190208.h5", mot0name="pix", mot1name="piy", axis0name="eta", axis0=np.arange(-30.0,120.01,.5), axis1name="phi", axis1=np.arange(-95.0,95.01,5)):
    """
    Generate a 2D look up table    
    """
    dir_names = os.listdir(path)
    
    #fn = dir_names[0]+"/"+dir_names[0]+".h5"
    xx, yy = np.meshgrid(axis0, axis1) 
    zz_mot0 = np.zeros(xx.shape)
    zz_mot1 = np.zeros(xx.shape)
    zz_shift0 = np.zeros(xx.shape)
    zz_shift1 = np.zeros(xx.shape)
       
    for ii,axis1_pos in enumerate(axis1):
        # find filename
        for name in dir_names:
            if name.startswith(prefix):
                if int(name.split("_phi_")[-1])==int(axis1_pos):
                    fn=name+"/"+name+".h5"
                    print(axis1_pos, name, fn)
        
        # extract the data
        with h5.File(path+fn.split(".h5")[0]+suffix,'r') as shift_data:
            print(fn.split("/")[-1])
            #pdb.set_trace()
            tmp = shift_data[fn.split("/")[-1]].value
            tmp1 = np.core.records.fromarrays(tmp.transpose(),dtype=[('eta','f8'),('pix','f8'),('piy','f8')])
            #pdb.set_trace()
            tmp = np.sort(tmp1,axis=0)
            zz_mot0[ii,:] = tmp["pix"]
            zz_mot1[ii,:] = tmp["piy"]
            
            tmp = shift_data[fn.split("/")[-1]+'pixels'].value
            tmp1 = np.core.records.fromarrays(tmp.transpose(),dtype=[('eta','f8'),('x','f8'),('y','f8')])
            #pdb.set_trace()
            tmp = np.sort(tmp1,axis=0)
            zz_shift0[ii,:] = tmp["x"]
            zz_shift1[ii,:] = tmp["y"]
            
    with h5.File(path+outfn) as outf:
        outf["axis0"] = xx
        outf["axis1"] = yy   
        outf["axis0_name"] = axis0name
        outf["axis1_name"] = axis1name
        outf["mot0"] = zz_mot0
        outf["mot1"] = zz_mot1
        outf["mot0_name"] = mot0name
        outf["mot1_name"] = mot1name
        outf["shift0"] = zz_shift0
        outf["shift1"] = zz_shift1
    
def get_value_from_LUT(fn="LUT.h5", pos=np.array([0,0])):
    """
    get a value from the LUT
    """
    with h5.File(fn) as outf:
        #mot0shift = interpolate.interp2d(outf["axis0"].value, outf["axis1"].value, outf["mot0"].value, kind='linear')
        #mot1shift = interpolate.interp2d(outf["axis0"], outf["axis1"], outf["mot0"], kind='linear')
        mot0shift = interpolate.RectBivariateSpline(outf["axis1"].value[:,0],outf["axis0"].value[0,:], outf["mot0"].value)
        mot1shift = interpolate.RectBivariateSpline(outf["axis1"].value[:,0],outf["axis0"].value[0,:], outf["mot1"].value)
    return np.array([mot0shift(pos[0],pos[1])[0,0], mot1shift(pos[0],pos[1])[0,0]])
    
def get_relative_shift(fn="LUT.h5", pos_start = np.array([0,0]), pos_end = np.array([0,0])):
    """
    get the relative shift between two positions
    """
    start = get_value_from_LUT(fn,pos_start)
    end = get_value_from_LUT(fn,pos_end)
    #print(start,end)
    delta_pos = end-start
    #print(delta_pos)
    return delta_pos

def generate_LUT_old(path, prefix, outfn_calib, refImage = None,mot0name="pix", mot1name="piy", axis0name="eta", axis0=np.arange(-30.0,120.01,.5), axis1name="phi", axis1=np.arange(-95.0,95.01,5)):
    """
    Generate a 2D look up table 
    """    
    dir_names = os.listdir(path)
    xx, yy = np.meshgrid(axis0, axis1) 
    zz_mot0 = np.zeros(xx.shape)
    zz_mot1 = np.zeros(xx.shape)

    # find reference image
    for name in dir_names:
        if name.startswith(prefix):
                fn=name+"/"+name+".h5"
                print( name, fn)
                if refImage == None and fn.count("_phi_000")>0:
                    print(fn)
                    with h5.File(path+fn,'r') as tmpfile:
                        refImg = tmpfile["0.000"].value  
                          
    for ii,axis1_pos in enumerate(axis1):
        # find filename
        for name in dir_names:
            if name.startswith(prefix):
                if int(name.split("_phi_")[-1])==int(axis1_pos):
                    fn=name+"/"+name+".h5"
                    print(axis1_pos, name, fn)
                    
        # extract the data
        with h5.File(path+fn.split(".h5")[0]+".h5",'r') as raw_images:
            print(fn.split("/")[-1])
            for jj,axis0_pos in enumerate(axis0):
                tmpImg = raw_images["%.3f"%axis0_pos].value 
                rotatedImg = scipy.ndimage.rotate(tmpImg, -axis1_pos, reshape=False)  #(-phi)
                rotatedImg=rotateImage(tmpImg,-axis1_pos,[284,262])#[284-170:284+170,262-170:262+170]
                try:
                    offset = image_offset(refImg[284-170:284+170,262-170:262+170],rotatedImg[284-170:284+170,262-170:262+170])#[70:-70,130:-130],rotatedImg[70:-70,130:-130])
                    # calculate offset in microns
                    with h5.File(path+outfn_calib,'r') as calib:
                        shifts = calc_coords(offset,calib["mot1_calib"].value, calib["mot2_calib"].value)
                    print(axis0_pos,axis1_pos,shifts)
                    zz_mot0[ii,jj] = shifts[0]
                    zz_mot1[ii,jj] = shifts[1]  
                except:
                    print(axis0_pos,axis1_pos,"failed")
                    #import pylab as pl
                    #pl.figure(1)
                    #pl.imshow(refImg[284-170:284+170,262-170:262+170])#[70:-70,130:-130])
                    #pl.figure(2)
                    #pl.imshow(rotatedImg[284-170:284+170,262-170:262+170])#[70:-70,130:-130])
                    #pl.show()
                
    with h5.File(path+"finalLUT1.h5") as outf:
        outf["axis0"] = xx
        outf["axis1"] = yy   
        outf["axis0_name"] = axis0name
        outf["axis1_name"] = axis1name
        outf["mot0"] = zz_mot0
        outf["mot1"] = zz_mot1
        outf["mot0_name"] = mot0name
        outf["mot1_name"] = mot1name
        
def rotateImage(img, angle, pivot):
    padX = [img.shape[1] - pivot[0], pivot[0]]
    padY = [img.shape[0] - pivot[1], pivot[1]]
    imgP = np.pad(img, [padY, padX], 'constant')
    imgR = scipy.ndimage.rotate(imgP, angle, reshape=False)
    return imgR[padY[0] : -padY[1], padX[0] : -padX[1]]

def rotate(x,y,xo,yo,theta): #rotate x,y around xo,yo by theta (rad)
    xr = math.cos(theta)*(x-xo)-math.sin(theta)*(y-yo) + xo
    yr = math.sin(theta)*(x-xo)+math.cos(theta)*(y-yo) + yo
    return [xr,yr]
    
def makeGif(h5pathindex="20190123_160530_phi_-95.h5", h5fn="20190123_160530_phi_-95_results20190208_0.h5", h5pathimstack = "20190123_160530_phi_-95.h5_shift", fps=10,scale=1.0,greyscale=True):
    """
    make a gif of a stack of images
    """
    # get right indices        
    with h5.File(h5fn, 'r') as data:
        index = data[h5pathindex].value[:,0].tolist()
        sortedindex = np.sort(index.copy())
        dataarray = data[h5pathimstack].value
        array = np.zeros(dataarray.shape)
        for ii, indexValue in enumerate(sortedindex):
            array[ii,:,:] = dataarray[index.index(indexValue),:,:]
    #generate the gif
    if array.ndim == 3:
        tmparray = array[..., np.newaxis] * np.ones(3) 
        if not greyscale:
            #correct with luminosity method for RGB           
            tmparray[:,:,:,2]= tmparray[:,:,:,0]/3.0*0.11
            tmparray[:,:,:,1]= tmparray[:,:,:,0]/3.0*0.59 
            tmparray[:,:,:,0]= tmparray[:,:,:,0]/3.0*0.3 
        
    array = tmparray    
    clip = ImageSequenceClip(list(array), fps=fps).resize(scale)
    clip.write_gif("TEST.gif", fps=fps)
    return clip

def find_outlier_pixels(data,tolerance=.25,worry_about_edges=True):
    #This function finds the hot or dead pixels in a 2D dataset. 
    #tolerance is the number of standard deviations used to cutoff the hot pixels
    #If you want to ignore the edges and greatly speed up the code, then set
    #worry_about_edges to False.
    #
    #The function returns a list of hot pixels and also an image with with hot pixels removed

    from scipy.ndimage import median_filter
    blurred = median_filter(Z, size=3)
    difference = data - blurred
    threshold = np.std(difference)

    #find the hot pixels, but ignore the edges
    hot_pixels = np.nonzero((np.abs(difference[1:-1,1:-1])>threshold) )
    hot_pixels = np.array(hot_pixels) + 1 #because we ignored the first row and first column

    fixed_image = np.copy(data) #This is the image with the hot pixels removed
    for y,x in zip(hot_pixels[0],hot_pixels[1]):
        fixed_image[y,x]=blurred[y,x]

    if worry_about_edges == True:
        height,width = np.shape(data)

        ###Now get the pixels on the edges (but not the corners)###

        #left and right sides
        for index in range(1,height-1):
            #left side:
            med  = np.median(data[index-1:index+2,0:2])
            diff = np.abs(data[index,0] - med)
            if diff>threshold: 
                hot_pixels = np.hstack(( hot_pixels, [[index],[0]]  ))
                fixed_image[index,0] = med

            #right side:
            med  = np.median(data[index-1:index+2,-2:])
            diff = np.abs(data[index,-1] - med)
            if diff>threshold: 
                hot_pixels = np.hstack(( hot_pixels, [[index],[width-1]]  ))
                fixed_image[index,-1] = med

        #Then the top and bottom
        for index in range(1,width-1):
            #bottom:
            med  = np.median(data[0:2,index-1:index+2])
            diff = np.abs(data[0,index] - med)
            if diff>threshold: 
                hot_pixels = np.hstack(( hot_pixels, [[0],[index]]  ))
                fixed_image[0,index] = med

            #top:
            med  = np.median(data[-2:,index-1:index+2])
            diff = np.abs(data[-1,index] - med)
            if diff>threshold: 
                hot_pixels = np.hstack(( hot_pixels, [[height-1],[index]]  ))
                fixed_image[-1,index] = med

        ###Then the corners###

        #bottom left
        med  = np.median(data[0:2,0:2])
        diff = np.abs(data[0,0] - med)
        if diff>threshold: 
            hot_pixels = np.hstack(( hot_pixels, [[0],[0]]  ))
            fixed_image[0,0] = med

        #bottom right
        med  = np.median(data[0:2,-2:])
        diff = np.abs(data[0,-1] - med)
        if diff>threshold: 
            hot_pixels = np.hstack(( hot_pixels, [[0],[width-1]]  ))
            fixed_image[0,-1] = med

        #top left
        med  = np.median(data[-2:,0:2])
        diff = np.abs(data[-1,0] - med)
        if diff>threshold: 
            hot_pixels = np.hstack(( hot_pixels, [[height-1],[0]]  ))
            fixed_image[-1,0] = med

        #top right
        med  = np.median(data[-2:,-2:])
        diff = np.abs(data[-1,-1] - med)
        if diff>threshold: 
            hot_pixels = np.hstack(( hot_pixels, [[height-1],[width-1]]  ))
            fixed_image[-1,-1] = med

    return hot_pixels,fixed_image



def optimiseFocusMicroscope(motObj,url = "http://vidid011.esrf.fr/jpg/1/image.jpg", navg=1,model = None, enhance = True, roi=None):
    """
    optimise the sample position (motObj) relative to the focal plane of some microscope image
    url = url of image
    navg = number of images to average - in case of noise
    model = "diff" (default), "msd", "gradient"
    enhance = boost image contrast
    roi = image ROI (0,446,0,704) or "manual to deinfe your own"
    """
    
    # TODO add ROI selector or beam defined pixel position and deltas from this
    # TODO remove gradients from images
    
    if roi == "manual":
        # make 
        im = image.url2array(url, navg=navg)
        plt.figure()
        plt.imshow(im)
        # TODO add interation to return ROI
        plt.show()
    
    if model not in image._models:
        model = image._models[0]
    
    im = image.url2array(url, navg=navg)
    im = im[60:-70]
    print("start position: ", motObj.position, ", constrast: ",image.contrast(im, model), "residual: ", 1/image.contrast(im, model))
    
    ### do the autofocus
    if roi is None:
        roi = tuple([160,-170]) + tuple([20,504])  # 60:-70 removes the text from microscope image
    af = microscope.AutoFocus(url,motObj,enhance=enhance,roi=roi)
    af.url = url
    af.roi = roi

    print("Starting autofocus...")
    try:

        fit = af.focus() ### Do the focusing
        print("Done. Status: %s"%fit.message)
        print("New Position: %s=%f"%(af.motor.name,fit.x.item()))
    except Exception as emsg:
        print("Error: %s"%emsg)

def find_microscope_focus(shexaAxisZ,stroke_shexaAxisZ=.4):
    """
    find the focus but limit the motor to prevent overrun expecially with hexapod - find other parts of the optics..
    """
    limits_Z = shexaAxisZ.limits
    start_Zpos = shexaAxisZ.position
    shexaAxisZ.limits = (shexaAxisZ.position-stroke_shexaAxisZ/2.,shexaAxisZ.position+stroke_shexaAxisZ/2.)
    optimiseFocusMicroscope(shexaAxisZ,navg=10,enhance=True,roi=(100,460,100,600))
    shexaAxisZ.limits = limits_Z
    deltaZ = start_Zpos-shexaAxisZ.position
    return shexaAxisZ.position, deltaZ

def makeSampleFlat(shexaAxisX, shexaAxisY,shexaAxisZ,shexaAxisrotX,shexaAxisrotY,stroke_shexaAxisZ=1,stroke=4):
    """
    make the sample flat in the light microscope
    naturally this could be faster when we have the long stroke PI.
    """
    limits_Z = shexaAxisZ.limits
    start_Zpos = shexaAxisZ.position
    shexaAxisZ.limits = (shexaAxisZ.position-stroke_shexaAxisZ/2.,shexaAxisZ.position+stroke_shexaAxisZ/2.)
    optimiseFocusMicroscope(shexaAxisZ,navg=10,enhance=True,roi=(100,460,100,600))
    umvr(shexaAxisX,-stroke/2.)
    optimiseFocusMicroscope(shexaAxisZ,navg=10,enhance=True,roi=(100,460,100,600))
    loc_minus = shexaAxisZ.position
    umvr(shexaAxisX,stroke,shexaAxisZ,(start_Zpos-loc_minus)*2)
    optimiseFocusMicroscope(shexaAxisZ,navg=10,enhance=True,roi=(100,460,100,600))
    loc_plus = shexaAxisZ.position
    umvr(shexaAxisX,-stroke/2.)
    shexaAxisZ.limits=limits_Z
    
    rotY_ang_deg = np.rad2deg(np.arctan((loc_plus-loc_minus)/stroke))

    # to make the sample flat 
    umvr(shexaAxisrotY,rotY_ang_deg)
    shexaAxisZ.limits = (shexaAxisZ.position-stroke_shexaAxisZ/2.,shexaAxisZ.position+stroke_shexaAxisZ/2.)
    optimiseFocusMicroscope(shexaAxisZ,navg=10,enhance=True,roi=(100,460,100,600))
    start_Zpos = shexaAxisZ.position

    umvr(shexaAxisY,-stroke/2.)
    optimiseFocusMicroscope(shexaAxisZ,navg=10,enhance=True,roi=(100,460,100,600))
    loc_minus = shexaAxisZ.position
    umvr(shexaAxisY,stroke,shexaAxisZ,(start_Zpos-loc_minus)*2)
    optimiseFocusMicroscope(shexaAxisZ,navg=10,enhance=True,roi=(100,460,100,600))
    loc_plus = shexaAxisZ.position
    umvr(shexaAxisY,-stroke/2.)
    
    rotX_ang_deg = np.rad2deg(np.arctan((loc_plus-loc_minus)/stroke))*-1

    # to make the sample flat 
    umvr(shexaAxisrotX,rotX_ang_deg)

    optimiseFocusMicroscope(shexaAxisZ,navg=10,enhance=True,roi=(100,460,100,600))
    shexaAxisZ.limits = limits_Z

def shiftBetweenImages(image0,image1,upsample_factor=1000):
    """
    return shift between images
    """


    cc, error, diffphase = phase_cross_correlation( image0, 
                                                    image1, 
                                                    upsample_factor = upsample_factor)

    return cc, error, diffphase

def get_microscope_image( url = "http://vidid011.esrf.fr/jpg/1/image.jpg", navg=10, enhance=True):
    """
    return a microscope image from a URL
    """
    im = image.url2array(url, navg=navg)
    
    #if enhance:
    #    self._stretch = 5., 95.
    #    im = image.stretch_contrast(im, *self._stretch)
        
    return im

def calibrate_microscope(axisX,axisY,stroke=0.1,unit=1e-3):
    """
    pixels per unit for camview, typically max 100 micron stroke.
    coordinate system described as 0,0 from bottom left of image
    """
    image0 = get_microscope_image()[100:460,100:600]
    umvr(axisX,stroke)
    image1 = get_microscope_image()[100:460,100:600]
    ccx, errorx, diffphasex = shiftBetweenImages(image0,image1)
    umvr(axisX,-stroke)
    image0 = get_microscope_image()[100:460,100:600]
    umvr(axisY,stroke)
    image1 = get_microscope_image()[100:460,100:600]
    ccy, errory, diffphasey = shiftBetweenImages(image1,image0)
    umvr(axisY,-stroke)
    calibMicroX = ccx/(stroke/unit)
    calibMicroY = ccy/(stroke/unit)
    return calibMicroX, calibMicroY

def camview_beaconObj():
    """
    save all the relevant data here -  camview can interact with it and keep ROIs COR etc...
    """
    pass

def microscope_shift_on_axis_move(axis, stroke=0.1):
    """
    locate the center of rotation of the hexapod 
    """

    image0 = get_microscope_image()[100:460,100:600]
    umvr(axis,stroke)
    image1 = get_microscope_image()[100:460,100:600]
    cc, error, diffphase = shiftBetweenImages(image0,image1)
    umvr(axis,-stroke)
    return cc

def microscope_focus_on_axis_move(axis_move,axis_focus, stroke_move= 0.1,stroke_focus=0.1):
    """
    locate the center of rotation of the hexapod 
    """

    posZ0, deltaZ0 = find_microscope_focus(axis_focus,stroke_shexaAxisZ = stroke_focus)
    umvr(axis_move,stroke_move)
    posZ1, deltaZ1 = find_microscope_focus(axis_focus,stroke_shexaAxisZ = stroke_focus)
   
    umvr(axis_move,-stroke_move)
    return posZ1, deltaZ1

def shexa_objCoord_vs_observed_shift_Xmicroscope_hexaZ(shexaAxisrotX,strokeX, plot = True, stroke = 8, step =1):
    """
    we are looking for a linear relationship so only need to probe a few points to get a good fit

    typically +-2mm with 8 points should get close
    """
    ccX_list0 = []
    ccX_list1 = []
    user_pose, obj_pose = shexaAxisrotX.controller.get_origin()


    hexapod_otz = np.arange(obj_pose.tz-stroke/2.,obj_pose.tz+stroke/2.,step)
    for new_tz in hexapod_otz:
        shexa_set_pose(shexaAxisrotX.controller,
                       user_pose.tx,
                       user_pose.ty,
                       user_pose.tz,
                       user_pose.rx,
                       user_pose.ry,
                       user_pose.rz,
                       obj_pose.tx,
                       obj_pose.ty,
                       new_tz,
                       obj_pose.rx,
                       obj_pose.ry,
                       obj_pose.rz)
        
        cc = microscope_shift_on_axis_move(shexaAxisrotX,strokeX)
        ccX0,ccX1 = cc[0],cc[1]
        ccX_list0.append(ccX0)
        ccX_list1.append(ccX1)

    if plot:
        intersect = plot_shexa_objCoord_vs_observed_shift(hexapod_otz,ccX_list1,shexaAxisrotX)
        print(f"shexa_set_pose({shexaAxisrotX.controller.name},{user_pose.tx},{user_pose.ty},{user_pose.tz},{user_pose.rx},{user_pose.ry},{user_pose.rz},{obj_pose.tx},{obj_pose.ty},{intersect},{obj_pose.rx},{obj_pose.ry},{obj_pose.rz})")
        shexa_set_pose(shexaAxisrotX.controller,
                       user_pose.tx,
                       user_pose.ty,
                       user_pose.tz,
                       user_pose.rx,user_pose.ry,
                       user_pose.rz,
                       obj_pose.tx,
                       obj_pose.ty,
                       intersect,
                       obj_pose.rx,
                       obj_pose.ry,
                       obj_pose.rz)

    return hexapod_otz,ccX_list0,ccX_list1, shexaAxisrotX

def shexa_objCoord_vs_observed_shift_Ymicroscope_hexaZ(shexaAxisrotX,strokeX, plot = True, stroke = 8, step =1):
    """
    we are looking for a linear relationship so only need to probe a few points to get a good fit

    typically +-2mm with 8 points should get close
    """
    ccX_list0 = []
    ccX_list1 = []
    user_pose, obj_pose = shexaAxisrotX.controller.get_origin()


    hexapod_otz = np.arange(obj_pose.tz-stroke/2.,obj_pose.tz+stroke/2.,step)
    for new_tz in hexapod_otz:
        shexa_set_pose(shexaAxisrotX.controller,
                       user_pose.tx,
                       user_pose.ty,
                       user_pose.tz,
                       user_pose.rx,
                       user_pose.ry,
                       user_pose.rz,
                       obj_pose.tx,
                       obj_pose.ty,
                       new_tz,
                       obj_pose.rx,
                       obj_pose.ry,
                       obj_pose.rz)
        cc = microscope_shift_on_axis_move(shexaAxisrotX,strokeX)
        ccX0,ccX1 = cc[0],cc[1]
        ccX_list0.append(ccX0)
        ccX_list1.append(ccX1)

    if plot:
        intersect = plot_shexa_objCoord_vs_observed_shift(hexapod_otz,ccX_list0,shexaAxisrotX)
        print(f"shexa_set_pose({shexaAxisrotX.controller.name},{user_pose.tx},{user_pose.ty},{user_pose.tz},{user_pose.rx},{user_pose.ry},{user_pose.rz},{obj_pose.tx},{obj_pose.ty},{intersect},{obj_pose.rx},{obj_pose.ry},{obj_pose.rz})")
        shexa_set_pose(shexaAxisrotX.controller,
                       user_pose.tx,
                       user_pose.ty,
                       user_pose.tz,
                       user_pose.rx,
                       user_pose.ry,
                       user_pose.rz,
                       obj_pose.tx,
                       obj_pose.ty,
                       intersect,
                       obj_pose.rx,
                       obj_pose.ry,
                       obj_pose.rz)

    return hexapod_otz,ccX_list0,ccX_list1, shexaAxisrotX


def plot_shexa_objCoord_vs_observed_shift(hexapod_ot,cc_list,shexaAxisrotX,hexapod_ot_name="otz"):
    plt.figure(figsize=(8,4))
    plt.plot(hexapod_ot,cc_list,label="observed shift")
    m,b = np.polyfit(hexapod_ot,cc_list,1)
    plt.plot(hexapod_ot,m*hexapod_ot+b,"--",label = f"fit ({m}*x+{b})")
    plt.ylabel("shift (um)")
    plt.xlabel("hexapod reference frame 'otz' (mm)")
    plt.title(f"{shexaAxisrotX.name}")
    plt.vlines(-b/m,plt.ylim()[0],plt.ylim()[1],linestyle="--",color="g",label = f"COR on surface ({-b/m})") 
    plt.legend()
    plt.show()
    return -b/m


def shexa_objCoord_vs_observed_shift_Xmicroscope_hexaY(shexaAxisrotX,shexaAxisZ,strokerotX = 1,strokeZ = 0.4, plot = True, stroke = 4, step =1):
    """
    we are looking for a linear relationship so only need to probe a few points to get a good fit

    typically +-2mm with 8 points should get close
    """
    pos = []
    delta = []
    user_pose, obj_pose = shexaAxisrotX.controller.get_origin()


    hexapod_oty = np.arange(obj_pose.ty-stroke/2.,obj_pose.ty+stroke/2.,step)
    for new_ty in hexapod_oty:
        #breakpoint()
        #print(shexaAxisrotX.controller.axes['tobx'].position,
        #      shexaAxisrotX.controller.axes['toby'].position,
        #      shexaAxisrotX.controller.axes['tobz'].position,
        #      shexaAxisrotX.controller.axes['robx'].position,
        #      shexaAxisrotX.controller.axes['roby'].position,
        #      shexaAxisrotX.controller.axes['robz'].position)
        shexa_set_pose(shexaAxisrotX.controller,
                       user_pose.tx,
                       user_pose.ty,
                       user_pose.tz,
                       user_pose.rx,
                       user_pose.ry,
                       user_pose.rz,
                       obj_pose.tx,
                       new_ty,
                       obj_pose.tz,
                       obj_pose.rx,
                       obj_pose.ry,
                       obj_pose.rz)
        
        print(shexaAxisrotX.controller.get_origin())
        #posZ, deltaZ = find_microscope_focus(shexaAxisZ,stroke_shexaAxisZ = strokeZ)
        posZ, deltaZ = microscope_focus_on_axis_move(shexaAxisrotX, shexaAxisZ , stroke_move = strokerotX, stroke_focus=strokeZ)
        pos.append(posZ)
        delta.append(deltaZ)

    #breakpoint()
    if plot:
        intersect = plot_shexa_objCoord_vs_observed_shift(hexapod_oty,delta,shexaAxisrotX,"oty")
        print(f"shexa_set_pose({shexaAxisrotX.controller.name},{user_pose.tx},{user_pose.ty},{user_pose.tz},{user_pose.rx},{user_pose.ry},{user_pose.rz},{obj_pose.tx},{intersect},{obj_pose.tz},{obj_pose.rx},{obj_pose.ry},{obj_pose.rz})")

        shexa_set_pose(shexaAxisrotX.controller,
                       user_pose.tx,
                       user_pose.ty,
                       user_pose.tz,
                       user_pose.rx,
                       user_pose.ry,
                       user_pose.rz,
                       obj_pose.tx,
                       intersect,
                       obj_pose.tz,
                       obj_pose.rx,
                       obj_pose.ry,
                       obj_pose.rz)

    return hexapod_oty,delta, shexaAxisrotX


def shexa_objCoord_vs_observed_shift_Ymicroscope_hexaX(shexaAxisrotX,shexaAxisZ,strokerotX = 1,strokeZ = 0.4, plot = True, stroke = 4, step =1):
    """
    we are looking for a linear relationship so only need to probe a few points to get a good fit

    typically +-2mm with 8 points should get close
    """
    pos = []
    delta = []
    user_pose, obj_pose = shexaAxisrotX.controller.get_origin()


    hexapod_otx = np.arange(obj_pose.ty-stroke/2.,obj_pose.ty+stroke/2.,step)
    for new_tx in hexapod_otx:
        #breakpoint()
        #print(shexaAxisrotX.controller.axes['tobx'].position,
        #      shexaAxisrotX.controller.axes['toby'].position,
        #      shexaAxisrotX.controller.axes['tobz'].position,
        #      shexaAxisrotX.controller.axes['robx'].position,
        #      shexaAxisrotX.controller.axes['roby'].position,
        #      shexaAxisrotX.controller.axes['robz'].position)
        shexa_set_pose(shexaAxisrotX.controller,
                       user_pose.tx,
                       user_pose.ty,
                       user_pose.tz,
                       user_pose.rx,
                       user_pose.ry,
                       user_pose.rz,
                       new_tx,
                       obj_pose.ty,
                       obj_pose.tz,
                       obj_pose.rx,
                       obj_pose.ry,
                       obj_pose.rz)
        
        print(shexaAxisrotX.controller.get_origin())
        #posZ, deltaZ = find_microscope_focus(shexaAxisZ,stroke_shexaAxisZ = strokeZ)
        posZ, deltaZ = microscope_focus_on_axis_move(shexaAxisrotX, shexaAxisZ , stroke_move = strokerotX, stroke_focus=strokeZ)
        pos.append(posZ)
        delta.append(deltaZ)

    if plot:
        intersect = plot_shexa_objCoord_vs_observed_shift(hexapod_otx,delta,shexaAxisrotX,"otx")
        print(f"shexa_set_pose({shexaAxisrotX.controller.name},{user_pose.tx},{user_pose.ty},{user_pose.tz},{user_pose.rx},{user_pose.ry},{user_pose.rz},{intersect},{obj_pose.ty},{obj_pose.tz},{obj_pose.rx},{obj_pose.ry},{obj_pose.rz})")

        shexa_set_pose(shexaAxisrotX.controller,
                       user_pose.tx,
                       user_pose.ty,
                       user_pose.tz,
                       user_pose.rx,
                       user_pose.ry,
                       user_pose.rz,
                       intersect,
                       obj_pose.ty,
                       obj_pose.tz,
                       obj_pose.rx,
                       obj_pose.ry,
                       obj_pose.rz)

    return hexapod_otx,delta, shexaAxisrotX


def find_shexaCOR(shexaAxisrotX,calibMicroX,shexaAxisrotY, calibMicroY, strokeX=0.1,strokeY=0.1):
    """
    locate the center of rotation of the hexapod 
    ths doesn't work we get very strange values for the distance to the COR
    """

    #breakpoint()
    image0 = get_microscope_image()[100:460,100:600]
    umvr(shexaAxisrotX,strokeX)
    image1 = get_microscope_image()[100:460,100:600]
    cc, error, diffphase = shiftBetweenImages(image0,image1)

    # get microns per pixel 
    #TODO make microscope object with various calibration parameters
    # convert shift to microns
    shift_Y = (cc/calibMicroY)[1]
    
    dist2cor = shift_Y/np.tan(np.deg2rad(strokeX))

    print(dist2cor)



    # check signs on the above

    # check the above works for both X and Y axes

    image0 = get_microscope_image()[100:460,100:600]
    umvr(shexaAxisrotY,strokeY)
    image1 = get_microscope_image()[100:460,100:600]
    cc, error, diffphase = shiftBetweenImages(image1,image0)

    # get microns per pixel 
    #TODO make microscope object with various calibration parameters
    # convert shift to microns
    shift_X = (cc/calibMicroX)[1]
    
    dist2cor = shift_X/np.tan(np.deg2rad(strokeY))

    print(dist2cor)


    # define the hexapod coordinate system and check with the macro written for sample flattening


