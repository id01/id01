# -*- coding: utf-8 -*-

# ID01 BLISS git repo
#       authors:
#         Steven Leake, leake@esrf.fr
#         modified from microscope focus function by Carsten Richter
#       Created at: 20. May 09:00:00 CET 2021
#
#----------------------------------------------------------------------
# Description: 
#    
#
#
"""
    Originally these were some functions that were used by CamView, but
    they can be useful for other routines and scripts 
           >> hence we bring them to BLISS, thanks Carsten!
"""

from builtins import breakpoint
import time
import platform
from scipy import ndimage
import numpy as np


import id01.scripts.image as image
from bliss import setup_globals
from bliss.shell.standard import mv, mvr, umvr
from bliss.common.scans import sct
import pdb
import matplotlib.pyplot as plt


class AutoFocus(object):
    """
        A class providing auto-focus functionality based on
        counters and an interface to BLISS
        that allows to move a motor for focusing.

        At the moment, the optimization of focus is done
        via bounded univariate scalar function minization where
        the function returns the sharpness of the image 
        (see function `contrast`).

        As images carry noise, it would be better to use an
        optimizer for noisy functions (noisyopt?).
    """
    def __init__(self, motor=None,
                       counter=None,
                       limits=None,
                       roi=None,
                       navg=1,
                       enhance=False,
                       detObj=None,
                       exposure=None):
        #self._specclient = SpecClientWrapper.SpecClientSession()
        if motor is None: # default piz
            self.motor = setup_globals.piz
        else:
            self.motor = motor
        self.counter = counter
        if limits == None:
            self.limits = self.motor.limits
        else:
            self.limits = limits
        self.roi = roi
        self.navg = navg
        self.stretch = enhance
        self.contrast = "diff"
        

    ###### MOTOR
    @property
    def motor(self):
        return self._motor
    @motor.setter
    def motor(self, motObj):
        self._motor = motObj
    
    ###### LIMITS
    @property
    def limits(self):
        """ Limits of the auto focusing motor """
        return self._ll, self._ul
    @limits.setter
    def limits(self, val):
        if val is None:
            self._ll = -np.inf
            self._ul =  np.inf
        else:
            val = np.array(val, dtype=float, ndmin=1)
            assert len(val)==2, "Need 2 scalar values: upper and lower limit"
            self._ll = val.min()
            self._ul = val.max()
    @limits.deleter
    def limits(self):
        self._ll = -np.inf
        self._ul =  np.inf

    ###### ROI
    @property
    def roi(self):
        """
            Defines the region of interest on the picture used to evaluate the
            contrast:
                (dim0_min, dim0_max, dim1_min, dim1_max)
        """
        return (self._slice_0.start,
                self._slice_0.stop,
                self._slice_1.start,
                self._slice_1.stop)
    @roi.setter
    def roi(self, val):
        if val is None:
            self._slice_0 = slice(None,None)
            self._slice_1 = slice(None,None)
        else:
            #val = np.array(val, dtype=int)
            self._slice_0 = slice(*val[0:2])#slice(*np.sort(val[0:2]))
            self._slice_1 = slice(*val[2:4])#slice(*np.sort(val[2:4]))
    @roi.deleter
    def roi(self):
        self._slice_0 = slice(None,None)
        self._slice_1 = slice(None,None)

    ###### NAVG
    @property
    def navg(self):
        """ Number of images to average """
        return self._navg
    @navg.setter
    def navg(self, val):
        self._navg = int(val)

    ###### STRETCH
    @property
    def stretch(self):
        """
            Percentiles to stretch contrast:
                between 0 and 100, or True/False
        """
        return self._stretch
    @stretch.setter
    def stretch(self, val):
        if val is True:
            self._stretch = 5., 95.
        elif val is False:
            self._stretch = False
        else:
            val = np.array(val, dtype=float, ndmin=1)
            assert len(val)==2, ("Need 2 scalar values: upper and lower "
                                 "percentile")
            self._stretch = val.min(), val.max()

    ###### CONTRAST
    @property
    def contrast(self):
        """
            Name of model for contrast evaluation:
                One of: ["diff", "msd", "gradient"]
        """
        return self._contrast
    @contrast.setter
    def contrast(self, val):
        if not val in image._models:
            #print("Problem")
            raise ValueError("Contrast model needs to be one of [%s]"
                             %", ".join(image._models))
        self._contrast = val

    def focus(self, navg=None, stretch=None, contrast=None, **leastsq_kw):
        """
            do the actual focusing
        """
        if not hasattr(self, "_optimize"):
            self._optimize = __import__('scipy.optimize',
                                       globals(),
                                       locals(),
                                       ['leastsq'])

        if not navg is None:
            self.navg = navg

        if not stretch is None:
            self.stretch = stretch

        if not contrast is None:
            self.contrast = contrast

        startval = self.get_motor_pos()

#        kw = dict(full_output=True,
#                  ftol=1e-5,
#                  xtol=1e-3,
#                  maxfev=0,
#                  factor=5.)
#        kw.update(leastsq_kw)
#
#        self.result = self._optimize.leastsq(self._costfunction, startval, **kw)

        kw = dict(bracket=None,
                  bounds=self.limits,
                  method='Bounded',
                  tol=1e-3, options=None)
        kw.update(leastsq_kw)

        self.result = self._optimize.minimize_scalar(self._costfunction, **kw)
#        kw = dict(bracket=None,
#                  bounds=self.limits,
#                  method='L-BFGS-B',
#                  tol=1e-3, options=None)
#        kw.update(leastsq_kw)
#
#        self.result = self._optimize.minimize_scalar(self._costfunction)
        return self.result

    def get_motor_pos(self):
        return self.motor.position

    def movemotor(self, position):
        if position > self._ll and position < self._ul:
            mv(self.motor,position)
        else:
            raise ValueError("Setpoint hits limits: %f"%position)


    def _costfunction(self, newpos=None):
        """
            return a value proportional to the inverse
            sharpness -- the function which ought to be minimized
        """
        if newpos is not None:
            self.movemotor(newpos)
        # counter value
        self.get_sct_images()
        counter_val = self.scan_obj.get_data(self.counter)
        residual = 1./counter_val
        print("Value: %f"%residual)
        return residual


    # move all scripts below to a new class that inherits from this one to keep it clean
    def get_sct_images(self, exposure = .1):
        """
        return an array from the desired detector ct
        """
        if exposure != 1:
            self.exposure = exposure
        else:
            self.exposure = 1
            
        scan_obj = sct(exposure,  run=False)  # use activeMG instead of self.det_obj,  
        scan_obj.run()
        #image = scan_obj.get_data()['{}:image'.format(self.det_obj.name)].as_array()
        self.scan_obj = scan_obj
        #self.last_image = image
        #return self.last_image

    def focus_ct(self, navg=None, stretch=None, contrast=None, **leastsq_kw):
        """
            do the actual focusing
        """
        if not hasattr(self, "_optimize"):
            self._optimize = __import__('scipy.optimize',
                                       globals(),
                                       locals(),
                                       ['leastsq'])

        if not navg is None:
            self.navg = navg

        if not stretch is None:
            self.stretch = stretch

        if not contrast is None:
            self.contrast = contrast

        startval = self.get_motor_pos()

#        kw = dict(full_output=True,
#                  ftol=1e-5,
#                  xtol=1e-3,
#                  maxfev=0,
#                  factor=5.)
#        kw.update(leastsq_kw)
#
#        self.result = self._optimize.leastsq(self._costfunction, startval, **kw)

        kw = dict(bracket=None,
                  bounds=self.limits,
                  method='Bounded',
                  tol=1e-3, options=None)
        kw.update(leastsq_kw)

        self.result = self._optimize.minimize_scalar(self._costfunction, **kw)
#        kw = dict(bracket=None,
#                  bounds=self.limits,
#                  method='L-BFGS-B',
#                  tol=1e-3, options=None)
#        kw.update(leastsq_kw)
#
#        self.result = self._optimize.minimize_scalar(self._costfunction)
        return self.result



class AutoFocus_light(object):
    """

        Autofucs but without the scipy step
        
        A class providing auto-focus functionality based on
        counters and an interface to BLISS
        that allows to move a motor for focusing.

        At the moment, the optimization of focus is done
        via bounded univariate scalar function minization where
        the function returns the sharpness of the image 
        (see function `contrast`).

        As images carry noise, it would be better to use an
        optimizer for noisy functions (noisyopt?).
    """
    def __init__(self, motor=None,
                       counter=None,
                       limits=None,
                       roi=None,
                       navg=1,
                       enhance=False,
                       detObj=None,
                       exposure=None):
        #self._specclient = SpecClientWrapper.SpecClientSession()
        if motor is None: # default piz
            self.motor = setup_globals.piz
        else:
            self.motor = motor
        self.counter = counter
        if limits == None:
            self.limits = self.motor.limits
        else:
            self.limits = limits
        self.roi = roi
        self.navg = navg
        self.stretch = enhance
        self.contrast = "diff"
        

    ###### MOTOR
    @property
    def motor(self):
        return self._motor
    @motor.setter
    def motor(self, motObj):
        self._motor = motObj
    
    ###### LIMITS
    @property
    def limits(self):
        """ Limits of the auto focusing motor """
        return self._ll, self._ul
    @limits.setter
    def limits(self, val):
        if val is None:
            self._ll = -np.inf
            self._ul =  np.inf
        else:
            val = np.array(val, dtype=float, ndmin=1)
            assert len(val)==2, "Need 2 scalar values: upper and lower limit"
            self._ll = val.min()
            self._ul = val.max()
    @limits.deleter
    def limits(self):
        self._ll = -np.inf
        self._ul =  np.inf

    ###### ROI
    @property
    def roi(self):
        """
            Defines the region of interest on the picture used to evaluate the
            contrast:
                (dim0_min, dim0_max, dim1_min, dim1_max)
        """
        return (self._slice_0.start,
                self._slice_0.stop,
                self._slice_1.start,
                self._slice_1.stop)
    @roi.setter
    def roi(self, val):
        if val is None:
            self._slice_0 = slice(None,None)
            self._slice_1 = slice(None,None)
        else:
            #val = np.array(val, dtype=int)
            self._slice_0 = slice(*val[0:2])#slice(*np.sort(val[0:2]))
            self._slice_1 = slice(*val[2:4])#slice(*np.sort(val[2:4]))
    @roi.deleter
    def roi(self):
        self._slice_0 = slice(None,None)
        self._slice_1 = slice(None,None)

    ###### NAVG
    @property
    def navg(self):
        """ Number of images to average """
        return self._navg
    @navg.setter
    def navg(self, val):
        self._navg = int(val)

    ###### STRETCH
    @property
    def stretch(self):
        """
            Percentiles to stretch contrast:
                between 0 and 100, or True/False
        """
        return self._stretch
    @stretch.setter
    def stretch(self, val):
        if val is True:
            self._stretch = 5., 95.
        elif val is False:
            self._stretch = False
        else:
            val = np.array(val, dtype=float, ndmin=1)
            assert len(val)==2, ("Need 2 scalar values: upper and lower "
                                 "percentile")
            self._stretch = val.min(), val.max()

    ###### CONTRAST
    @property
    def contrast(self):
        """
            Name of model for contrast evaluation:
                One of: ["diff", "msd", "gradient"]
        """
        return self._contrast
    @contrast.setter
    def contrast(self, val):
        if not val in image._models:
            #print("Problem")
            raise ValueError("Contrast model needs to be one of [%s]"
                             %", ".join(image._models))
        self._contrast = val

    def focus(self, navg=None, stretch=None, contrast=None, **leastsq_kw):
        """
            do the actual focusing
        """
        if not hasattr(self, "_optimize"):
            self._optimize = __import__('scipy.optimize',
                                       globals(),
                                       locals(),
                                       ['leastsq'])

        if not navg is None:
            self.navg = navg

        if not stretch is None:
            self.stretch = stretch

        if not contrast is None:
            self.contrast = contrast

        startval = self.get_motor_pos()

#        kw = dict(full_output=True,
#                  ftol=1e-5,
#                  xtol=1e-3,
#                  maxfev=0,
#                  factor=5.)
#        kw.update(leastsq_kw)
#
#        self.result = self._optimize.leastsq(self._costfunction, startval, **kw)

        kw = dict(bracket=None,
                  bounds=self.limits,
                  method='Bounded',
                  tol=1e-3, options=None)
        kw.update(leastsq_kw)

        self.result = self._optimize.minimize_scalar(self._costfunction, **kw)
#        kw = dict(bracket=None,
#                  bounds=self.limits,
#                  method='L-BFGS-B',
#                  tol=1e-3, options=None)
#        kw.update(leastsq_kw)
#
#        self.result = self._optimize.minimize_scalar(self._costfunction)
        return self.result

    def get_motor_pos(self):
        return self.motor.position

    def movemotor(self, position):
        if position > self._ll and position < self._ul:
            mv(self.motor,position)
        else:
            raise ValueError("Setpoint hits limits: %f"%position)


    def _costfunction(self, newpos=None):
        """
            return a value proportional to the inverse
            sharpness -- the function which ought to be minimized
        """
        if newpos is not None:
            self.movemotor(newpos)
        # counter value
        self.get_sct_images()
        counter_val = self.scan_obj.get_data(self.counter)
        residual = 1./counter_val
        print("Value: %f"%residual)
        return residual


    # move all scripts below to a new class that inherits from this one to keep it clean
    def get_sct_images(self, exposure = .1):
        """
        return an array from the desired detector ct
        """
        if exposure != 1:
            self.exposure = exposure
        else:
            self.exposure = 1
            
        scan_obj = sct(exposure,  run=False)  # use activeMG instead of self.det_obj,  
        scan_obj.run()
        #image = scan_obj.get_data()['{}:image'.format(self.det_obj.name)].as_array()
        self.scan_obj = scan_obj
        #self.last_image = image
        #return self.last_image

    def focus_ct(self, navg=None, stretch=None, contrast=None, **leastsq_kw):
        """
            do the actual focusing
        """
        if not hasattr(self, "_optimize"):
            self._optimize = __import__('scipy.optimize',
                                       globals(),
                                       locals(),
                                       ['leastsq'])

        if not navg is None:
            self.navg = navg

        if not stretch is None:
            self.stretch = stretch

        if not contrast is None:
            self.contrast = contrast

        startval = self.get_motor_pos()

#        kw = dict(full_output=True,
#                  ftol=1e-5,
#                  xtol=1e-3,
#                  maxfev=0,
#                  factor=5.)
#        kw.update(leastsq_kw)
#
#        self.result = self._optimize.leastsq(self._costfunction, startval, **kw)

        kw = dict(bracket=None,
                  bounds=self.limits,
                  method='Bounded',
                  tol=1e-3, options=None)
        kw.update(leastsq_kw)

        self.result = self._optimize.minimize_scalar(self._costfunction, **kw)
#        kw = dict(bracket=None,
#                  bounds=self.limits,
#                  method='L-BFGS-B',
#                  tol=1e-3, options=None)
#        kw.update(leastsq_kw)
#
#        self.result = self._optimize.minimize_scalar(self._costfunction)
        return self.result




def chase_the_beam_with_motor(motor,step_size,counter,exposure=0.1):
    """
    check +- a motor step_size with a counter
    """

    scan_obj = sct(exposure,  run=True)
    ref_val = scan_obj.get_data(counter)
    new_val = ref_val+1
    iterNo = 0
    check_negative = True
    check_positive = True

    while check_negative:
        if new_val >= ref_val:
            #get a new value i.e. keep going
            #print("check_nagtive")
            #breakpoint()
            umvr(motor,-step_size)
            scan_obj = sct(exposure,  run=True)
            new_val = scan_obj.get_data(counter)
            iterNo+=1

        else:
            #restores last step
            umvr(motor,step_size)
            check_negative = False
            
    while check_positive:
        if new_val >= ref_val:
            #print("check positive")
            #breakpoint()
            #get a new value i.e. keep going
            umvr(motor,step_size)
            scan_obj = sct(exposure,  run=True)
            new_val = scan_obj.get_data(counter)
            iterNo+=1

        else:
            #restores last step
            umvr(motor,-step_size)
            check_positive = False

def chase_the_beam(motors_list,step_size_list,counter,exposure=0.1):
    """
    infinite alignment of the crystal based on motors and their step size
    # note should be half the beam size
    """
    while True:
        for motor,step_size in zip(motors_list,step_size_list):
            chase_the_beam_with_motor(motor,step_size,counter,exposure=exposure)

from bliss.scanning.chain import ChainPreset
from bliss.common.event import dispatcher

class FollowCountersValue(ChainPreset):
    def __init__(self,*counters):
        self._counters = counters
        self._channels = list()
        self._channel_names = set()
        
    def prepare(self,chain):
        #breakpoint()
        # start of scan
        nodes = set()

        for cnt in self._counters:
            node = list(chain.get_node_from_devices(cnt))
            if node[0] is None:
                continue
            nodes.add(node[0])
            self._channel_names.add(cnt.fullname)

            
        for acq_object in nodes:
            for channel in acq_object.channels:
                if channel.name in self._channel_names:
                    dispatcher.connect(self.new_data_received,"new_data",channel)
                    self._channels.append(channel)

    def stop(self,chain):
        #breakpoint()
        # end of scan
        for channel in self._channels:
            dispatcher.disconnect(self.new_data_received,"new_data",channel)
        self._channels = list()
        self._channel_names = set()

    def new_data_received(self, event_dict=None, signal=None, sender=None):
        channel_data = event_dict.get("data")
        if channel_data is None:
            return
        channel = sender

        print(f"received datas from {channel.fullname} : {channel_data}\n\n\n")

        # logic for alignment

#somescan = dscan(simmot1,0,1,10,0.1,run=False)
#a=FollowCountersValue(counters=[simcam1.counters.steven_avg,simcam1.counters.roi7_sum])
#somescan.acq_chain.add_preset(a)
#somescan.run()


class chaseBeamOnCounter(ChainPreset):
    def __init__(self,*counters):
        self._counters = counters
        self._channels = list()
        self._channel_names = set()
        self.ref_val=0
        
    def prepare(self,chain):
        #breakpoint()
        # start of scan
        nodes = set()

        for cnt in self._counters:
            node = list(chain.get_node_from_devices(cnt))
            if node[0] is None:
                continue
            nodes.add(node[0])
            self._channel_names.add(cnt.fullname)

            
        for acq_object in nodes:
            for channel in acq_object.channels:
                if channel.name in self._channel_names:
                    dispatcher.connect(self.new_data_received,"new_data",channel)
                    self._channels.append(channel)

    def stop(self,chain):
        #breakpoint()
        # end of scan
        for channel in self._channels:
            dispatcher.disconnect(self.new_data_received,"new_data",channel)
        self._channels = list()
        self._channel_names = set()

    def new_data_received(self, event_dict=None, signal=None, sender=None):
        self.channel_data = event_dict.get("data")
        if self.channel_data is None:
            return
        channel = sender

        print(f"received datas from {channel.fullname} : {self.channel_data}\n\n\n")

        # logic for alignment
        self.ref_val = self.channel_data

    def chase_the_beam_with_motor(motor,step_size,counter,exposure=0.1):
        """
        check +- a motor step_size with a specified counter, this requires a motor:counter dict
        """

        new_val = self.channel_data
        iterNo = 0
        check_negative = True
        check_positive = True

        while check_negative:
            if new_val >= self.ref_val:
                #get a new value i.e. keep going
                umvr(motor,-step_size)
                #scan_obj = sct(exposure,  run=True)
                new_val = scan_obj.get_data(counter)
                iterNo+=1

            else:
                #restores last step
                umvr(motor,step_size)
                check_negative = False
                
        while check_positive:
            if new_val >= ref_val:
                #get a new value i.e. keep going
                umvr(motor,step_size)
                scan_obj = sct(exposure,  run=True)
                new_val = scan_obj.get_data(counter)
                iterNo+=1

            else:
                #restores last step
                umvr(motor,-step_size)
                check_positive = False


# talk to SEB

# how to integrate a single motor
# I dont think the structure is conducive to what we want to do

# move(user_target_pos, wait=True, relative=False, polling_time=None)

