#
# HISTORY
#
# 2015-04-13 V1.0 PB %BR%
# 2015-04-13 V1.1 PB %BR% check item
# 2015-05-22 V1.2 PB %BR% Be added to default config,
#                         added function refract_f_biconcave()
# 2015-08-27 V1.3 PB %BR% refract_alpha added (replaces refract_alpha2_),
#                         default parameters added for 2nm0 and 2nm6 multilayers
# 2015-08-28 V1.4 PB %BR% refract_mm_nrj added: calculates the reflected energy of
#                         a two component multilayer for a given angle of incidence
# 2015-08-28 V1.5 PB %BR% refract_mm_dispersion_nrj added: calculates the reflected
#                         energy of a two component multilayer for a given angle of
#                         incidence taking into account energy dispersion.
# 2015-08-28 V1.6 PB %BR% refract_mm_dispersion_alpha added: calculates the required
#                         incidence angle of a two component multilayer for a given
#                         energy taking into account energy dispersion.
# 2016-02-05 V1.7 PB %BR% formula for calculating REFRACT_AA and REFRACT_BB added
# 2016-02-07 V1.8 PB %BR% spec -> python
# 2016-02-07 V1.8 PB %BR% material() added
# 2019-12-13 V1.9 PB %BR% indentations corrected for BLISS
#                         __info__ added
# 2022-01-21 V1.10 PB %BR% Vacuum added to default config
# 2022-04-04 V1.11 PB %BR% output of write() matches setup format
# 2022-04-28 V1.12 PB %BR% mm_dispersion_nrj check added: if nrjnext is not None
#

from __future__ import print_function
import sys
import io
import math

class Refract:
    '''
    Refract(default=1)

    FUNCTIONS

    dump([<dumpfile>])
    setup( <material>, <Z>, <A[g/mol]>, <rho[g/cm3]>)
    alpha2( delta1, delta2, alpha1[mrad] )
    alpha( delta, alpha0[mrad] )
    n( material, nrj ) 
    delta( material, nrj[keV] )
    nrjc( material, alpha[mrad] ) [keV]
    alphac( material, nrj ) [mrad]
    f_biconcave( material, nrj[keV], radius[mm] )

    MULTILAYER CALCULATIONS

    mm_nrj( delta1, thickness1[nm], delta2, thickness2[nm], alpha[mrad] )
    mm_dispersion_nrj( material1, thickness1[nm], material2, thickness2[nm], alpha[mrad] )
    mm_dispersion_alpha( material1, thickness1[nm], material2, thickness2[nm], nrj[keV] )

    EXAMPLES

    m=Refract()
    m.add("Vacuum",0,0,0)
    m.add("Be",4  ,9.01200008,1.84800005)
    m.add("Si",14  ,28.086,2.33)
    m.add("Rh",45,102.91,12.44)
    m.add("Pt",78,195.090,21.37)

    self.data["Vacuum"]={'Z':0,'A':0,'rho':0}
    self.data["Be"]={'Z':4,'A':9.01200008,'rho':1.84800005}
    self.data["Si"]={'Z':14,'A':28.086,'rho':2.33}
    self.data["Rh"]={'Z':45,'A':102.91,'rho':12.44}
    self.data["Pt"]={'Z':78,'A':195.090,'rho':21.37}

    m.show()

    DESCRIPTION

    material  Z        A[g/mol]       rho[g/cm^3]
    Vacuum     0          0             0
        Be     4          9.01200008    1.84800005
        Si    14         28.086         2.33
        Rh    45        102.91         12.44
        Pt    78        195.090        21.37

    As a test: The critical angle for Si at 10.0 keV is 3.10 mrad

    m.alphac ("Si", 10)
      => 3.105487450569384

    m.nrjc("Si",3.105487450569384)
      => 10.0

    m.delta ( "Si", 10)
      => 4.822026122694581e-06

    m.n ( "Si", 10)
      => 0.9999951779738773

    CALCULATION

    alphac[mrad] = 23.24*sqrt((Z/A)*rho[g/cm^3])*(1.23985/nrjc[keV])
                 = (23.24*1.23985)*sqrt((Z/A)*rho[g/cm^3])/nrjc[keV]
                 = 28.81*sqrt((Z/A)*rho[g/cm^3])*/nrjc[keV]
                 = REFRACT_AA*sqrt((Z/A)*rho[g/cm^3])/nrjc[keV]

    nrjc[keV]    = 23.24*sqrt((Z/A)*rho[g/cm^3])*(1.23985/alphac[mrad])
                 = (23.24*1.23985)*sqrt((Z/A)*rho[g/cm^3])/alphac[mrad]
                 = 28.81*sqrt((Z/A)*rho[g/cm^3])/alphac[mrad]
                 = REFRACT_AA*sqrt((Z/A)*rho[g/cm^3])/alphac[mrad]

    delta        = 2.701e-4*((Z/A)*rho[g/cm^3])*wvl^2[nm]
                 = 2.701e-4*((Z/A)*rho[g/cm^3])*(1.2398419/nrj[keV])^2
                 = 4.152e-4*((Z/A)*rho[g/cm^3])/(nrj[keV])^2
                 = REFRACT_BB*((Z/A)*rho[g/cm^3])/(nrj[keV])^2

    refractive index n = 1 - delta

    '''
    #=================================================================================#
    #  INIT                                                                           #
    #=================================================================================#
    def __init__(self,default=1):
        self.AA = 28.815937 # (sqrt(NA*ec*ec/e0/me)*h/2_pi)*(1/keV*sqrt(1/cm3)/mrad)
        self.BB = 4.1517911e-4  # 1/2*NA*ec*ec/e0/me*h*h/2_pi/2_pi*(1/cm3/keV/keV)
        self.HC_KEVNM = 1.2398419  # keV*nm
        self.HC_MAXITER = 20
        self.EPS = 1e-30
        self.data={}
        if default == 1:
            # defaults (see http://www.csrri.iit.edu/periodic-table.html)
            self.data["Vacuum"]={'Z':0,'A':0,'rho':0}
            self.data["Be"]={'Z':4,'A':9.01200008,'rho':1.84800005}
            self.data["Si"]={'Z':14,'A':28.086,'rho':2.33}
            self.data["Rh"]={'Z':45,'A':102.91,'rho':12.44}
            self.data["Pt"]={'Z':78,'A':195.090,'rho':21.37}
                # "Ni96V4_2nm0", 2780, 5838.007871, 6.75
            self.data["Ni96V4_2nm0"]={'Z':2780,'A':5838.007871,'rho':6.75}
            # "B4C_2nm0", 26, 55.2539998, 2.9
            self.data["B4C_2nm0"]={'Z':26,'A':55.2539998,'rho':2.9}
            # "Ni96V4_2nm6", 2780, 5838.007871, 7.25
            self.data["Ni96V4_2nm6"]={'Z':2780,'A':5838.007871,'rho':7.25}
            # "B4C_2nm6", 26, 55.2539998, 3.2
            self.data["B4C_2nm6"]={'Z':26,'A':55.2539998,'rho':3.2}
    #=================================================================================#
    #  INFO                                                                           #
    #=================================================================================#
    def __info__(self):
        info=""
        info+="AA={}\n".format(self.AA)
        info+="BB={}\n".format(self.BB)
        info+="HC_KEVNM={}\n".format(self.HC_KEVNM)
        info+="HC_MAXITER={}\n".format(self.HC_MAXITER)
        info+="EPS={}\n".format(self.EPS)
        info+="Materials Data\n"
        for material in self.data:
            info+="    {:>14}    ".format(material)
            material_data=self.data[material]
            for pars in material_data:
                info+="{}:{} ".format(pars,material_data[pars])
            info+="\n"
        return(info)
    #=================================================================================#
    #  VERSION                                                                        #
    #=================================================================================#
    def version(self):
        version_string=u"$Revision: 1.9 $ / $Date: 2019/12/13 16:53:46 $ Peter Boesecke"
        print (version_string)

    #=================================================================================#
    #  SETUP FUNCTIONS                                                                #
    #=================================================================================#
    def setup(self,material,Z,A,rho=None):
        '''add( material, Z, A[g/mol], rho[g/cm3] )'''
        if (material is not None) and \
                   (Z is not None) and \
                   (A is not None) and \
                   (rho is not None): 
            self.data[material]={'Z':Z,'A':A,'rho':rho}
    def unsetup(self,material):
        if (material in self.data): del(self.data[material])
    def write(self,material):
        if (material in self.data):
            ###str=u"refractsetup {0:s} {1:d} {2:g} {3:g}".format(material,\
            ###                self.data[material]['Z'],\
            ###                self.data[material]['A'],\
            ###                self.data[material]['rho'])
            str=u'setup("{0:s}",{1:d},{2:g},{3:g})'.format(material,\
                     self.data[material]['Z'],\
                     self.data[material]['A'],\
                     self.data[material]['rho'])
            return(str)
    def dump(self,filename=None):
        '''Dump the setup strings of all materials to stdout (default) or to a file'''
        if (filename!=None):
            dump_file=io.open(filename,mode='w',encoding='utf-8')
        else:
            dump_file=sys.stdout
        for material in self.data:
            print(self.write(material),end=u'\n',file=dump_file)
        if (filename!=None):
            dump_file.close()
    def load(self,filename=None):
        '''Load the setup strings from stdin (default) or from a file'''
        if (filename!=None):
            load_file=io.open(filename,mode='r',encoding='utf-8')
        else:
            load_file=sys.stdin
        for str in load_file:
            print('{0:s}'.format(str),end=u'')
            str_list=str.split()
            #refractsetup, material, Z, A, rho
            if str_list[0] == "refractsetup":
                self.setup(str_list[1],int(str_list[2]),float(str_list[3]),float(str_list[4]))
        load_file.close()
    def show(self,material=None):
        if (material is not None):
            if (material in self.data):
                print("{0}:{1}".format(material,self.data[material]))
        else:
            for material in self.data:
                print("{0}:{1}".format(material,self.data[material]))
    def materials(self):
        '''Returns the name of all defined materials in a single string'''
        str=u""
        for material in self.data:
            str = str+u" "+material
        return(str)
    def material(self,name):
        '''Returns the material definition dictionary containg Z, A [g/mol] and rho [g/cm3]'''
        if name in self.data:
            return(self.data[name])
        else:
            return(None)
    #=================================================================================#
    #  CALCULATIONS FUNCTION                                                          #
    #=================================================================================#
    def alphac ( self, material, nrj, rho=None ):
        '''
        alphac( material, nrj[keV] [, rho[g/cm3]] )
        Calculate the critical reflection angle [mrad] of material at nrj [keV]
        Without the argument rho the tabulated density of the material is used.
        '''
        alfac=None
        if material in self.data:
            Z = self.data[material]["Z"]
            A = self.data[material]["A"]
        if rho == None:
            rho = self.data[material]["rho"]
        alfac = self.AA*math.sqrt((Z/A)*rho)/nrj
        return(alfac)
    def nrjc(self, material, alpha, rho=None ):
        '''
        nrjc( material, alpha[mrad] [, rho[g/cm3]] )
        Calculate the critical reflection energy nrj [keV] of material at alpha [mrad]
        Without the argument rho the tabulated density of the material is used.
        '''
        energyc=None
        if material in self.data:
            Z = self.data[material]["Z"]
            A = self.data[material]["A"]
        if rho == None:
            rho = self.data[material]["rho"]
        energyc = self.AA*math.sqrt((Z/A)*rho)/alpha
        return(energyc)
    def delta(self,material,nrj,rho=None):
        '''
        delta( material, nrj[keV] [, rho[g/cm3]] )
        Return delta = 1-n of the material at nrj [keV], 
        where n is the refractive index.
        Without the argument rho the tabulated density of the material is used.
        '''
        delta=None
        if material in self.data:
            Z = self.data[material]["Z"]
            A = self.data[material]["A"]
        if rho == None:
            rho = self.data[material]["rho"]
        if A != 0:
          delta = self.BB*((Z/A)*rho)/pow(nrj,2)
        else:
          delta = 0
        return(delta)
    def n(self,material,nrj,rho=None):
        '''
        n( material, nrj[keV] [, rho[g/cm3]] )
        Return the refractive index n of the material at nrj [keV].
        Without the argument rho the tabulated density of the material is used.
        '''
        return ( 1.0 - self.delta ( material, nrj, rho ) )
    def alpha2( self, delta1=0, delta2=0, alpha1=0 ):
        '''
        alpha2( delta1, delta2, alpha1[mrad] )
        Return the angle alpha2 [mrad] in medium2 of the beam with angle1 [mrad] 
        in medium1. alpha1 and alpha2 are the grazing angles of the beam with
        the interface in medium1 and medium2 respectively.
        The refractive indices are n1 in medium1 and n2 in medium2:
        n1*cos(alpha1)=n2*cos(alpha2), with n1=1-delta1, n2=1-delta2
        '''
        if (abs(alpha1)<=10 )and(abs(delta2)<0.001)and(abs(delta2)<0.001):
            alfa2 = self.alpha( delta2-delta1, alpha1 )
        else:
            cosalpha1 = math.cos(alpha1*0.001)
            cosalpha2 = ((1-delta1) * cosalpha1) / (1-delta2)
            if (abs(cosalpha2)<=1):
                alfa2 = 1000 * math.acos( cosalpha2 )
            else:
                alfa2 = None
        return( alfa2 )
    def sinalpha(self,delta=0, sinalpha0=0):
        '''
        sinalpha( delta, alpha0[mrad] )
        Returns the angle alpha in the medium for a beam with angle
        alpha0 in vacuum.
        alpha and alpha0 are the absolute values of the angles between the
        tangent of the interface with the beam direction.
        The refractive indices are 1 vacuum and n in the medium:
        cos(alpha0)=n*cos(alpha), with  n=1-delta
        Returns sinalpha in case of success or "NaN" in case of error
        '''
        sin2alpha0 = sinalpha0*sinalpha0
        delta2 = delta*delta

        tmp = sin2alpha0 - 2*delta + delta2
        n = 1-delta

        if (tmp>=0)and(abs(n)>self.EPS):
            sinalpha = math.sqrt(tmp)/n
            if (abs(sinalpha)>1):
                sinalpha = None
        else:
            sinalpha = None
        return( sinalpha )
    def alpha(self,delta=0, alpha0=0):
        '''
        alpha( delta, alpha0[mrad] )
        Returns the angle alpha [mrad] in the medium for a beam with angle
        alpha0 in vacuum.
        alpha and alpha0 are the absolute values of the angles between the
        tangent of the interface with the beam direction.
        The refractive indices are 1 vacuum and n in the medium:
        cos(alpha0)=n*cos(alpha), with  n=1-delta
        '''
        alfa = self.sinalpha( delta, math.sin( alpha0*0.001 ) )
        if (alfa!=None): 
            alfa = math.asin( alfa ) * 1000
        return( alfa )
    def f_biconcave(self, material, nrj, radius, rho=None ):
        '''
        f_biconcave( material, nrj[keV], radius[mm] [, rho[g/cm3]] )
        Returns the focal length in mm of a single biconcave 
        refractive lens for the given radius in mm
        Without the argument rho the tabulated density of the material is used.
        '''
        f=None
        delta = self.delta ( material, nrj, rho )
        if (delta != None):
            if (abs(delta)>self.EPS):
                f = radius/(2*delta)
        return(f)
    def mm_nrj(self, delta1, thickness1, delta2, thickness2, alpha ):
        '''
        mm_nrj( delta1, thickness1[nm], delta2, thickness2[nm], alpha[mrad] )
        Calculate the principal harmonic photon energy in keV of a two component
        multilayer system for the incidence angle alpha using given refractive
        indices and layer thicknesses. 

        The calculation is purely geometric for rays that are reflected after
        passing through a double layer that is repeated many times in the multilayer. 
        It does not take into account reflectivities of layer boundaries. 
        Reflectivities of a single layer are ignored.

        For refining the true refractive indices at the selected
        Bragg energy mm_dispersion_nrj must be used.
        Returns nrj in keV
        '''
        lambda0 = 0
        alpha0 = alpha*0.001
        sinalpha0 = math.sin( alpha0 )
        cosalpha0 = math.cos( alpha0 )
        #
        n1 = 1-delta1
        sinalpha1 = self.sinalpha( delta1, sinalpha0 )
        if (sinalpha1==None):
             return( None )
        cosalpha1 = math.sqrt( 1 - sinalpha1*sinalpha1 )
        #
        n2 = 1-delta2
        sinalpha2 = self.sinalpha( delta2, sinalpha0 )
        if (sinalpha2==None):
            return( None )
        cosalpha2 = math.sqrt( 1 - sinalpha2*sinalpha2 )
        #
        if (abs(sinalpha1)>self.EPS)and(abs(sinalpha2>self.EPS)):
            lambda0 = 2* ( \
              (thickness1/sinalpha1)*(n1-cosalpha0*cosalpha1) + \
              (thickness2/sinalpha2)*(n2-cosalpha0*cosalpha2) \
            )
        else:
            return( None )
        #
        if (abs(lambda0)<self.EPS):
            return( None )
        #
        return( self.HC_KEVNM/lambda0 )
    def mm_dispersion_nrj( self,material1, thickness1, material2, thickness2, alpha, rho1=None, rho2=None ):
        '''
        mm_dispersion_nrj( material1, thickness1[nm], material2, thickness2[nm], alpha[mrad] [, rho1[g/cm3]][, rho2[g/cm3]] )
        Calculate the principal harmonic photon energy in keV of a two component
        multilayer for the incidence angle alpha taking into account the
        energy dispersion of the refractive index using recursion.
        Without the arguments rho1 or rho2 the tabulated densities of the materials are used.
        Returns nrj in keV
        '''
        alpha0 = alpha*0.001
        nrj = self.HC_KEVNM/(2*(thickness1+thickness2)*math.sin(alpha0))
        delta1 = self.delta( material1, nrj, rho1 )
        delta2 = self.delta( material2, nrj, rho2 )
        nrjnext = self.mm_nrj( delta1, thickness1, delta2, thickness2, alpha )
        if nrjnext is not None:
            nrjdiff = nrjnext-nrj
            loopcnt = 1
            while (abs(nrjdiff)>=self.EPS) and (loopcnt<=self.HC_MAXITER):
                nrj = nrjnext
                delta1 = self.delta( material1, nrj, rho1 )
                delta2 = self.delta( material2, nrj, rho2 )
                nrjnext = self.mm_nrj( delta1, thickness1, delta2, thickness2, alpha )
                if nrjnext is not None:
                    nrjdiff = nrjnext-nrj
                loopcnt+=1
        return( nrj )
    def mm_dispersion_alpha( self, material1, thickness1, material2, thickness2, nrj, rho1=None, rho2=None ):
        '''
        mm_dispersion_alpha( material1, thickness1[nm], material2, thickness2[nm], nrj[keV] [, rho1[g/cm3]][, rho2[g/cm3]] )
        Calculate the incidence angle alpha in mrad of a two component
        multilayer for the photon energy nrj taking into account the
        energy dispersion of the refractive index using recursion.
        Without the arguments rho1 or rho2 the tabulated densities of the materials are used.
        Returns alpha in mrad.
        '''
        delta1 = self.delta( material1, nrj, rho1 )
        delta2 = self.delta( material2, nrj, rho2 )
        # estimate start angle
        sinalpha =  (self.HC_KEVNM/nrj)/(2*(thickness1+thickness2))
        alpha = math.asin( sinalpha )*1000
        #
        nrjcalc = self.mm_nrj( delta1, thickness1, delta2, thickness2, alpha )
        nrjdiff = nrj-nrjcalc
        # estimate first iteration step
        deltanrj = nrjdiff
        sin2alpha = sinalpha*sinalpha
        cosalpha = math.sqrt(1-sin2alpha)
        deltaalpha = - (2*(thickness1+thickness2)*(sin2alpha/cosalpha)/self.HC_KEVNM)*nrjdiff*1000
        alphanext = alpha + deltaalpha
        #
        loopcnt = 1
        while (abs(nrjdiff)>=self.EPS)and(abs(deltanrj)>=self.EPS)and(loopcnt<=self.HC_MAXITER):
            nrjcalcnext = self.mm_nrj( delta1, thickness1, delta2, thickness2, alphanext )
            #
            deltanrj   = nrjcalcnext-nrjcalc
            deltaalpha = alphanext-alpha
            #
            alpha   = alphanext
            nrjcalc = nrjcalcnext
            nrjdiff = nrj-nrjcalc
            #
            if abs(deltanrj)>=self.EPS:
                  alphanext = alpha + (deltaalpha/deltanrj) * nrjdiff
            else:
                  alphanext = alpha
            #
            loopcnt+=1
        return( alpha )

