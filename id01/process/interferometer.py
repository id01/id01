# IDS3010 interferometer set up

# looking at the diffractometer, beam left to right, 
#   sensor 1 points outboard and up
#   sensor 2 points downstream and up
#   sensor 3 points upstream and up

# define g_o1,g_o2,g_o3 as orthonormal base i.e. our diffractometer  pix,piy,piz

# define g_1,g_2,g_3 as skewed base i.e. our interferometer

from bliss.config.beacon_object import BeaconObject
from bliss.common.scans import ct


import numpy as np

# I need a bliss object to hold my interferometer values
class hexapod_interferometer(): # BeaconObject
    #offset_g_1 = BeaconObject.property_setting('offset_g_1',default=0)
    #offset_g_2 = BeaconObject.property_setting('offset_g_2',default=0)
    #offset_g_3 = BeaconObject.property_setting('offset_g_3',default=0)

    def __init__(self,name,config):

        #self.offset_g_1 = self.config.get("offset_g_1")
        #self.offsets = np.array([self.offset_g_1,self.offset_g_2,self.offset_g_3])
        self.offsets = np.array([0,0,0])
        # g_om = A_lm*g_m
        self.a_lm = np.array([[np.cos(np.deg2rad(35)),-np.cos(np.deg2rad(35)),-np.cos(np.deg2rad(35))*np.sin(np.deg2rad(30))],
                        [0,np.cos(np.deg2rad(35))*np.cos(np.deg2rad(30)),-np.cos(np.deg2rad(35))*np.cos(np.deg2rad(30))],
                        [np.sin(np.deg2rad(35)),np.sin(np.deg2rad(35)),np.sin(np.deg2rad(35))]]) 

        # g_m = A_lm^-1*g_om

        self.a_lm_inv = np.linalg.inv(self.a_lm)
        self.config = config

        self.cnt_g_1 = config["inputs"][0]["counter"]
        self.cnt_g_2 = config["inputs"][1]["counter"]
        self.cnt_g_3 = config["inputs"][2]["counter"]

        self.mot_g_o1 = config["inputs"][3]["motor"]
        self.mot_g_o2 = config["inputs"][4]["motor"]
        self.mot_g_o3 = config["inputs"][5]["motor"]
        

    def get_interferometer_offsets(self):
        return self.offsets
    
    def set_interferometer_offsets(self,offset_g_1 = None,offset_g_2 = None,offset_g_3 = None):
        """
        define the deltas between interferometer 0,0,0 and lab frame 0,0,0
        """
        if offset_g_1 is None or offset_g_2 is None or offset_g_3 is None:
            # take a ct
            scan_obj = ct(exposure_time,self.cnt_g_1,self.cnt_g_2,self.cnt_g_3)
            if offset_g_1 is None:
                offset_g_1 = scan_obj.get_data(self.cnt_g_1.name)[0]
            if offset_g_2 is None:
                offset_g_2 = scan_obj.get_data(self.cnt_g_2.name)[0]
            if offset_g_3 is None:
                offset_g_3 = scan_obj.get_data(self.cnt_g_3.name)[0]        
        
        print("deltas: ", self.offsets - np.array([offset_g_1,offset_g_2,offset_g_3]))
        self.offsets = np.array([offset_g_1,offset_g_2,offset_g_3])

        
    def target_interf_from_g_o(self,):
        """
        return the calculated target position for the interferometer
        """
        current_pos = np.array([self.mot_g_o1.position,self.mot_g_o2.position,self.mot_g_o3.position])
        return np.matmul(self.a_lm,current_pos)+self.offsets

    def target_g_o_from_interf(self,exposure_time=0.005):
        """
        return the calculated value for g_o from the interferometer
        """

        scan_obj = ct(exposure_time,self.cnt_g_1,self.cnt_g_2,self.cnt_g_3)
        interf_pos = np.array([scan_obj.get_data(self.cnt_g_1.name)[0],
                               scan_obj.get_data(self.cnt_g_2.name)[0],
                               scan_obj.get_data(self.cnt_g_3.name)[0]])

        return np.matmul(self.a_lm_inv,interf_pos-self.offsets)


import numpy as np 
import matplotlib.pyplot as plt


def plot_interf_mesh(filename,scan_no,hex_interferometer):
    """
    parse a phi/eta scan and return the observed drifts in piezo coordinates.
    """
    data = openScan(filename,scan_no)


    x=data.getRoiData("phi")                                                                                                    
    y=data.getRoiData("eta")                                                                                                   
    z1=data.getRoiData("IN1")                                                                                                  
    z2=data.getRoiData("IN2")                                                                                                  
    z3=data.getRoiData("IN3")
    data_shape = z1.shape
    z1 = z1.flatten()
    z2 = z2.flatten()
    z3 = z3.flatten()     

                                                                                                                             
    b=np.array([])                                                                                                                 
    i=0                                                                                                                            
    for a in np.c_[z1,z2,z3]:                                                                                                      
      if i==0:                                                                                                                   
          b = np.matmul(hex_interferometer.a_lm,a)                                                                               
      else:                                                                                                                      
          b = np.c_[b,np.matmul(hex_interferometer.a_lm,a)]                                                                      
                                                                                                                                 
      i+=1                                                                                                                       
                                                                                                                                                                                                                  
           
plt.subplots(figsize = (18,6))                                                                                                
plt.subplot(1,3,1)                                                                                                            
plt.pcolormesh(x.reshape(data_shape),y.reshape(data_shape),b[0].reshape(data_shape)/1e5)
plt.title("pix")
plt.xlabel("phi")
plt.ylabel("eta")                                                       
plt.colorbar()                                                                                                                
plt.subplot(1,3,2)                                                                                                            
plt.pcolormesh(x.reshape(data_shape),y.reshape(data_shape),b[1].reshape(data_shape)/1e5)                                                       
plt.title("piy")  
plt.xlabel("phi")
plt.ylabel("eta")                                                      
plt.colorbar()                                                                                                                
plt.subplot(1,3,3)                                                                                                            
plt.pcolormesh(x.reshape(data_shape),y.reshape(data_shape),b[2].reshape(data_shape)/1e5)                                                       
plt.title("piz")  
plt.xlabel("phi")
plt.ylabel("eta")                                                      
plt.colorbar()                                                                                                              
plt.show()  