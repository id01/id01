#!/usr/bin/env python
# -*- coding: utf-8 -*-
#----------------------------------------------------------------------
# Description:  from id01sware modified by SL for BLISS 20211217
# Author: Carsten Richter <carsten.richter@esrf.fr>
# Created at: Wed Jun  7 18:28:51 CEST 2017
# Computer: rnice8-0207 
# System: Linux 3.16.0-4-amd64 on x86_64
#----------------------------------------------------------------------
"""
    Originally these were some functions that were used by CamView, but
    they can be useful for other routines and scripts 
           >> hence we bring them to BLISS, thanks Carsten!
"""

import time
import platform
from scipy import ndimage
PV = platform.python_version()
if PV.startswith("2."):
    from urllib2 import urlopen
    from urlparse import urlparse
elif PV.startswith("3."):
    from urllib.request import urlopen
    import urllib.parse as urlparse

import numpy as np


import id01.scripts.image as image
from bliss import setup_globals
from bliss.shell.standard import mv, mvr
import pdb
import matplotlib.pyplot as plt




def _url_validator(x):
    try:
        result = urlparse.urlparse(x)
        #print(result)
        return all(map(bool, (result.scheme, result.netloc, result.path)))
    except:
        return False



class AutoFocus(object):
    """
        A class providing auto-focus functionality based on
        webcam images and an interface to BLISS
        that allows to move a motor for focusing.

        At the moment, the optimization of focus is done
        via bounded univariate scalar function minization where
        the function returns the sharpness of the image 
        (see function `contrast`).

        As images carry noise, it would be better to use an
        optimizer for noisy functions (noisyopt?).
    """
    def __init__(self, url,
                       motor=None,
                       limits=None,
                       roi=None,
                       navg=1,
                       enhance=False,
                       detObj=None,
                       exposure=None):
        #self._specclient = SpecClientWrapper.SpecClientSession()
        self.url = url
        if motor is None: # default piz
            self.motor = setup_globals.piz
        else:
            self.motor = motor
        if limits == None:
            self.limits = self.motor.limits
        else:
            self.limits = limits
        self.roi = roi
        self.navg = navg
        self.stretch = enhance
        self.contrast = "diff"

    ###### URL
    @property
    def url(self):
        return self._url
    @url.setter
    def url(self, val):
        if not _url_validator(val):
            raise ValueError("Invalid url: %s"%str(val))
        self._url = val

    ###### MOTOR
    @property
    def motor(self):
        return self._motor
    @motor.setter
    def motor(self, motObj):
        self._motor = motObj
    
    ###### LIMITS
    @property
    def limits(self):
        """ Limits of the auto focusing motor """
        return self._ll, self._ul
    @limits.setter
    def limits(self, val):
        if val is None:
            self._ll = -np.inf
            self._ul =  np.inf
        else:
            val = np.array(val, dtype=float, ndmin=1)
            assert len(val)==2, "Need 2 scalar values: upper and lower limit"
            self._ll = val.min()
            self._ul = val.max()
    @limits.deleter
    def limits(self):
        self._ll = -np.inf
        self._ul =  np.inf

    ###### ROI
    @property
    def roi(self):
        """
            Defines the region of interest on the picture used to evaluate the
            contrast:
                (dim0_min, dim0_max, dim1_min, dim1_max)
        """
        return (self._slice_0.start,
                self._slice_0.stop,
                self._slice_1.start,
                self._slice_1.stop)
    @roi.setter
    def roi(self, val):
        if val is None:
            self._slice_0 = slice(None,None)
            self._slice_1 = slice(None,None)
        else:
            #val = np.array(val, dtype=int)
            self._slice_0 = slice(*val[0:2])#slice(*np.sort(val[0:2]))
            self._slice_1 = slice(*val[2:4])#slice(*np.sort(val[2:4]))
    @roi.deleter
    def roi(self):
        self._slice_0 = slice(None,None)
        self._slice_1 = slice(None,None)

    ###### NAVG
    @property
    def navg(self):
        """ Number of images to average """
        return self._navg
    @navg.setter
    def navg(self, val):
        self._navg = int(val)

    ###### STRETCH
    @property
    def stretch(self):
        """
            Percentiles to stretch contrast:
                between 0 and 100, or True/False
        """
        return self._stretch
    @stretch.setter
    def stretch(self, val):
        if val is True:
            self._stretch = 5., 95.
        elif val is False:
            self._stretch = False
        else:
            val = np.array(val, dtype=float, ndmin=1)
            assert len(val)==2, ("Need 2 scalar values: upper and lower "
                                 "percentile")
            self._stretch = val.min(), val.max()

    ###### CONTRAST
    @property
    def contrast(self):
        """
            Name of model for contrast evaluation:
                One of: ["diff", "msd", "gradient"]
        """
        return self._contrast
    @contrast.setter
    def contrast(self, val):
        if not val in image._models:
            #print("Problem")
            raise ValueError("Contrast model needs to be one of [%s]"
                             %", ".join(image._models))
        self._contrast = val

    def focus(self, navg=None, stretch=None, contrast=None, **leastsq_kw):
        """
            do the actual focusing
        """
        if not hasattr(self, "_optimize"):
            self._optimize = __import__('scipy.optimize',
                                       globals(),
                                       locals(),
                                       ['leastsq'])

        if not navg is None:
            self.navg = navg

        if not stretch is None:
            self.stretch = stretch

        if not contrast is None:
            self.contrast = contrast

        startval = self.get_motor_pos()

#        kw = dict(full_output=True,
#                  ftol=1e-5,
#                  xtol=1e-3,
#                  maxfev=0,
#                  factor=5.)
#        kw.update(leastsq_kw)
#
#        self.result = self._optimize.leastsq(self._costfunction, startval, **kw)

        kw = dict(bracket=None,
                  bounds=self.limits,
                  method='Bounded',
                  tol=1e-3, options=None)
        kw.update(leastsq_kw)

        self.result = self._optimize.minimize_scalar(self._costfunction, **kw)
#        kw = dict(bracket=None,
#                  bounds=self.limits,
#                  method='L-BFGS-B',
#                  tol=1e-3, options=None)
#        kw.update(leastsq_kw)
#
#        self.result = self._optimize.minimize_scalar(self._costfunction)
        return self.result

    def get_motor_pos(self):
        return self.motor.position

    def movemotor(self, position):
        if position > self._ll and position < self._ul:
            mv(self.motor,position)
        else:
            raise ValueError("Setpoint hits limits: %f"%position)


    def _costfunction(self, newpos=None):
        """
            return a value proportional to the inverse
            sharpness -- the function which ought to be minimized
        """
        if newpos is not None:
            self.movemotor(newpos)
        img = image.url2array(self.url, navg=self.navg)
        img = img[self._slice_0, self._slice_1]
        if self.stretch:
            img = image.stretch_contrast(img, *self._stretch)
        self._image = img
        # for debugging
        #plt.figure()
        #plt.imshow(img)
        #plt.show()
        img_contrast = image.contrast(img, self.contrast)
        residual = 1./img_contrast
        print("Value: %f"%residual)
        return residual


    # move all scripts below to a new class that inherits from this one to keep it clean
    def get_sct_images(self, exposure = .1):
        """
        return an array from the desired detector ct
        """
        if exposure != 1:
            self.exposure = exposure
        else:
            self.exposure = 1
            
        scan_obj = sct(exposure, self.det_obj, run=False)  
        scan_obj.run()
        image = scan_obj.get_data()['{}:image'.format(self.det_obj.name)].as_array()

        self.last_image = image
        return self.last_image

    def _costfunction_ct(self, newpos=None):
        """
            return a value proportional to the inverse
            sharpness -- the function which ought to be minimized
        """
        if newpos is not None:
            self.movemotor(newpos)
        
        img = get_sct_images()
        if self.stretch:
            img = image.stretch_contrast(img, *self.stretch)
        self._image = img
        # for debugging
        #plt.figure()
        #plt.imshow(img)
        #plt.show()
        img_contrast = image.contrast(img, self.contrast)
        residual = 1./img_contrast
        print("Value: %f"%residual)
        return residual

    def focus_ct(self, navg=None, stretch=None, contrast=None, **leastsq_kw):
        """
            do the actual focusing
        """
        if not hasattr(self, "_optimize"):
            self._optimize = __import__('scipy.optimize',
                                       globals(),
                                       locals(),
                                       ['leastsq'])

        if not navg is None:
            self.navg = navg

        if not stretch is None:
            self.stretch = stretch

        if not contrast is None:
            self.contrast = contrast

        startval = self.get_motor_pos()

#        kw = dict(full_output=True,
#                  ftol=1e-5,
#                  xtol=1e-3,
#                  maxfev=0,
#                  factor=5.)
#        kw.update(leastsq_kw)
#
#        self.result = self._optimize.leastsq(self._costfunction, startval, **kw)

        kw = dict(bracket=None,
                  bounds=self.limits,
                  method='Bounded',
                  tol=1e-3, options=None)
        kw.update(leastsq_kw)

        self.result = self._optimize.minimize_scalar(self._costfunction_ct, **kw)
#        kw = dict(bracket=None,
#                  bounds=self.limits,
#                  method='L-BFGS-B',
#                  tol=1e-3, options=None)
#        kw.update(leastsq_kw)
#
#        self.result = self._optimize.minimize_scalar(self._costfunction)
        return self.result


class AlignImages(object):
    """
    NOT MODIFIED FOR BLISS TODO
        A class providing image alignment based on
        webcam images and an interface to spec (SpecClient)
        that allows to move a motor for focusing.

    """
    def __init__(self, url,
                       motors=("pix", "piy"),
                       steps=(10, 10),
                       waittime=1.,
                       method="correlation",
                       roi=None):
        self._specclient = SpecClientWrapper.SpecClientSession()
        self.url = url
        self.motor1 = motors[0]
        self.motor2 = motors[1]
        self.steps = steps
        self.waittime = waittime
        self.roi = roi
        self.navg = 1
        self.stretch = False
        self.method = method

    ###### URL
    @property
    def url(self):
        return self._url
    @url.setter
    def url(self, val):
        if not _url_validator(val):
            raise ValueError("Invalid url: %s"%str(val))
        self._url = val

    ###### MOTORS
    @property
    def motor1(self):
        return self._motor1
    @motor1.setter
    def motor1(self, val):
        if not isinstance(val, (str, bytes)):
            raise ValueError("Need string as input for `motor1`")
        self._motor1 = val
        self._motor1num = self._specclient.send_sc("motor_num('%s')"%val)

    @property
    def motor2(self):
        return self._motor2
    @motor2.setter
    def motor2(self, val):
        if not isinstance(val, (str, bytes)):
            raise ValueError("Need string as input for `motor2`")
        self._motor2 = val
        self._motor2num = self._specclient.send_sc("motor_num('%s')"%val)


    ###### STEPS
    @property
    def steps(self):
        """ steps of the auto focusing motor """
        return self._step1, self._step2
    @steps.setter
    def steps(self, val):
        val = np.array(val, dtype=float, ndmin=1)
        assert len(val)==2, "Need 2 scalar values: upper and lower limit"
        self._step1 = val[0]
        self._step2 = val[1]

    ###### ROI
    @property
    def roi(self):
        """
            Defines the region of interest on the picture used to evaluate the
            contrast:
                (dim0_min, dim0_max, dim1_min, dim1_max)
        """
        return (self._slice_0.start,
                self._slice_0.stop,
                self._slice_1.start,
                self._slice_1.stop)
    @roi.setter
    def roi(self, val):
        if val is None:
            self._slice_0 = slice(None,None)
            self._slice_1 = slice(None,None)
        else:
            #val = np.array(val, dtype=int)
            self._slice_0 = slice(*val[0:2])#slice(*np.sort(val[0:2]))
            self._slice_1 = slice(*val[2:4])#slice(*np.sort(val[2:4]))
    @roi.deleter
    def roi(self):
        self._slice_0 = slice(None,None)
        self._slice_1 = slice(None,None)

    ###### WAITTIME
    @property
    def waittime(self):
        """ Time to wait after motor move """
        return self._waittime
    @waittime.setter
    def waittime(self, val):
        self._waittime = float(val)

    ###### NAVG
    @property
    def navg(self):
        """ Number of images to average """
        return self._navg
    @navg.setter
    def navg(self, val):
        self._navg = int(val)

    ###### STRETCH
    @property
    def stretch(self):
        """
            Percentiles to stretch contrast:
                between 0 and 100, or True/False
        """
        return self._stretch
    @stretch.setter
    def stretch(self, val):
        if val is True:
            self._stretch = 5., 95.
        elif val is False:
            self._stretch = False
        else:
            val = np.array(val, dtype=float, ndmin=1)
            assert len(val)==2, ("Need 2 scalar values: upper and lower "
                                 "percentile")
            self._stretch = val.min(), val.max()


    def get_motor_pos(self):
        if not -1 in (self._motor1num, self._motor2num):
            pos1 = self._specclient.get_sv("A[%i]"%self._motor1num)
            pos1 = float(pos1[str(self._motor1num)])
            pos2 = self._specclient.get_sv("A[%i]"%self._motor2num)
            pos2 = float(pos2[str(self._motor2num)])
            return pos1, pos2


    def fetchimage(self):
        img = image.url2array(self.url, navg=self.navg)
        img = img[self._slice_0, self._slice_1]
        if self.stretch:
            img = image.stretch_contrast(img, *self.stretch)
        return img


    def align(self, shift, navg=None, stretch=None, waittime=None, method=None):
        """
            do the alignment
        """
        if not navg is None:
            self.navg = navg
        if not stretch is None:
            self.stretch = stretch
        if not waittime is None:
            self.waittime = waittime
        if not method is None:
            self.method = method

        shift = np.array(shift)
        assert len(shift)==2

        startpos = self.get_motor_pos()
        steps = self.steps

        im_0 = self.fetchimage()

        self._specclient.send_sc("mvr %s %f"%(self.motor1, steps[0]))
        time.sleep(self.waittime)
        im_1 = self.fetchimage()

        cc1 = image.get_shift(im_1, im_0, method=method)


        self._specclient.send_sc("mvr %s %f"%(self.motor2, steps[1]))
        time.sleep(self.waittime)
        im_2 = self.fetchimage()

        cc2 = image.get_shift(im_2, im_1, method=method)

        m = np.array([[cc1[0]/steps[0], cc2[0]/steps[1] ],
                      [cc1[1]/steps[0], cc2[1]/steps[1] ]])


        step = np.linalg.inv(m).dot(shift)
        target = step + np.array(startpos)

        result = (self.motor1, target[0], self.motor2, target[1])
        self._specclient.send_sc("mv %s %f %s %f"%result)

        return result
