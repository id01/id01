from id01.controllers.amptek import Amptek

#* a = config.get('amptek')                                                                                                   │
#* s = a.get_sca(1)                                                                                                           │
#* s.threshold                                                                                                                │
#* s.output_level                                                                                                             │
#* a.read_spectrum()                                                                                                          │
#* s.threshold                                                                                                                │
#* a.identity()                                                                                                               │
#* a.read_spectrum()                                                                                                          │
#* a.identity()                                                                                                               │
#* a.read_spectrum()                                                                                                          │
#* a.identity()                                                                                                               │
#* a.read_spectrum()                                                                                                          │
#* a.clear_spectrum()                                                                                                         │
#* s=a.get_sca(8)                                                                                                             │
#* s.threshold                                                                                                                │
#* history()     

def get_fluo_SCA_window(amptekObj):
    sca = amptekObj.get_sca(8)
    return sca.threshold

def set_fluo_SCA_window(amptekObj,min,max):
    amptekObj.threshold=(min,max)
    print(f"window set to channels: ", get_fluo_SCA_window(amptekObj))

# need a.read and clear_spectrum as a counter in bliss?
# need to add a create_counter function call in the controller itself?

# the gain can be set easily with amptekObj.gain = , There must be a lookup table for actual gains - we can use the raw value for now as it is not so critical to have a fine gain it seems

def set_fluo_SCA_gain(amptekObj,value):
    amptekObj.gain = value

def get_fluo_SCA_gain(amptekObj):
    return amptekObj.gain

def fluo_SCA_gain_increase(amptekObj)
    amptekObj
    pass
