# ID01 BLISS git repo
#       authors:
#         Peter Boesecke, boesecke@esrf.fr
#
#       Created at: 
#
#----------------------------------------------------------------------
# Description:
#       class Slit2dGap(CalcController)
#       Calculates standard gap/offset motors (vg,vo,hg,ho)
#       from standard blade motors (up,down,fron,back) 
#       class Slit2dGap (ID01_slit2dgap.py) must be used together with class 
#       Slit2D (ID01_slit2D.py) which replaces the controller class slits.
#
# History:
#   2022-11-07 PB 
#   2022-11-09 PB All slit dump functions now in module ID01_slitgap.py
#   2022-11-22 PB orientation added
#   2022-11-23 PB __info__ added
#   2022-11-24 PB after changing the orientation, a sync is needed,
#                 otherwise the motors can move to a (single) buffered 
#                 position
#   2022-11-28 PB Including functionality of ID01_slit.py 
#                 The slit name must now be defined as part of
#                 Slit2dGap, the real motors can be gap/offset
#                 motors, without the need of additional CalcMotors.
#                 For using the dump method with a real gap slit, e.g. 
#                 o7, the motors must be defined in the following way:
#                 - plugin: emotion
#                   package: id01.scripts.ID01_slit2dgap
#                   class: Slit2dGap
#                   name: o7
#                   axes:
#                   - name: $sim_o7vg
#                     tags: real vgap
#                   - name: $sim_o7vo
#                     tags: real voffset
#                   - name: $sim_o7hg
#                     tags: real hgap
#                   - name: $sim_o7ho
#                     tags: real hoffset
#                   The slit orientation cannot be changed.
#                   The slit position can then be dumped with:
#                     sim_o7.dump()                                                                                                                                              
#                     Out [9]: 'sim_o7.gap(1.0,2.0);sim_o7.offset(3.0,3.0);'
#                   and moved with:
#                     sim_o7.gap(1.0,2.0)
#                     Out [10]: '1.0,2.0'
#                     sim_o7.offset(3.0,3.0)
#                     Out [11]: '3.0,3.0'
#
#                  orientation() returns always the orientation determined by
#                  Slit2d(). If Slit2d is not used None is returned.
#
#                  dump, move, gap, offset expecting slit parameter in the legacy
#                  order:  svg, shg, svo, shg
#
#   2022-11-29 PB removing legacy order by changing the parameter order of 
#                 dump, move, gap, offset to shg,svg,shg,svo,
#                 dump: option 'info=1' added for showing motor tags,
#                 print(<slit>.dump(info=1))
#                 dump: dump the orientation first
#
#                 __info__ writes also Slit2d.__info__ if available
#
#   2022-12-05 PB dump formatting adjusted, orientation included
#   2023-01-16 PB update for BLISS version 1.9 ++ 
#                 Removing 'orientation' from axis class, because
#                 it returns None.
#                 The orientation is now directly read from 
#                 front_mot.controller.orientation().
#   2023-01-17 PB class Axis -> CalcAxis
#
#----------------------------------------------------------------------

import numpy as np
from bliss.controllers.motor import CalcController
from bliss.common.axis import CalcAxis as AxisBase

from bliss.config.settings import HashObjSetting

from bliss.setup_globals import *
from bliss.common import session
from bliss.config import static

from id01.scripts.qmove import qmove

from id01.scripts.specmot_utils import *

import time

import logging
#logging.basicConfig(level=logging.DEBUG)
#logging.basicConfig(level=logging.WARNING)
#logging.debug('This will get logged')
#logging.warning('This will get logged')

_log=logging.getLogger("id01.scripts.ID01_slit2dgap")
#The log level can be set in the bliss session after import logging
#and defining _log as in the above command.
#_log.setLevel("DEBUG")

# constant tags

HG_TAG = "hgap"
HO_TAG = "hoffset"
VG_TAG = "vgap"
VO_TAG = "voffset"

FRONT_TAG   = "front"
BACK_TAG    = "back"
UP_TAG      = "up"
DOWN_TAG    = "down"

class Slit2dGap(CalcController):
    """
    Calculates gap motors (vgap,voffset,hgap,hoffset)
    from blade motors (up,down,front,back)

    The standard laboratory system is defined with respect to the
    primary beam:
        translation along the primary beam: +x
        vertical translation:   +z (upwards)
        horizontal translation: +y (left)

    In the laboratory system the 4 blade motors are called:
      - up, down, front, back,
      - standard suffix u, d, f, b

    In the laboratory system the 4 gap-offset motors are called:
      - vertical gap, vertical offset, horizontal gap, horizontal offset
      - standard suffix hg, ho, vg, vo

    Both descriptions are equivalent and exchangeable:

      - hg = front+back
      - ho = (front-back)/2
      - vg = up+down
      - vo = (up-down)/2

      - front = hg/2 + ho
      - back  = hg/2 - ho
      - up    = vg/2 + vo
      - down  = vg/2 - vo

    The directions follow the standard rules.
    x,y,z follow the right hand rule:
        x points along the beam,
        y points left (horizontal),
        z points upwards (vertical)

    The sign of each blade/gap motor must be configured in such a way that a
    movement in positive user direction opens the aperture.

    """

    def __init__(self,config,*args,**kwargs):
        super().__init__(config,*args,**kwargs)
        '''
        The parameters are kept in Params.
        '''
        #see ID01_rbs.py

        # The real and calculated motors must be defined as tagged motors
        # in <device>.yml.
        # motors specified in <device>.yml:
        #  - name: $<axis3>
        #    tags: real front
        #
        #  - name: $<axis4>
        #    tags: real back
        #
        #  - name: $<axis1>
        #    tags: real up
        #
        #  - name: $<axis2>
        #    tags: real down
        #
        # ...
        #  - name: <hgap>
        #    tags: hgap
        #
        #  - name: <voffset>
        #    tags: voffset
        #
        #  - name: <vg>
        #    tags: vgap
        #
        #  - name: <voffset>
        #    tags: voffset
        #
        # ...

        ### The motors are defined as tagged motors in <device>.yml

        self._config=config
        self.EPS=1e-32

    def __info__(self):
        '''
        Returns a string with the current configuration
        '''
        info=""

        slit_name=self.name
        info+="slit unit {}\n".format(slit_name)

        info+="\n"

        if hasattr(self,"_tagged"):

            got_controller_info=False

            if self.orientation() is not None: 
                # then get the controller info
                tmp=self._tagged.get(FRONT_TAG)
                if tmp is not None:
                    front_mot=tmp[0]

                    if hasattr(front_mot,"controller"):
                        if callable(getattr(front_mot.controller,'__info__', None)):
                            info+=front_mot.controller.__info__()
                            got_controller_info=True

            if not got_controller_info:
                # the get your own info

                # FRONT_TAG
                tmp=self._tagged.get(FRONT_TAG)
                if tmp is not None:
                    front_name=tmp[0].name
                    info+="{} {} motor\n".format(front_name, FRONT_TAG)

                # BACK_TAG
                tmp=self._tagged.get(BACK_TAG)
                if tmp is not None:
                    back_name=tmp[0].name
                    info+="{} {} motor\n".format(back_name, BACK_TAG)

                # UP_TAG
                tmp=self._tagged.get(UP_TAG)
                if tmp is not None:
                    up_name=tmp[0].name
                    info+="{} {} motor\n".format(up_name, UP_TAG)

                # DOWN_TAG
                tmp=self._tagged.get(DOWN_TAG)
                if tmp is not None:
                    down_name=tmp[0].name
                    info+="{} {} motor\n".format(down_name, DOWN_TAG)

                info+="\n"

            # HG_TAG
            tmp=self._tagged.get(HG_TAG)
            if tmp is not None:
                hgap_name=tmp[0].name
                info+="{} horizontal gap motor (tag={})\n".format(hgap_name,HG_TAG)


            # HO_TAG
            tmp=self._tagged.get(HO_TAG)
            if tmp is not None:
                hoffset_name=tmp[0].name
                info+="{} horizontal offset motor (tag={})\n".format(hoffset_name,HO_TAG)

            # VG_TAG
            tmp=self._tagged.get(VG_TAG)
            if tmp is not None:
                vgap_name=tmp[0].name
                info+="{} vertical gap motor (tag={})\n".format(vgap_name, VG_TAG)

            # VO_TAG
            tmp=self._tagged.get(VO_TAG)
            if tmp is not None:
                voffset_name=tmp[0].name
                info+="{} vertical offset motor (tag={})\n".format(voffset_name, VO_TAG)

            info+="\n"

        orientation=self.orientation(info=1)
        if orientation is not None:
            info+="{}\n".format(orientation)

        info+="\n"

        return(info)


    def calc_from_real(self, level1_positions):
        '''
        Calculate the level2_positions (hg, ho, vg, vo) [mm]
        from the level1_positions (front, back, up, down) [mm].
        '''
        _log.debug("  calc_from_real BEGIN (level1={})".format(level1_positions))

        calc_level2_positions={}

        # horizontal
        front=level1_positions.get(FRONT_TAG)
        back=level1_positions.get(BACK_TAG)

        if (front is not None and back is not None):
            hg = front+back
            ho = (front-back)/2

            calc_level2_positions[HG_TAG]=hg
            calc_level2_positions[HO_TAG]=ho

            # vertical
            up=level1_positions[UP_TAG]
            down=level1_positions[DOWN_TAG]

            vg = up+down
            vo = (up-down)/2

            calc_level2_positions[VG_TAG]=vg
            calc_level2_positions[VO_TAG]=vo

        _log.debug("  calc_from_real END (calc_level2={})".format(calc_level2_positions))

        return calc_level2_positions


    def calc_to_real(self, level2_positions):
        '''
        Calculate the level1_positions (hg, ho, vg, vo) [mm]
        from the level2_positions (front, back, up, down) [mm].
        '''
        _log.debug("  calc_to_real BEGIN (level2={})".format(level2_positions))

        calc_level1_positions={}

        # horizontal
        hg=level2_positions[HG_TAG]
        ho=level2_positions[HO_TAG]

        front = hg/2 + ho
        back  = hg/2 - ho

        calc_level1_positions[FRONT_TAG]=front
        calc_level1_positions[BACK_TAG]=back

        # vertical
        vg=level2_positions[VG_TAG]
        vo=level2_positions[VO_TAG]

        up    = vg/2 + vo
        down  = vg/2 - vo

        calc_level1_positions[UP_TAG]=up
        calc_level1_positions[DOWN_TAG]=down

        _log.debug("  calc_to_real END (calc_level1={})".format(calc_level1_positions))

        return calc_level1_positions


    def orientation(self,orientation=None,info=None):
        '''
        Sets and returns the current orientation
        - orientation = 1 .. 8 (default 1)
        If info is not 0 a string is returned.

        This value defines the actual spatial orientation of the slit system.
        In total, 8 orientations are possible (z==vertical, y==horizontal):

        The horizontal axis is the first axis and the vertical axis the second.

            h v
           ----
        1: +a+b (h==a,  v==b)  standard (default)
        2: -a+b (h==-a, v==b)  horizontal translation inverted
        3: +a-b (h==a,  v==-b  vertical translation inverted
        4: -a-b (h==-a, v==-b) both translations inverted
        5: +b+a (h==b,  v==a)  directions swapped
        6: +b-a (h==b,  v==-a) -90 deg rotation around x
        7: -b+a (h==-b, v==a)  +90 deg rotation around x
        8: -b-a (h==-b, v==-a) directions swapped and both translations inverted


        The directions follow the standard rules.
        x,y,z follow the right hand rule:
            x points along the beam,
            y points left (horizontal),
            z points upwards (vertical)

        The sign of each blade/gap motor must be configured in such a way that a
        movement in positive user direction opens the aperture.
        '''

        # get/set orientation using the front motor
        # all others must have the same orientation


        orientation_read=None
        if hasattr(self,"_tagged"):
            tmp=self._tagged.get(FRONT_TAG)
            if tmp is not None:
                front_mot=tmp[0]

                if hasattr(front_mot,"controller"):
                    if callable(getattr(front_mot.controller,'orientation', None)):
                        orientation_read=front_mot.controller.orientation(orientation)

        if info is None:
           info=0

        if info is not 0:
            if orientation_read is None:
                return "The orientation of slit {} cannot be changed".format(self.name)
            else:
                return "The orientation of slit {} is {}".format(self.name,orientation_read)
        else:
            return(orientation_read)


    def gap(self,shg=None,svg=None,**kwargs):
        """
        Move the gaps to shg (horizontal gap) and svg (vertical gap)
        """

        verbose=kwargs.get("verbose")
        if verbose is None:
            verbose=0
            kwargs['verbose']=verbose

        (shg,svg,sho,svo)=self.move(shg=shg,svg=svg,sho=None,svo=None,**kwargs)

        return (shg,svg)


    def offset(self,sho=None,svo=None,**kwargs):
        """
        Move the offsets to sho (horizontal offset) and svo (vertical offset)
        """

        verbose=kwargs.get("verbose")
        if verbose is None:
            verbose=0
            kwargs['verbose']=verbose

        (shg,svg,sho,svo)=self.move(shg=None,svg=None,sho=sho,svo=svo,**kwargs)

        return (sho,svo)


    def dump(self,**kwargs):
        '''
        Dumps the positions in the form <slit_name>.move(hg,vg,ho,vo);
        If the orientation can be changed it is dumped as well.
        '''

        info=kwargs.get("info")
        if info is None:
            info=1
        # don't pass info to move
        kwargs['info']=0

        out=""
        orientation=self.orientation()
        if orientation is None:
            orientation_string=None
        else:
            orientation_string="orientation={}".format(orientation)
            orientation_string=",{}".format(orientation_string.rjust(14))
        if info==0:
            move_out=self.move(**kwargs)
            if orientation_string is None:
                out+='{0}.move({1: 14.05}, {2: 14.05}, {3: 14.05}, {4: 14.05})'.\
                    format(self.name,move_out[0],move_out[1],move_out[2],move_out[3])
            else:
                out+='{0}.move({1: 14.05}, {2: 14.05}, {3: 14.05}, {4: 14.05}{5})'.\
                    format(self.name,move_out[0],move_out[1],move_out[2],move_out[3],orientation_string)

        else:
            move_out=self.move(info=1)
            out+='#  move({1:14}, {2:14}, {3:14}, {4:14})'.format(self.name,move_out[0].rjust(14),move_out[1].rjust(14),move_out[2].rjust(14),move_out[3].rjust(14))
            out+='\n'
            move_out=self.move(**kwargs)
            if orientation_string is None:
                out+='{0}.move({1: 14.05}, {2: 14.05}, {3: 14.05}, {4: 14.05})'.\
                    format(self.name,move_out[0],move_out[1],move_out[2],move_out[3])
            else:
                out+='{0}.move({1: 14.05}, {2: 14.05}, {3: 14.05}, {4: 14.05}{5})'.\
                    format(self.name,move_out[0],move_out[1],move_out[2],move_out[3],orientation_string)

        print(out) 


    def move(self,shg=None,svg=None,sho=None,svo=None,**kwargs):
        """
        Move the slit motors to the destination shg, svg, sho, svo
        """

        verbose=kwargs.get("verbose")
        if verbose is None:
            verbose=0
            kwargs['verbose']=verbose

        info=kwargs.get("info")
        if info is None:
            info=0
            kwargs['info']=info

        # check orientation
        orientation=kwargs.get("orientation")
        if orientation is not None:
            current_orientation=self.orientation()
            assert orientation==current_orientation, "Orientation mismatch: current {}, requested {}".format(current_orientation,orientation)

        dest=()

        # HG_TAG
        tmp=self._tagged.get(HG_TAG)
        if tmp is not None:
            shg_mot=tmp[0]
            shg_name=shg_mot.name
            if shg is not None:
                # move to destination
                dest+=(shg_mot,shg,)

        # VG_TAG
        tmp=self._tagged.get(VG_TAG)
        if tmp is not None:
            svg_mot=tmp[0]
            svg_name=svg_mot.name
            if svg is not None:
                # move to destination
                dest+=(svg_mot,svg,)

        # HO_TAG
        tmp=self._tagged.get(HO_TAG)
        if tmp is not None:
            sho_mot=tmp[0]
            sho_name=sho_mot.name
            if sho is not None:
                # move to destination
                dest+=(sho_mot,sho,)

        # VG_TAG
        tmp=self._tagged.get(VO_TAG)
        if tmp is not None:
            svo_mot=tmp[0]
            svo_name=svo_mot.name
            if svo is not None:
                # move to destination
                dest+=(svo_mot,svo,)

        # all motor movements must be started at the same time
        if len(dest)>0:
            qmove(*dest,**kwargs)

        if info==0:
            shg=shg_mot.position
            svg=svg_mot.position
            sho=sho_mot.position
            svo=svo_mot.position

            return (shg,svg,sho,svo)

        else:
            return (HG_TAG,VG_TAG,HO_TAG,VO_TAG)


class CalcAxis(AxisBase):
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        # calculated motor positions do not need to be buffered
        # => disable buffering
        self.settings.disable_cache("_set_position") #disable target pos
        self.settings.disable_cache("position") #disable current pos
        self.settings.disable_cache("state") #disbale the state

