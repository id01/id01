# -*- coding: utf-8 -*-

# ID01 BLISS git repo
#       authors:
#         Steven Leake, leake@esrf.fr
#
#       Created at: 05. Dec 18:37:00 CET 2021
#
#----------------------------------------------------------------------
# Description: 
#      functions for fitting alignment scans
#      inspiration from the bliss.scanning.scan_math
#
# 

from scipy import optimize
from scipy import stats

import numpy as np

def gauss(x, H, A, x0, sigma):
    return H + A * np.exp(-(x - x0) ** 2 / (2 * sigma ** 2))

def gauss_fit(x, y):
    mean = sum(x * y) / sum(y)
    sigma = np.sqrt(sum(y * (x - mean) ** 2) / sum(y))
    popt, pcov = optimize.curve_fit(gauss, x, y, p0=[min(y), max(y), mean, sigma])
    return popt

def linear_fit(x, y):
    res = stats.linregress(x, y)
    print(f"R-squared: {res.rvalue**2:.6f}")
    return res

