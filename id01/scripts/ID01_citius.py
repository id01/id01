import numpy as np
import pandas as pd
import math
import csv 
import subprocess
import time

server = 'lcb-citius'
user = 'citiusdevel'

#f = pd.read_csv(r'tmp_14_days/pad/20keV.txt', sep='\t')
#thick = np.asarray(f['Al filter thickness (µm)'])
#att_Al = np.asarray(f['transmission_nist'])
#filters = np.asarray(f['Hexa'])
#print(thick)
#print(att_Al)
#print(filters)

# data at 20keV with Al filters
thick=[8000,7000,6000,5000,4000,3000,2840,2720,2600,2480,2360,2240,2120,2000,1900,1840,1780,1720,1660,1600,1540,1480,1420,1360,1300,1240,1180,1120,1000,900,840,780,720,660,600,540,480,420,360,300,240,180,120,60,0]
filters=[192,112,96,80,64,48,46,44,42,40,38,36,34,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0]
transmission_Al=[0.000593,0.001502,0.003802,0.009624,0.024361,0.061663,0.071541,0.079975,0.089403,0.099943,0.111725,0.124897,0.139621,0.156081,0.17127,0.181084,0.191461,0.202432,0.214032,0.226297,0.239265,0.252975,0.267472,0.282799,0.299004,0.316138,0.334253,0.353407,0.39507,0.433518,0.45836,0.484625,0.512396,0.541758,0.572802,0.605626,0.64033,0.677023,0.715818,0.756837,0.800206,0.84606,0.894542,0.945803,1]
#print(filters)

# CITIUS FUNCTIONS

def scan_citius_linearity(filters=filters, folder='bm05_test'):
    
    #filters=[255,96,48,0]

    takeframe = 1
    
    eh1.close()
    
    if takeframe == 1:
        change_savedir(folder+'/Dark1')
        get_savedir()
        start_daq_save(1)
        while not check_writer_status():  # Waiting for saving data to be finished
            time.sleep(1)
            print('Done')
    
    eh1.open()
    time.sleep(1)

    print("start linearity scan of Citius detector ========")
    for filt in filters:
        
        print("filter %d"%(filt))
        umv(Cfilt,filt)
        gevent.sleep(1)
        print("linearity scan with citius")
        #filepath = folder + filename + '%dfilt'%filt 
        #citiustake(n_frames, filepath)
        if takeframe == 1:
            change_savedir(folder+'/%2.2f' % filt)
            get_savedir()
            start_daq_save(1)
            while not check_writer_status():  # Waiting for saving data to be finished
                time.sleep(1)
            print('Done')

        #gevent.sleep(60)
    eh1.close()  
    
    if takeframe == 1:
        change_savedir(folder+'/Dark2')
        get_savedir()
        start_daq_save(1)
        while not check_writer_status():  # Waiting for saving data to be finished
            time.sleep(1)
            print('Done')
    
    print("end of citius linearity scan ========")
    
def scan_citius_stab(folder='bm05_test'):

    takeframe = 1
    beam_on = 0
    
    eh1.close()
    
    for i in range(84):
        print(i)
        change_savedir(folder+'/%u'%time.time())
        get_savedir()
        start_daq_save(1)
        while not check_writer_status():  # Waiting for saving data to be finished
            time.sleep(1)
            print('Done')
    
        if beam_on ==1:
            eh1.open()
            timescan(1,srcur,npoints=598)
            eh1.close() 
            time.sleep(1)
        else:
            time.sleep(599)
    
    print("end of citius stability scan ========")
    
def scan_citius_delay(folder='bm05_test'):

    eh1.open()
    
    for delay in range(0,300,3):
        print('BCDU8 delay set to %u'%delay)
        bcdu8.chan.set('O1',period=53568000,width=100,delay=delay)
        
        time.sleep(1)
        change_savedir(folder+'/delay%u'%delay)
        get_savedir()
        start_daq_save(1)
        while not check_writer_status():  # Waiting for saving data to be finished
            time.sleep(1)
            print('Done')
    
    time.sleep(1)
    eh1.close()
    
    print("end of citius delay scan ========")
    
    
    
    
    
#def scan_diode_linearity(filters=filters, folder='/data/visitor/blc13924/bm05/20220720', filename='20keV_diode_linearity'):
def scan_diode_linearity(filters=filters):
    #global Cfilt
    eh1.open()
    print("start linearity scan of diode ========")
    for filt in filters:
        print("filter %d"%(filt))
        umv(Cfilt,filt)
        gev#ent.sleep(1)
        print("linearity scan with diode")
        sct(1, pico3)
        #gevent.sleep(60)
    eh1.close()         
    print("end of sequence ========")

#def citiustake(n_frames,filepath):

def check_writer_status():
    '''
    checks file writing status.
    :return:
        True if no more data queue to be written to files
    '''
    cmd = ['ssh',  '-x' ,'{0}@{1}'.format(user, server),
           'zcomclient', '-h', 'localhost', '-c', 'get/daq_writer/status', '--']
    output = subprocess.check_output(cmd).decode("utf-8")
    print(output)
    for out in output.split():
        if out.find('reply') == 0 and out == 'reply=0_0_0':
            print(out)
            return True
            break
    return False

def change_savedir(dirname):
    cmd = ['ssh', '-x' , '{0}@{1}'.format(user, server),
           'zcomclient', '-h', 'localhost', '-c', 'put/daq_save/dir', dirname]
    output = subprocess.check_output(cmd).decode("utf-8")
    print(output)

def get_savedir():
    cmd = ['ssh', '-x' , '{0}@{1}'.format(user, server),
           'zcomclient', '-h', 'localhost', '-c', 'get/daq_save/dir', '--']
    print(cmd)
    output = subprocess.check_output(cmd).decode("utf-8")
    print(output)

def start_daq_save(num_trains=1):
    cmd = ['ssh', '-x' , '{0}@{1}'.format(user, server),
           'zcomclient', '-h', 'localhost', '-c', 'put/daq_save/enable', '-1', '{0:d}'.format(num_trains)]
    print(cmd)
    output = subprocess.check_output(cmd).decode("utf-8")
    print(output)
    time.sleep(1)

def set_exposure_width(width):
    # Disabling the external trigger first (by switching to disabled internal trigger)
    cmd = ['ssh', '{0}@{1}'.format(user, server),
           'python3', '/home/citiusdevel/misc_tools/ESRF_2022July/set_trigger.py', 'OFF', 'INT']
    output = subprocess.check_output(cmd).decode("utf-8")
    print(output)
    time.sleep(1)
    
    cmd = ['ssh', '{0}@{1}'.format(user, server),
           'python3', '/home/citiusdevel/misc_tools/ESRF_2022July/set_exposure_time.py', '{0:d}'.format(width)]
    output = subprocess.check_output(cmd).decode("utf-8")
    print(output)
    time.sleep(1)

    # Enabling the external trigger
    cmd = ['ssh', '{0}@{1}'.format(user, server),
           'python3', '/home/citiusdevel/misc_tools/ESRF_2022July/set_trigger.py', 'OFF', 'EXT']
    output = subprocess.check_output(cmd).decode("utf-8")
    print(output)
    time.sleep(3)


def scan_citius_exposure(folder='bm05_test',trains =10):

    #eh1.open()
    #time.sleep(1)
    #bcdu8.chan.set('O1',period=53568000,width=100,delay=0)
    
    #print('Setting exposure to %u'%exposure)
    
    
    #bcdu8.chan.set('O1','off')
    #time.sleep(1)
    #set_exposure_width(exposure)
    time.sleep(1)
    #bcdu8.chan.set('O1',period=53568000,width=100,delay=0)
    #time.sleep(1)
    change_savedir(folder)  
    get_savedir()
    start_daq_save(trains) # no trains 
    while not check_writer_status():  # Waiting for saving data to be finished
        time.sleep(1)
        print('Done')
    
    time.sleep(1*trains)
    #eh1.close()
    
    #print("end of citius delay scan ========")


# 10 trains of 2000  which is about 1 second
#
#scan_citius_exposure(folder= "20221011/ID01/ptycho/",trains=10)
#scan_citius_exposure(folder= "20221011/ID01/ptycho/",trains=1)
