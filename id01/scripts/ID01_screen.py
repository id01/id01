import time
import numpy as np

from ansi.colour import fg, bg
from ansi.colour.fx import reset

from bliss import current_session
from bliss.config.beacon_object import BeaconObject


def screenin(*screens):
    """
    Move a beamline component into the beam.
    screenin(att2_MP,1)
    screenin(att2_MP,"position_string")
    """

    if isinstance(screens[-1],np.int) or isinstance(screens[-1],np.str):
        target = screens[-1]   # last screen is the target position
        screens = screens[:-1] # the rest should be screens
    else:
        target = None

    for screen in screens:
        # for EBVS
        if screen.name.count("ebv")>0:
            _ebvin(screen)
        # for MP objects  (ATT11,ATT12, ATT13, ATT2, QBPM, FS1, EXP1, micro)
        elif screen.name.count("_MP"):
            screenlabels = list(screen.targets_dict.keys())
            if isinstance(target,np.int):
                if target > len(screenlabels)-1:
                    print(f"OOPS position does not exist choose from:")
                    for i,label in enumerate(screenlabels):
                        print(i,":",label)
                    break                    
                label = screenlabels[target]

            elif isinstance(target,np.str):
                if screenlabels.count(target)>0:
                    label = target
                else:
                    print(f"OOPS position '{target}' is not one of:")
                    for label in screenlabels:
                        print(label)
                    break

            
            print(f"Moving to '{label}' of {screen.name}")
            screen.move(label)


        # fluo1
        elif screen.name.count("fluo1"):
            screen.screen_in()
        
        # opt1
        elif screen.name.count("opt1"):
            screen.screen_in()            
        # fs2

        # filter

        # micro
        #if screen.name == "micro_MP":
        #    screen.screen_in()
        #    wait4micro = True
        #    while wait4micro: 
        #        if screen.state == "READY":
        #            wait4micro = False
        #        else:
        #            time.sleep(0.05)
        # led


def screenout(*screens):
    """
    Move a beamline component out of the beam.
    """
    for screen in screens:
        # for EBVS
        if screen.name.count("ebv"):
            _ebvout(screen)
        elif screen.name.count("_MP") or screen.name.count("fluo1") or screen.name.count("opt1"):
            screen.screen_out()  
        else:
            print("screen {screen} is not available")

        # filter

        # led


def _ebvin(screen):
    """
    method for moving EBV in
    """
    print(f"{screen.name} status is {screen.screen_status}")

    if screen.screen_status == "OUT":                                                                                         
        # for ebvs
        try:
            screen.screen_in()
            wait4ebv = True
            while wait4ebv: 
                if screen.screen_status == "IN":
                    wait4ebv = False
                else:
                    time.sleep(0.05)
                
            print(f"screen state is now {screen.screen_status}")
        except:
            print(f"failed to move {screen.name} in...")
    else:
        print(f"{screen.name} is already in")


def _ebvout(screen):
    """
    method for moving EBV out
    """
    print(f"{screen.name} status is {screen.screen_status}")

    if screen.screen_status == "IN":
        # for ebvs
        try:
            screen.screen_out()
            wait4ebv = True
            while wait4ebv: 
                if screen.screen_status == "OUT":
                    wait4ebv = False
                else:
                    time.sleep(0.05)
            print(f"screen state is now {screen.screen_status}")
        except:
            print(f"failed to move {screen.name} out...")
    else:
        print(f"{screen.name} is already out")


def screenshow(*screens):
    # TODO default show all screens - pull all objects from current session and show them
    if not len(screens):
        screens=[]
        for screen in ["att11_MP","att12_MP","att13_MP","bv1h_MP","att2_MP","qbpm_MP","exp1_MP","micro_MP","fs1_MP","fs2_MP","ebv1","ebv2","ebv3","ebv4","opt1_screen","fluo1_screen"]:
            screens.append(current_session.env_dict[screen]) 


    for screen in screens:
        
        # for ebvs        
        if screen.name.count("ebv")>0:

            state = screen.screen_status
            if state == 'IN':
                msg = (bg.green, fg.white, state, reset)
            elif state == 'OUT':
                msg = (bg.red, fg.white, state, reset)
            else:
                msg = (bg.brown, fg.white, state, reset)

            msg = ''.join(map(str, msg))

            status_str = f"{screen.name+' status:': <30} {msg}"
            print(status_str)

        elif screen.name.count("opt1")>0 or screen.name.count("fluo1")>0:

            state = screen.state()
            if state == 'IN':
                msg = (bg.green, fg.white, state, reset)
            elif state == 'OUT':
                msg = (bg.red, fg.white, state, reset)
            else:
                msg = (bg.brown, fg.white, state, reset)

            msg = ''.join(map(str, msg))
            status_str = f"{screen.name+' status:': <30} {msg}"
            print(status_str)

        elif screen.name.count("_MP"):
            state = screen.position
            if state == 'screen_out':
                msg = (bg.red, fg.white, f"OUT", reset)
                msgt = (bg.red, fg.white, f"screen_out", reset)
            elif state == 'set_dial_on_lim':
                msg = (bg.cyan, fg.white, f"LIM", reset)
                msgt = (bg.cyan, fg.white, f"set_dial_on_lim", reset)
            elif state == 'parked':
                msg = (bg.cyan, fg.white, f"OUT", reset)
                msgt = (bg.cyan, fg.white, f"parked", reset)
            else:
                msg = (bg.green, fg.white, f"IN ({state})", reset)
                msgt = (bg.green, fg.white, f"{state}", reset)

            msg = ''.join(map(str, msg))
            msgt = ''.join(map(str, msgt))

            ss="["
            for dic in screen.positions_list:
                if dic["label"] == screen.position:
                    ss+=msgt+', '
                else:
                    ss+=dic["label"]+', '
            ss+="]"

            status_str = f"{screen.name+' status:': <30} {msg}"

            print(f"{status_str: <60} {ss}")

        # filter

        # led


# other screen objects

class opt1_MP(BeaconObject):

    def __init__(self,name,config):
        
        super().__init__(config,name,share_hardware=False)
        self.name = "opt1_screen"

    def screen_in(self,):
        if self.device.get("opt1PushIn")==1 and self.device.get("opt1PushOut")==0:
            print("fluo1 is already in - do nothing")
        else:
            if self.device.get("fluo1PushIn")==1 or self.device.get("fluo1PushOut")==0:
                print(f"fluo1 is in - 5 seconds to remove..")
                self.device.set("fluo1PushIn",0)
                self.device.set("fluo1PushOut",1)
                time.sleep(5)

            self.device.set("opt1PushIn",1)
            self.device.set("opt1PushOut",0)

    def screen_out(self,):
        if self.device.get("opt1PushIn")==0 and self.device.get("opt1PushOut")==1:
            print(f"opt1 is already OUT - do nothing")
        else:        
            self.device.set("opt1PushIn",0)
            self.device.set("opt1PushOut",1)
            time.sleep(5)

    def state(self,verbose = False):
        if self.device.get("opt1PushIn")==0 and self.device.get("opt1PushOut")==1:
            state = "OUT"
        elif self.device.get("opt1PushIn")==1 and self.device.get("opt1PushOut")==0:
            state = "IN"
        else:
            state = "UNKNOWN"
            for item in ["opt1PushIn","opt1PushOut"]:
                print(f"{item} : {self.device.get(item)}")

        if verbose:
            print(f"state: {state}")
        return state

class fluo1_MP(BeaconObject):

    def __init__(self,name,config):
        
        super().__init__(config,name,share_hardware=False)
        self.name = "fluo1_screen"

    def screen_in(self,):
        if self.device.get("fluo1PushIn")==1 and self.device.get("fluo1PushOut")==0:
            print("fluo1 is already IN - do nothing")
        else:
            if self.device.get("opt1PushIn")==1 or self.device.get("opt1PushOut")==0:
                print(f"opt1 is in - 5 seconds to remove..")
                self.device.set("opt1PushIn",0)
                self.device.set("opt1PushOut",1)
                time.sleep(5)

            self.device.set("fluo1PushIn",1)
            self.device.set("fluo1PushOut",0)

    def screen_out(self,):
        # add check for out
        if self.device.get("fluo1PushIn")==0 and self.device.get("fluo1PushOut")==1:
            print(f"fluo1 is already OUT - do nothing")
        else:
            self.device.set("fluo1PushIn",0)
            self.device.set("fluo1PushOut",1)
            time.sleep(5)

    def state(self, verbose = False):
        if self.device.get("fluo1PushIn")==0 and self.device.get("fluo1PushOut")==1:
            state = "OUT"
        elif self.device.get("fluo1PushIn")==1 and self.device.get("fluo1PushOut")==0:
            state = "IN"
        else:
            state = "UNKNOWN"
            for item in ["fluo1PushIn","fluo1PushOut"]:
                print(f"{item} : {self.device.get(item)}")
        if verbose:
            print(f"state: {state}")
        return state