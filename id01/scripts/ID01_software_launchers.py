import subprocess

def Camview():
    """Open Camview"""

    _launch_camview()


def _launch_camview():
    args = f"/users/blissadm/local/id01sware/bin/CamView"
    return subprocess.Popen(args)
