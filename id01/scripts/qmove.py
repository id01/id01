# -*- coding: utf-8 -*-
#
# ID01 BLISS git repo
#       authors:
#         Peter Boesecke, boesecke@esrf.fr
#
#----------------------------------------------------------------------
# Description:
#    Quiet move functions, complementing existing verbose move functions
#
#     qmv
#     qmvr
#     qmvd
#     qmvdr
#     qmove
#     qumv
#     qumvr
#     qumvd
#     qumvdr
#     qumove
#
# Example:
#    mv(t1hg,1,t1ho,11)                                                                                                                           
#    Moving t1hg from 2.5 to 1
#    Moving t1ho from 10 to 11
#    Moving t1u from 12.55 to 12.55
#    Moving t1d from -11.05 to -11.05
#    Moving t1f from 11.25 to 11.5
#    Moving t1b from -8.75 to -10.5
#    Moving sim_t1bf from 11.25 to 11.5
#    Moving sim_t1bb from -8.75 to -10.5
#
#    qmv(t1hg,1,t1ho,11)                                                                                                                          
#
# History:
#    2023-01-13: Seb,PB  updating arguments of _move and _umove for 
#                        BLISS version 1.9.0 and python V3.9:
#                        _move(*args, **kwargs)
#                        => _move(grouped(args,2), **kwargs)

from bliss.common.utils import (
    modify_annotations,
    typecheck_var_args_pattern,
    custom_error_msg,
    TypeguardTypeError
)

from bliss.common.types import (
    Scannable,
    _float
)

from bliss.common.utils import grouped
from bliss.common.logtools import disable_user_output

from bliss.common.standard import _move
from bliss.shell.standard import _umove

import logging
#logging.basicConfig(level=logging.DEBUG)
#logging.basicConfig(level=logging.WARNING)
#logging.debug('This will get logged')
#logging.warning('This will get logged')

_log=logging.getLogger("id01.scripts.qmove")
#The log level can be set in the bliss session after import logging
#and defining _log as in the above command.
#_log.setLevel("DEBUG")

def qmv(*args):
    qmove(*args)

def qmvr(*args):
    with disable_user_output():
        _move(grouped(args,2), relative=True)

def qmvd(*args):
    with disable_user_output():
        _move(grouped(args,2), relative=False, dial=True)

def qmvdr(*args):
    with disable_user_output():
        _move(grouped(args,2), relative=True, dial=True)

def qmove(*args, **kwargs):
    verbose=kwargs.get("verbose")
    if verbose is None: verbose=0
    if verbose != 0:
        _move(list(grouped(args,2)), **kwargs)
    else:
        with disable_user_output():
            _move(list(grouped(args,2)), **kwargs)

def qumove(*args, **kwargs):
    verbose=kwargs.get("verbose")
    if verbose is None: verbose=0
    if verbose != 0:
        _umove(grouped(args,2), **kwargs)
    else:
        with disable_user_output():
            _umove(grouped(args,2), **kwargs)

@custom_error_msg(
    TypeguardTypeError,
    "intended usage: umv(motor1, target_position_1, motor2, target_position_2, ... )",
    new_exception_type=RuntimeError,
    display_original_msg=False,
)
@modify_annotations({"args": "motor1, pos1, motor2, pos2, ..."})
@typecheck_var_args_pattern([Scannable, _float])
def qumv(*args):
    """
    Moves given axes to given absolute positions providing updated display of
    the motor(s) position(s) while it(they) is(are) moving.
    Hiding additional motor information.

    Arguments are interleaved axis and respective absolute target position.
    """
    with disable_user_output():
        _umove(grouped(args,2))

@custom_error_msg(
    TypeguardTypeError,
    "intended usage: umvr(motor1, relative_displacement_1, motor2, relative_displacement_2, ... )",
    new_exception_type=RuntimeError,
    display_original_msg=False,
)
@modify_annotations({"args": "motor1, rel. pos1, motor2, rel. pos2, ..."})
@typecheck_var_args_pattern([Scannable, _float])
def qumvr(*args):
    """
    Moves given axes to given relative positions providing updated display of
    the motor(s) position(s) while it(they) is(are) moving.
    Hiding additional motor information.

    Arguments are interleaved axis and respective relative target position.
    """
    with disable_user_output():
        _umove(grouped(args,2), relative=True)

@custom_error_msg(
    TypeguardTypeError,
    "intended usage: umvd(motor1, target_position_1, motor2, target_position_2, ... )",
    new_exception_type=RuntimeError,
    display_original_msg=False,
)
@modify_annotations({"args": "motor1, pos1, motor2, pos2, ..."})
@typecheck_var_args_pattern([Scannable, _float])
def qumvd(*args):
    """
    Moves given axes to given absolute dial positions providing updated display of
    the motor(s) user position(s) while it(they) is(are) moving.
    Hiding additional motor information.

    Arguments are interleaved axis and respective absolute target position.
    """
    with disable_user_output():
        _umove(grouped(args,2), dial=True)


@custom_error_msg(
    TypeguardTypeError,
    "intended usage: umvdr(motor1, relative_displacement_1, motor2, relative_displacement_2, ... )",
    new_exception_type=RuntimeError,
    display_original_msg=False,
)
@modify_annotations({"args": "motor1, rel. pos1, motor2, rel. pos2, ..."})
@typecheck_var_args_pattern([Scannable, _float])
def qumvdr(*args):
    """
    Moves given axes to given relative dial positions providing updated display of
    the motor(s) user position(s) while it(they) is(are) moving.
    Hiding additional motor information.

    Arguments are interleaved axis and respective relative target position.
    """

    with disable_user_output():
        _umove(grouped(args,2), relative=True, dial=True)
