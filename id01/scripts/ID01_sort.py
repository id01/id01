from bliss import setup_globals
from bliss import current_session
from bliss.common.measurementgroup import MeasurementGroup


from scipy.interpolate import griddata
from matplotlib.colors import LogNorm
import numpy as np
import matplotlib.pyplot as plt
import h5py as h5
import re
import os.path as osp

from id01.process.interactive import *
from id01.process.h5utils import *
import click
#from id01.scripts.ID01_sxdmutils import get_scans_title_str_hdf5


## TODO autopep8 install on bliss_dev
## fscan_align
## ascan_align,axscan
## dscan_align,dxscan
## linup, pointscan
## make lookup_inspect
## make mesh_inspect

def lookup_plot(scan_no, counter, xname = None, yname = None, filename = None):
    """
    plot x,y intensity data from a lookupscan e.g. ptycho spiral
    """
    
    x,y,intensity, title = get_lookupscan(  scan_no, 
                                            counter, 
                                            xname = xname, 
                                            yname = yname, 
                                            filename = filename,
                                            prefix = "")
    
    xlims = (x.min(),x.max())
    ylims = (y.min(),y.max())
    
    xi = np.linspace(xlims[0],xlims[1],int(np.sqrt(x.shape))*2)
    yi = np.linspace(ylims[0],ylims[1],int(np.sqrt(x.shape))*2)
    zi = griddata((x, y), intensity, (xi[None,:], yi[:,None]), method='cubic')
    
    plt.figure()
    plt.pcolormesh(xi,yi,zi,norm=LogNorm(),cmap="Greys") 
    plt.colorbar() # draw colorbar
    plt.title("#%i : "%scan_no+title)
    plt.xlabel(xname)
    plt.ylabel(yname)
    plt.show()
    
def mesh_plot(scan_no, counter, xname = None, yname = None, filename = None, cmap="Greys"):
    """
    plot x,y intensity data from a mesh scan
    """
    x,y,intensity, title = get_meshscan(scan_no, 
                                        counter, 
                                        xname = xname, 
                                        yname = yname, 
                                        filename=filename,
                                        prefix="")
    
    plt.figure()
    plt.pcolormesh(x,y,intensity,norm=LogNorm(), cmap=cmap) 
    plt.colorbar()
    plt.title("#%i : "%scan_no+title)
    plt.xlabel(xname)
    plt.ylabel(yname)
    plt.show()
    
def lookup_align(nb = -1,counters = [], filename = None,  mynorm = 'log', move_motors = False):
    """
    counters: the counters to plot
    filename: hdf5 filename
    nb: scan number (accepts list -n nomenclature)
    
    TODO: counters in hdf5 do not match counters in measurement group e.g _sum
        
    TODO make channels a counters list or MG or something more sensible
    the counter channels names varies between hdf5 and SCANS object which is nasty
    
    TODO Add check for other motor positions - to make sure positioners havent changed
    
    """
    # take the live scan if not defined
    if filename is None: 
        globals_dict = current_session.env_dict
        filename = globals_dict["SCAN_SAVING"].filename
    
    #negative index pulls from the list in hdf5 file like np slicing
    if nb<0:
        nb = lookup_scans[nb]
         
    h5path_counters = "/%i.1/measurement/"%nb
    h5path_title = "/%i.1/title"%nb
    counters_names = counters
    
    scan_title = get_dataset_hdf5(h5path_title, filename)
            
    if scan_title.count("lookup")!=1:
        print("warning not lookup data")
        # check scans in hdf5 file                   
        mesh_scans = get_scans_title_str_hdf5(filename, "mesh")
        
    h5path_title = "/%i.1/title"%(nb) 
    title_data = get_dataset_hdf5(h5path_title, filename)

    #parse mot0, mot1 if xname/yname = None from the title_data
    motnames = re.findall(r'\((.*?,.*?)\)',title_data)[0].split(",")
    m0name = motnames[0]
    m1name = motnames[1]

    counters_keys = get_scan_counters_dict_hdf5(nb, filename, counters = counters_names)
    
    counters = None
    for i,counter in enumerate(counters_keys.keys()):
        #print(i,counter)
        if counters is None:

            x,y,intensity, title = get_lookupscan(  nb, 
                                                    counter, 
                                                    xname = m0name, 
                                                    yname = m1name, 
                                                    filename = filename,
                                                    prefix="")
            
            xlims = (x.min(),x.max())
            ylims = (y.min(),y.max())
            xi = np.linspace(xlims[0],xlims[1],int(np.sqrt(x.shape))*2)
            yi = np.linspace(ylims[0],ylims[1],int(np.sqrt(x.shape))*2)

            zi = griddata((x, y), intensity, (xi[None,:], yi[:,None]), method='cubic')

            (m0s,m0e) = (x.min(),x.max())
            (m1s,m1e) = (y.min(),y.max())

            counters = np.zeros((len(counters_keys.keys()),xi.shape[0],yi.shape[0]))
            zi[np.isnan(zi)] = 0
            counters[i,:,:] = zi
        else:
            try:

                x,y,intensity, title = get_lookupscan(  nb, 
                                                        counter, 
                                                        xname = m0name, 
                                                        yname = m1name, 
                                                        filename = filename,
                                                        prefix="")

                xlims = (x.min(),x.max())
                ylims = (y.min(),y.max())
                xi = np.linspace(xlims[0],xlims[1],int(np.sqrt(x.shape))*2)
                yi = np.linspace(ylims[0],ylims[1],int(np.sqrt(x.shape))*2)

                zi = griddata((x, y), intensity, (xi[None,:], yi[:,None]), method='cubic')
                zi[np.isnan(zi)]=0
                counters[i,:,:] = zi              
            except:
                print("Failed for channel: %s"%counter)
    #import pdb
    #pdb.set_trace()                
    # look at them
    try:
        fig = plt.figure(figsize=(9,6))
        ax = fig.add_subplot(111)
        fig.suptitle(" File: %s \n Scan: %i"%(filename,nb))
        tracker = GenericIndexTracker(  ax, 
                                        data = counters, 
                                        norm = mynorm,
                                        exit_onclick = True, 
                                        move_motors = move_motors)
        ax.set_xlabel(m0name)
        ax.set_ylabel(m1name)
        tracker.set_extent(m0s,m0e,m1s,m1e)
        tracker.set_axes_properties(title=counters_names)
        plt.show()
    except:
        print("An error occurred")

    
def mesh_align(nb = -1,counters = [], filename = None,  mynorm = 'log', move_motors = False):
    """
    counters: the counters to plot
    filename: hdf5 filename
    nb: scan number (accepts list -n nomenclature)
    
    TODO: counters in hdf5 do not match counters in measurement group e.g _sum
        
    TODO make channels a counters list or MG or something more sensible
    the counter channels names varies between hdf5 and SCANS object which is nasty
    
    TODO Add check for other motor positions - to make sure positioners havent changed
    
    """
    # take the live scan if not defined
    if filename is None: 
        globals_dict = current_session.env_dict
        filename = globals_dict["SCAN_SAVING"].filename
    
    #negative index pulls from the list in hdf5 file like np slicing
    if nb<0:
        nb = lookup_scans[nb]
         
    h5path_counters = "/%i.1/measurement/"%nb
    h5path_title = "/%i.1/title"%nb
    counters_names = counters
    
    scan_title = get_dataset_hdf5(h5path_title, filename)
            
    if scan_title.count("mesh")!=1:
        print("warning not mesh data")
        # check scans in hdf5 file                   
        mesh_scans = get_scans_title_str_hdf5(filename, "mesh")
        
    m0name,m0s,m0e,m0_pts, m1name,m1s,m1e,m1_pts,exposure = scan_title.split(" ")[1:]
    m0s,m0e,m0_pts,m1s,m1e,m1_pts,exposure = float(m0s),float(m0e),int(m0_pts),float(m1s),float(m1e),int(m1_pts),float(exposure)
    m0name,m1name = m0name.strip(), m1name.strip() 

    counters_keys = get_scan_counters_dict_hdf5(nb,filename,counters=counters_names,prefix="")
    
    counters = np.zeros((len(counters_keys),m1_pts+1,m0_pts+1))
    for i,counter in enumerate(counters_keys):
        try:
            tmp_path = osp.join(h5path_counters,counter)
            counters[i,:,:] = get_dataset_hdf5(tmp_path, filename).reshape(m1_pts+1,m0_pts+1)                
        except:
            print("Failed for channel: %s"%counter)

    # look at them
    try:
        fig = plt.figure(figsize=(9,6))
        ax = fig.add_subplot(111)
        fig.suptitle(" File: %s \n Scan: %i"%(filename,nb))
        tracker = GenericIndexTracker(  ax, 
                                        data = counters, 
                                        norm = mynorm, 
                                        exit_onclick = True, 
                                        move_motors = move_motors)
        ax.set_xlabel(m0name)
        ax.set_ylabel(m1name)
        tracker.set_extent(m0s,m0e,m1s,m1e)
        tracker.set_axes_properties(title = counters_names)
        plt.show()
    except:
        print("An error occurred")
    
    
def get_lookupscan(scan_no, counter, xname = None, yname = None, filename = None, prefix=""):
    """
    return a previously measure lookupscan for use in the BLISS terminal
    """
    
    if filename is None: 
        globals_dict = current_session.env_dict
        filename = globals_dict["SCAN_SAVING"].filename

    # scans in hdf5 file                   
    #lookup_scans = get_scans_title_str_hdf5(filename, "lookup")

    h5path_title = "/%s%i.1/title"%(prefix,scan_no) 
    title_data = get_dataset_hdf5(h5path_title, filename)

    #parse mot0, mot1 if xname/yname = None from the title_data
    motnames = re.findall(r'\((.*?,.*?)\)',title_data)[0].split(",")
    #print(motnames)
    if xname == None:
        xname = motnames[0]
    if yname == None:
        yname = motnames[1]
    
    # get counters x,y,intensity    
    h5path_counter_x = "/%s%i.1/measurement/%s"%(prefix,scan_no,xname)
    h5path_counter_y = "/%s%i.1/measurement/%s"%(prefix,scan_no,yname)
    h5path_counter = "/%s%i.1/measurement/%s"%(prefix,scan_no,counter)

    counter_data_x = get_dataset_hdf5(h5path_counter_x, filename)
    counter_data_y = get_dataset_hdf5(h5path_counter_y, filename)
    counter_data = get_dataset_hdf5(h5path_counter, filename)
        
    if title_data.count("lookup") == 0:
        raise ValueError("Scan %i is not a lookupscan dataset"%scan_no)

    return counter_data_x, counter_data_y, counter_data, title_data
    
    
def get_meshscan(scan_no, counter, xname = None, yname = None, filename=None,prefix=""):
    """
    return a previously measure mesh for use in the BLISS terminal
    """
    
    if filename is None: 
        globals_dict = current_session.env_dict
        filename = globals_dict["SCAN_SAVING"].filename

    # scans in hdf5 file                   
    #mesh_scans = get_scans_title_str_hdf5(filename, "mesh")

    h5path_title = "/%s%i.1/title"%(prefix,scan_no) 
    title_data = get_dataset_hdf5(h5path_title, filename)
    #parse mot0, mot1 if xname/yname = None from the title_data
    params = title_data.split(" ")

    if xname == None:
        xname = params[1]
    if yname == None:
        yname = params[5]
        
    print("motors: ",xname, yname)
    
    x_pts = int(params[4])+1
    y_pts = int(params[8])+1
    # get counters x,y,intensity    
    h5path_counter_x = "/%s%i.1/measurement/%s"%(prefix,scan_no,xname)
    h5path_counter_y = "/%s%i.1/measurement/%s"%(prefix,scan_no,yname)
    h5path_counter = "/%s%i.1/measurement/%s"%(prefix,scan_no,counter)

    counter_data_x = get_dataset_hdf5(h5path_counter_x, filename).reshape(y_pts,x_pts)
    counter_data_y = get_dataset_hdf5(h5path_counter_y, filename).reshape(y_pts,x_pts)
    counter_data = get_dataset_hdf5(h5path_counter, filename).reshape(y_pts,x_pts)
        
    if title_data.count("mesh") == 0:
        raise ValueError("Scan %i is not a mesh scan dataset"%scan_no)

    return counter_data_x, counter_data_y, counter_data, title_data


from skimage import  registration
from skimage.registration import phase_cross_correlation
from skimage.feature import peak_local_max

def get_shift_between_meshscans(scan0_nb,counter0_name,scan1_nb,counter1_name,filename=None):
    """
    autocorrelation - use get_KMAP function
    """
    # TODO
    
    def get_shift_between_images_CC(image0, image1, upsample_factor=1000):
        """
        calculate the sub pixel shift between two images using sckit-image
        """
        cc, error, diffphase = registration.phase_cross_correlation(image0,image1, upsample_factor=upsample_factor)
        return cc,error,diffphase
        
    counter0_data_x, counter0_data_y, counter0_data, title0_data = get_meshscan(scan0_nb, counter0_name,  filename=filename)
    counter1_data_x, counter1_data_y, counter1_data, title1_data = get_meshscan(scan1_nb, counter1_name,  filename=filename)
    #breakpoint()
    #get_shift_between_images_CC(np.log(counter0_data), np.log(counter1_data), upsample_factor=1000)
    #get_shift_between_images_CC(counter0_data, np.roll(counter_data,1), upsample_factor=1000)
    cc,error,diffphase = get_shift_between_images_CC(counter0_data, counter1_data, upsample_factor=1000)  
    
    # TODO add calculated shift in normalised units
    x_step = np.mean(counter0_data_x[:,1:]-counter0_data_x[:,:-1])
    
    y_step = np.mean(counter0_data_y[1:,:]-counter0_data_y[:-1,:])
    return cc,error,diffphase,cc[0]*x_step,cc[1]*y_step



#from id01.scripts.ID01_sort import lookup_align, mesh_align
#lookup_align(1,counters = ['eiger2M_roi1', 'eiger2M_roi1_max', 'eiger2M_roi2', 'eiger2M_roi2_max'], filename="/data/visitor/mi1398/id01/si_indented/si_indented_0023/si_indented_0023.h5")
#lookup_plot(3,"eiger2M_roi2",filename = "/data/visitor/mi1398/id01/si_indented/si_indented_0006/si_indented_0006.h5") 
#mesh_plot(6,"mpx1x4_roi2",filename = "/data/visitor/mi1398/id01/si_indented/si_indented_0004/si_indented_0004.h5") 


from id01.process.microscope_drift_calibration import get_relative_shift

def hexapod_drift_correction(eta_start, eta_end, phi_start, phi_end):
    """
    estimate offset in the piezo pix/piy due to orientational change of the hexapod
    """
    LUT_fn = '/data/id01/inhouse/leake/data_analysis/id01projects/20190211_hexapod_characterisation/camview/experiment_6/LUT_20190212.h5'
    motion = get_relative_shift(LUT_fn,np.array([phi_start,eta_start]),np.array([phi_end,eta_end]))
    
    #m0 = current_session.env_dict["pix"]
    #m1 = current_session.env_dict["piy"]
    
    print("pix",":",-motion[0],"  ...   ","piy",":",-motion[1])
    print(f"umvr(pix,{-motion[0]},piy,{-motion[1]})")
    #umvr(m0,-motion[0],m1,-motion[1])


def id01_plotselect_sxdm():
    """
    Guess the needed
    """
    active_mg = setup_globals.ACTIVE_MG
    available = active_mg.available
    enabled = active_mg.enabled


    dlgs = [
        user_dialog.UserCheckBox(label=name, defval=name in enabled)
        for name in enabled
    ]
    nb_counter_per_column = 18
    cnts = [
        user_dialog.Container(dlgs[i : i + nb_counter_per_column], splitting="h")
        for i in range(0, len(dlgs), nb_counter_per_column)
    ]

    dialog = pt_widgets.BlissDialog(
        [cnts],
        title=f"SXDM counters to plot: **{active_mg.name}**  "
    )
    rval = dialog.show()
    if rval:
        selected = set(
            [
                cnt_name
                for cnt_name, enable_flag in zip(enabled, rval.values())
                if enable_flag
            ]
        )
        currently_enabled = set(enabled)
        counters2plot = selected
    return counters2plot


def id01_plotselect():
    """
    Guess the needed
    """
    active_mg = setup_globals.ACTIVE_MG
    available = active_mg.available
    enabled = active_mg.enabled


    dlgs = [
        user_dialog.UserCheckBox(label=name, defval=name in enabled)
        for name in enabled
    ]
    nb_counter_per_column = 18
    cnts = [
        user_dialog.Container(dlgs[i : i + nb_counter_per_column], splitting="h")
        for i in range(0, len(dlgs), nb_counter_per_column)
    ]

    dialog = pt_widgets.BlissDialog(
        [cnts],
        title=f"SXDM counters to plot: **{active_mg.name}**  "
    )
    rval = dialog.show()
    if rval:
        selected = set(
            [
                cnt_name
                for cnt_name, enable_flag in zip(available, rval.values())
                if enable_flag
            ]
        )
        currently_enabled = set()
        counterSelected = selected
    plotselect(counterSelected)



def get_scan_motors_pos(scan_no, axeslist = None, point_nb = 0, filename = None, execute_motion = False):
    """
    return all values for axes in axislist in scan_no
    """
    
    if filename is None: 
        globals_dict = current_session.env_dict
        filename = globals_dict["SCAN_SAVING"].filename

    if axeslist is None:
        tmp_axeslist = get_scan_motor_names(scan_no, filename)
        # need axis objects...
        axeslist=[]
        for axisname in tmp_axeslist:
            try:
                axeslist.append(current_session.env_dict[axisname])
            except:
                print(f"axis ({axisname}) is unavailable in the current session")

    motion = ()
    print(f"#########################\nfilename: {filename} \nscan_no:  {scan_no}")
    for axis in axeslist:
        pos = get_scan_motor_hdf5(scan_no,filename,axis.name,prefix="")
        if type(pos) == np.ndarray:
            pos = pos[point_nb]
            print(f" {axis.name} : {pos} [scan point:{point_nb}]")
        else:
            print(f" {axis.name} : {pos}")

        motion += (axis,float(pos))

    if execute_motion:
        print("motor position deltas:")
        for axis in axeslist:
            pos = get_scan_motor_hdf5(scan_no,filename,axis.name,prefix="")
            if type(pos) == np.ndarray:
                pos = pos[point_nb]
            delta = axis.position - pos
            print(f" {axis.name} : {delta}")

        ans = click.confirm("\nDo you want to move the motors?")
        if ans:
            umv(*motion)
    else:
        return motion

def get_scan_motor_names(scan_no, filename,prefix=""):
    """
    return a list of scan motors in hdf5 file
    """
    h5path = "/%s%i.1/instrument/positioners"%(prefix,scan_no)

    
    with h5.File(filename,'r') as h5f:
        positioners = h5f[h5path]
        list_positionersnames = list(positioners.keys())
        
    return list_positionersnames


from id01.process.h5utils import *
import id01.scripts.ID01_positionInstances as bl_posInst
from id01.process.interactive import *
import matplotlib.pyplot as pl


def mesh_inspect_counters(channels = ["",], filename = None, nb = -1,  mynorm = 'linear', positionsGroup = True):
    """
    channels: the counters to plot
    filename: hdf5 filename
    nb: scan number 
    
    TODO: counters in hdf5 do not match counters in measurement group e.g _sum
    
    note found an upper limit on memory - ask SEB where this comes from 
    needa check if sxdm is more than 1000 points should use the hdf5 file.
    
    TODO this uses the input values not the raw adc for plotting
    pcolormesh could be used here??
    
    TODO make channels a counters list or MG or something more sensible
    the counter channels names varies between hdf5 and SCANS object which is nasty
    
    TODO Add check for other motor positions - to make sure positioners havent changed
    
    TODO     #toggle button for log/linear + slide bar for colorbar?

    """


    # load all of the counters
    if filename is None: 
        globals_dict = current_session.env_dict
        filename = globals_dict["SCAN_SAVING"].filename
        # we are working with the deque SCANS / scan number in memory
        #m0name,m0s,m0e,m0_pts, m1name,m1s,m1e,m1_pts,x,y,counters,channels_dict = sxdmdatahandler_fast(nb, channels)
        #channels_list = list(channels_dict.keys())
        
    if filename is not None: # we are working with hdf5
             
        print("\nfilename: %s, scan_no: %s\n"%(filename,nb))
        h5path_counters = "/%i.1/measurement/"%nb
        h5path_title = "/%i.1/title"%nb
        
        scanObj = openScan(filename,nb)
        
        channels_list = scanObj.getAllCountersList()
        
        channel_keys={}
        if channels==[]:
            channels = channels_list
                
        for channel in channels:
            if channels_list.count(channel):
                try:
                    channel_keys[channel]=channel
                except:
                    print("channel:%s not found"%channel) 

        if len(channel_keys)==0:
            print("#################\n Available channels: ")
            for item in channels_list:
                print(item)
            print("#################\n")

        x,y = scanObj.motor1.shape        
        counters = np.zeros((len(channel_keys),x,y))
        for i,channel in enumerate(channel_keys):
            try:
                counters[i,:,:] = scanObj.getRoiData(channel)
            except:
                print("Failed for channel: %s"%channel)
        channel_list = list(channel_keys)

    # look at them
    try:    
        fig = pl.figure(figsize=(9,6))
        ax = fig.add_subplot(111)
        fig.suptitle(" File: %s \n Scan: %i"%(filename,nb))
        tracker = GenericIndexTracker(ax, data = counters, norm=mynorm,exit_onclick=False)
        ax.set_xlabel(scanObj.motor1_name)
        ax.set_ylabel(scanObj.motor2_name)
        tracker.set_extent(scanObj.motor1.min(),scanObj.motor1.max(),scanObj.motor2.min(),scanObj.motor2.max())
        tracker.set_axes_properties(title=channels)
        pl.show()
        
    except:
        print("An error occurred")
    
    if positionsGroup:
        # place in a positions group
        instance_counter = 0
        # check env dict if the group exists - offer the option to clean it or add to it
        globals_dict = current_session.env_dict
        
        try:
            if globals_dict['positionsGroup_sxdm_inspect']:
                print("positionsGroup_sxdm_inspect exists.. [space_bar] to overwrite or any other key to append")
                ans = click.getchar(echo=False)
                if ans == " ":
                    globals_dict['positionsGroup_sxdm_inspect'].parameters.purge()
                    globals_dict['positionsGroup_sxdm_inspect'] = bl_posInst.ID01Positions("sxdm_inspect",tracker.POI_list[0][0][0],tracker.POI_list[0][0][2])
                else:
                    instance_counter = len(globals_dict['positionsGroup_sxdm_inspect'].parameters.instances)-2 # 2 defaults
                    print(instance_counter)
        except:    
            globals_dict['positionsGroup_sxdm_inspect'] = bl_posInst.ID01Positions("sxdm_inspect",tracker.POI_list[0][0][0],tracker.POI_list[0][0][2])
        
        for i,item in enumerate(tracker.POI_list):
            item = item[0]
            globals_dict['positionsGroup_sxdm_inspect'].switch("p%i"%(i+instance_counter))
            # hard coded could be neater TODO
            globals_dict['positionsGroup_sxdm_inspect'].set_axis_position(item[0],item[1])
            globals_dict['positionsGroup_sxdm_inspect'].set_axis_position(item[2],item[3])
        
    return tracker.POI_list


def mesh_inspect_scans(channel = "", filename = None, nbs = [],  mynorm = 'linear', positionsGroup = True, make_gif=False, gif_duration=2,gif_fn=""):
    """
    channel: the counter to plot
    filename: hdf5 filename
    nbs: scan numbers (accepts list -n nomenclature)
    
    TODO: counters in hdf5 do not match counters in measurement group e.g _sum
    
    note found an upper limit on memory - ask SEB where this comes from 
    needa check if sxdm is more than 1000 points should use the hdf5 file.
    
    TODO this uses the input values not the raw adc for plotting
    pcolormesh could be used here??
    
    TODO make channels a counters list or MG or something more sensible
    the counter channels names varies between hdf5 and SCANS object which is nasty
    
    TODO Add check for other motor positions - to make sure positioners havent changed
    
    TODO     #toggle button for log/linear + slide bar for colorbar?

    """


    # load all of the counters
    if filename is None: 
        globals_dict = current_session.env_dict
        filename = globals_dict["SCAN_SAVING"].filename
        # we are working with the deque SCANS / scan number in memory
        #m0name,m0s,m0e,m0_pts, m1name,m1s,m1e,m1_pts,x,y,counters,channels_dict = sxdmdatahandler_fast(nb, channels)
        #channels_list = list(channels_dict.keys())
        
    if filename is not None: # we are working with hdf5
        #print("\nfilename: %s, scan_no: %s\n"%(filename,nb))
        scanObj = openScan(filename,nbs[0])
        channels_list = scanObj.getAllCountersList()
        if channels_list.count(channel)==0:
            print("#################\n Available channels: ")
            for item in channels_list:
                print(item)
            print("#################\n")

        actual_nbs = []
        for i,nb in enumerate(nbs):
            #breakpoint()
            scanObj = openScan(filename,nb)
            actual_nbs.append(scanObj.scan_string)
            if nbs[0]==nb:
                x,y = scanObj.motor1.shape        
                counters = np.zeros((len(nbs),x,y))
            
            try:
                counters[i,:,:] = scanObj.getRoiData(channel)
            except:
                print("Failed for scan: %s"%nb)

            
            

    # look at them
    if make_gif:
        MakeGif(counters,name=gif_fn,duration=gif_duration,mynorm=mynorm,scanObj=scanObj,filename=filename,channel=channel) 
        return counters
    else:
        try:    
            fig = pl.figure(figsize=(9,6))
            ax = fig.add_subplot(111)
            fig.suptitle(" File: %s \n Counter: %s"%(filename,channel))
            tracker = GenericIndexTracker(ax, data = counters, norm=mynorm,exit_onclick=False)
            ax.set_xlabel(scanObj.motor1_name)
            ax.set_ylabel(scanObj.motor2_name)
            tracker.set_extent(scanObj.motor1.min(),scanObj.motor1.max(),scanObj.motor2.min(),scanObj.motor2.max())
            tracker.set_axes_properties(title=actual_nbs)
            pl.show()
            return tracker.POI_list, counters    
        except:
            print("An error occurred")
       

    """
    if positionsGroup:
        # place in a positions group
        instance_counter = 0
        # check env dict if the group exists - offer the option to clean it or add to it
        globals_dict = current_session.env_dict
        
        try:
            if globals_dict['positionsGroup_sxdm_inspect']:
                print("positionsGroup_sxdm_inspect exists.. [space_bar] to overwrite or any other key to append")
                ans = click.getchar(echo=False)
                if ans == " ":
                    globals_dict['positionsGroup_sxdm_inspect'].parameters.purge()
                    globals_dict['positionsGroup_sxdm_inspect'] = bl_posInst.ID01Positions("sxdm_inspect",tracker.POI_list[0][0][0],tracker.POI_list[0][0][2])
                else:
                    instance_counter = len(globals_dict['positionsGroup_sxdm_inspect'].parameters.instances)-2 # 2 defaults
                    print(instance_counter)"/data/visitor/hc4755/id01
          ...: /VO2_vshape/VO2_vshape_0020/VO2_vshape_0020.h5"
        except:    
            globals_dict['positionsGroup_sxdm_inspect'] = bl_posInst.ID01Positions("sxdm_inspect",tracker.POI_list[0][0][0],tracker.POI_list[0][0][2])
        
        for i,item in enumerate(tracker.POI_list):
            item = item[0]
            globals_dict['positionsGroup_sxdm_inspect'].switch("p%i"%(i+instance_counter))
            # hard coded could be neater TODO
            globals_dict['positionsGroup_sxdm_inspect'].set_axis_position(item[0],item[1])
            globals_dict['positionsGroup_sxdm_inspect'].set_axis_position(item[2],item[3])
    """
    


# import numpy as np
# import matplotlib.pyplot as plt
# import matplotlib.animation as animation

# def make_gif_4m_array(array3D,out_fn='movie.gif',fps=5):
#     """
#     make a gif movie from a 3D python array - this doesn't work due to some permissions conflict on lid01nano3
#     """
#     fig, ax = plt.subplots(figsize=(4, 4))

#     frame = 0
#     im = plt.imshow(array3D[frame], origin='lower')
#     plt.colorbar(shrink=0.5)

#     def update(*args):
#         global frame

#         im.set_array(array3D[frame])

#         frame += 1
#         frame %= array3D.shape[0]

#         return im,
    
#     # ani = animation.FuncAnimation(fig, update, interval=500)
#     # plt.show()

#     ani = animation.FuncAnimation(fig, update, array3D.shape[0])
#     writer = animation.ImageMagickFileWriter(fps=fps)
#     ani.save(out_fn, writer=writer)


import gif

@gif.frame
def extractImageForGif(data, n, mynorm="log", scanObj=None,filename=None,channel=None):
    
    if mynorm=='log':
        data = np.log(data)
    
    fig, ax = plt.subplots(1,1,figsize=(5,5))
    ax.matshow(data[n])

    if filename is not None and channel is not None:
        fig.suptitle(" File: %s \n Counter: %s"%(filename.split("/")[-1],channel))
    
    ax.set_title('scan {}'.format(n), fontsize=15)
    if scanObj is not None:
        ax.set_xlabel(scanObj.motor1_name)
        ax.set_ylabel(scanObj.motor2_name)
        ax.images[-1].set_extent((scanObj.motor1.min(),scanObj.motor1.max(),scanObj.motor2.min(),scanObj.motor2.max()))
    fig.tight_layout()
    return

def MakeGif(data, name='gif_default_name', duration=10, mynorm='log', scanObj=None,filename=None,channel=None):
            
    frames = [extractImageForGif(data, n, mynorm=mynorm,scanObj=scanObj,filename=filename,channel=channel) for n in range(data.shape[0])]
    gif.save(frames, name+'.gif', duration=duration, unit='s', between='startend')
    
    return


"""
import gif

@gif.frame
def extractImageForGif(data, n, log_scale=False):
    
    if log_scale :
        data = np.log(data)
    
    fig, ax = plt.subplots(1,1,figsize=(5,5))
    ax.matshow(data[n])
    ax.set_title('image {}'.format(n), fontsize=15)
    fig.tight_layout()
    return

def MakeGif(data, name='gif_default_name', duration=10, log_scale=False):
            
    frames = [extractImageForGif(data, n, log_scale=log_scale) for n in range(data.shape[0])]
    gif.save(frames, name+'.gif', duration=duration, unit='s', between='startend')
    
    return

# data should be of shape ( nb of images, image vertical size, image horizontal size)
#MakeGif(data, name='gif_default_name', duration=10, log_scale=False)
"""