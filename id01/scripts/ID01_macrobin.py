# -*- coding: utf-8 -*-
# ID01 BLISS git repo
#       authors:
#         Steven Leake, leake@esrf.fr
#
#       Created at: 06. Dec 00:23:00 CET 2021
#
#----------------------------------------------------------------------
# Description: 
#    bin for all macro functions - put them here until they are moved to the right place
#
#
#
# FUNCTIONS:
#
#----------------------------------------------------------------------

import numpy as np
from bliss.shell.standard import umv, umvr
import fscan.fscan as fscan
from bliss.common.scans import dscan
from bliss.scanning.scan_tools import goto_peak
from bliss import current_session, setup_globals

#from id01.scans.fscan_mux import FScanMux
#_fscan_mux = FScanMux(multiplexer_eh,musst1)
#fscan_eh.add_scan_preset(_fscan_mux)
#fscan = fscan_eh.get_runner("fscan")

def macro_night1(delta,nu,phi,multiplexer_eh,musst1,fscan_eh):
    
    from id01.scans.fscan_mux import FScanMux
    _fscan_mux = FScanMux(multiplexer_eh,musst1)
    fscan_eh.add_scan_preset(_fscan_mux)
    fscan = fscan_eh.get_runner("fscan")
    sign=1
    for i in np.linspace(30,90,13):
        for j in np.linspace(12.5,92.5,17):
            umv(delta,j,nu,i)
            if sign == 1:
                fscan(phi,-45,0.025,5000,0.02)  # 4mins
            if sign ==-1:
                fscan(phi,80,-0.025,5000,0.02)  # 4mins
            sign=sign*-1


def CDI_align(SCANS,pix,piy): 
    dscan(pix,-5,5,30,0.1)
    #a=SCANS[-1] 
    #a.goto_peak("mpx4_int_sum") 
    goto_peak(setup_globals.mpx1x4.counters.mpx4_int_sum)
    dscan(piy,-10,10,30,0.1)
    #a=SCANS[-1]
    #a.goto_peak("mpx4_int_sum")   
    goto_peak(setup_globals.mpx1x4.counters.mpx4_int_sum)

                                                                                                                                                                        
def CDI_measure(SCANS,pix,piy,eta): 
    CDI_align(SCANS,pix,piy)  
    dscan(eta,-0.5,0.5,200,1) 
    CDI_align(SCANS,pix,piy)  
    fscan = setup_globals.fscan_eh.get_runner("fscan")
    fscan(eta,19.18-0.5,0.00015,6666,0.03) 
    umv(eta,19.18)
    CDI_align(SCANS,pix,piy)  
    fscan(eta,19.18-0.5,0.0003,3333,0.03)
    umv(eta,19.18)
    CDI_align(SCANS,pix,piy)  
    fscan(eta,19.18-0.5,0.0005,2000,0.03) 
    umv(eta,19.18)

def CDI_measure1(SCANS,pix,piy,eta): 
    CDI_align(SCANS,pix,piy)  
    dscan(eta,-0.5,0.5,200,1) 
    CDI_align(SCANS,pix,piy)  
    fscan = setup_globals.fscan_eh.get_runner("fscan")
    fscan(eta,19.18-0.5,0.00015,6666,0.03) 
    umv(eta,19.18)


