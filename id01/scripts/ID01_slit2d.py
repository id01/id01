# ID01 BLISS git repo
#       authors:
#         Peter Boesecke, boesecke@esrf.fr
#
#       Created at: 
#
#----------------------------------------------------------------------
# Description:
#       class Slit2d(CalcController)
#       Calculates standard blade motors (up,down,front,back) 
#       for arbitrarily oriented 2D blade and gap slit units
#       The standard controller class slits cannot be used with Slit2D,
#       use the calc controller class Slit2dGap instead. 
#
# History:
#   2022-11-02 PB creation
#   2022-11-04 PB is there any way to get the tags of the motors, e.g.
#                 b3u.controller.axes["sim_b3k1"].has_tag("blade")?
#                 It returns always False.
#   2022-11-07 PB The BLISS controller class slits cannot be used
#                 together with the class Slit2d. The class slits
#                 moves horizontal and vertical motors independently, 
#                 while Slit2d moves all motors at the same time.
#                 When using the class slits the movements are 
#                 blocking each other. Use the class Slit2dGap instead.
#   2022-11-25 PB sync_hard() on level2 calculated motors FRONT, BACK,
#                 UP, DOWN after changing the orientation,
#                 blade tags changed to apos, aneg, bpos, bneg 
#                 In orientation 1 apos is up, aneg is down, bpos is
#                 front and bneg is back.
#   2022-11-29 PB consistent variable names (a1,a2,b1,b2, etc.)
#   2022-12-02 PB There was only a single orientation HashObjSetting
#                 used for all slits. Using now an individual name
#                 for each slit2d constructed with self.name.
#   2022-12-05 PB orientation_axes_info added,
#                 orientation_axes_info updated
#                 motor.settings.clear("_set_position") 
#                     <>  motor.sync_hard()
#   2023-01-17 PB __info__ updated
#   2023-01-17 PB class Axis -> CalcAxis
#                 
#----------------------------------------------------------------------

import numpy as np
from bliss.controllers.motor import CalcController
from bliss.common.axis import CalcAxis as AxisBase
from bliss.config.settings import HashObjSetting

from bliss.setup_globals import *
from bliss.common import session
from bliss.config import static

from id01.scripts.specmot_utils import *

import time

import logging
#logging.basicConfig(level=logging.DEBUG)
#logging.basicConfig(level=logging.WARNING)
#logging.debug('This will get logged')
#logging.warning('This will get logged')

_log=logging.getLogger("id01.scripts.ID01_slit2d")
#The log level can be set in the bliss session after import logging
#and defining _log as in the above command.
#_log.setLevel("DEBUG")

# constant tags

# blades
AP_TAG = "apos"
AN_TAG = "aneg"
BP_TAG = "bpos"
BN_TAG = "bneg"

# gap/offset
AG_TAG = "agap"
AO_TAG = "aoffset"
BG_TAG = "bgap"
BO_TAG = "boffset"

FRONT_TAG   = "front"
BACK_TAG    = "back"
UP_TAG      = "up"
DOWN_TAG    = "down"

class Slit2d(CalcController):
    """
    Calculates standard blade motors (front,back,up,down) 
    for arbitrarily oriented 2D blade and gap slit units:
    apos, aneg, bpos, bneg or agap, aoffset, bgap, boffset,
    where a and b specify axes in the same direction, either 
    horizontal or vertical.

    The standard laboratory system is defined with respect to the
    primary beam:
        translation along the primary beam: +x
        vertical translation:   +z (upwards)
        horizontal translation: +y (left)

    In the laboratory system the 4 blade motors are called:
      - front, back, up, down
      - standard suffix f, b, u, d

    In the laboratory system the 4 gap-offset motors are called:
      - horizontal gap, horizontal offset, vertical gap, vertical offset
      - standard suffix hg, ho, vg, vo

    Both descriptions are equivalent and exchangeable:

      - hg = front+back
      - ho = (front-back)/2
      - vg = up+down
      - vo = (up-down)/2

      - front = hg/2 + ho
      - back  = hg/2 - ho
      - up    = vg/2 + vo
      - down  = vg/2 - vo

    - orientation = 1 .. 8 (default 1)
    This value defines the actual spatial orientation of the slit system.
    In total, 8 orientations are possible (y==horizontal, z==vertical):

    The horizontal axis is the first axis and the vertical axis the second.

        h v
       ----
    1: +a+b (h==a,  v==b)  standard (default)
    2: -a+b (h==-a, v==b)  horizontal translation inverted
    3: +a-b (h==a,  v==-b  vertical translation inverted
    4: -a-b (h==-a, v==-b) both translations inverted
    5: +b+a (h==b,  v==a)  directions swapped
    6: +b-a (h==b,  v==-a) -90 deg rotation around x
    7: -b+a (h==-b, v==a)  +90 deg rotation around x
    8: -b-a (h==-b, v==-a) directions swapped and both translations inverted

    The directions follow the standard rules.
    x,y,z follow the right hand rule:
        x points along the beam,
        y points left (horizontal),
        z points upwards (vertical)

    The sign of each blade/gap motor must be configured in such a way that a
    movement in positive user direction opens the aperture.
    """

    def __init__(self,config,*args,**kwargs):
        super().__init__(config,*args,**kwargs)
        '''
        The parameters are kept in slit2d_dict.
        '''

        # The real and calculated motors must be defined as tagged motors 
        # in <device>.yml.
        # motors specified in <device>.yml:
        # if blade slit
        #  - name: $<axis1>
        #    tags: real apos
        #
        #  - name: $<axis2>
        #    tags: real aneg
        #
        #  - name: $<axis3>
        #    tags: real bpos
        #
        #  - name: $<axis4>
        #    tags: real bneg
        #
        # or if gap slit:
        #  - name: $<axis1>
        #    tags: real agap
        #
        #  - name: $<axis2>
        #    tags: real aoffset
        #
        #  - name: $<axis3>
        #    tags: real bgap
        #
        #  - name: $<axis4>
        #    tags: real boffset
        # ...
        # 
        # The calculated motors are:
        #
        #  - name: <front>
        #    tags: front
        #
        #  - name: <back>
        #    tags: back
        #
        #  - name: <up>
        #    tags: up
        #
        #  - name: <down>
        #    tags: down
        # ...


        self._config=config
        self.EPS=1e-32

        # Save the current orientation setting in slit2d_setting_dict in redis
        slit2d_setting_name="{}_setting".format(self.name)
        self.slit2d_setting_dict=HashObjSetting(slit2d_setting_name)

        # There a two types of slits, "blades" and "gap" slits
        self.slit_type_a = None
        self.slit_type_b = None

        if 'slit2d_dict' in config:
            self.slit2d_dict=config['slit2d_dict']
        else:
            self.default()
            self.savetoyml()

        if self.slit2d_setting_dict.get("orientation") is None:
            self.slit2d_setting_dict["orientation"] = self.slit2d_dict["default_orientation"]

    def __info__(self):
        '''
        Returns a string with the current configuration
        '''
        info=""

        if hasattr(self,"_tagged"):

            # AP_TAG or AG_TAG
            tmp=self._tagged.get(AP_TAG)
            if tmp is not None:
                apos_name=tmp[0].name
                info+="{} {} motor\n".format(apos_name, AP_TAG)
            else:
                tmp=self._tagged.get(AG_TAG)
                if tmp is not None:
                    bgap_name=tmp[0].name
                    info+="{} {} motor\n".format(bgap_name, AG_TAG)

            # AN_TAG or AO_TAG
            tmp=self._tagged.get(AN_TAG)
            if tmp is not None:
                aneg_name=tmp[0].name
                info+="{} {} motor\n".format(aneg_name, AN_TAG)
            else:
                tmp=self._tagged.get(AO_TAG)
                if tmp is not None:
                    boffset_name=tmp[0].name
                    info+="{} {} motor\n".format(boffset_name, AO_TAG)

            # BP_TAG or BG_TAG
            tmp=self._tagged.get(BP_TAG)
            if tmp is not None:
                bpos_name=tmp[0].name
                info+="{} {} motor\n".format(bpos_name, BP_TAG)
            else:
                tmp=self._tagged.get(BG_TAG)
                if tmp is not None:
                    agap_name=tmp[0].name
                    info+="{} {} motor\n".format(agap_name, BG_TAG)

            # BN_TAG or BO_TAG
            tmp=self._tagged.get(BN_TAG)
            if tmp is not None:
                bneg_name=tmp[0].name
                info+="{} {} motor\n".format(bneg_name, BN_TAG)
            else:
                tmp=self._tagged.get(BO_TAG)
                if tmp is not None:
                    aoffset_name=tmp[0].name
                    info+="{} {} motor\n".format(aoffset_name, BO_TAG)

        info+="\n"

        orientation=self.orientation()
        info+="{} orientation (h,v)=={}\n".format(orientation,self.orientation_state(orientation))

        info+="{}\n".format(self.orientation_axes_info(orientation))

        if hasattr(self,"_tagged"):

            # if the input slits are blade slits the motor names can be used
            motor_tuple=self.orientation_tuple(orientation)

            info+="\n"
            motor_string=""
            sep=""

            tmp=self._tagged.get(FRONT_TAG)
            if tmp is not None:
                front_name=tmp[0].name
                info+="{} {} motor ({})\n".format(front_name, FRONT_TAG, motor_tuple[0])
                motor_string+="{}{}".format(sep,front_name)
                sep=","

            tmp=self._tagged.get(BACK_TAG)
            if tmp is not None:
                back_name=tmp[0].name
                info+="{} {} motor ({})\n".format(back_name, BACK_TAG, motor_tuple[1])
                motor_string+="{}{}".format(sep,back_name)
                sep=","

            tmp=self._tagged.get(UP_TAG)
            if tmp is not None:
                up_name=tmp[0].name
                info+="{} {} motor ({})\n".format(up_name, UP_TAG, motor_tuple[2])
                motor_string+="{}{}".format(sep,up_name)
                sep=","

            tmp=self._tagged.get(DOWN_TAG)
            if tmp is not None:
                down_name=tmp[0].name
                info+="{} {} motor ({})\n".format(down_name, DOWN_TAG, motor_tuple[3])
                motor_string+="{}{}".format(sep,down_name)
                sep=","


            info+="\n"

            info+="For calculating correctly the motors {}\nthe motor axes a and b must be oriented as displayed\nwith respect to Y and Z.\n".format(motor_string)

            info+="\n"

        return(info)


    def default(self):
        '''
        Set slit parameters to default values
        '''

        # default slit configuration
        self.slit2d_dict={}

        # The motors are defined as tagged motors in <device>.yml

        S2d={}
        S2d["default_orientation"] = 1            # orientation 1 .. 8

        self.slit2d_dict=S2d

    def savetoyml(self):
        '''
        Save current ID01 slit2d parameters to yml file
        '''
        self._config['slit2d_dict']=self.slit2d_dict
        self._config.save()

    def orientation(self,orientation=None):
        '''
        Sets and returns the current orientation
        - orientation = 1 .. 8 (default 1)
        This value defines the actual spatial orientation of the slit system.
        In total, 8 orientations are possible (z==vertical, y==horizontal):

        The horizontal axis is the first axis and the vertical axis the second.

            h v
           ----
        1: +a+b (h==a,  v==b)  standard (default)
        2: -a+b (h==-a, v==b)  horizontal translation inverted
        3: +a-b (h==a,  v==-b  vertical translation inverted
        4: -a-b (h==-a, v==-b) both translations inverted
        5: +b+a (h==b,  v==a)  directions swapped
        6: +b-a (h==b,  v==-a) -90 deg rotation around x
        7: -b+a (h==-b, v==a)  +90 deg rotation around x
        8: -b-a (h==-b, v==-a) directions swapped and both translations inverted
        '''

        _log.debug("  orientation BEGIN (orientation={})".format(orientation))

        if orientation is None:
            orientation=self.slit2d_setting_dict.get("orientation") 
        else:
            previous_orientation=self.slit2d_setting_dict.get("orientation")
            self.slit2d_setting_dict["orientation"]=orientation

            # motors must be synced after changing the orientation
            if orientation!=previous_orientation:
                if hasattr(self,"_tagged"):
                    axes={}
                    axes[FRONT_TAG]=self._tagged.get(FRONT_TAG)[0]
                    axes[BACK_TAG]=self._tagged.get(BACK_TAG)[0]
                    axes[UP_TAG]=self._tagged.get(UP_TAG)[0]
                    axes[DOWN_TAG]=self._tagged.get(DOWN_TAG)[0]
                    for axis in axes:
                        motor=axes[axis]
                        if motor is not None:
                            _log.debug("  orientation: {}.sync_hard() ".format(motor.name))
                            motor.sync_hard()
                            # an alternative should be motor.settings.clear("_set_position")
                            ##_log.debug('  orientation: {}.settings.clear("_set_position")'.format(motor.name))
                            ##motor.settings.clear("_set_position")

        if orientation is None:
            orientation=self.slit2d_dict["default_orientation"]
            self.slit2d_setting_dict["orientation"]=orientation

        _log.debug("  orientation END (orientation={})".format(orientation))

        return orientation


    def orientation_state(self,orientation=None):
        """
        Returns a string with the (h,v)-directions as function of the input slit motors (a,b)
        """

        if orientation is None:
            orientation=self.orientation()
        info=""
        if orientation==1: 
            info+="(a,b)"
        elif orientation==2:
            info+="(-a,b)"
        elif orientation==3:
            info+="(a,-b)"
        elif orientation==4:
            info+="(-a,-b)"
        elif orientation==5:
            info+="(b,a)"
        elif orientation==6:
            info+="(b,-a)"
        elif orientation==7:
            info+="(-b,a)"
        elif orientation==8:
            info+="(-b,-a)"

        return(info)

    def orientation_axes_info(self,orientation=None):
        """
        Returns graphically the axes X,Y,Z,a,b
        """

        if orientation is None:
            orientation=self.orientation()
        axes_info=""
        if orientation==1: 
            #info+="(a,b)"
            #axes_info="     b\n     ^\n     |\n     |\n     |\na<---X"
            axes_info="      Z,b\n       ^\n       |\n       |\n       |\nY,a<---X"

        elif orientation==2:
            #info+="(-a,b)"
            #axes_info="b\n^\n|\n|\n|\nX--->a"
            axes_info="    Z,b\n     ^\n     |\n     |\n     |\nY<---X--->a"

        elif orientation==3:
            #info+="(a,-b)"
            #axes_info="a<---X\n     |\n     |\n     |\n     v\n     b"
            axes_info="       Z\n       ^\n       |\n       |\n       |\nY,a<---X\n       |\n       |\n       |\n       v\n       b"

        elif orientation==4:
            #info+="(-a,-b)"
            #axes_info="X--->a\n|\n|\n|\nv\nb"
            axes_info="     Z\n     ^\n     |\n     |\n     |\nY<---X--->a\n     |\n     |\n     |\n     v\n     b"

        elif orientation==5:
            #info+="(b,a)"
            #axes_info="     a\n     ^\n     |\n     |\n     |\nb<---X"
            axes_info="      Z,a\n       ^\n       |\n       |\n       |\nY,b<---X"
        elif orientation==6:
            #info+="(b,-a)"
            #axes_info="b<---X\n     |\n     |\n     |\n     v\n     a"
            axes_info="       Z\n       ^\n       |\n       |\n       |\nY,b<---X\n       |\n       |\n       |\n       v\n       a"
        elif orientation==7:
            #info+="(-b,a)"
            #axes_info="a\n^\n|\n|\n|\nX--->b"
            axes_info="    Z,a\n     ^\n     |\n     |\n     |\nY<---X--->b"
        elif orientation==8:
            #info+="(-b,-a)"
            #axes_info="X--->b\n|\n|\n|\nv\na"
            axes_info="     Z\n     ^\n     |\n     |\n     |\nY<---X--->b\n     |\n     |\n     |\n     v\n     a"

        return(axes_info)


    def orientation_tuple(self,orientation=None,motors=(AP_TAG,AN_TAG,BP_TAG,BN_TAG)):
        """
        Returns the four blade names according to orientation.
        """
        apos=motors[0]
        aneg=motors[1]
        bpos=motors[2]
        bneg=motors[3]

        output=()
        if orientation is None:
            orientation=self.orientation()
        info=""
        if orientation==1:
            #1: +a+b (h==a,  v==b)  standard (default)
            output=(apos,aneg,bpos,bneg)
        elif orientation==2:
            #2: -a+b (h==-a, v==b)  horizontal translation inverted
            output=(aneg,apos,bpos,bneg)
        elif orientation==3:
            #3: +a-b (h==a,  v==-b  vertical translation inverted
            output=(apos,aneg,bneg,bpos)
        elif orientation==4:
            # 4: -a-b (h==-a, v==-b) both translations inverted
            output=(aneg,apos,bneg,bpos)
        elif orientation==5:
            #5: +b+a (h==b,  v==a)  directions swapped
            output=(bpos,bneg,apos,aneg)
        elif orientation==6:
            #6: +b-a (h==b,  v==-a) -90 deg rotation around x
            output=(bpos,bneg,aneg,apos)
        elif orientation==7:
            #7: -b+a (h==-b, v==a)  +90 deg rotation around x
            output=(bneg,bpos,apos,aneg)
        elif orientation==8:
            #8: -b-a (h==-b, v==-a) directions swapped and both translations inverted
            output=(bneg,bpos,aneg,apos)

        return(output)


    def default_orientation(self,orientation=None):
        '''
        Sets, applies and returns the default orientation
        '''
        if orientation is None:
            orientation=self.slit2d_dict["default_orientation"]
        else:
            self.slit2d_dict["default_orientation"]=orientation
        self.slit2d_setting_dict["orientation"]=orientation
        return orientation


    def calc_from_real(self, level1_positions):
        '''
        Calculate the level2_positions (front, back, up, down) [mm] from 
        the level1_positions (a1, a2, b1, b2) [mm].
        (a1, a2) can either be blades (apos, aneg) or 
	gap/offset values (bgap, boffset) (in this order). The same 
        for b1, b2 can either be blades (bpos, bneg) or
        gap/offset values (agap, aoffset).
        '''
        _log.debug("  calc_from_real BEGIN (level1={})".format(level1_positions))

        a1=level1_positions.get(AP_TAG)
        if a1 is None:
            a1=level1_positions.get(AG_TAG)

            if a1 is not None:
                a2=level1_positions[AO_TAG]
                self.slit_type_a="gap"
        else:
            a2=level1_positions[AN_TAG]
            self.slit_type_a="blades"


        b1=level1_positions.get(BP_TAG)
        if b1 is None:
            b1=level1_positions.get(BG_TAG)

            if b1 is not None:
                b2=level1_positions[BO_TAG]
                self.slit_type_b="gap"
        else:
            b2=level1_positions[BN_TAG]
            self.slit_type_b="blades"

        (front, back, up, down) = self.axes_to_blades(a1, a2, b1, b2, self.slit_type_a, self.slit_type_b)

        calc_level2_positions={}
        calc_level2_positions[FRONT_TAG]=front
        calc_level2_positions[BACK_TAG]=back
        calc_level2_positions[UP_TAG]=up
        calc_level2_positions[DOWN_TAG]=down

        _log.debug("  calc_from_real END (calc_level2={})".format(calc_level2_positions))

        return calc_level2_positions


    def calc_to_real(self, level2_positions):
        '''
        Calculate the level1_positions (a1, a2, b1, b2) [mm]
        from the level2_positions (front, back, up, down) [mm].
        -- Attention, the names b and a are still swapped!!!!
        '''
        _log.debug("  calc_to_real BEGIN (level2={})".format(level2_positions))

        front=level2_positions[FRONT_TAG]
        back=level2_positions[BACK_TAG]
        up=level2_positions[UP_TAG]
        down=level2_positions[DOWN_TAG]

        (a1, a2, b1, b2) = self.blades_to_axes(front, back, up, down, self.slit_type_a, self.slit_type_b)

        calc_level1_positions={}
        if self.slit_type_a == "blades":
            calc_level1_positions[AP_TAG]=a1
            calc_level1_positions[AN_TAG]=a2
        elif self.slit_type_a == "gap":
            calc_level1_positions[AG_TAG]=a1
            calc_level1_positions[AO_TAG]=a2

        if self.slit_type_b == "blades":
            calc_level1_positions[BP_TAG]=b1
            calc_level1_positions[BN_TAG]=b2
        elif self.slit_type_b == "gap":
            calc_level1_positions[BG_TAG]=b1
            calc_level1_positions[BO_TAG]=b2


        _log.debug("  calc_to_real END (calc_level1={})".format(calc_level1_positions))

        return calc_level1_positions


    def axes_to_blades(self, a1, a2, b1, b2, slit_type_a, slit_type_b):
        """
        Calculates standard blade positions (front, back, up, down) from 4 slit motors
        (a1, a2, b1, b2) depending on the geometrical orientation
        of the motor axes. The motor axes must either be blade movements 
        (slit_type_a,slit_type_b="blades") or gap/offset movements (type_a,type_b="gap",
        ?1 must be the gap and ?2 must be the offset).
        The real motors (blades, gaps) must be configured in such a way that 
        positive user movements increase the aperture. The sign of the offsets depend
        on the geometrical orientation.
        If this is the case the orientation parameter (1..8) can be chosen in such a 
        way that the calculated blade values are (front, back, up, down).
        """

        # The horizontal axis is the first axis and the vertical axis the second.

        #     h v
        #    ----
        # 1: +a+b (h==a,  v==b)  standard (default)
        # 2: -a+b (h==-a, v==b)  horizontal translation inverted
        # 3: +a-b (h==a,  v==-b  vertical translation inverted
        # 4: -a-b (h==-a, v==-b) both translations inverted
        # 5: +b+a (h==b,  v==a)  directions swapped
        # 6: +b-a (h==b,  v==-a) -90 deg rotation around x
        # 7: -b+a (h==-b, v==a)  +90 deg rotation around x
        # 8: -b-a (h==-b, v==-a) directions swapped and both translations inverted

        orientation=self.orientation()

        #  1 -> 1, 8 -> 8, 9 -> 1
        orientation %= 8
        if orientation==0: orientation=8

        # - means up/down front/back swapped
        if orientation == 1:
            # 1: +a+b (h==a,  v==b)  standard (default)
            if slit_type_a == "blades":
                front = a1
                back  = a2
            else: #  "gap":
                front = a1/2+a2
                back  = a1/2-a2

            if slit_type_b == "blades":
                up   = b1
                down = b2
            else: # "gap":
                up   = b1/2+b2
                down = b1/2-b2


        elif orientation == 2:
            # 2: -a+b (h==-a, v==b)  horizontal translation inverted
            if slit_type_a == "blades":
                front = a2
                back  = a1
            else: # "gap":
                front = a1/2-a2
                back  = a1/2+a2

            if slit_type_b == "blades":
                up   = b1
                down = b2
            else: # "gap":
                up   = b1/2+b2
                down = b1/2-b2


        elif orientation == 3:
            # 3: +a-b (h==a,  v==-b  vertical translation inverted
            if slit_type_a == "blades":
                front = a1
                back  = a2
            else: #  "gap":
                front = a1/2+a2
                back  = a1/2-a2

            if slit_type_b == "blades":
                up   = b2
                down = b1
            else: # "gap":
                up   = b1/2-b2
                down = b1/2+b2


        elif orientation == 4:
            # 4: -a-b (h==-a, v==-b) both translations inverted
            if slit_type_a == "blades":
                front = a2
                back  = a1
            else: # "gap":
                front = a1/2-a2
                back  = a1/2+a2

            if slit_type_b == "blades":
                up   = b2
                down = b1
            else: # "gap":
                up   = b1/2-b2
                down = b1/2+b2

        elif orientation == 5:
            # 5: +b+a (h==b,  v==a)  directions swapped
            if slit_type_b == "blades":
                front = b1
                back  = b2
            else: #  "gap":
                front = b1/2+b2
                back  = b1/2-b2

            if slit_type_a == "blades":
                up   = a1
                down = a2
            else: # "gap":
                up   = a1/2+a2
                down = a1/2-a2

        elif orientation == 6:
            # 6: +b-a (h==b,  v==-a) -90 deg rotation around x
            if slit_type_b == "blades":
                front = b1
                back  = b2
            else: # "gap":
                front = b1/2+b2
                back  = b1/2-b2

            if slit_type_a == "blades":
                up   = a2
                down = a1
            else: # "gap":
                up   = a1/2-a2
                down = a1/2+a2


        elif orientation == 7:
            # 7: -b+a (h==-b, v==a)  +90 deg rotation around x

            if slit_type_b == "blades":
                front = b2
                back  = b1
            else: # "gap":
                front = b1/2-b2
                back  = b1/2+b2

            if slit_type_a == "blades":
                up   = a1
                down = a2
            else: # "gap":
                up   = a1/2+a2
                down = a1/2-a2

        else: # orientation == 8
            # 8: -b-a (h==-b, v==-a) directions swapped and both translations inverted
            if slit_type_b == "blades":
                front = b2
                back  = b1
            else: # "gap":
                front = b1/2-b2
                back  = b1/2+b2

            if slit_type_a == "blades":
                up   = a2
                down = a1
            else: # "gap":
                up   = a1/2-a2
                down = a1/2+a2

        return(front, back, up, down)


    def blades_to_axes(self, front, back, up, down, slit_type_a, slit_type_b):

        # The horizontal axis is the first axis and the vertical axis the second.

        #     h v
        #    ----
        # 1: +a+b (h==a,  v==b)  standard (default)
        # 2: -a+b (h==-a, v==b)  horizontal translation inverted
        # 3: +a-b (h==a,  v==-b  vertical translation inverted
        # 4: -a-b (h==-a, v==-b) both translations inverted
        # 5: +b+a (h==b,  v==a)  directions swapped
        # 6: +b-a (h==b,  v==-a) -90 deg rotation around x
        # 7: -b+a (h==-b, v==a)  +90 deg rotation around x
        # 8: -b-a (h==-b, v==-a) directions swapped and both translations inverted

        orientation=self.orientation()

        #  1 -> 1, 8 -> 8, 9 -> 1
        orientation %= 8
        if orientation==0: orientation=8

        # - means up/down front/back swapped
        if orientation == 1:
            # 1: +a+b (h==a,  v==b)  standard (default)
            if slit_type_a == "blades":
                a1 = front
                a2 = back
            else: # "gap":
                a1 =  front+back     # hg
                a2 = (front-back)/2  # ho

            if slit_type_b == "blades":
                b1 = up
                b2 = down
            else: # "gap":
                b1 =  up+down     # vg
                b2 = (up-down)/2  # vo

        elif orientation == 2:
            # 2: -a+b (h==-a, v==b)  horizontal translation inverted
            if slit_type_a == "blades":
                a1 = back
                a2 = front
            else: # "gap":
                a1 =  front+back     # hg
                a2 = -(front-back)/2  # ho

            if slit_type_b == "blades":
                b1 = up
                b2 = down
            else: # "gap":
                b1 =  up+down     # vg
                b2 = (up-down)/2  # vo

        elif orientation == 3:
            # 3: +a-b (h==a,  v==-b  vertical translation inverted
            if slit_type_a == "blades":
                a1 = front
                a2 = back
            else: # "gap":
                a1 =  front+back     # hg
                a2 = (front-back)/2  # ho

            if slit_type_b == "blades":
                b1 = down
                b2 = up
            else: # "gap":
                b1 =  up+down     # vg
                b2 = -(up-down)/2  # vo

        elif orientation == 4:
            # 4: -a-b (h==-a, v==-b) both translations inverted
            if slit_type_a == "blades":
                a1 = back
                a2 = front
            else: # "gap":
                a1 =  front+back     # hg
                a2 = -(front-back)/2  # ho

            if slit_type_b == "blades":
                b1 = down
                b2 = up
            else: # "gap":
                b1 =  up+down     # vg
                b2 = -(up-down)/2  # vo

        elif orientation == 5:
            # 5: +b+a (h==b,  v==a)  directions swapped
            if slit_type_a == "blades":
                a1 = up
                a2 = down
            else: # "gap":
                a1 =  up+down     # vg
                a2 = (up-down)/2  # vo

            if slit_type_b == "blades":
                b1 = front
                b2 = back
            else: # "gap":
                b1 =  front+back     # hg
                b2 = (front-back)/2  # ho

        elif orientation == 6:
            # 6: +b-a (h==b,  v==-a) -90 deg rotation around x
            if slit_type_a == "blades":
                a1 = down
                a2 = up
            else: # "gap":
                a1 =  up+down     # vg
                a2 = -(up-down)/2  # vo

            if slit_type_b == "blades":
                b1 = front
                b2 = back
            else: # "gap":
                b1 =  front+back     # hg
                b2 = (front-back)/2  # ho


        elif orientation == 7:
            # 7: -b+a (h==-b, v==a)  +90 deg rotation around x
            if slit_type_a == "blades":
                a1 = up
                a2 = down
            else: # "gap":
                a1 =  up+down     # vg
                a2 = (up-down)/2  # vo

            if slit_type_b == "blades":
                b1 = back
                b2 = front
            else: # "gap":
                b1 =  front+back     # hg
                b2 = -(front-back)/2  # ho

        else:
            # 8: -b-a (h==-b, v==-a) directions swapped and both translations inverted
            if slit_type_a == "blades":
                a1 = down
                a2 = up
            else: # "gap":
                a1 =  up+down     # vg
                a2 = -(up-down)/2  # vo

            if slit_type_b == "blades":
                b1 = back
                b2 = front
            else: # "gap":
                b1 =  front+back     # hg
                b2 = -(front-back)/2  # ho

        return(a1, a2, b1, b2)


class CalcAxis(AxisBase):
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        # calculated motor positions do not need to be buffered
        # => disable buffering
        self.settings.disable_cache("_set_position") #disable target pos
        self.settings.disable_cache("position") #disable current pos
        self.settings.disable_cache("state") #disbale the state

    def orientation(self,orientation=None):
        return self.controller.orientation(orientation)

    def default_orientation(self,orientation=None):
        return self.controller.default_orientation(orientation)

