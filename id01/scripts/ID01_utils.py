# -*- coding: utf-8 -*-

# ID01 BLISS git repo
#       authors:
#         Steven Leake, leake@esrf.fr
#
#       Created at: 05. Dec 17:41:00 CET 2021
#
#----------------------------------------------------------------------
# Description: 
#    utility functions for ID01
# 
#
# TODO:
#   see TODOs
#
#
# FUNCTIONS:
#   fsFORCEOPEN
#   fsFORCECLOSE
#   fsP201
#   sc
#   so
#   shexa_COR2beam << NOT TESTED
#   limaroiID01user
#   limaroiID01horiz
#   limaroiID01vert
#----------------------------------------------------------------------
from bliss import setup_globals
from bliss import current_session
from bliss.shell.standard import umv 
from bliss.config import settings
from bliss.controllers.lima.roi import Roi 
from bliss.controllers.lima.limatools import limatake 
from bliss.common.scans import ct
from bliss.common.scans import dscan

from bliss import current_session
from bliss.icat.client import icat_client_from_config


from time import sleep
import matplotlib.pyplot as pl
import os
import click
import sys
import subprocess
import h5py as h5
import tifffile


from matplotlib.colors import LogNorm
import functools
import numpy as np
import pylab as plt


from bliss.common.scans import ct

from id01.process.h5utils import get_scan_motor_hdf5, openScan
from id01.process.interactive import GenericIndexTracker
from id01.scans.step_scan_mux import fshut_mode_force_open, fshut_mode_p201
from id01.scripts.ID01_scan_math import gauss_fit


def sc(): 
    """close safety shutter"""
    setup_globals.safshut.close()

def so(): 
    """open safety shutter"""
    st = setup_globals.safshut.state_string
    print('... trying to open the safety shutter ...\n')
    for item in st:
        print(item)
    while st[0] != 'Open':
        sleep(2)
        setup_globals.safshut.open()
        tmp_st = setup_globals.safshut.state_string
        if tmp_st[0] != st[0]:
            print(tmp_st)
            st=tmp_st
        else:
            print(".") # TODO control std_out and print it on a single line
            # TODO turn off the logging warning messages

    
def shexa_COR2beam(shexa): 
    """
    Move the COR of the hexapod to the beam position.
    shexa = setup_globals.shexa1
    """
    #TODO test
    from collections import namedtuple

    ROLES = "tx", "ty", "tz", "rx", "ry", "rz"
    Pose = namedtuple("Pose", ROLES)
    shexa_curr_pos = {i:shexa.axes[i].position for i in shexa.axes.keys()}
    
    shexa_axes_sign = {i:shexa.axes[i].sign for i in shexa.axes.keys()}

    # hexapod generic, as long as translation startswith "t" and ends with axis x/y/z
    for i in shexa.axes.keys():
        if i.startswith("t"):
            if i.endswith("x"):
                y_offset = shexa_curr_pos[i]
                y_sign = shexa_axes_sign[i]
            if i.endswith("y"):
                x_offset = shexa_curr_pos[i]
                x_sign = shexa_axes_sign[i]
            if i.endswith("z"):
                z_offset = shexa_curr_pos[i]
                z_sign = shexa_axes_sign[i]

    user_coord, object_coord = shexa.get_origin()
    pose_dict = dict(((r, None) for r in ROLES))
    pose_dict['tx'] = object_coord.tx - x_offset*x_sign
    pose_dict['ty'] = object_coord.ty - y_offset*y_sign
    pose_dict['tz'] = object_coord.tz - z_offset*z_sign
    pose_dict['rx'] = object_coord.rx
    pose_dict['ry'] = object_coord.ry
    pose_dict['rz'] = object_coord.rz
    pose = Pose(**pose_dict)  # to modify a namedtuple use ._replace(tx=1)
    shexa.set_origin(user_coord, pose)
    #sync()
    # sync explicit axes not all motors in the session
    for i in shexa.axes.keys():
        if i.startswith("t"):
            shexa.axes[i].sync_hard()
            
    globals_dict = current_session.env_dict
    globals_dict[shexa.name+"_object_coord"] = pose   # TODO execute this on a resetup >> needs to go to redis
    globals_dict[shexa.name+"_user_coord"] = user_coord   # TODO execute this on a resetup >> needs to go to redis
    
def shexa_set_pose(shexa, utx, uty, utz, urx, ury, urz, otx, oty, otz, orx, ory, orz):
    """
    set the shexa reference for dummies
    """
    from collections import namedtuple

    ROLES = "tx", "ty", "tz", "rx", "ry", "rz"
    Pose = namedtuple("Pose", ROLES)    
    
    shexa.set_origin(Pose(tx=utx, ty=uty, tz=utz, rx=urx, ry=ury, rz=urz), Pose(tx=otx, ty=oty, tz=otz,rx=orx, ry=ory, rz=orz))
    print(f"shexa_set_pose({shexa.name},{utx}, {uty}, {utz}, {urx}, {ury}, {urz}, {otx}, {oty}, {otz}, {orx}, {ory}, {orz})")
    for i in shexa.axes.keys():
        if i.startswith("t"):
            shexa.axes[i].sync_hard()  
    
    

def user_pickroi(data):
    """
    provide the Xmin,Xmax,Ymin,Ymax values for an ROI
    """
    
    #tmp_scan = ct(0.1,detObj)
    #data = tmp_scan.get_data(f"{detObj.name}:image").as_array()
    fig = pl.figure()
    ax = fig.add_subplot(111)
    #norm=self._norm(data[self.ind]))
    ax.imshow(data, interpolation="nearest", origin="upper")
    #annotate = PScanTracker.Annotate(ax, specsession, exit_onclick=False,
    #                                 rectangle_onclick=True)
    annotate = GenericIndexTracker(ax,
                                   norm = "log",
                                   exit_onclick = False,
                                   rectangle_onclick = True)
    pl.show()  
      
    if annotate.y0==annotate.y1 or  annotate.x0==annotate.x1:
      print("..WARNING: two positions were the same")
      sys.exit()

    xpos = np.sort([annotate.x0, annotate.x1])
    ypos = np.sort([annotate.y0, annotate.y1])

    Xmin, Xmax = xpos.round().astype(int)
    Ymin, Ymax = ypos.round().astype(int)
    
    return({"Xmin":Xmin,"Xmax":Xmax,"Ymin":Ymin,"Ymax":Ymax})


def lima_pickroi(detObj, user_roi = None, symmetric_extents = 100, exposure=0.1 ):
    """
    create a symmetric ROI around the direct beam
    user_roi = roiname or special "symmetric", if None it will 
    append an roi%i where i is the next available name
    symmetric_extents: axis perpendicular to central ROI
    """
    
    tmp_scan = ct(exposure,detObj)
    data = tmp_scan.get_data(f"{detObj.name}:image").as_array()
    fig = pl.figure()
    ax = fig.add_subplot(111)
    #norm=self._norm(data[self.ind]))
    ax.imshow(data, interpolation="nearest", origin="upper")
    #annotate = PScanTracker.Annotate(ax, specsession, exit_onclick=False,
    #                                 rectangle_onclick=True)
    annotate = GenericIndexTracker(ax,
                                   norm = "log",
                                   exit_onclick = False,
                                   rectangle_onclick = True)
    pl.show()  
      
    if annotate.y0==annotate.y1 or  annotate.x0==annotate.x1:
      print("..WARNING: two positions were the same")
      sys.exit()

    xpos = np.sort([annotate.x0, annotate.x1])
    ypos = np.sort([annotate.y0, annotate.y1])

    Xmin, Xmax = xpos.round().astype(int)
    Ymin, Ymax = ypos.round().astype(int)
    if user_roi == 'symmetric':
        try:
            distance = symmetric_extents
        except:
            distance = 100


        coords = np.array([
                    [Xmin, Xmax, Ymin, Ymax],
                    [Xmin - distance, Xmin, Ymin, Ymax],
                    [Xmax, Xmax + distance, Ymin, Ymax],
                    [Xmin, Xmax, Ymin - distance, Ymin],
                    [Xmin, Xmax, Ymax, Ymax + distance]
                 ])
                 
        # clean up the overflow - bad ROIs
        coords[:,[0,1]] = np.clip(coords[:,[0,1]], 0, data.shape[1]-1)
        coords[:,[2,3]] = np.clip(coords[:,[2,3]], 0, data.shape[0]-1)


        #make all of the other ROIs
        for j, roi in enumerate(coords):
            detObj.roi_counters.set("roi%i"%j, (roi[0],roi[2],roi[1]-roi[0],roi[3]-roi[2]))

    else:
        toggle_new_roi = True
        roi_keys = detObj.roi_counters.keys()
        for roi in roi_keys:
            if user_roi == roi:
                # user_roi already defined overwrite 
                detObj.roi_counters.set(user_roi, (Xmin,Ymin,Xmax-Xmin,Ymax-Ymin))
                toggle_new_roi = False
            
            
        if user_roi != 'symmetric' and toggle_new_roi:
            if user_roi is None:
                roi_keys = detObj.roi_counters.keys()
                ii=0
                for roi in roi_keys:
                    if roi.count("roi"):
                        ii+=1
                user_roi = "roi%i"%ii    
                                        
            detObj.roi_counters.set(user_roi, (Xmin,Ymin,Xmax-Xmin,Ymax-Ymin))
            #clean counters
            ACTIVE_MG_Obj = current_session.env_dict["ACTIVE_MG"]
            try:
                ACTIVE_MG_Obj.disable(f"*{user_roi}*std*")
                ACTIVE_MG_Obj.disable(f"*{user_roi}*min*")
                ACTIVE_MG_Obj.disable(f"*{user_roi}*avg*")
            except:
                pass


def limaroiID01_direct_beam_old(detObj,beam_pos = [0,0], beam_size = [10,10]):
    """
    Generate standard ROIs for a user experiment.
    TODO det_calib should be done before to make the ROIs around the beam position
    TODO add det_calib - beam size on detector
    """
    
    rois = settings.HashObjSetting(f"{detObj.name}:roi_counters:default")
    
    det_size = [detObj.image.width,detObj.image.height]
    hlim = [beam_pos[0]-beam_size[0]/2,beam_pos[0]+beam_size[0]/2]
    vlim = [beam_pos[1]-beam_size[1]/2,beam_pos[1]+beam_size[1]/2]
    # print("user ROIs should have the tag roiXX, please do not alter other ROIs")
    # TODO make the mpx4* labelled ROIs uneditable , these will be controlled by det_calib (+ mpxy and mpxz motors for the maxipix)
    # TODO add beam_pos dependency
    if detObj.name=="mpx1x4":
        prefix = "mpx4"
    
    elif detObj.name=="mpxgaas":
        prefix = "mpxgaas"

    elif  detObj.name=="eiger2M":
        prefix = "ei2m"

    elif detObj.name.count("bv"):
        prefix = detObj.name.split("_")[1]
    
    else:
        print(f"detector {detObj.name} is not defined in limaroiID01_direct_beam() - please update the function") 
        print(f"using default prefix {detObj.name}")      
        prefix = detObj.name

    detObj.roi_counters.set(f"{prefix}_cen", [hlim[0],vlim[0],beam_size[0],beam_size[1]]) 
    detObj.roi_counters.set(f"{prefix}_hcen", [0,vlim[0],det_size[0],beam_size[1]]) 
    detObj.roi_counters.set(f"{prefix}_vcen", [hlim[0],0,beam_size[0],det_size[1]]) 
    detObj.roi_counters.set(f"{prefix}_int", [0,0,det_size[0],det_size[1]])         
           
def limaroiID01_direct_beam(detObj,beam_pos = [0,0], beam_size = [10,10]):
    """
    Generate standard ROIs for a user experiment.
    TODO det_calib should be done before to make the ROIs around the beam position
    TODO add det_calib - beam size on detector
    """
    
    rois = settings.HashObjSetting(f"{detObj.name}:roi_counters:default")
    
    det_size = [detObj.image.width,detObj.image.height]
    hlim = [beam_pos[0]-beam_size[0]/2,beam_pos[0]+beam_size[0]/2]
    vlim = [beam_pos[1]-beam_size[1]/2,beam_pos[1]+beam_size[1]/2]
    # print("user ROIs should have the tag roiXX, please do not alter other ROIs")
    # TODO make the mpx4* labelled ROIs uneditable , these will be controlled by det_calib (+ mpxy and mpxz motors for the maxipix)
    # TODO add beam_pos dependency

    # BLISS automatically adds a prefix in the hdf5 file
    detObj.roi_counters.set(f"cen", [hlim[0],vlim[0],beam_size[0],beam_size[1]]) 
    detObj.roi_counters.set(f"hcen", [0,vlim[0],det_size[0],beam_size[1]]) 
    detObj.roi_counters.set(f"vcen", [hlim[0],0,beam_size[0],det_size[1]]) 
    detObj.roi_counters.set(f"int", [0,0,det_size[0],det_size[1]])                 


def limaroiID01vert(detObj,no_rois,limits_vert=[0,0]):
    """
    Make no_rois to split the detector in the vertical direction 
    """
    det_size = detObj.image.width
    roi_size = int(np.floor(det_size/no_rois)) # TODO
    if limits_vert==[0,0]:
        limits_vert=[0,detObj.image.height]
    for i in np.arange(no_rois):
        try:
            detObj.roi_counters.set("det_roi%i"%i, (roi_size*i,limits_vert[0],roi_size,limits_vert[1]-limits_vert[0])) 
            print("det_roi%i successsfully added..."%i)
        except:
            print("det_roi%"%i, (roi_size*i,0,roi_size,detObj.image.height))

    ACTIVE_MG_Obj = current_session.env_dict["ACTIVE_MG"]

    #clean counters
    ACTIVE_MG_Obj.disable("*std*")
    ACTIVE_MG_Obj.disable("*min*")
    ACTIVE_MG_Obj.disable("*avg*")
    


def limaroiID01horiz(detObj,no_rois,limits_horiz=[0,0]):
    """
    Make no_rois to split the detector in the horizontal direction
    ||||
    ||||
    ||||
    """
    det_size = detObj.image.height
    roi_size = int(np.floor(det_size/no_rois)) # TODO
    if limits_horiz==[0,0]:
        limits_horiz=[detObj.image.width,0]
    for i in np.arange(no_rois):
        try:
            detObj.roi_counters.set("det_roi%i"%i, (limits_horiz[0],roi_size*i,limits_horiz[1]-limits_horiz[0],roi_size)) 
            print("det_roi%i successsfully added..."%i)
        except:
            print("det_roi%i"%i,(0,roi_size*i,detObj.image.width,roi_size))
    
    ACTIVE_MG_Obj = current_session.env_dict["ACTIVE_MG"]

    #clean counters
    ACTIVE_MG_Obj.disable("*std*")
    ACTIVE_MG_Obj.disable("*min*")
    ACTIVE_MG_Obj.disable("*avg*")
            
        
def analyse_BL_freqs():
    """
    Measure the vibrations in the beam by analysing the incident flux
    variations, typically with a silicon wedge in the beam looking
    at the reflected signal
    """
    pass

def auto_alignSiWedge4BL_freqs():
    """
    define a path to automatically align the silicon wedge for this 
    measurement the goal is this should be a one command execution 
    once the wedge is in the beam
    
    e.g
    wedge to half cut position
    fscan eta to find the reflected beam at some th/2th
    measure the beam size in angle 
    change the incident angle so you can barely see the reflected beam 
    above the direct beam.
    """
    pass

def logbook_plotroi():
    pass
    
def auto_alignDCM():
    """
    align the DCM based on s3 or s6 depending on the counter you use.
    """
    
    ## check vertical
    #kapin bv4
    #rcmd_eval("nano3:optics","plotselect bv4")
    #rcmd_eval("nano3:optics","vfcgain bv4 4") # 9kev
    ##rcmd_eval("nano3:optics","vfcgain bv4 2") # 21kev
    
    #rcmd_eval("nano3:slits","slitgap s3 0.1 3") # 9kev
    ##rcmd_eval("nano3:slits","slitgap s3 0.2 3") # 21kev

    #rcmd_eval("nano3:optics","dscan dcm2rol -0.003 0.003 60 0.1")
    #rcmd_eval("nano3:optics","piccen")
    
    ##check horizontal
    #kapout bv4
    ##rcmd_eval("nano3:optics","plotselect exp1")
    #kapin opt1
    #rcmd_eval("nano3:slits","slitgap s3 3 0.1")
    ##rcmd_eval("nano3:optics","vfcgain exp1 2")     # should be 1 with two u27 ... 
    #rcmd_eval("nano3:optics","vfcgain opt1 1")
    #rcmd_eval("nano3:optics","plotselect opt1")
    #rcmd_eval("nano3:optics","dscan dcm2pit -0.00125 0.00125 60 0.1")
    #rcmd_eval("nano3:optics","pic")
    #rcmd_eval("nano3:optics","umvr dcm2pit -0.0004") # 9keV
    ##rcmd_eval("nano3:optics","umvr dcm2pit -0.0003") # 8keV
    
    #kapout opt1
    ##kapin bv4
    ##rcmd_eval("nano3:optics","plotselect bv4")
    ##rcmd_eval("nano3:optics","vfcgain bv4 4")    
    ##sanity check - should be in the middle if the above went well
    ##rcmd_eval("nano3:slits","slitgap s3 0.1 3")
    ##rcmd_eval("nano3:slits","dscan s3vo -3 3 60 0.1")
    
    ##rcmd_eval("nano3:slits","slitgap s3 3 0.1")
    ##rcmd_eval("nano3:slits","dscan s3ho -3 3 60 0.1")
  
    #rcmd_eval("nano3:slits","slitgap s3 3 3 ")
    pass
    
   
def limaAlarm():
    """
    set up the necessary checks of beam/ auto valves
    using the limaAlarm function in ID01sware
    """
    # TODO
    # TODO PENNING + PIRANI done - now ion pumps on the beamline
    # rather than pulling from device servers make objects in BLISS
    pass

def hexapod_drift_correction():
    """
    calculate the hexapod correction required to move from eta0,phi0
    to eta1,phi1
    """
    #TODO look id01lib/processing/camview...
    
    #LUT_fn = '/data/id01/inhouse/leake/data_analysis/id01projects/20190211_hexapod_characterisation/camview/experiment_6/LUT_20190212.h5'
    #camview_drift_calib.get_relative_shift(LUT_fn,np.array([0,0]),np.array([phi,eta]))
    pass



class ANSI():
    """
    background: allows background formatting. Accepts ANSI codes between 40 and 47, 100 and 107
    style_text: corresponds to formatting the style of the text. Accepts ANSI code between 0 and 8
    color_text:  Corresponds to the text of the color. Accepts ANSI code between 30 and 37, 90 and 97
    """
    def background(code,string):
        return "\33[{code}m{string}\33[0m".format(code=code,string=string)
  
    def style_text(code,string):
        return "\33[{code}m{string}\33[0m".format(code=code,string=string)
  
    def color_text(code,string):
        return "\33[{code}m{string}\33[0m".format(code=code,string=string)
  
  
#example_ansi = ANSI.background(
#    97) + ANSI.color_text(35) + ANSI.style_text(4) + " TESTE ANSI ESCAPE CODE"
#print(example_ansi)

# # lazy slit function for use immediately
# def sd():
#     gaps = f"slitgap_s6(%.3f,%.3f)"%(setup_globals.s6vg.position,setup_globals.s6hg.position)
#     offsets = f"slitoffsets_s6(%.3f,%.3f)"%(setup_globals.s6vo.position,setup_globals.s6ho.position)
#     print(gaps+" ; "+offsets)
    
# def slitgap_s6(s6vg_pos,s6hg_pos):
#     umv(setup_globals.s6vg,s6vg_pos,setup_globals.s6hg,s6hg_pos) 

# def slitoffsets_s6(s6vo_pos,s6ho_pos):
#     umv(setup_globals.s6vo,s6vo_pos,setup_globals.s6ho,s6ho_pos) 

# class Slits():
#     """
#     Maybe make a slits object which holds all of the slits a la spec
#     """
#     def __init__():
#         # add all motors
#         pass

     
def unix_exec(command):
    """
    utility function for executing commands on the command line
    """
    os.system(command)
    
def plot_ct(count_time=1):
    """
    Plot an (unsaved) count in a matplotlib window with useful info.
    EZ 20220330 - first simple version
    """
    _im = ct(count_time).get_data()['mpx1x4:image'].as_array()

    plt.figure(facecolor='whitesmoke')
    plt.imshow(_im, norm=mpl.colors.LogNorm(), cmap='magma')
    plt.colorbar()

    title  = f'\n $\mathbf{{\eta}}={eta.position:.3f}\degree, $'
    title += f'$\mathbf{{\delta}}={delta.position:.3f}\degree, $'
    title += f'$\mathbf{{\phi}}={phi.position:.3f}\degree$'
    title += f'\n {datetime.now().strftime("%b %d | %H:%M:%S")}'

    plt.title(title)
    plt.tight_layout()
    plt.show()
    
    
from bliss.shell import standard
from bliss.common import session as session_mod

def goto_flintcursor():
    """
    use flint to move to a position on a scan
    """
    session = session_mod.get_current_session()
    scans = session.env_dict['SCANS']
    if not scans:
        raise RuntimeError("No scan available; Need to do a scan first!")
    last_scan = scans[-1]
    data = last_scan.get_data()
    axis_names = [x for x in data.keys() if x.find('axis:') > -1]
    #if len(axis_names) > 1:
    #    raise RuntimeError("Not managed yet ;)")

    f = standard.flint()
    if len(axis_names) == 1:
        axis_name = axis_names[0].split(':')[-1]
        axis = session.env_dict[axis_name]
        p = f.get_live_plot("default-curve")

        position = p.select_points(1)
        motor_pos = position[0][0]
        umv = session.env_dict['umv']
        umv(axis,motor_pos)

    elif len(axis_names) == 2:
        ax0_name = p.xaxis_channel_name #axis_names[0].split(':')[-1]
        ax0 = session.env_dict[ax0_name]
        ax1_name = p.yaxis_channel_name #axis_names[1].split(':')[-1]
        ax1 = session.env_dict[ax1_name]
        p = f.get_live_plot("default-scatter")        
        position = p.select_points(1)
        motor_pos0 = position[0][0]
        motor_pos1 = position[0][1]
        umv = session.env_dict['umv']
        umv(ax0,motor_pos0,ax1,motor_pos1)

    else:
        print("scan type not managed yet ....")

def move_on_click():
    session = session_mod.get_current_session()
    umv = session.env_dict['umv']
    scans = session.env_dict['SCANS']
    if not scans:
        raise RuntimeError("No scan available; Need to do a scan first!")
    last_scan = scans[-1]
    data = last_scan.get_data()

    f = standard.flint()

    scatter_plot = False
    plots = last_scan.scan_info.get('plots',[])
    if isinstance(plots,[]):
        for plot_info in plots:
            kind = plot_info.get('kind')
            if kind == "scatter":
                scatter_plot = True
                break

    if scatter_plot:
        p = f.get_live_plot("default-scatter")
        axis_1_name =  p.xaxis_channel_name
        axis_2_name = p.yaxis_channel_name
        
        if (not axis_1_name.startswith('axis:') or
            not axis_2_name.startswith('axis:')):
            raise RuntimeError("One of scatter axis is not a motor")

        axis1 = session.env_dict[axis_1_name]
        axis2 = session.env_dict[axis_2_name]

        position = p.select_points(1)
        axis_1_pos,axis_2_pos = position[0]
        umv(axis1,axis_1_pos,axis2,axis_2_pos)
    else:
        p = f.get_live_plot("default-curve")
        axis_name = p.xaxis_channel_name
        if axis_name.find('axis:') == -1:
            raise RuntimeError("Can't find an axis on plot")

        axis_name = axis_name.split(':')[-1]
        axis = session.env_dict[axis_name]

        position = p.select_points(1)
        motor_pos = position[0][0]
        umv(axis,motor_pos)



def diff_motpos_from_scan(scan_fn_no={},*motors):
    """
    return the difference in motor positions between two scans
    if only one scan number is provided it will compare to where the motors are now
    scan_fn_no = dict of filename:list scan nos, if >1 key all scans will be compared to the first in the list
    fn = optional if the dataset has changed
    """

    #TODO pretify the output with that similar to positionsGroup.

    #TODO add a move functionality in the case where there is just 1 or 2 inputs

    # the handling of the scan_fn_no dict is a little sensitive
    # solution is fn:list of scan numbers
    nb_scans=0
    for key in scan_fn_no:
        nb_scans += len(scan_fn_no[key])

    if nb_scans == 1:
        # compare to motor positions now
        fn = list(scan_fn_no.keys())[0]
        #if np.isscalar(scan_fn_no[fn]):
        #    nb = scan_fn_no[fn]
        #else:
        nb = scan_fn_no[fn][0]

        scan_dict = dict()
        posnow_dict = dict()

        for axis in motors:
            positioner = get_scan_motor_hdf5(nb,fn,axis.name)
            if not np.isscalar(positioner):
                # i.e. an array
                print(f"WARNING scan ({nb}) axis: {axis.name} was scanned...")
            scan_dict[axis.name] = positioner.mean()

            posnow_dict[axis.name] = axis.position

        for axis in motors:
            delta = posnow_dict[axis.name] - scan_dict[axis.name]
            print(f"{axis.name}: Scan {nb}: {scan_dict[axis.name]:.02f} Pos Now: {posnow_dict[axis.name]:.02f} Delta: {delta:.02f}")

        return scan_dict, posnow_dict


    if nb_scans>1:
        # compare all scans to the first scan

        # ref_dict
        fn = list(scan_fn_no.keys())[0]
        nb = scan_fn_no[fn][0]
        ref_nb = nb
        ref_fn = fn
        ref_dict = dict()

        for axis in motors:
            positioner = get_scan_motor_hdf5(nb,fn,axis.name)
            if not np.isscalar(positioner):
                # i.e. an array
                print(f"WARNING scan ({nb}) axis: {axis.name} was scanned...")
            ref_dict[axis.name] = positioner.mean()

        for key in scan_fn_no:
            fn = key
            nbs = scan_fn_no[fn]
            for nb in nbs:
                if ref_fn == fn and ref_nb==nb:
                    pass
                else:
                    scan_dict = dict()

                    for axis in motors:
                        positioner = get_scan_motor_hdf5(nb,fn,axis.name)
                        if not np.isscalar(positioner):
                            # i.e. an array
                            print(f"WARNING scan ({nb}) axis: {axis.name} was scanned...")
                        scan_dict[axis.name] = positioner.mean()

                    for axis in motors:
                        delta = ref_dict[axis.name] - scan_dict[axis.name]
                        print(f"{axis.name}: Scan {nb}: {scan_dict[axis.name]:.02f} Scan {ref_nb}: {ref_dict[axis.name]:.02f} Delta: {delta:.02f}")


def move_to_scan_motpos(nb,filename = None,*motors):
    """
    move all requested motors to the positions found in scan(nb) of fn
    """
    query_user=True # forced
    motion = ()
    for axis in motors:
        try:
            positioner = get_scan_motor_hdf5(nb,filename,axis.name) 
        except KeyError:
            print(f"could not find {axis.name} in filename:{filename}")
        motion += (axis,positioner.mean(),)

    if query_user:
        ans = click.confirm(f"Do you want to move the motors?")
            
        if not ans:
            print(" ... you chose no ... exiting ...")
            return motion
        else:
            umv(*motion)
            
def CamView():
	# only valid for lid01nano3!!! until it is set up correctly
	subprocess.Popen("/home/blissadm/local/id01sware/bin/CamView")
	

def find_active_detector():
    '''
    Automatically find the currently active detector and return (hard-coded) pixel size
    '''
    for i in current_session.env_dict['ACTIVE_MG'].available: 

        if i.count("mpx1x4:image"): 
            print("found mpx1x4") 
            detector_found = 'mpx1x4'
            pixel_size = 55e-6

        if i.count("mpxgaas:image"): 
            print("found mpxgaas")
            detector_found = 'mpxgaas'
            pixel_size = 55e-6

        if i.count("eiger2M:image"): 
            print("found eiger2M")
            detector_found = 'eiger2M'
            pixel_size = 75e-6

        if i.count("simcam1:image"): 
            print("found simcam1")
            detector_found = 'simcam1'
            pixel_size = 55e-6        
    return detector_found, pixel_size

def create_delta_nu_arrays(data,deltaMne="delta",nuMne="nu"):
    
    '''
    Careful, the detector calibration must be done. Otherwise the two-theta array will be wrong.
    
    args
    :data: detector array from a ct()
    
    return
    :two_theta: two_theta array values per detector pixel
    '''
    
    detector_found, pixel_size = find_active_detector()
    
    y,x = np.indices(data.shape, dtype='float64')

    if detector_found=='mpx1x4': # Is it true that metadata are the same for mpxgaas?
        distance = current_session.env_dict['detcalib_mpx1x4'].distance 
        y -= current_session.env_dict['detcalib_mpx1x4'].beam_center_y
        x -= current_session.env_dict['detcalib_mpx1x4'].beam_center_x
        
    if detector_found=='mpxgaas': # Is it true that metadata are the same for mpxgaas?
        distance = current_session.env_dict['detcalib_mpxgaas'].distance 
        y -= current_session.env_dict['detcalib_mpxgaas'].beam_center_y
        x -= current_session.env_dict['detcalib_mpxgaas'].beam_center_x

    if detector_found=='eiger2M' :
        distance = current_session.env_dict['detcalib_eiger2M_bigpipe'].distance 
        y -= current_session.env_dict['detcalib_eiger2M_bigpipe'].beam_center_y
        x -= current_session.env_dict['detcalib_eiger2M_bigpipe'].beam_center_x

    if detector_found=='simcam1' :
        distance = current_session.env_dict['detcalib_simcam1'].distance 
        y -= current_session.env_dict['detcalib_simcam1'].beam_center_y
        x -= current_session.env_dict['detcalib_simcam1'].beam_center_x
    y *= pixel_size
    x *= pixel_size
    
    
    # Hope the formulas are correct
    delta_grid = np.deg2rad(current_session.env_dict[deltaMne].position) - np.arctan(y/distance)
    nu_grid  = np.deg2rad(current_session.env_dict[nuMne].position) - np.arctan(x/(distance*np.cos(delta_grid)))
    
    return np.rad2deg(delta_grid), np.rad2deg(nu_grid)

def create_two_theta_array(data, deltaMne="delta", nuMne="nu"):
    
    delta_grid, nu_grid = create_delta_nu_arrays(data, deltaMne=deltaMne,nuMne=nuMne)
    two_theta = np.arccos( np.cos(np.deg2rad(delta_grid)) * np.cos(np.deg2rad(nu_grid)) )
    
    return np.rad2deg(two_theta)


def get_index_sign_change(array):
    signs = np.sign(array)
    signchange = ((np.roll(signs, 1) - signs) != 0).astype(int)
    arg_signchange = np.where(signchange)[0]
    return arg_signchange

def interactive_two_theta_plot_text(ax,
                                    levels, two_theta):
    
    y,x = np.indices(two_theta.shape) 
    
    for n in range(len(levels)):
        printed = False
        
        # Try to print on the left border
        array = two_theta[:,-1]-levels[n]
        arg_signchange = get_index_sign_change(array)
        if arg_signchange.size != 0 and printed is False:
            ax.text(np.max(x)*1.01, y[arg_signchange[-1], -1], round(levels[n],2))
            printed = True
           
        if printed is False:
            array = two_theta-levels[n]
            sign_change = (np.sign(array)[1:]-np.sign(array)[:-1]) != 0
            y_index, x_index = np.argwhere(sign_change !=0)[-1]
            ax.text(x_index, y_index , round(levels[n],2))     
            
    return


def interactive_two_theta_plot(exposure=1):

    tmp_scan = ct(exposure)
    data = tmp_scan.get_data("image").as_array()

    two_theta = create_two_theta_array(data, )
    
    def handler(fig, ax, event):
        '''
        Overcomplicated function to get the correct pixel index when clicking on the figure
        This worked fine with mpx1x4. 
        '''
        if ax.in_axes(event):
            # Transform the event from display to axes coordinates
            ax_pos = ax.transAxes.inverted().transform((event.x, event.y))
            #ex situ pristine test before November Beamtime
            # ax_pos contains two float values between 0 and 1 that I then transform into pixel index
            y_index_clicked = y[::-1][int(y.shape[0]*ax_pos[1])][0]
            x_index_clicked = x[:,int(x.shape[1]*ax_pos[0])][0]

            print('pixel clicked indexes : ', y_index_clicked, x_index_clicked) # To check that the clicking works properly
            print('two_theta :', two_theta[y_index_clicked, x_index_clicked])

    fig, ax = plt.subplots(1, 1)
    ax.matshow(np.log(data))#, norm=LogNorm())

    y,x = np.indices(data.shape) 
    levels = np.linspace(np.min(two_theta), np.max(two_theta), 15)

    plt.contour(x,y,two_theta, levels=levels, colors='k', alpha=.5)

    interactive_two_theta_plot_text(ax, levels, two_theta)
    

    handler_wrapper = functools.partial(handler, fig, ax)
    fig.canvas.mpl_connect('button_press_event', handler_wrapper)

    print('Careful, if the detector calibration is not done yet, the two_theta array will be wrong.')
    plt.show()

    return


def limatakeID01(exposure, npts,*limadevs):
    """
    normal limatake 
    it will open the fast shutter
    expsosure time and number of points
    """
    fshut_mode_force_open()
    limatake(exposure,npts,*limadevs,save=True,acq_mode="ACCUMULATION")
    fshut_mode_p201()



def fit_scan_gauss(fn,scan_no,axis_name, counter, norm="linear", verbose = False):
    data=h5.File(fn)
    intensity00=data[f"{scan_no}.1/measurement/{counter}"][()]
    #TODO imcompatible with dsxdm pix is pix_position!
    
    #if axis_name.count("pi"):
    #    axis_name=axis_name+"_position"
    x00 = data[f"{scan_no}.1/measurement/{axis_name}"][()]
    label = data[f"{scan_no}.1/title"][()]
    H, A, x0, sigma = gauss_fit(x00, intensity00)
    FWHM = 2.35482 * sigma

    if verbose:
        print(f'\nscan {scan_no}:')
        print('The offset of the gaussian baseline:      ', H)
        print('The center of the gaussian fit:           ', x0)
        print('The sigma of the gaussian fit:            ', sigma)
        print('The maximum intensity of the gaussian fit:', H + A)
        print('The Amplitude of the gaussian fit:        ', A)
        print('The FWHM of the gaussian fit:             ', FWHM)
    return (x00,intensity00,label,x0,sigma,H,A,FWHM)

def get_scan_intensity_max_sum(fn,scan_no, counter, verbose = False):
    data=h5.File(fn)
    intensity=data[f"{scan_no}.1/measurement/{counter}"][()]
    return np.max(intensity),np.sum(intensity)

#def ID01_run_macro(path2filename):
#    exec(open(path2filename).read())
# TODO needs to import everything when executed this way....
try:
    icat = icat_client_from_config(current_session.scan_saving.icat_client_config)

    def elogbook_send_file(filename):   
        icat.send_binary_file(filename)
except:
    print("couldnt find icat info")


def fswrap_dscan(motObj,start,stop,intervals,count_time):
    """
    wrap a dscan with a fast shutter open/close... the fast shutter was not working well so we were losing some points.
    """
    fshut_mode_force_open()
    dscan(motObj,start,stop,intervals,count_time)
    fshut_mode_p201()


# doesnt handle this well
#def run_macro(filepath):
#    exec(open(filepath).read()) 

def convertScan2tiff(filepath, nb):
    scan = openScan(filepath,nb)
    data = scan.getImages()

    paths = filepath.split("/RAW_DATA/")
    newpath = os.path.join(paths[0],"PROCESSED_DATA/",*paths[1].split("/")[:-1],"scan%.4i"%nb)
    try:
        os.makedirs(newpath)
    except:
        print(f"couldnt make dirs {newpath}")

    newpath=newpath+".tiff"

    print(f"saving tiffs to {newpath}")
    tifffile.imwrite(newpath,data)

