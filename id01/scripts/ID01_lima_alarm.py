# -*- coding: utf-8 -*-

# ID01 BLISS git repo
#       authors:
#         Steven Leake, leake@esrf.fr
#
#       Created at: 15. Feb 16:36:00 CET 2022
#
#----------------------------------------------------------------------
# Description: 
#       alarm systems for the beamline
#       installed an older version of pygame for 1.9.6
#
# TODO:
#      TODO optimise output with stdout
#----------------------------------------------------------------------


import sys
import os
import pygame
import time
import numpy as np

from bliss import current_session
from bliss import setup_globals as SG
from bliss.controllers.tango_shutter import TangoShutterState

from id01.scripts.ID01_utils import ANSI

   
def get_directory_size(start_path = '.'):
    """
    recursively find the size of the folder and all inside it except symbolic links
    """
    total_size = 0
    for dirpath, dirnames, filenames in os.walk(start_path):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            # skip if it is symbolic link
            if not os.path.islink(fp):
                total_size += os.path.getsize(fp)

    return total_size
    
def email_alert(message = "the beamline is calling", email_adds = ("leake@esrf.fr",)):
    """
    send a message to email addresses
    """
    for email in email_adds:
        os.system(f'echo `date`  | mail -s "{message}" {email}')


class ID01_lima_alarm():
    """
    ID01 alarm system - provides text, audible, email warnings
    """
    
    def __init__(self, email_adds = ("leake@esrf.fr",), pts_moving_avg=10):
        self.pts_moving_avg  = pts_moving_avg
        self.moving_avg = dict()
        self._ii = 0
        self.email_adds = email_adds
    

        # "SR_Filling_Mode"
        self.current_expected = dict()
        self.current_expected["7/8 multibunch"] = 200
        self.current_expected["16 bunch"] = 35
        self.current_expected["4 bunch" ] = 10
        self.current_expected["28*12+1 bunch" ] = 200
        self.current_expected["uniform multibunch"] = 200


        # LN2 cryo pressure DCM
        self.ref_vals = dict()
        self.ref_vals["cryo_p1"] = 6.67 #+0.3 #7.30 #6.95  #6/92
        self.ref_vals["cryo_p2"] = 7.53 #+0.3 #7.82 #7.51  #7.8
        self.ref_vals["cryo_p3"] = 7.61 #+0.3 #7.96 #7.68  #7.9    
        
        self.toggle_alerts = dict()
        self.toggle_alerts["audible_alert"] = True                   
        self.toggle_alerts["terminal_alert"] = True                   
        self.toggle_alerts["email_alert"] = True                   
    
    def set_email_adds(self, email_adds):
        self.email_adds = email_adds

    def set_pts_moving_avg(self, pts_moving_avg):
        self.pts_moving_avg = pts_moving_avg
                            
    def audible_alert(self, sound_fn, text_alert = ""):
        if sound_fn is not None:
            if self.toggle_alerts["audible_alert"]:
                # make a noise
                pygame.mixer.init()
                pygame.mixer.music.load(sound_fn)
                pygame.mixer.music.play()

    def terminal_alert(self, message = "the beamline is calling"):
        if self.toggle_alerts["terminal_alert"]:
            sys.stdout.write("\n"+message+"\n")
        
    def email_alert(self, message = "the beamline is calling"):
        if self.toggle_alerts["email_alert"]:
            for email in self.email_adds:
                os.system(f'echo `date`  | mail -s "{message}" {email}')

                    
    def check_SR_current(self, sound_fn ,tolerance = 0.9):
        """
        check the storage ring current, account for ring modes and top up
        """
        EBSobj = SG.EBS
        bunch_mode_current = self.current_expected[EBSobj.all_information["SR_Filling_Mode"]]

        key = "SR_current"
        
        try:
            tmp = self.moving_avg[key]
        except:
            self.moving_avg[key] = np.zeros(self.pts_moving_avg)
            
        self.moving_avg[key] = np.roll(self.moving_avg[key],-1,0)
        self.moving_avg[key][-1] = EBSobj.counters.current.value
        
        if self._ii > self.pts_moving_avg:
            if self.moving_avg[key].mean() < bunch_mode_current*tolerance:
                self.audible_alert(sound_fn)
                self.terminal_alert()
                self.email_alert()
                
                #  wait for refill
                while EBSobj.counters.refill.value == 0:
                    sleep(1)
                    print("waiting for refill") 
    
    def check_DCM_cryo(self, sound_fn, tolerance = 0.025):
        """
        check the pressure through the cryo loop of the DCM
        """
        wagoObj = current_session.env_dict["wcid01cryo1"]
        
        for key in ["cryo_p1","cryo_p2","cryo_p3"]:
            
            try:
                tmp = self.moving_avg[key]
            except:
                self.moving_avg[key] = np.zeros(self.pts_moving_avg)
                            
            self.moving_avg[key] = np.roll(self.moving_avg[key],-1,0)
            self.moving_avg[key][-1] = wagoObj.get(key)
            if self._ii > self.pts_moving_avg:
                if self.moving_avg[key].mean() < self.ref_vals[key]*(1-tolerance) or self.moving_avg[key].mean() > self.ref_vals[key]*(1+tolerance):
                    self.audible_alert(sound_fn)
                    self.terminal_alert(message = ANSI.color_text(31, "WARNING: LIQUID NITROGEN DCM PRESSURE FLUCTUATION"))
                    self.email_alert(message = "OH DEAR - LN2 pressure warning - check the beamline")
            


    def check_vacuum_valves(self, ):
        """
        check all vacuum valves and return those that are not in an Open state
        """
        valves = []
        for _i in np.arange(20):
            if (_i!=2 and _i!=3 and _i!=10 and _i!=14 and _i!=18):
                valveObj = current_session.env_dict["rv%i"%_i]
                if valveObj.state != TangoShutterState("Open"):
                    valves.append(valveObj)
                    
        # TODO add warning?
        return valves

    def check_penning_gauges(self,):
        """
        check all penning gauges and return those that are not in an ON state
        """
        gauges = []
        for _i in [11,21,31,41,51,61,71,81,91]:  # TODO check pir12
            gaugeObj = current_session.env_dict["pir%i"%_i]

            if gaugeObj.state != "ON":
                gauges.append(gaugeObj)
                
        # TODO add warning?
        return gauges


    def check_FE_automatic_mode(self, sound_fn):
        """
        make sure FE is in automatic mode, i.e. open and recovers after a beam loss
        """
        EBSobj = current_session.env_dict["EBS"]
        FEobj = current_session.env_dict["fe"]
        if FEobj.state != TangoShutterState("Automatic opening"):
            #FEobj.open() 
            EBSobj.automatic_mode = True
            
    def check_safety_shutter(self, sound_fn, detObj=None):
        """
        make sure the safety shutter is open during acquisition
        """
        safshutObj = current_session.env_dict["safshut"]
        
        if detObj is None:
            if safshutObj.state != TangoShutterState("Open"):
                self.audible_alert(sound_fn)
                self.terminal_alert(message = ANSI.color_text(31, " OPENING THE SAFETY SHUTTER"))
                self.email_alert(message = "Safety shutter was closed - trying to open")            
                try:
                    safshutObj.open()
                    if safshutObj.state != TangoShutterState("Open"):
                        self.audible_alert(sound_fn)
                        self.terminal_alert(message = ANSI.color_text(32, " OPENED THE SAFETY SHUTTER"))
                        self.email_alert(message = "Safety shutter was reopened") 
                except:
                    print("....Failed please investigate") 
        else:
            if safshutObj.state != TangoShutterState("Open") and detObj.acquisition.status == "Running":
                self.audible_alert(sound_fn)
                self.terminal_alert(message = ANSI.color_text(31, " PLEASE OPEN THE SAFETY SHUTTER"))
    
    def check_detector_direct_beam(self, detObj, filtObj, sound_fn):
        """
        check if the detector is in the direct beam - warn that filters should be used
        """
        
        # if the detector is close to the direct beam eta/delta < 2
        eta = current_session.env_dict["eta"]
        delta = current_session.env_dict["delta"]
        if eta.position < 2 and delta.position < 2 and filtObj.filter <= 5:
            self.audible_alert(sound_fn)
            self.terminal_alert(message = ANSI.color_text(31, " DETECTOR CLOSE TO DIRECT BEAM and only f%i in"%filtObj.filter))
            
        # TODO deal with the Eiger

    def check_detector_max_cnts(self, detObj, filtObj, max_counter = None):
        """
        check if the detector is in the direct beam - warn that filters should be used
        """
               
        # TODO for this the only way I know is to pull the image from the device server directly.
               
        # TODO deal with the Eiger/maxipix other      
        pass  

    def check_sample_filter(self, detObj, filtObj, filter_limit, sound_fn=None):
        """
        check and warn that enough filters are used
        """
        filtVal = filtObj.filter  # TODO suppress text output in filter
        if filtVal < filter_limit and detObj.acquisition.status == "Running":
            self.audible_alert(sound_fn)
            self.terminal_alert(message = ANSI.color_text(31, f" FILTERS BELOW LIMIT: {filter_limit} \n     current setting: {filtVal}"))

    def start_alarm_system_unattended(self,):
        """
        Alarm system for saving the beamline during the night
        """
        attempt_reopen=0

        while True:
            
            ## SR current
            sound_fn = "/users/blissadm/local/id01/id01/media/drumroll.mp3"
            self.check_SR_current(sound_fn)
            
            ## LN2 pressure
            sound_fn = "/users/blissadm/local/id01/id01/media/drumroll.mp3"
            self.check_DCM_cryo(sound_fn)

            ## Vacuum valves
            sound_fn = "/users/blissadm/local/id01/id01/media/drumroll.mp3"
            valves = self.check_vacuum_valves()
            try:
                valves.remove(current_session.env_dict["rv19"])  # TODO deal with rv19
            except:
                pass
            if len(valves):
                for valveObj in valves:
                    try:
                        valveObj.open()
                    except:
                        self.audible_alert(sound_fn)
                        self.terminal_alert(message = ANSI.color_text(31, f"WARNING: Valve {valveObj.name} not opening "))
                        self.email_alert(message = f"OH DEAR - Valve {valveObj.name} not opening -  check the beamline")
                                    
            ## Penning gauges
            sound_fn = "/users/blissadm/local/id01/id01/media/drumroll.mp3"
            gauges = self.check_penning_gauges()
            if len(gauges):
                for gaugeObj in gauges:
                    try:
                        if gaugeObj.name == "rv19":  # Be window
                            pass  # TODO
                        #gaugeObj.set_on()  # TODO
                        #gaugeObj.reset()  # TODO
                        pass
                    except:
                        self.audible_alert(sound_fn)
                        self.terminal_alert(message = ANSI.color_text(31, f"WARNING: Valve {gaugeObj.name} not opening "))
                        self.email_alert(message = "OH DEAR - Valve {gaugeObj.name} not opening -  check the beamline")            
            
            ## Ion pumps
            
            ## FE state
            sound_fn = "/users/blissadm/local/id01/id01/media/drumroll.mp3"
            self.check_FE_automatic_mode(sound_fn)
            
            ## safety shutter
            sound_fn = "/users/blissadm/local/id01/id01/media/drumroll.mp3"
            self.check_safety_shutter(sound_fn) 
                       
            ## anything else
            
            ###check filters for kmap in case the user forgot t remove them
            
            # perhaps the mirror cooling

            # protect the Eiger from the direct beam

            ## define frequency
            time.sleep(5)
            self._ii+=1
            sys.stdout.write("\r %i"%self._ii)
            sys.stdout.flush()


    def start_alarm_system_attended(self, detObj, filtObj,filter_limit=1):
        """
        Alarm system for saving the beamline during user attendedoperation
        """
        attempt_reopen=0

        while True:
            
            ## SR current
            sound_fn = "/users/blissadm/local/id01/id01/media/siren.wav"
            self.check_SR_current(sound_fn)
            
            """
            ## LN2 pressure
            sound_fn = "/users/blissadm/local/id01/id01/media/drumroll.mp3"
            self.check_DCM_cryo(sound_fn)

            ## Vacuum valves
            sound_fn = "/users/blissadm/local/id01/id01/media/drumroll.mp3"
            valves = self.check_vacuum_valves()
            try:
                valves.remove(current_session.env_dict["rv19"])  # TODO deal with rv19
            except:
                pass
            if len(valves):
                for valveObj in valves:
                    try:
                        valveObj.open()
                    except:
                        self.audible_alert(sound_fn)
                        self.terminal_alert(message = ANSI.color_text(31, f"WARNING: Valve {valveObj.name} not opening "))
                        self.email_alert(message = f"OH DEAR - Valve {valveObj.name} not opening -  check the beamline")
                                    
            ## Penning gauges
            sound_fn = "/users/blissadm/local/id01/id01/media/drumroll.mp3"
            gauges = self.check_penning_gauges()
            if len(gauges):
                for gaugeObj in gauges:
                    try:
                        if gaugeObj.name == "rv19":  # Be window
                            pass  # TODO
                        #gaugeObj.set_on()  # TODO
                        #gaugeObj.reset()  # TODO
                        pass
                    except:
                        self.audible_alert(sound_fn)
                        self.terminal_alert(message = ANSI.color_text(31, f"WARNING: Valve {gaugeObj.name} not opening "))
                        self.email_alert(message = "OH DEAR - Valve {gaugeObj.name} not opening -  check the beamline")            
            """
            ## Ion pumps
            
            """
            ## FE state
            sound_fn = "/users/blissadm/local/id01/id01/media/drumroll.mp3"
            self.check_FE_automatic_mode(sound_fn)
            """
            
            ## safety shutter
            sound_fn = "/users/blissadm/local/id01/id01/media/cymbal.wav"
            self.check_safety_shutter(sound_fn, detObj) 
            
            ## check detector direct beam
            sound_fn = "/users/blissadm/local/id01/id01/media/siren.wav"
            self.check_detector_direct_beam(detObj, filtObj, sound_fn)                        
            
            ## anything else
            
            ## USER
            #sound_fn = "/users/blissadm/local/id01/id01/media/cymbal.wav"
            #self.check_sample_filter(detObj, filtObj, filter_limit, sound_fn)
            
            ###check filters for kmap in case the user forgot t remove them
            
            # protect the Eiger from the direct beam

            ## define frequency
            time.sleep(0.5)
            self._ii+=1
            sys.stdout.write("\r %i"%self._ii)
            sys.stdout.flush()

#TODO: add this https://www.narakeet.com/app/text-to-audio/?projectId=6ee4a07a-cb54-43fc-8151-c8086fc6d485 