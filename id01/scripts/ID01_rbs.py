# ID01 BLISS git repo
#       authors:
#         Peter Boesecke, boesecke@esrf.fr
#
#       Created at: 11. Oct 17:22:00 CET 2022
#
#----------------------------------------------------------------------
# Description:
#       class Rbs(CalcController)
#       Macros for positioning the rotative beamstop for saxs (rbs)
#
# History:
#   2022-10-11 PB creation
#   2022-10-20 PB all t -> trans
#   2022-10-24 PB ID01_sxrbsvalve
#   2022-10-25 PB HashObjSetting  (keeping currently used settings in redis)
#                 ID01_sxrbsvalve.py -> ID01_rbsvalve.py
#                 ID01_sxrbs.py -> ID01_rbs.py
#   2022-10-27 PB rbsnow, bvop, bvcl, bvstatus, bvitlck,
#                 bigvalve_automatic_reopen
#   2022-10-28 PB testing
#   2022-10-31 PB saving rbsvalve parameters in config
#   2022-11-02 PB savetoyml saves rbs and rbsvalve
#   2023-01-17 PB class Axis -> CalcAxis
#                 __init__ call updated (removing ctrl_name)
#
#----------------------------------------------------------------------

import numpy as np
from bliss.controllers.motor import CalcController
from bliss.common.axis import CalcAxis as AxisBase
from bliss.config.settings import HashObjSetting

from bliss.setup_globals import *
from bliss.common import session
from bliss.config import static

from id01.scripts.specmot_utils import *

from id01.scripts.ID01_rbsvalve import *

import time

import logging
#logging.basicConfig(level=logging.DEBUG)
#logging.basicConfig(level=logging.WARNING)
#logging.debug('This will get logged')
#logging.warning('This will get logged')

_log=logging.getLogger("id01.scripts.ID01_rbs")
#The log level can be set in the bliss session after import logging
#and defining _log as in the above command.
#_log.setLevel("DEBUG")

class Rbs(CalcController):
    """
    Calculated motors for positioning a rotative beamstop

              Geometric parameters

              h0[mm]      : horizontal offset [mm]
              v0[mm]      : vertical offset [mm]
              radius[mm]  : radius of arm holding the beamstop [mm]
              alpha0[deg] : inclination of the translation motor [deg]

              mode        : calculation of trans, either trans1 (mode 0) or trans2 (mode!=0)


    The motors 'h' [mm] and 'v' [mm] are calculated from the real motors 
    'trans' and 'alpha' in the following way using the parameters of the active stripe:

           h[mm] = H0[mm]+deltah[mm] : horizontal position
           v[mm] = V0[mm]+deltav[mm] : vertical position

    (1a)   deltah[mm] = Radius[mm]*cos(rad(alpha[deg]))+trans[mm]*cos(rad(Alpha0[deg]))
    (1b)   deltav[mm] = Radius[mm]*sin(rad(alpha[deg]))+trans[mm]*sin(rad(Alpha0[deg]))

    The real motors trans and alpha are calculated from 'h' and 'v' 
    in the following way, depending on mode:

           mode==0:
    (2a)   trans1[mm] = deltah[mm]*cos(rad(Alpha0[deg]))+deltav[mm]*sin(rad(Alpha0[deg]))+ 
                  cos(rad(Alpha0[deg]))*sqrt(-((deltav[mm]-deltah[mm]*tan(rad(Alpha0[deg])))^2)+((Radius[mm]/cos(rad(Alpha0[deg])))^2))
           mode!=0:
    (2b)   trans2[mm] = deltah[mm]*cos(rad(Alpha0[deg]))+deltav[mm]*sin(rad(Alpha0[deg]))- 
                  cos(rad(Alpha0[deg]))*sqrt(-((deltav[mm]-deltah[mm]*tan(rad(Alpha0[deg])))^2)+((Radius[mm]/cos(rad(Alpha0[deg])))^2))
    (3)    alpha[deg] = deg(atan2(deltav[mm]-trans[mm]*sin(rad(Alpha0[deg])),deltah[mm]-trans[mm]*cos(rad(Alpha0[deg]))))

           deltah[mm] = h[mm] - H0[mm]
           deltav[mm] = v[mm] - V0[mm]

    """
    def __init__(self,config,*args,**kwargs):
        super().__init__(config,*args,**kwargs)
        '''
        The parameters are kept in Params.
        '''
        self._config=config
        self.rbs_dict={}  # 
        self.EPS=1e-32

        if 'rbs_dict' in config:
            self.rbs_dict=config['rbs_dict']
        else:
            self.default()
            self.savetoyml()

        # if rbsvalve_dict is None, RbsValve(None) gets the default
        rbsvalve_dict=self.rbs_dict["saxs"].get("rbsvalve_dict")
        self.rbsvalve=RbsValve(rbsvalve_dict)
        if rbsvalve_dict is None:  # default!
            self.rbs_dict["saxs"]["rbsvalve_dict"]=self.rbsvalve.rbsvalve_dict
            self.savetoyml()

        bigvalve_name = self.rbs_dict["saxs"]["bigvalve_name"]
        if bigvalve_name is not None:
            self.bigvalve=static.get_config().get(bigvalve_name)
        else:
            self.bigvalve = None

        # "saxs" is the only available beamstop unit
        self.rbs_setting_dict=HashObjSetting("rbs_setting_{}".format("saxs"))

        # Activate big valve software interlock
        self.bvitlck(True)

        # do not cache _set_position
        ##self.axis_settings.persistent_setting['_set_position'] = False

        # The real and calculated motors must be defined as tagged motors 
        # in <device>.yml.
        # motors specified in <device>.yml:
        #  - name: $<axis1>
        #    tags: real trans
        #
        #  - name: $<axis2>
        #    tags: real alpha
        # ...
        #  - name: hh
        #    tags: hh
        #
        #  - name: vv
        #    tags: vv
        # ...

        ### The motors are defined as tagged motors in <device>.yml

    def default(self):
        '''
        Set ID01 beamstop parameters to default values
        '''

        # default beamstop configuration
        self.rbs_dict={}

        # The motors are defined as tagged motors in <device>.yml

        # saxs
        Rbs={}
        Rbs["trans_mot_tag"] = "trans" # tag of the translation motor (real)
        Rbs["alpha_mot_tag"] = "alpha" # tag of the rotation motor (real)
        Rbs["h_mot_tag"] = "hh"        # tag of the horizontal motor (calculated)
        Rbs["v_mot_tag"] = "vv"        # tag of the vertical motor (calculated)
        Rbs["h0"] = 235 # mm
        Rbs["v0"] = -15 # mm
        Rbs["radius"] = 235.0 # mm
        Rbs["alpha0"] = 172.5 # deg
        Rbs["mode"] = 0 # 0: use solution trans1, otherwise use solution trans2
        
        Rbs["rbsvalve_alpha_max"] = 115 # deg

        Rbs["rbsvalve_positions_default"] = {'bs2':1}

        # default beamstop position
        Rbs["alpha_mot_in_default"] =  176.49000
        Rbs["trans_mot_in_default"] =  4.43600

        Rbs["alpha_mot_search_switch"] = "lim-"
        Rbs["alpha_mot_search_switch_dial"] = 0.0
        Rbs["alpha_mot_dial_lim_neg"] = -0.5
        Rbs["alpha_mot_dial_lim_pos"] = 93.4365
        Rbs["alpha_mot_offset"] = 110.0005 
        Rbs["alpha_mot_dial_out"] = 1.0

        Rbs["trans_mot_search_switch"] = None  # "lim-" # no switch reachable, no search done
        Rbs["trans_mot_search_switch_dial"] = 0.0
        Rbs["trans_mot_dial_lim_neg"] = 0.0
        Rbs["trans_mot_dial_lim_pos"] = 18.6
        Rbs["trans_mot_offset"] = -9.3
        Rbs["trans_mot_dial_out"] = None # no movement required

        Rbs["bigvalve_name"] = 'rv21'
        Rbs["bigvalve_automatic_reopen"] = False # True opens the big valve if it was previously open

        # "saxs" is the only available beamstop unit
        self.rbs_dict["saxs"]=Rbs

        # "saxs" is the only available beamstop unit
        # The rbs_settings are permanent and available in other bliss sessions (~STATUS)
        self.rbs_setting_dict=HashObjSetting("rbs_setting_{}".format("saxs"))


    def show(self):
        for Rbs in self.rbs_dict:
            Rbs = self.rbs_dict[Rbs]
            print("{}:".format(Rbs))
            for key in Rbs:
                print("    {:10} : {}".format(key,Rbs[key]))

    def dump(self,motor=None):
        motor_name="motor" if motor is None else motor.name
        rbs_motor_dumpstr=set()
        rbs_motor_name=''
        for Rbs in self.rbs_dict:
            # dump rbs config
            print('{}.controller.rbs_dict["{}"]={}'.format(motor_name,Rbs,self.rbs_dict[Rbs]))

        print('#{}.controller.savetoyml()'.format(motor_name))
        

    def calc_from_real(self, level1_positions):
        '''
        Calculate the level2_positions of 'h' [mm] and 'v' [mm] from the
        level1_positions 'trans' [mm] and 'alpha' [deg]:

               h[mm] = H0[mm]+deltah[mm] : horizontal position
               v[mm] = V0[mm]+deltav[mm] : vertical position

        (1a)   deltah[mm] = Radius[mm]*cos(rad(alpha[deg]))+trans[mm]*cos(rad(Alpha0[deg]))
        (1b)   deltav[mm] = Radius[mm]*sin(rad(alpha[deg]))+trans[mm]*sin(rad(Alpha0[deg]))
        '''
        _log.debug("  calc_from_real BEGIN ({})".format(level1_positions))

        calc_level2_positions={}
        level2_names={}

        rbs_name="saxs" # the only available beamstop unit
        if rbs_name in self.rbs_dict:
            Rbs=self.rbs_dict[rbs_name]

            _log.debug("  calc_from_real RBS_PSEUDO_ON:{}".format(self.rbspseudo()))

            if self.rbspseudo():

                trans_mot_tag = Rbs["trans_mot_tag"]
                alpha_mot_tag = Rbs["alpha_mot_tag"]
                h_mot_tag     = Rbs["h_mot_tag"]
                v_mot_tag     = Rbs["v_mot_tag"]

                h_mot         = self._tagged[h_mot_tag][0]
                v_mot         = self._tagged[v_mot_tag][0]

                h0_mm = Rbs["h0"]
                v0_mm = Rbs["v0"]
                radius_mm = Rbs["radius"]
                alpha0_deg = Rbs["alpha0"]
                mode = Rbs["mode"]      # 0: use solution trans1, otherwise use solution trans2

                _log.debug("    h0_mm={}, v0_mm={}, radius_mm={}, alpha0_deg={}, mode={}, trans_mot_tag={}, alpha_mot_tag={}, h_mot_tag={}, v_mot_tag={}".\
                  format(h0_mm, v0_mm, radius_mm, alpha0_deg, mode, trans_mot_tag, alpha_mot_tag, h_mot_tag, v_mot_tag))

                #Get input positions
                trans_mm = level1_positions[trans_mot_tag]
                alpha_deg = level1_positions[alpha_mot_tag]

                _log.debug("    trans_mm={}, alpha_deg={}".format(trans_mm,alpha_deg))

                alpha0_rad = np.deg2rad(alpha0_deg)
                alpha_rad = np.deg2rad(alpha_deg)

                # Calculate horizontal and vertical position
                deltah_mm = radius_mm*np.cos(alpha_rad)+trans_mm*np.cos(alpha0_rad)
                deltav_mm = radius_mm*np.sin(alpha_rad)+trans_mm*np.sin(alpha0_rad)

                h_mm = h0_mm+deltah_mm # horizontal position
                v_mm = v0_mm+deltav_mm # vertical position

                calc_level2_positions={h_mot_tag:h_mm,v_mot_tag:v_mm}
                level2_names={h_mot_tag:h_mot.name,v_mot_tag:v_mot.name}

        _log.debug("  calc_from_real {} END ({})".format(level2_names,calc_level2_positions))

        return calc_level2_positions

    def calc_to_real(self, level2_positions):
        '''
        Calculate the level1_positions 't' [mm] and 'alpha' [deg] from the
        level2_positions 'h' [mm] and 'v' [mm]:

               mode==0:
        (2a)   trans1[mm] = deltah[mm]*cos(rad(Alpha0[deg]))+deltav[mm]*sin(rad(Alpha0[deg]))+ 
                      cos(rad(Alpha0[deg]))*sqrt(-((deltav[mm]-deltah[mm]*tan(rad(Alpha0[deg])))^2)+((Radius[mm]/cos(rad(Alpha0[deg])))^2))
               mode!=0:
        (2b)   trans2[mm] = deltah[mm]*cos(rad(Alpha0[deg]))+deltav[mm]*sin(rad(Alpha0[deg]))- 
                      cos(rad(Alpha0[deg]))*sqrt(-((deltav[mm]-deltah[mm]*tan(rad(Alpha0[deg])))^2)+((Radius[mm]/cos(rad(Alpha0[deg])))^2))
        (3)    alpha[deg] = deg(atan2(deltav[mm]-trans[mm]*sin(rad(Alpha0[deg])),deltah[mm]-trans[mm]*cos(rad(Alpha0[deg]))))

               deltah[mm] = h[mm] - H0[mm]
               deltav[mm] = v[mm] - V0[mm]

        '''
        _log.debug("  calc_to_real BEGIN ({})".format(level2_positions))

        calc_level1_positions={}
        level1_names={}

        rbs_name="saxs" # the only available beamstop unit
        if rbs_name in self.rbs_dict:
            Rbs=self.rbs_dict[rbs_name]

            _log.debug("  calc_to_real RBS_PSEUDO_ON:{}".format(self.rbspseudo()))

            trans_mot_tag = Rbs["trans_mot_tag"]
            alpha_mot_tag = Rbs["alpha_mot_tag"]
            h_mot_tag     = Rbs["h_mot_tag"]
            v_mot_tag     = Rbs["v_mot_tag"]

            trans_mot     = self._tagged[trans_mot_tag][0]
            alpha_mot     = self._tagged[alpha_mot_tag][0]

            if self.rbspseudo():
                h0_mm = Rbs["h0"]
                v0_mm = Rbs["v0"]
                radius_mm = Rbs["radius"]
                alpha0_deg = Rbs["alpha0"]
                mode = Rbs["mode"]      # 0: use solution trans1, otherwise use solution trans2

                _log.debug("    h0_mm={}, v0_mm={}, radius=_mm{}, alpha0_deg={}, mode={}, trans_mot_tag={}, alpha_mot_tag={}, h_mot_tag={}, v_mot_tag={}".\
                  format(h0_mm, v0_mm, radius_mm, alpha0_deg, mode, trans_mot_tag, alpha_mot_tag, h_mot_tag, v_mot_tag))

                #Get input positions
                h_mm = level2_positions[h_mot_tag]
                v_mm = level2_positions[v_mot_tag]

                _log.debug("    h_mm={}, v_mm={}".format(h_mm,v_mm))

                alpha0_rad = np.deg2rad(alpha0_deg)

                deltah_mm = h_mm - h0_mm
                deltav_mm = v_mm - v0_mm

                if mode == 0:
                    trans_mm = deltah_mm*np.cos(alpha0_rad)+deltav_mm*np.sin(alpha0_rad)+ \
                          np.cos(alpha0_rad)*np.sqrt(-pow(deltav_mm-deltah_mm*np.tan(alpha0_rad),2)+pow((radius_mm/np.cos(alpha0_rad)),2))
                else:
                    trans_mm = deltah_mm*np.cos(alpha0_rad)+deltav_mm*np.sin(alpha0_rad)- \
                          np.cos(alpha0_rad)*np.sqrt(-pow(deltav_mm-deltah_mm*np.tan(alpha0_rad),2)+pow((radius_mm/np.cos(alpha0_rad)),2))
  
                alpha_deg = np.rad2deg(np.arctan2(deltav_mm-trans_mm*np.sin(alpha0_rad),deltah_mm-trans_mm*np.cos(alpha0_rad)))

                # alpha_deg range: -90 <= alpha_deg < 270
                if alpha_deg<-90:
                    alpha_deg+=360
                elif alpha_deg>=270:
                     alpha_deg-=360

                calc_level1_positions={trans_mot_tag:trans_mm, alpha_mot_tag:alpha_deg}
                level1_names={trans_mot_tag:trans_mot.name,alpha_mot_tag:alpha_mot.name}


        _log.debug("  calc_to_real {} END ({})".format(level1_names,calc_level1_positions))

        return calc_level1_positions

    def rbs_alpha_lim_state(self):

        Rbs=self.rbs_dict["saxs"]

        alpha_mot_tag = Rbs["alpha_mot_tag"]

        motor=self._tagged[alpha_mot_tag][0]

        neg_lim_status=motor.hw_state.LIMNEG
        pos_lim_status=motor.hw_state.LIMPOS

        return(neg_lim_status,pos_lim_status)

    def rbshome(self):
        """
        Recalibrates the dial position of alpha_mot and trans_mot at limit switches
        """

        Rbs=self.rbs_dict["saxs"]

        trans_mot_tag = Rbs["trans_mot_tag"]
        alpha_mot_tag = Rbs["alpha_mot_tag"]

        trans_mot     = self._tagged[trans_mot_tag][0]
        alpha_mot     = self._tagged[alpha_mot_tag][0]

        # deactivate the pseudo motors
        self.rbspseudo(0)

        # close the big valve
        self.bvcl()

        # if big valve is closed
        if self.bvstatus() == 0:  # status is 0 if closed!

            alpha_mot_search_switch = Rbs["alpha_mot_search_switch"]
            if alpha_mot_search_switch is not None:

                # update data collector variable RBS_SAFE -> unsafe
                self.rbssafe(0)

                #load the last valve positions
                positions = self.rbs_setting_dict.get("rbsvalve_positions")
                _log.debug("  rbsout: Moving rbsvalves to positions {}".format(positions))
                if positions is None:
                   _log.warning("rbsout: no previously defined rbsvalve positions")
                self.rbsvalve.moveall(positions)

                alpha_mot_search_switch_dial = Rbs["alpha_mot_search_switch_dial"]
                alpha_mot_dial_lim_neg = Rbs["alpha_mot_dial_lim_neg"]
                alpha_mot_dial_lim_pos = Rbs["alpha_mot_dial_lim_pos"]
                alpha_mot_offset = Rbs["alpha_mot_offset"]

                # Calibrate alpha_mot at switch

                _log.debug("  rbshome: Searching {}  of motor {}".format(alpha_mot_search_switch,alpha_mot.name))
                chg_dial( alpha_mot, alpha_mot_search_switch)
                alpha_mot.wait_move()

                # Change dial value to alpha_mot_search_switch_dial
                if alpha_mot_search_switch_dial is not None:
                    chg_dial( alpha_mot, alpha_mot_search_switch_dial)
                    alpha_mot.wait_move()

                # wait 1 second
                time.sleep(1)

                if alpha_mot_dial_lim_neg and alpha_mot_dial_lim_pos is not None:
                    alpha_mot.dial_limits=(alpha_mot_dial_lim_neg,alpha_mot_dial_lim_pos)

                if alpha_mot_offset is not None:
                     alpha_mot.offset=alpha_mot_offset

            trans_mot_search_switch = Rbs["trans_mot_search_switch"]
            if trans_mot_search_switch is not None:

                # update data collector variable RBS_SAFE -> unsafe
                self.rbssafe(0)

                # Calibrate t_mot at switch

                trans_mot_search_switch_dial =Rbs["trans_mot_search_switch_dial"]
                trans_mot_dial_lim_neg = Rbs["trans_mot_dial_lim_neg"]
                trans_mot_dial_lim_pose = Rbs["trans_mot_dial_lim_pose"]
                trans_mot_offset = Rbs["trans_mot_offset"]

                # Calibrate trans_mot at switch

                _log.debug("  rbshome: Searching {}  of motor {}".format(trans_mot_search_switch,trans_mot.name))
                chg_dial( trans_mot, trans_mot_search_switch)
                trans_mot.wait_move()

                # Change dial value to trans_mot_search_switch_dial
                if trans_mot_search_switch_dial is not None:
                    chg_dial( trans_mot, trans_mot_search_switch_dial)
                    trans_mot.wait_move()

                # wait 1 second
                time.sleep(1)

                if trans_mot_dial_lim_neg is not None and trans_mot_dial_lim_pos is not None:
                    trans_mot.dial_limits=(trans_mot_dial_lim_neg,trans_mot_dial_lim_pos)

                if trans_mot_offset is not None:
                    trans_mot.offset=trans_mot_offset

        else:
            print("ERROR: The big valve is not closed, the beamstop must not be removed");

        # activate the pseudo motors
        self.rbspseudo(1)

        # switch off all valves
        self.rbsvalve.move(None,0)

    def rbsout(self):
        """
        Removes the beamstop from the direct beam by rotating alpha_mot.
        """

        Rbs=self.rbs_dict["saxs"]

        trans_mot_tag = Rbs["trans_mot_tag"]
        alpha_mot_tag = Rbs["alpha_mot_tag"]
        h_mot_tag     = Rbs["h_mot_tag"]
        v_mot_tag     = Rbs["v_mot_tag"]

        trans_mot     = self._tagged[trans_mot_tag][0]
        alpha_mot     = self._tagged[alpha_mot_tag][0]
        h_mot         = self._tagged[h_mot_tag][0]
        v_mot         = self._tagged[v_mot_tag][0]

        alpha_mot_search_switch = Rbs["alpha_mot_search_switch"]

        if self.rbspseudo():

            # check, that h_mot and v_mot are inside limits
            rbs_bad_lim=self.rbs_chk_lim(motor=h_mot,verbose=0,rbs_bad_lim=0)
            rbs_bad_lim=self.rbs_chk_lim(motor=v_mot,verbose=0,rbs_bad_lim=rbs_bad_lim)
            _log.debug("  rbsout: rbs_bad_lim={}".format(rbs_bad_lim))

            # remember the start position if the pseudo motors are not outside limits
            if rbs_bad_lim==0:
                _log.debug("  rbsout: remembering start position")
                #sync 
                self.rbs_setting_dict["last_alpha_mot_in"]=alpha_mot.position
                self.rbs_setting_dict["last_trans_mot_in"]=trans_mot.position
                self.rbs_setting_dict["last_h_mot_lim_pos"]=h_mot.limits[1]
                self.rbs_setting_dict["last_h_mot_lim_neg"]=h_mot.limits[0]
                self.rbs_setting_dict["last_v_mot_lim_pos"]=v_mot.limits[1]
                self.rbs_setting_dict["last_v_mot_lim_neg"]=v_mot.limits[0]

                # remember the status of the big valve, if the beamstop is still inserted
                if self.rbssafe(): 
                    self.rbs_setting_dict["last_bigvalve_status"]=self.bvstatus() # 0: closed, 1: open

        # check that the negative limit (lim-) of alpha is not already activated
        # then switch off pseudo motors and search lim- of alpha_mot
        alpha_mot.wait_move()
        if alpha_mot_search_switch == "lim-":
            switch_state = alpha_mot.hw_state.LIMNEG
        elif alpha_mot_search_switch == "lim+":
            switch_state = alpha_mot.hw_state.LIMPOS
        else:
            assert alpha_mot_search_switch in ["lim-","lim+"], "Unimplemented search switch {}".format(alpha_mot_search_switch)

        _log.debug("  rbsout: searching {} of motor {}.".format(alpha_mot_search_switch,alpha_mot.name))

        #if alpha_mot.hw_state.LIMNEG==0:
        if switch_state==0:

            # activate the valves of the inserted beamstops
            #load the last valve positions
            positions = self.rbs_setting_dict.get("rbsvalve_positions")
            _log.debug("  rbsout: Moving rbsvalves to positions {}".format(positions))
            if positions is None:
               _log.warning("rbsout: no previously defined rbsvalve positions")
            self.rbsvalve.moveall(positions)

            # deactivate the pseudo motors
            self.rbspseudo(0)

            # close the big valve
            self.bvcl()

            # if big valve is closed 
            if self.bvstatus() == 0:  # status is 0 if closed!

                alpha_mot_search_switch = Rbs["alpha_mot_search_switch"]
                if alpha_mot_search_switch is not None:

                    # update data collector variable RBS_SAFE -> unsafe
                    self.rbssafe(0)

                    # Search alpha_mot_search_switch

                    alpha_mot_search_switch_dial =Rbs["alpha_mot_search_switch_dial"]
                    alpha_mot_dial_lim_neg = Rbs["alpha_mot_dial_lim_neg"]
                    alpha_mot_dial_lim_pos = Rbs["alpha_mot_dial_lim_pos"]
                    alpha_mot_offset = Rbs["alpha_mot_offset"]

                    # Calibrate alpha_mot at switch

                    _log.debug("  rbsout: Searching {}  of motor {}".format(alpha_mot_search_switch,alpha_mot.name))
                    chg_dial( alpha_mot, alpha_mot_search_switch)
                    alpha_mot.wait_move()

                    # Change dial value to alpha_mot_search_switch_dial
                    if alpha_mot_search_switch_dial is not None:
                        chg_dial( alpha_mot, alpha_mot_search_switch_dial)
                        alpha_mot.wait_move()

                    # wait 1 second
                    time.sleep(1)

                    if alpha_mot_dial_lim_neg and alpha_mot_dial_lim_pos is not None:
                        alpha_mot.dial_limits=(alpha_mot_dial_lim_neg,alpha_mot_dial_lim_pos)


                    if alpha_mot_offset is not None:
                         alpha_mot.offset=alpha_mot_offset

                # Moving to the out position (Releasing the limit switch)
                alpha_mot_dial_out=Rbs["alpha_mot_dial_out"]
                if alpha_mot_dial_out is not None:
                    _log.debug("  rbsout: Moving motor {} to out position (dial {})".format(alpha_mot.name,alpha_mot_dial_out))
                    mvd(alpha_mot,alpha_mot_dial_out)   # mvrd(alpha_mot,alpha_mot_dial_out-alpha_mot_search_switch_dial)

            else:
                print("ERROR: The big valve is not closed, the beamstop must not be removed");

            # switch off all valves
            self.rbsvalve.move(None,0)

            # activate the pseudo motors
            self.rbspseudo(1)

        else:
            _log.warning("rbsout: The search switch is already active! Cannot remove the beamstop")

        _log.debug("  rbsout: The remembered valve positions are {}".format(self.rbs_setting_dict.get("rbsvalve_positions")))


    def rbsin(self,beamstops=None):
        """
        Inserting beamstops
        Beamstop names or numbers must be entered space separated
        Without parameter it inserts the previously used beamstops 
        """

        Rbs=self.rbs_dict["saxs"]

        positions=None
        if beamstops is not None:
            argv=beamstops.split()
            argc=len(argv)

            if argc>0:
                positions={}
                for argn in range(0,argc):
                    beamstop = argv[argn]
                    try:
                        ibs=int(beamstop)
                        positions[ibs]=1
                    except:
                        positions[argv[argn]]=1

        else:
           # use previous beamstops
           positions=self.rbs_setting_dict.get("rbsvalve_positions")

        # check whether the new beamstops are safe
        if self.rbsvalve.safecnt(positions) > 0:

            #Before inserting new beamstops, the old ones must be removed
            if self.rbs_chk_alpha_for_insertion():
                self.rbsout()

            self._rbsin(positions)
        else:
            print("The beamstops {} are not safe".format(positions))

        # reopen the big valve, when safe
        bigvalve_automatic_reopen=Rbs["bigvalve_automatic_reopen"]
        if bigvalve_automatic_reopen:
            if self.rbs_setting_dict["last_bigvalve_status"] == 1:
                self.bvop()


    def _rbsin(self,positions=None):
        """
        Activating valve positions and rotating to last_alpha_mot_in, last_t_mot_in
        position
        """
        
        _log.debug("  rbsin: The valve positions are {}".format(positions))

        Rbs=self.rbs_dict["saxs"]

        trans_mot_tag = Rbs["trans_mot_tag"]
        alpha_mot_tag = Rbs["alpha_mot_tag"]
        h_mot_tag     = Rbs["h_mot_tag"]
        v_mot_tag     = Rbs["v_mot_tag"]

        trans_mot     = self._tagged[trans_mot_tag][0]
        alpha_mot     = self._tagged[alpha_mot_tag][0]
        h_mot         = self._tagged[h_mot_tag][0]
        v_mot         = self._tagged[v_mot_tag][0]

        rbsvalve_alpha_max = Rbs["rbsvalve_alpha_max"]
        rbsvalve_positions_default = Rbs["rbsvalve_positions_default"] 
        alpha_mot_in_default = Rbs["alpha_mot_in_default"]
        trans_mot_in_default = Rbs["trans_mot_in_default"]

        last_alpha_mot_in=self.rbs_setting_dict.get("last_alpha_mot_in")
        if last_alpha_mot_in is None:
            last_alpha_mot_in = alpha_mot_in_default
            _log.warning("rbsin: {} no previous working position available, using default {}".format(alpha_mot.name,alpha_mot_in_default))

        last_trans_mot_in=self.rbs_setting_dict.get("last_trans_mot_in")
        if last_trans_mot_in is None:
            last_trans_mot_in = trans_mot_in_default
            _log.warning("rbsin: {} no previous working position available, using default {}".format(trans_mot.name,trans_mot_in_default))

        # if positions is not set, use remembered valve positions
        if positions is None:
            positions=self.rbs_setting_dict.get("rbsvalve_positions")
            if positions is None:
                _log.warning("rbsin: no remembered valve positions")
                positions={}

        # check whether mot_in position is initialized
        if not ((last_alpha_mot_in is None) or (last_trans_mot_in is None)):
            alpha=alpha_mot.position
            if alpha <= rbsvalve_alpha_max:
                if positions=={}:
                    positions = rbsvalve_positions_default
                    _log.warning("rbsin: no valve positions, using default {}".format(positions))
                    if positions is None:
                        positions={}
                else:
                    _log.debug("  rbsin: the valve positions are {}".format(positions))

                if positions!={}:
                    # update valve positions in rbs_setting_dict
                    self.rbs_setting_dict["rbsvalve_positions"]=positions

                # close all valves
                _log.debug("  rbsin: Closing all valves")
                self.rbsvalve.move(None,0)
                _log.debug("  rbsin: Moving valves to positions {}".format(positions))
                self.rbsvalve.moveall(positions)
                # ...

            else:
                _log.warning("rbsin: Cannot insert beamstops at this position of motor {}. Use rbsout() first".format(alpha_mot.name))
                # no beamstop can be inserted
                positions={}

            mv(alpha_mot,last_alpha_mot_in,trans_mot,last_trans_mot_in)

            # close all valves
            self.rbsvalve.move(None,0)

            # pseudo motors on
            self.rbspseudo(1)

            # set the limits of the pseudo motors
            h_mot_lim_pos = self.rbs_setting_dict.get("last_h_mot_lim_pos")
            h_mot_lim_neg = self.rbs_setting_dict.get("last_h_mot_lim_neg")
            v_mot_lim_pos = self.rbs_setting_dict.get("last_v_mot_lim_pos")
            v_mot_lim_neg = self.rbs_setting_dict.get("last_v_mot_lim_neg")

            if h_mot_lim_pos is not None and h_mot_lim_neg is not None:
                h_mot.limits=(h_mot_lim_neg,h_mot_lim_pos)
            if v_mot_lim_pos is not None and v_mot_lim_neg is not None:
                v_mot.limits=(v_mot_lim_neg,v_mot_lim_pos)

            # update data collector variable safe
            now=self._rbsnow()

            if now==0:
                # update data collector variable RBS_SAFE -> safe
                self.rbssafe(1)
            else:
                # update data collector variable RBS_SAFE -> unsafe
                self.rbssafe(0)

        else:
            _log.warning("rbsin: There is no previous working position. Please insert the beamstop by moving the motors.")


    def rbs_chk_alpha_for_insertion(self,alpha_max=None):
        """
        Checks, whether rbsout must be used before inserting beamstops
         0: no need to move
         1: need to run rbsout before inserting beamstops 
        -1: problem
        """

        status=-1

        Rbs=self.rbs_dict["saxs"]

        alpha_mot_tag = Rbs["alpha_mot_tag"]

        alpha_mot     = self._tagged[alpha_mot_tag][0]

        if alpha_max is None:
            alpha_max = Rbs.get("rbsvalve_alpha_max")

        if alpha_max is not None:
            if alpha_mot.position > alpha_max:
                status=1
            else:
                status=0

        return(status)
           

    def rbs_chk_lim(self,motor=None,verbose=1,rbs_bad_lim=0):
        """
        Returns rbs_bad_lim. If the motor position is outside a limit 
        rbs_bad_lim is incremented, otherwise not.
        """

        if motor is not None:
            u = motor.dial
            lower = motor.dial_limits[0] # lim-
            higher = motor.dial_limits[1] # lim+
            if u < lower:
                if verbose>0:
                    print("{} will hit low limit at {}.",motor.name,motor.limits[0])
                rbs_bad_lim+=1
            elif u > higher:
                if verbose>0:
                    print("{} will hit high limit at {}.",motor.name,motor.limits[1])
                rbs_bad_lim+=1

        return(rbs_bad_lim)
            

    def _rbsnow(self,positions=None):
        """
        Returns the beamstop status
        -1: Fatal error
         0: OK, the beamstop motors are inside the limits and at least 1 valid beamstop is used
         1: beamstop motors are outside the limits
         2: motor positions unknown, pseudo motors are off
         >10: this number + number of inserted beamstops * 10
         99: beamstop motors are OK, but no valid beamstop is used
        """

        Rbs=self.rbs_dict["saxs"]

        if positions is None:
            positions=self.rbs_setting_dict.get("rbsvalve_positions")

        now=-1
        if self.rbspseudo():

            #if h_mot and v_mot are inside limits
            h_mot_tag     = Rbs["h_mot_tag"]
            v_mot_tag     = Rbs["v_mot_tag"]

            h_mot         = self._tagged[h_mot_tag][0]
            v_mot         = self._tagged[v_mot_tag][0]

            # check, that h_mot and v_mot are inside limits
            rbs_bad_lim=self.rbs_chk_lim(motor=h_mot,verbose=0,rbs_bad_lim=0)
            rbs_bad_lim=self.rbs_chk_lim(motor=v_mot,verbose=0,rbs_bad_lim=rbs_bad_lim)
            if rbs_bad_lim == 0:
                _log.debug("  rbsnow: the beamstop unit is correctly positioned (rbs_bad_lim={})".format(rbs_bad_lim))
                now=0
            else:
                _log.debug("  rbsnow: the beamstop unit is not correctly positioned (rbs_bad_lim={})".format(rbs_bad_lim))
                now=1
        else:
            _log.debug("  rbsnow: Pseudo motors are off, cannot check position of beamstop unit.")
            now=2

        safecnt = self.rbsvalve.safecnt(positions)
        if now==0:
            # check, that at least 1 safe beamstop is inserted
            if safecnt > 0:
                _log.debug("  rbsnow: the beamstop selection is safe (safecnt={})".format(safecnt))
            else:
                _log.debug("  rbsnow: the beamstop selection is not safe (safecnt={})".format(safecnt))
                now=99
        else:
            _log.debug("  rbsnow: problem with position of the beamstop unit (safecnt={})".format(safecnt))
            now+=10*safecnt

        return(now)


    def rbsnow(self,verbose=1):
        """
        Prints the beamstop motor position and updates RBS_SAFE
        """

        now=self._rbsnow()


        if verbose:
            if now==99:
                print("No valid beamstop is inserted")
            else:
                posOK=now%10
                cnt=int(now//10)

                if now==0:
                    print("At least 1 valid beamstop is correctly positioned")
                    #load the last valve positions
                    positions = self.rbs_setting_dict.get("rbsvalve_positions")
                    self.rbsvalve.show(prvalpos=1,positions=positions)
                    # update data collector variable RBS_SAFE -> safe
                    self.rbssafe(1)  # -> safe
                elif posOK==1:
                    print("The beamstop is not in place")
                    # update data collector variable RBS_SAFE -> unsafe
                    self.rbssafe(0)
                elif posOK==2:
                    print("The beamstop position cannot be determined. The pseudo motors are off.")
                    # update data collector variable RBS_SAFE -> unsafe
                    self.rbssafe(0)
                elif now<0:
                    print("Fatal Error")
                    # update data collector variable RBS_SAFE -> unsafe
                    self.rbssafe(0)
                else:
                    printf("The beamstop retured the unknown code {}\n",now)
                    # update data collector variable RBS_SAFE -> unsafe
                    self.rbssafe(0)

        return(now)
        

    def rbspseudo(self,pseudoon=None):
        """
        Switches on/off/returns the pseudo motor calculation status
        0: off
        1: on
        Without parameter it returns the current pseudoon parameter.
        """

        if pseudoon is not None:
            self.rbs_setting_dict["RBS_PSEUDO_ON"]=pseudoon
        _pseudoon = self.rbs_setting_dict.get("RBS_PSEUDO_ON")
        if _pseudoon is None:
            _pseudoon = 1
            self.rbs_setting_dict["RBS_PSEUDO_ON"]=_pseudoon
        _log.debug("  rbspseudo: RBS_PSEUDO_ON -> {}".format(_pseudoon))

        return(_pseudoon)


    def rbssafe(self,safe=None):
        """
        Updates RBS_SAFE on redis with safe and returns the current value of RBS_SAFE:
        0: not safe, do not open the big valve (bv)
        1: safe, it is possible opening the big valve (bv)
        
        """
        if safe is not None:
            self.rbs_setting_dict["RBS_SAFE"]=safe
        _safe = self.rbs_setting_dict.get("RBS_SAFE")
        if _safe is None:
            _safe = 1
            self.rbs_setting_dict["RBS_SAFE"]=_safe
        _log.debug("  safe: RBS_SAFE -> {}".format(_safe))

        return(_safe)

    def _bvitlck(self,itlck=None):
        """ Sets and returns bigvalve RBS_ITLCK """
        if itlck is not None:
            self.rbs_setting_dict["RBS_ITLCK"]=itlck
        return(self.rbs_setting_dict["RBS_ITLCK"])

    def bvitlck(self,itlck=True):
        """ Activates and deactivates bigvalve RBS_ITLCK """
        return(self._bvitlck(itlck))

    def bvstatus(self):
        """
        Returns the current status of the big valve
        0: closed
        1: open
        -1: Fault state
        -999: unknown
        """ 
        status=-999
        if self.bigvalve is not None:
            value = self.bigvalve.state.value
            if value == 'Closed':
                status=0
            elif value == 'Open':
                status=1
            elif value == 'Fault state':
                status=-1

        return(status)
               

    def bvcl(self):
        """close the big valve"""
        if self.bigvalve is not None:
            status=self.bvstatus()
            if status<0:
                self.bigvalve.reset()
                time.sleep(1)
            self.bigvalve.close() # give the command in all cases
            if status!=0: # moves only when it was not already closed
                time.sleep(6)
        else:
            _log.warning("  bvop: There is no big valve to close")

    def bvop(self):
        """open the big valve"""
        if self.bigvalve is not None:
            if self.rbssafe() or not self._bvitlck():
                status=self.bvstatus()
                if status<0:
                    self.bigvalve.reset()
                    time.sleep(1)
                self.bigvalve.open()
                if status!=1: # moves only when it was not already open
                    time.sleep(6)
            else:
                _log.warning("  bvop: Cannot open the bigvalve. Beamstop position is not safe.")
        else:
            _log.warning("  bvop: There is no big valve to open")

    def savetoyml(self):
        '''
        Save current ID01 rbs parameters to yml file
        '''
        self._config['rbs_dict']=self.rbs_dict
        self._config.save()


class CalcAxis(AxisBase):
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        # calculated motor positions do not need to be buffered
        # => disable buffering
        self.settings.disable_cache("_set_position") #disable target pos
        self.settings.disable_cache("position") #disable current pos
        self.settings.disable_cache("state") #disbale the state

    def dump(self):
        self.controller.dump(self)

    def show(self):
        self.controller.show()

    def rbspseudo(self,pseudoon=None):
        """
        Switches on/off/shows the pseudo motor calculation mode
        """
        return(self.controller.rbspseudo(pseudoon))



