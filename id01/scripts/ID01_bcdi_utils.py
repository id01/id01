# -*- coding: utf-8 -*-

# ID01 BLISS git repo
#       authors:
#         Steven Leake, leake@esrf.fr
#
#       Created at: 05. Dec 18:37:00 CET 2021
#
#----------------------------------------------------------------------
# Description: 
#    utility functions for ID01
#
#
# TODO:
#   see TODOs
#
#
# FUNCTIONS:
#   CDI_align
#   CDI_measure_rc
#   CDI_measure_frc
#   CDI_auto_bragg_align
#   ##CDI_move_crystal2COR ## TODO
#   CDI_align_labframe_x_z
#   CDI_align_labframe_x_z
#----------------------------------------------------------------------

import collections

from bliss import setup_globals
from bliss.common.scans import dscan
from bliss.scanning.scan_tools import goto_peak
from bliss.shell.standard import umvr, umv 
from bliss import current_session, setup_globals
from bliss.common.cleanup import cleanup, axis as cleanup_axis



from skimage.registration import phase_cross_correlation
from scipy.ndimage.fourier import fourier_shift
from scipy.ndimage.measurements import center_of_mass
import numpy as np
from scipy import stats
import sys
import matplotlib.pyplot as plt
sys.path.append('/users/blissadm/local/id01.git/id01/scripts/')

from id01.scripts.ID01_sxdm import asxdm, dsxdm
from id01.scripts.ID01_sxdmutils import alignpix, alignpiy, alignpiz, auto_alignSXDM_COM
from id01.scripts.ID01_scan_math import gauss_fit, gauss, linear_fit
from id01.process.h5utils import get_scans_title_str_hdf5 ,get_scan_motor_hdf5, get_scan_start_time_hdf5
from id01.scans.scan_counter_tools import get_selected_counters
from id01.scripts.ID01_utils import fit_scan_gauss, get_scan_intensity_max_sum
from id01.scans.fscan_utils import dfscan
from id01.scans.metadata_icat import ID01newdataset

# maybe make a class and host all of the sensible parameters here.
def CDI_align(motObj0,
              motObj1,
              mot0_stroke=10,
              mot1_stroke=10,
              counter=None): 
    """
    two motor alignment of an object in the beam
    """
    if counter is None:
        print("please select a counter")
        exit


    #TODO decorate scan functions with this information - more pythonic and less code everywhere    
    print(f"[INFO] scanning in {motObj0.name}")
    scaninfo = dscan(motObj0,-mot0_stroke/2.,mot0_stroke/2.,40,0.1)
    print(f" ... {scaninfo.scan_info['filename']} ... scan_no: {scaninfo.scan_info['scan_nb']}")
    goto_peak(counter)
    print(f"[INFO] scanning in {motObj1.name}")
    scaninfo = dscan(motObj1,-mot1_stroke/2.,mot1_stroke/2.,40,0.1)
    print(f" ... {scaninfo.scan_info['filename']} ... scan_no: {scaninfo.scan_info['scan_nb']}")
    goto_peak(counter)
                                                                                                                                                                        
def CDI_measure_rc(motObj0,
                   motObj1,
                   motObj2,
                   th_stroke=1,
                   npts=200,
                   exposure=1,
                   counter=None): 
    """
    two motor alignment followed by Rocking curve in step by step mode
    """
    CDI_align(motObj0,motObj1,counter=counter) 
    print(f"[INFO] scanning in {motObj2.name}")
    dscan(motObj2,-th_stroke/2.,th_stroke/2.,npts,exposure) 
    # TODO maybe add an alignment here?

def CDI_measure_frc(motObj0,
                    motObj1,
                    motObj2,
                    counter=None): 
    """
    two motor alignment followed by Rocking curve in continuous/fly mode
    """
    CDI_align(motObj0,motObj1,counter)
    pos_start = motObj2.position   
    fscan = setup_globals.fscan_eh.get_runner("fscan")
    fscan(motObj2,pos_start-0.5,0.00015,6666,0.03) 
    umvr(motObj2,-0.5)

def CDI_auto_Bragg_align(motObj0,
                   motObj1,
                   motObj2,
                   th_stroke=1,
                   npts=200,
                   exposure=1,
                   counter_rough=None,
                   counter_fine=None,
                   no_scans=5):
    """
    motObj0: piezo
    motObj1: piezo
    motObj2: rocking axis
    when moving to a group dumped Bragg peak
    find the crystal
    align it
    move the bragg peak to the right part of the detector
    run the measurement
    """
    globals_dict = current_session.env_dict
    
    sxdm_map = dsxdm(globals_dict["pix"],-10,10,40,globals_dict["piy"],-10,10,40,0.01)
    
    #TODO this function takes weird ROIs and fail because of it
    sleep(5)
    auto_alignSXDM_COM(sxdm_map.scan_info["scan_nb"],counter_rough,filename=sxdm_map.scan_info["filename"])
    
    # scan of RC until you hit the bragg
    ##dscan(motObj2,-.5,0.5,30,0.1)
    ##goto_peak(counter_fine)

    # move the signal to the target position on the detector

    #detcalib_mpx1x4.move_beam_to_pixel((125,125),motObjs=(delta,nu))

    #TODO add a check of the beam being on one of the gaps in the detector
    


    # check for offsets per degree if any apply them
    
    # run the measurement
    ID01newdataset("BCDI")
    for ii in np.arange(no_scans):
        CDI_measure_rc(motObj0,
                   motObj1,
                   motObj2,
                   th_stroke=th_stroke,
                   npts=npts,
                   exposure=exposure,
                   counter=counter_fine)  
        
    ID01newdataset("ALIGNMENT")

#CDI_align_fscan
#"""
#three 1D scans continuous, if fast enough could replace CDI_chase_crystal
#i.e. provide a better idea of where the crystal goes
#"""
def CDI_align_fscan(detObj,counter=None):
    """
    three 1D scans continuous, if fast enough could replace CDI_chase_crystal
    i.e. provide a better idea of where the crystal goes
    """
    dfscan(pix, -8 , 8, 30, detObj, debug = False)
    goto_peak(counter)
    dfscan(piz, -5 , 5, 30, detObj, debug = False)
    goto_peak(counter)
    dfscan(pix, -8 , 8, 30, detObj, debug = False)
    goto_peak(counter)

#CDI_move_crystal2COR(CORmotObj,... labmotObjx/y/z)
#"""
#5 fast scans, fit the peaks, fit the linear plot vs angle, 
#calculate the required offset - apply it and repreat 5 scans to confirm.
#"""
def CDI_calibrate_offset(motObj0, motObj1, max_dth, motObj1_start = -4, motObj1_stop = 4 , motObj1_intervals = 40 ,no_scans = 5, exposure=0.1):
    """
    motObj0: rotation motor
    motObj1: axis to correct with
    max_dth: maximum angle from the the central bragg peak, 5 scans are made
    """
    #with cleanup(motObj0,motObj1, restore_list=(cleanup_axis.POS,)):  # doesn't work with lab motors

    nb_list=[]
    startpos = motObj0.position
    for ii in np.linspace(-max_dth,max_dth,no_scans):
        umv(motObj0,startpos+ii)
        print(f"[INFO] scanning in {motObj1.name}")
        scaninfo = dscan(motObj1, motObj1_start, motObj1_stop, motObj1_intervals,exposure)
        print(f" ... {scaninfo.scan_info['filename']} ... scan_no: {scaninfo.scan_info['scan_nb']}")
        nb_list.append(scaninfo.scan_info['scan_nb'])
    umv(motObj0,startpos)


    #TODO add cleanup
        
    # TODO add the estimate for offsets here

    #TODO print a suggested plotting function to see the results

    
    auto_counter_name = get_selected_counters()[0].name.strip("_sum")
    detector_name = get_selected_counters()[0].fullname.split(":")[0]

    analyse_str=f"CDI_align_estimate_COR(\"{scaninfo.scan_info['filename']}\",{str(nb_list)},\"{motObj1.name}\",\"{motObj0.name}\",\"{detector_name}_{auto_counter_name}\")"
    print(analyse_str)

def CDI_calibrate_offset_fscan(motObj0, motObj1, max_dth, detObj, motObj1_start = -8, motObj1_stop = 8 , motObj1_intervals = 30, no_scans = 5):
    """
    motObj0: rotation motor
    motObj1: axis to correct with
    max_dth: maximum angle from the the central bragg peak, 5 scans are made
    """
    with cleanup(motObj0,motObj1, restore_list=(cleanup_axis.POS,)): 
        
        startpos = motObj0.position
        for ii in np.linspace(-max_dth,max_dth,no_scans):
            umv(motObj0,startpos+ii)
            dfscan(motObj1, motObj1_start, motObj1_stop, motObj1_intervals, detObj, debug = False)
        
        umv(motObj0,startpos)

        #TODO add cleanup
            
        # TODO add the estimate for offsets here

def CDI_calibrate_offset_sxdm(motObj0, motObj1, max_dth, detObj, no_scans = 5,max_exposure=0.1):
    """
    motObj0: rotation motor
    motObj1: axis to correct with
    max_dth: maximum angle from the the central bragg peak, 5 scans are made
    """
    with cleanup(motObj0,motObj1, restore_list=(cleanup_axis.POS,)): 
        nb_list=[]
        startpos = motObj0.position
        for ii,exposure in zip(np.linspace(-max_dth,max_dth,no_scans),(1*max_exposure,0.5*max_exposure,0.1*max_exposure,0.5*max_exposure,1*max_exposure)):  # TODO numbr of scans doesnt work for exposure
            umv(motObj0,startpos+ii)
            if motObj1.name == "pix":
                scaninfo = alignpix(16, 200, exposure)
            if motObj1.name == "piy":
                scaninfo = alignpiy(16, 200, exposure)
            if motObj1.name == "piz":
                scaninfo = alignpiz(9, 200, exposure)
            
            nb_list.append(scaninfo.scan_info['scan_nb'])

        umv(motObj0,startpos)
        #TODO add cleanup    
        # TODO add the estimate for offsets here
        
        analyse_str=f"CDI_align_estimate_COR(\"{scaninfo.scan_info['filename']}\",{str(nb_list)},\"{motObj1.name}\",\"{motObj0.name}\",\"mpx1x4_roi6\")"
        print(analyse_str)


#CDI_align_labframe_x_z   << for eta/phi rocking curves
"""
Requires lab frame motors i.e. a stack including PI,HEXAPOD, ETA and PHI , MU???
"""

#CDI_chase_crystal(counter,exposure_time,*positioners)
"""
optimise all motors in the list and optimise continuously based on 
counter value in exposure time
"""

def CDI_align_estimate_COR(fn,scan_nos,axis_name,rc_axis_name,counter, norm="log"):
    """
    from a number of alignment scans at different points on a rocking curve
    determine the location of the COR relative to the current position
    """
    ## TODO test this function
    fitpars = np.recarray((len(scan_nos),),dtype=[("scan_no","<f8"),("start_time","<f8"),("rc_angle","<f8"),("label","|S50"),("x0","<f8"),("sigma","<f8"),("H","<f8"),("A","<f8"),("FWHM","<f8")])
    fig = plt.figure(figsize=(16,8))
    for jj,ii in enumerate(scan_nos):
        print(f"scan_no: {ii}")

        x,intensity,label,x0,sigma,H,A,FWHM = fit_scan_gauss(fn,ii,axis_name, counter)
        # get the motor position for the RC scan
        rc_angle = get_scan_motor_hdf5(ii,fn,rc_axis_name)
        start_time = get_scan_start_time_hdf5(ii,fn)
        rc_motor_name = rc_axis_name ##
        fitpars[jj] = (ii,start_time,rc_angle,label,x0,sigma,H,A,FWHM)
        plt.plot(x,intensity,label = label)
        plt.plot(x, gauss(x, *gauss_fit(x, intensity)), '--r', label='fit - '+str(label))
        plt.vlines(x0,0,intensity.max())
    
    if norm == "log":
        plt.semilogy()
    plt.legend(loc=1)
    plt.xlabel(axis_name)
    plt.ylabel(counter)
    plt.show()
    # emit figure to logbook.

    x,y = fitpars["rc_angle"],fitpars["x0"]
    plt.plot(x,y)
    res = linear_fit(x,y)
    
    plt.plot(x, y, 'o', label='original data')
    plt.plot(x, res.intercept + res.slope*x, 'r', label='fitted line')
    plt.legend()
    plt.show()

    print(f"The observed offset per degree {rc_motor_name} for {axis_name} : {res.slope}")
    print(f"apply this to your scans with d2scans or")
    print(f".... you could move the crystal closer to the rotation axis with")
    distance_from_COR(1,res.slope)
    # calculate how far away the center of rotation is.
    # emit figure to logbook

    return fitpars

def CDI_inspect_batch_scans(fn,alignment_axes_names,rc_axis_name,counter, norm="log"):
    """
    from a number of alignment scans at different points on a rocking curve
    determine the location of the COR relative to the current position
    """
    
    for axis_name in alignment_axes_names:

        scan_nos = get_scans_title_str_hdf5(fn,axis_name)
    
        fitpars = np.recarray((len(scan_nos),),dtype=[("scan_no","<f8"),("start_time","<f8"),("label","|S50"),("x0","<f8"),("sigma","<f8"),("H","<f8"),("A","<f8"),("FWHM","<f8")])
        fig = plt.figure(figsize=(16,8))
        plt.subplot(1,2,1)
        for jj,ii in enumerate(scan_nos):
            print(f"scan_no: {ii}")

            x,intensity,label,x0,sigma,H,A,FWHM = fit_scan_gauss(fn,ii,axis_name, counter)
            # get the motor position for the RC scan
            #rc_angle = get_scan_motor_hdf5(ii,fn,rc_axis_name)
            start_time = get_scan_start_time_hdf5(ii,fn)
            fitpars[jj] = (ii,start_time,label,x0,sigma,H,A,FWHM)
            plt.plot(x,intensity,label = ii)
            plt.plot(x, gauss(x, *gauss_fit(x, intensity)), '--r', label=f'fit - {ii}')
            plt.vlines(x0,0,intensity.max())
        
        if norm == "log":
            plt.semilogy()
        plt.legend(loc=1)
        plt.xlabel(axis_name)
        plt.ylabel(counter)
        
        plt.subplot(1,2,2)

        x,y = fitpars["scan_no"],fitpars["x0"]
        plt.plot(x,y)
        res = linear_fit(x,y)
        
        plt.plot(x, y, 'o', label='original data')
        plt.plot(x, res.intercept + res.slope*x, 'r', label='fitted line')
        plt.xlabel("scan_no")
        plt.ylabel(f"{axis_name}")
        plt.legend()
        plt.show()

    # emit figure to logbook

    # RC axis
    axis_name = rc_axis_name

    scan_nos = get_scans_title_str_hdf5(fn,axis_name)

    fitpars = np.recarray((len(scan_nos),),dtype=[("scan_no","<f8"),("start_time","<f8"),("label","|S50"),("x0","<f8"),("sigma","<f8"),("H","<f8"),("A","<f8"),("FWHM","<f8")])
    fig = plt.figure(figsize=(16,8))
    plt.subplot(1,2,1)
    for jj,ii in enumerate(scan_nos):
        print(f"scan_no: {ii}")

        x,intensity,label,x0,sigma,H,A,FWHM = fit_scan_gauss(fn,ii,axis_name, counter)
        # get the motor position for the RC scan
        #rc_angle = get_scan_motor_hdf5(ii,fn,rc_axis_name)
        start_time = get_scan_start_time_hdf5(ii,fn)
        fitpars[jj] = (ii,start_time,label,x0,sigma,H,A,FWHM)
        plt.plot(x,intensity,label = ii)
        plt.plot(x, gauss(x, *gauss_fit(x, intensity)), '--r', label=f'fit - {ii}')
        plt.vlines(x0,0,intensity.max())
    
    if norm == "log":
        plt.semilogy()
    plt.legend(loc=1)
    plt.xlabel(axis_name)
    plt.ylabel(counter)
    ylims = plt.ylim()
    plt.ylim((100,ylims[1]))
    
    # peak position evolution
    ax21 = plt.subplot(2,2,2)
    ax22 = ax21.twinx()
    x,y = fitpars["scan_no"],fitpars["x0"]
    ax21.plot(x,y,"b-")
    res = linear_fit(x,y)
    
    ax21.plot(x, y, 'o', label='data')
    ax21.plot(x, res.intercept + res.slope*x, 'r', label='fit peaks')
    ax21.set_xlabel("scan_no")
    ax21.set_ylabel(f"{axis_name}")
    ax21.legend()

    # FWHM evolution

    x,y = fitpars["scan_no"],fitpars["FWHM"]
    ax22.plot(x,y,"g-")
    ax22.set_xlabel("scan_no")
    ax22.set_ylabel(f"FWHM",color="g")

    # compare intensities
    pars = np.recarray((len(scan_nos),),dtype=[("scan_no","<f8"),("start_time","<f8"),("label","|S50"),("max","<f8"),("sum","<f8")])
    for jj,ii in enumerate(scan_nos):
        print(f"scan_no: {ii}")
        pars[jj]["scan_no"]=ii
        pars[jj]["max"],pars[jj]["sum"] = get_scan_intensity_max_sum(fn,ii,counter)


    ax41 = plt.subplot(2,2,4)
    ax42 = ax41.twinx()

    x,y = pars["scan_no"],pars["max"]
    ax41.plot(x,y,"b-")
    ax41.set_xlabel("scan_no")
    ax41.set_ylabel("Max Intensity",color="b")

    x,y = pars["scan_no"],pars["sum"]
    ax42.plot(x,y,"g-")
    ax42.set_ylabel("Sum Intensity",color="g")
    plt.show()

    return fitpars,pars

def distance_from_COR(angle, move_to_beam, make_correction=False):
    """
    calculate the distance of an object from the center of rotation
    """

    distance = move_to_beam/np.tan(np.deg2rad(angle))

    if (angle>0 and move_to_beam>0) or (angle<0 and move_to_beam<0):
        # downstream of eta
        # downstream of phi
        print(f"suggest: umvr(lab_x,{distance})")

    if (angle>0 and move_to_beam<0) or (angle<0 and move_to_beam>0):
        # upstream of eta
        # upstream of phi
        print(f"suggest: umvr(lab_x,{-distance})")
        distance = distance*-1

    if make_correction:
        umvr(setup_globals.lab_x,distance)

def CDI_automated():
    """
    fully automate a BCDI measurement - for use on sxdm map i.e find peaks measure all crystals
    """
    # TODO
    # find peak on detector and create a ROI for it, plotselect it

    # line scans to align the crystal

    # choose motors for scan - favour phi it is a better due to hexapod
    # if nu < 20, use eta

    # calibrate the offset per degree

    # define how good the fit must be t use the offset correction factor

    # fast scan to check oversampling criterion

    # plot 3 slices zoomed and ask th user for confirmation

    # estimate total photon yield

    # adjust exposure time to yield desired resolution

    # run 3 measurements  50% overhead on desired total exposure

    # combine the datasets and launch the phasing 

    # update the necessary logbooks

    pass

def calcCDI_longitudinal_coh(wl, deltaLam):
    return wl/(2*deltaLam)

def calcCDI_transverse_coh(wl,sourceSampleDistance,sourceSize):
    return wl*sourceSampleDistance/(2*sourceSize)

def calcCDI_focal_spot_demag_limit(wl,NA):
    return wl/(2*NA)