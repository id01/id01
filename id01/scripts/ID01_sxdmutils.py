# -*- coding: utf-8 -*-

# ID01 BLISS git repo
#       authors:
#         Steven Leake, leake@esrf.fr
#
#       Created at: 20. May 09:00:00 CET 2021
#
#----------------------------------------------------------------------
# Description: 
#    functions for extending / interacting with sxdm scans
#    straight to daquiri? or use id01sware for basic easy to use stuff
#
#
# TODO:
#    
#----------------------------------------------------------------------
from bliss.shell.standard import *
from bliss.common.cleanup import cleanup, axis as cleanup_axis
from bliss import current_session, setup_globals

import numpy as np
import h5py as h5
import sys
import os.path as osp
import click
import matplotlib.pyplot as pl
import matplotlib.pyplot as plt
import time


from skimage import  registration
from skimage.registration import phase_cross_correlation
from skimage.feature import peak_local_max
#from scipy.ndimage.fourier import fourier_shift
from scipy.ndimage.measurements import center_of_mass
from scipy.ndimage.filters import maximum_filter, gaussian_filter    
from scipy.ndimage.morphology import generate_binary_structure, binary_erosion 


from id01.scripts.ID01_sxdm import asxdm, dsxdm
from id01.process.interactive import *
import id01.scripts.ID01_positionInstances as bl_posInst
import id01.scripts.ID01_beamlineParameters as bl_params
import id01.scripts.image as image
#import id01.process.h5utils as id01h5
from id01.process.interactive import cnorm
import id01.scripts.ID01_utils as ID01utils
#from id01.process.h5utils import openScan
import id01lib.io.h5utils as id01h5
#from id01.scripts.ID01_utils import elogbook_send_file


# id01sware utils

# TODO: the following macros do not exist yet / an issue is open on gitlab for flint interaction / daiquiri
# started work on PScanTracker_bliss

#  pscan_align%BR%
#  pscan_align1D%BR%
#  pscan_align2D%BR%
#  pscan_autoalign1D%BR%
#  pscan_autoalign2D%BR%
#  pscan_pickroi%BR%
#  pscan_inspect%BR%
#  pscan_inspect1D%BR%


def pizero(*axislist,suppress_umv=False):
    """
    pizero(pix,piy,piz)
    """
    motion = ()
    delta_motion = ()
    
    # OB hexapod under sample
    axis_dict = {"pix":current_session.env_dict["tobx"],
                 "piy":current_session.env_dict["toby"],
                 "piz":current_session.env_dict["tobz"]}

#    axis_dict = {"piy":current_session.env_dict["tobx"],
#                 "pix":current_session.env_dict["toby"],
#                 "piz":current_session.env_dict["tobz"]}
    
    # old hexapod 
    #axis_dict = {"pix":current_session.env_dict["thx"],
    #             "piy":current_session.env_dict["thy"],
    #             "piz":current_session.env_dict["thz"]}
    
    

#    sign_dict = {"pix":-1,
#                 "piy":1,
#                 "piz":1}
    # depends on the piezo
    sign_dict = {"pix":1,
                 "piy":1,
                 "piz":1}


    for axis in axislist:
        lims = axis.limits
        stroke = lims[1]-lims[0]
        cen_stroke = np.array(lims).sum()/2
        motion += (axis, cen_stroke)
        #if axis.name == "pix":
        #    delta_motion += (axis_dict[axis.name], - (axis.position-cen_stroke)/1000) # hexpaod hard coded
        #else:
        delta_motion += (axis_dict[axis.name], sign_dict[axis.name]*(axis.position-cen_stroke)/1000) # hexpaod hard coded
    
    if len(motion) and not suppress_umv:
        umv(*motion)
        
    if suppress_umv:
        return motion, delta_motion
        
        
def pizerohex(*axislist):
    """
    like a motor stack, move the hexapod to offset the PI to move 
    the beam to the center of the PI stroke
    i.e. maintaining alignment on the sample
    """
    
    motion, delta_motion = pizero(*axislist,suppress_umv=True)

    if len(delta_motion):
        umv(*motion)
        umvr(*delta_motion)
        
    # TODO dont hard code the hexapod consider piezo strokes signs etc.
        
        
def pisetstroke(strokelist, *axislist):
    """
    NOT IN USE YET
    pisetstroke([10,20,20],pix,piy,piz) 
    """
    
    # TODO pull values directly from PI controller
    # this is automatically identified by Bliss but not put anywhere i.e can be gained from the limits
    # TODO allow non zero min
    # error 
    if len(strokelist) != len(axislist):
        print("strokelist ({len(strokelist)}) != axislist ({len(axislist)})")
        exit
    for axis,stroke in zip(axislist,strokelist):
        axis.limits = (0,float(stroke))     
        
        
def alignpix(stroke,points,exposure):
    """
    1D pix scan using sxdm scans type
    stroke: total stroke of scan
    points: number of points
    exposure: time per point
    """
    motObj0 = current_session.env_dict["pix"]
    motObj1 = current_session.env_dict["piz"]
    deltaPos = stroke/2.
    return dsxdm(motObj0, -deltaPos,deltaPos, points, motObj1,-.1,.1,1,exposure)


def alignpiy(stroke,points,exposure):
    """
    1D piy scan using sxdm scans type
    stroke: total stroke of scan
    points: number of points
    exposure: time per point
    """ 
    motObj0 = current_session.env_dict["piy"]
    motObj1 = current_session.env_dict["piz"]
    deltaPos = stroke/2.
    return dsxdm(motObj0, -deltaPos,deltaPos, points, motObj1,-.1,.1,1,exposure)
    
    
def alignpiz(stroke,points,exposure):
    """
    1D pix scan using sxdm scans type
    stroke: total stroke of scan
    points: number of points
    exposure: time per point
    """    
    motObj0 = current_session.env_dict["piz"]
    motObj1 = current_session.env_dict["pix"]
    deltaPos = stroke/2.
    return dsxdm(motObj0, -deltaPos,deltaPos, points, motObj1,-.1,.1,1,exposure)

def _sxdm_align1D(channels = ["",], filename = None, nb = -1,  mynorm = 'linear', move_motors = True):
    # load all of the counters
    if filename is None: 
        globals_dict = current_session.env_dict
        filename = globals_dict["SCAN_SAVING"].filename
        
    if filename is not None: # we are working with hdf5
        
        # scans in hdf5 file                   
        sxdm_scans = id01h5.get_scans_title_str_hdf5(filename, "sxdm")
        
        #negative index pulls from the list in hdf5 file like np slicing
        if nb<0:
            nb = sxdm_scans[nb]
             
        print("\nfilename: %s, scan_no: %s\n"%(filename,nb))
        h5path_counters = "/%i.1/measurement/"%nb
        h5path_title = "/%i.1/title"%nb
        
        with h5.File(filename,'r') as h5f:
            channels_list = [key for key in h5f[h5path_counters].keys()]
            scan_title = h5f[h5path_title][()]
            if scan_title.count("sxdm")!=1:
                print("warning not sxdm data")
            m0name,m0s,m0e,m0_pts, m1name,m1s,m1e,m1_pts,exposure = scan_title.strip().split()[1:]
            m0s,m0e,m0_pts,m1s,m1e,m1_pts,exposure = float(m0s),float(m0e),int(m0_pts),float(m1s),float(m1e),int(m1_pts),float(exposure)
            m0name,m1name = m0name.strip(), m1name.strip() 
            xdata = h5f[h5path_counters+m0name+"_position"][()]
            channel_keys={}
            if channels==[]:
                channels = channels_list
                
            for channel in channels:
                if channels_list.count(channel):
                    try:
                        channel_keys[channel]=channel
                    except:
                        print("channel:%s not found"%channel) 

            if len(channel_keys)==0:
                print("#################\n Available channels: ")
                for item in channels_list:
                    print(item)
                print("#################\n")
            counters = np.zeros((len(channel_keys),m0_pts))
            for i,channel in enumerate(channel_keys):
                try:
                    counters[i,:] = h5f[h5path_counters][channel][()]
                except:
                    print("Failed for channel: %s"%channel)
            channel_list = list(channel_keys)
            
    try:    
        fig = pl.figure(figsize=(9,6))
        ax = fig.add_subplot(111)
        fig.suptitle(" File: %s \n Scan: %i"%(filename,nb))
        tracker = GenericIndexTracker1D(ax, xdata, data = counters, norm=mynorm,exit_onclick=True)
                
        ax.set_xlabel(m0name)
        ax.set_ylabel("Intensity (A.U)")
        #tracker.set_extent(m0s,m0e)
        tracker.set_axes_properties(title=channels)
        pl.show()
        
        if move_motors:
            print(tracker.motion)
            umv(*tracker.motion)
    except:
        if len(channel_keys)==0:
            print("#################\n Please select a channel frome the list above and provide it as an argument")
            print("  e.g sxdm_align([\"%s\"])"%channel_list[0])
        else:
            print("An unhandled error occurred contact leake@esrf.fr")
    
    
    
import id01.process.microscope as microscope 

def peak_optimise(motObj, detObj, counter = None, model = None,  exposure = None):
    """
    auto optimise a peak on a detector with a specified motor
    model = "diff" (default), "msd", "gradient"
    enhance = boost image contrast
    """

    pass
    
#def peak_optimise(motObj, detObj, counter = None, model = None, enhance = True, roi = None, exposure = None):
    #"""
    #auto optimise a peak on a detector with a specified motor
    #model = "diff" (default), "msd", "gradient"
    #enhance = boost image contrast
    #roi = image ROI (0,0,446,704) 
    
    ## TODO this has to be tested
    #"""
    #if counter == None:
        ## set the plotselect counter
            
    ## TODO add ROI selector or beam defined pixel position and deltas from this
    ## TODO remove gradients from images
      
    #if model not in image._models:
        #model = image._models[0]
    
    #print("start position: ", motObj.position, ", constrast: ",image.contrast(im, model), "residual: ", 1/image.contrast(im, model))
    
    #### do the autofocus
    #if counter is not None:
        #bliss_roi = detObj.roi_counters[counter].get_coords()
        #microscope_roi = (bliss_roi[0],bliss_roi[0]+bliss_roi[2],bliss_roi[1],bliss_roi[1]+bliss_roi[3])
        
    #af = microscope.AutoFocus(url="",motObj,enhance=enhance,roi=roi,detObj=detObj,exposure=exposure)

    #print("Starting autofocus...")
    #try:
        #fit = af.focus_ct() ### Do the focusing
        #print("Done. Status: %s"%fit.message)
        #print("New Position: %s=%f"%(af.motor.name,fit.x.item()))
    #except Exception as emsg:
        #print("Error: %s"%emsg)    


def dbeam_caustic_profile(mot0Obj, mot0stroke, mot0intervals, fastAxisObj, fastAxisstroke, fastAxisNpoints, exposure = 0.01, motOffsetObj=None, motOffsetStroke=None):
    """
    run a series of scans 
    """
    with cleanup(mot0Obj, restore_list=(cleanup_axis.POS,)):  # to be tested
        mot0_startpos = mot0Obj.position
        half_stroke = mot0stroke/2.

        if motOffsetObj is not None and motOffsetStroke is not None:
            motOffset_startpos = motOffsetObj.position
            half_offsetstroke = motOffsetStroke/2.
            for pos,offset_pos in zip(np.linspace(-half_stroke, half_stroke, mot0intervals+1) + mot0_startpos,np.linspace(-half_offsetstroke,half_offsetstroke,mot0intervals+1)+motOffset_startpos):
                umv(mot0Obj,pos,motOffsetObj,offset_pos)
                cmd = f"align{fastAxisObj.name}({fastAxisstroke},{fastAxisNpoints},{exposure})"
                exec(cmd)
            
            umv(motOffsetObj,motOffset_startpos)
        else:
            for pos in np.linspace(-half_stroke, half_stroke, mot0intervals+1) + mot0_startpos:
                umv(mot0Obj,pos)
                cmd = f"align{fastAxisObj.name}({fastAxisstroke},{fastAxisNpoints},{exposure})"
                exec(cmd)
    
    # TODO plot a series of scans so the peak can be determined.

def dbeam_caustic_contrast(mot0Obj, mot0stroke, mot0intervals, fastAxis0Obj, fastAxis0Start,fastAxis0End, fastAxis0Npoints,fastAxis1Obj, fastAxis1Start,fastAxis1End,fastAxis1Npoints, exposure = 0.01):
    """
    run a series of scans 
    """
    with cleanup(mot0Obj, fastAxis0Obj, fastAxis1Obj, restore_list=(cleanup_axis.POS,)):  # to be tested
        mot0_startpos = mot0Obj.position
        half_stroke = mot0stroke/2.
        for pos in np.linspace(-half_stroke, half_stroke, mot0intervals+1) + mot0_startpos:
            umv(mot0Obj,pos)
            dsxdm(fastAxis0Obj, fastAxis0Start,fastAxis0End, fastAxis0Npoints,fastAxis1Obj, fastAxis1Start,fastAxis1End,fastAxis1Npoints, exposure)
                       
            #exec(cmd)
            #print(cmd)
    
    # TODO plot a series of scans so the peak can be determined.    
    
def dbeam_caustic_profile_auto(axis, contrast_function, start_step = 0.5, end_step = 0.1):
    """
    run a series of scans and optimise based on the max intensity
    run a scan move 0.5mm
    run a scan
    """
    # TODO exploit peak optimise for a non linear search trajectory
    
    # TODO how to handle the geometry with generic macros.

def FZP_autoalign():
    """
    Assume you have a FZP roughly aligned the overfocused beam on the detector and so on
    """
    
    # TODO automatically find the beam and draw a symmteric ROI around it
    
    # TODO moved to a defined theta/2theta
    
    # TODO automatically put the wedge in and find the right height in Z
    
    # TODO fast scan to find the reflected beam
    
    # TODO run the dbeam_caustic_profile
    
    # TODO align to the maximum intensity


def asxdm_x2D(mot0Obj, mot0start, mot0end, mot0intervals, mot1Obj, mot1start, mot1end, mot1intervals, sxdm_m0_Obj, sxdm_m0_start, sxdm_m0_end, sxdm_m0_npts, sxdm_m1_Obj, sxdm_m1_start, sxdm_m1_end, sxdm_m1_npts, exposure = 0.01):
    """
    move hexapod through several positions and make maps
    use surface coordinates (not lab coordinates)
    """
    
    ## TODO warn for step size greater than fast scan range
    ## describe overlap in scan pixels
    
    for pos0 in np.linspace(mot0start, mot0end, mot0intervals+1):
        for pos1 in np.linspace(mot1start, mot1end, mot1intervals+1):
            umv(mot0Obj,pos0,mot1Obj,pos1)
            time.sleep(10)
            asxdm(  sxdm_m0_Obj, 
                    sxdm_m0_start, 
                    sxdm_m0_end, 
                    sxdm_m0_npts, 
                    sxdm_m1_Obj, 
                    sxdm_m1_start, 
                    sxdm_m1_end, 
                    sxdm_m1_npts, 
                    exposure)
    
    # TODO in abs coords << this should use the sample stack
    # TODO use the stroke of the PI - requires this to be saved somewhere.
    
    # TODO plot the data + interact with it ... maybe more a dacquiri thing

def sxdm_plot_n(counter,*filename_scan_nos, fig_width = 8, norm = "log"):
    """
    plot multiple sxdm maps on the same axes for counter
    sxdm_plot_n("mpx1x4","filename", [1,2,3,4,])
    
    code from EB modified by SL
    it will skip scans which are not sxdm so you can feed it all scans in the file
    """
    # piezo_hexapod_dict = {'pix' : {'hexa_mot' :'thx', 'sign' : -1},
    #                       'piy' : {'hexa_mot' :'thy', 'sign' : +1},
    #                       'piz' : {'hexa_mot' :'thz', 'sign' : +1}}
                          
    piezo_hexapod_dict = {'pix' : {'hexa_mot' :'tobx', 'sign' : -1},
                         'piy' : {'hexa_mot' :'toby', 'sign' : +1},
                         'piz' : {'hexa_mot' :'tobz', 'sign' : +1}}
    scans = dict()
    
    for fn,nbs in zip(filename_scan_nos[::2],filename_scan_nos[1::2]):
        scans[fn] = nbs
    
    fig, ax = plt.subplots(figsize=(fig_width,fig_width))
    
    for fn in scans.keys():
        id01h5.get_scans_title_str_hdf5(fn, "sxdm")
        for scan in scans[fn]:
            try:
                print("filename: %s, scan_no: %s"%(fn,scan))
                
                counter_data, title_data = get_SXDM(scan, counter, fn,prefix="")
                m0name,m0s,m0e,m0_pts,m1name,m1s,m1e,m1_pts,exposure = get_SXDM_params4mtitle(title_data)
                
                m0_pos, title = get_SXDM(scan, m0name+"_position", fn,prefix="")
                m1_pos, title = get_SXDM(scan, m1name+"_position", fn,prefix="")
                
                # add hexapod
                
                m0_hex = id01h5.get_scan_motor_hdf5(scan,fn,piezo_hexapod_dict[m0name]['hexa_mot'])
                m1_hex = id01h5.get_scan_motor_hdf5(scan,fn,piezo_hexapod_dict[m1name]['hexa_mot'])
                
                m0_pos = m0_pos*piezo_hexapod_dict[m0name]['sign']/1e3 + m0_hex
                m1_pos = m1_pos*piezo_hexapod_dict[m1name]['sign']/1e3 + m1_hex
                                
                ax.pcolormesh(m0_pos.reshape(m0_pts,m1_pts), m1_pos.reshape(m0_pts,m1_pts), counter_data.reshape(m0_pts,m1_pts), cmap='jet',norm=cnorm(counter_data,norm))
                
                ax.set_aspect('equal')

                ax.set_xlabel('{} [mm]'.format(piezo_hexapod_dict[m0name]['hexa_mot']))
                ax.set_ylabel('{} [mm]'.format(piezo_hexapod_dict[m1name]['hexa_mot']))
                
                # ax.scatter(m0_hex, m1_hex, marker='x', c='r')
                # ax.text(
                #     m0_pos.mean(), 
                #     m1_pos.mean(), 
                #     f'{scan}', 
                #     fontdict=dict(fontsize='x-large', fontweight='bold')
                #     )

            except:
                pass
    
    fig.tight_layout() 
    plt.show() 
    
def sxdm_plot_n_PI_under_optic(counter,*filename_scan_nos, fig_width = 8, norm = "log"):
    """
    plot multiple sxdm maps on the same axes for counter
    sxdm_plot_n("mpx1x4","filename", [1,2,3,4,])
    
    code from EB modified by SL
    it will skip scans which are not sxdm so you can feed it all scans in the file
    """
    # piezo_hexapod_dict = {'pix' : {'hexa_mot' :'thx', 'sign' : -1},
    #                       'piy' : {'hexa_mot' :'thy', 'sign' : +1},
    #                       'piz' : {'hexa_mot' :'thz', 'sign' : +1}}
                          
    # piy has no impact so ignore it
    piezo_hexapod_dict = {'pix' : {'hexa_mot' :'tobx', 'sign' : -1},
                         'piz' : {'hexa_mot' :'toby', 'sign' : -1}}
    scans = dict()
    
    for fn,nbs in zip(filename_scan_nos[::2],filename_scan_nos[1::2]):
        scans[fn] = nbs
    
    fig, ax = plt.subplots(figsize=(fig_width,fig_width))
    
    for fn in scans.keys():
        id01h5.get_scans_title_str_hdf5(fn, "sxdm")
        for scan in scans[fn]:
            try:
                print("filename: %s, scan_no: %s"%(fn,scan))
                
                counter_data, title_data = get_SXDM(scan, counter, fn,prefix="")
                m0name,m0s,m0e,m0_pts,m1name,m1s,m1e,m1_pts,exposure = get_SXDM_params4mtitle(title_data)
                
                m0_pos, title = get_SXDM(scan, m0name+"_position", fn,prefix="")
                m1_pos, title = get_SXDM(scan, m1name+"_position", fn,prefix="")
                
                # add hexapod
                
                m0_hex = id01h5.get_scan_motor_hdf5(scan,fn,piezo_hexapod_dict[m0name]['hexa_mot'])
                m1_hex = id01h5.get_scan_motor_hdf5(scan,fn,piezo_hexapod_dict[m1name]['hexa_mot'])
                
                alpha = id01h5.get_scan_motor_hdf5(scan,fn,"eta")
                factor = np.sin(np.deg2rad(alpha))
                
                if m0name =="piz":
                    m0_pos = m0_pos / factor
                elif m1name == "piz":
                    m1_pos = m1_pos / factor
                else:
                    print("no piz motor")
                    
                print(m0name,m0_hex,m1name,m1_hex)
                
                m0_pos = m0_pos*piezo_hexapod_dict[m0name]['sign']/1e3 + m0_hex
                m1_pos = m1_pos*piezo_hexapod_dict[m1name]['sign']/1e3 + m1_hex
                                
                ax.pcolormesh(m0_pos.reshape(m1_pts,m0_pts), m1_pos.reshape(m1_pts,m0_pts), counter_data.reshape(m1_pts,m0_pts), cmap='jet',norm=cnorm(counter_data,norm))
                
                ax.set_aspect('equal')

                ax.set_xlabel('{} [mm]'.format(piezo_hexapod_dict[m0name]['hexa_mot']))
                ax.set_ylabel('{} [mm]'.format(piezo_hexapod_dict[m1name]['hexa_mot']))
                
                # ax.scatter(m0_hex, m1_hex, marker='x', c='r')
                # ax.text(
                #     m0_pos.mean(), 
                #     m1_pos.mean(), 
                #     f'{scan}', 
                #     fontdict=dict(fontsize='x-large', fontweight='bold')
                #     )

            except:
                pass
    
    fig.tight_layout() 
    plt.show() 

#def sxdm_plot_n_old(counter,*filename_scan_nos, fig_width = 8):
    #"""
    #plot multiple sxdm maps on the same axes for channels = ["somecounter",]
    #("filename", [1,2,3,4,])
    
    #code from EB modified by SL
    
    ## this doesn't work
    #"""
    #piezo_hexapod_dict = {'pix' : {'hexa_mot' :'thx', 'sign' : -1},
                          #'piy' : {'hexa_mot' :'thy', 'sign' : +1},
                          #'piz' : {'hexa_mot' :'thz', 'sign' : +1}}
    #scans = dict()
    #import pdb
    ##pdb.set_trace()
    #for fn,nbs in zip(filename_scan_nos[::2],filename_scan_nos[1::2]):
        #scans[fn] = nbs
    
    
    #fig, ax = plt.subplots(figsize=(fig_width,fig_width))
    #m0_pos_arr = np.empty((0))
    #m1_pos_arr = np.empty((0))
    #counter_arr = np.empty((0))
    #for fn in scans.keys():
        #get_scans_title_str_hdf5(fn, "sxdm")
        #for scan in scans[fn]:
            #try:
                #print("filename: %s, scan_no: %s"%(fn,scan))
                
                #counter_data, title_data = get_SXDM(scan, counter, fn,prefix="")
                #m0name,m0s,m0e,m0_pts,m1name,m1s,m1e,m1_pts,exposure = get_SXDM_params4mtitle(title_data)
                
                #m0_pos, title = get_SXDM(scan, m0name+"_position", fn,prefix="")
                #m1_pos, title = get_SXDM(scan, m1name+"_position", fn,prefix="")
                
                ## add hexapod
                
                #m0_hex = id01h5.get_scan_motor_hdf5(scan,fn,piezo_hexapod_dict[m0name]['hexa_mot'])
                #m1_hex = id01h5.get_scan_motor_hdf5(scan,fn,piezo_hexapod_dict[m1name]['hexa_mot'])
                
                #m0_pos = m0_pos*piezo_hexapod_dict[m0name]['sign']/1e3 + m0_hex
                #m1_pos = m1_pos*piezo_hexapod_dict[m1name]['sign']/1e3 + m1_hex
                                
                #m0_pos_arr = np.r_[m0_pos_arr,m0_pos]
                #m1_pos_arr = np.r_[m1_pos_arr,m1_pos]
                #counter_arr = np.r_[counter_arr,counter_data]               
              
                
            #except:
                #pass
                
    #ax.pcolormesh(m0_pos_arr, m1_pos_arr, counter_arr, cmap='jet')
    ##plt.colorbar(cb)
    #ax.set_aspect('equal')

    #ax.set_xlabel('{} [mm]'.format(piezo_hexapod_dict[m0name]['hexa_mot']))
    #ax.set_ylabel('{} [mm]'.format(piezo_hexapod_dict[m1name]['hexa_mot']))    
    #fig.tight_layout() 
    #plt.show()           


    
    
    
def dsxdm_x2D(mot0Obj, mot0start, mot0end, mot0intervals, mot1Obj, mot1start, mot1end, mot1intervals, sxdm_m0_Obj, sxdm_m0_start, sxdm_m0_end, sxdm_m0_npts, sxdm_m1_Obj, sxdm_m1_start, sxdm_m1_end, sxdm_m1_npts, exposure = 0.01):
    """
    move hexapod through several positions and make maps
    use surface coordinates (not lab coordinates)
    """
    
    ref_motion = (mot0Obj, mot0Obj.position, mot1Obj, mot1Obj.position, sxdm_m0_Obj, sxdm_m0_Obj.position, sxdm_m1_Obj, sxdm_m1_Obj.position)
    
    asxdm_x2D(  mot0Obj, 
                mot0start+mot0Obj.position, 
                mot0end+mot0Obj.position, 
                mot0intervals, mot1Obj, 
                mot1start+mot1Obj.position, 
                mot1end++mot1Obj.position, 
                mot1intervals, 
                sxdm_m0_Obj, 
                sxdm_m0_start+sxdm_m0_Obj.position, 
                sxdm_m0_end+sxdm_m0_Obj.position, 
                sxdm_m0_npts, 
                sxdm_m1_Obj, 
                sxdm_m1_start+sxdm_m1_Obj.position, 
                sxdm_m1_end+sxdm_m1_Obj.position, 
                sxdm_m1_npts, 
                exposure = exposure)
    # TODO in abs coords << this should use the sample stack
    # TODO use the stroke of the PI - requires this to be saved somewhere.
    umv(*ref_motion)
    # TODO plot the data + interact with it ... maybe more a dacquiri thing

def asxdm_3D(fast_mot,
    xmin,
    xmax,
    x_nb_points,
    slow_mot,
    ymin,
    ymax,
    y_nb_points,
    expo_time,
    slowest_mot,
    zmin,
    zmax,
    z_nb_points,
    offset_correction_factor=(0,0),
    latency_time=0.,
    save_flag=True,
    frames_per_file=None):
    
    """
    Usage:
    asxdm
       <X fast motor> <Xmin> <Xmax> <X nr points>
       <Y slow motor> <Ymin> <Ymax> <Y nr lines>
       <sampling time (s)>
       <Z slow motor> <Zmin> <Zmax> <Z nr 2Dmaps>
       <offset_correction_factor>

    offset_correction_factor - microns per degree
    """
    
    # invoke metadata newdataset
    # TODO invoke SXDM metadata
    newdataset("SXDM")
    
    z_step = (zmax-zmin)/(z_nb_points-1)
    zpos_start = np.linspace(zmin,zmax,z_nb_points)
    zpos_deltas = zpos_start-zpos_start.mean()
    xpos_start = zpos_deltas*offset_correction_factor[0]+(xmin+xmax)/2.
    xstroke = xmax-xmin
    ypos_start = zpos_deltas*offset_correction_factor[1]+(ymin+ymax)/2.
    ystroke = ymax-ymin

    for zpos,ypos,xpos in zip(zpos_start,ypos_start,xpos_start):
        umv(fast_mot,xpos,slow_mot,ypos,slowest_mot, zpos)
        #umvr(fast_mot, float(xpos) + z_step*offset_correction_factor)
        #umvr(slow_mot, float(ypos) + z_step*offset_correction_factor)
        #umvr(slowest_mot, float(zpos) + z_step*offset_correction_factor)
        
        dsxdm(fast_mot, -xstroke/2., xstroke/2., x_nb_points, slow_mot, -ystroke/2., ystroke/2., \
              y_nb_points, expo_time, latency_time, \
              save_flag=save_flag, frames_per_file=frames_per_file)
    
    #TODO port necessary preanalysis to the elogbook
    
    # close dataset
    enddataset()


def dsxdm_3D(fast_mot,
    xmin,
    xmax,
    x_nb_points,
    slow_mot,
    ymin,
    ymax,
    y_nb_points,
    expo_time,
    slowest_mot,
    zmin,
    zmax,
    z_nb_points,
    offset_correction_factor=(0,0),
    latency_time=0.,
    save_flag=True,
    frames_per_file=None,):

    xmin += fast_mot.position
    xmax += fast_mot.position
    ymin += slow_mot.position
    ymax += slow_mot.position
    zmin += slowest_mot.position
    zmax += slowest_mot.position
    
    #TODO cleanup adds up to the end of the scan and then all of the moves are made
    with cleanup(fast_mot, slow_mot, slowest_mot, restore_list=(cleanup_axis.POS,)):
        return asxdm_3D(
            fast_mot,
            xmin,
            xmax,
            x_nb_points,
            slow_mot,
            ymin,
            ymax,
            y_nb_points,
            expo_time,
            slowest_mot,
            zmin,
            zmax,
            z_nb_points,
            offset_correction_factor=offset_correction_factor,
            latency_time=latency_time,
            save_flag=save_flag,
            frames_per_file=frames_per_file,
        )
    
    
def sxdm_guess_offset_correction_factor(fast_mot,
            xmin,
            xmax,
            x_nb_points,
            slow_mot,
            ymin,
            ymax,
            y_nb_points,
            expo_time,
            slowest_mot,
            zmin,
            zmax,
            z_nb_points,
            offset_correction_factor=(0,0),
            latency_time=0.,
            save_flag=True,
            frames_per_file=None, 
            counter=None,
            filename = None):
    """    
    # make a series of sxdms and extract the roi of interest and populate an array with 2D images
    asxdm_3D(fast_mot, 
            xmin,
            xmax,
            x_nb_points,
            slow_mot,
            ymin,
            ymax,
            y_nb_points,
            expo_time,
            slowest_mot,
            zmin,
            zmax,
            z_nb_points,
            offset_correction_factor,
            latency_time,
            save_flag,
            frames_per_file)
    """
    
    #TODO split this function, one function to calculate offsets

    degperZpoint= (zmax-zmin)/(z_nb_points-1)
    umperdegree = [((xmax-xmin)/(x_nb_points-1))/degperZpoint, ((ymax-ymin)/(y_nb_points-1))/degperZpoint]
    
    

    if filename is None: 
        globals_dict = current_session.env_dict
        filename = globals_dict["SCANS"][-1].scan_info["filename"]    
        scan_nb = globals_dict["SCANS"][-1].scan_info["scan_nb"]    

    h5path_title = "/%i.1/title"%scan_nb # to check for sxdm
    with h5.File(filename,'r') as h5f:
        scan_title = h5f[h5path_title][()]
        m0name,m0s,m0e,m0_pts, m1name,m1s,m1e,m1_pts,exposure = scan_title.strip().split()[1:]
        m0s,m0e,m0_pts,m1s,m1e,m1_pts,exposure = float(m0s),float(m0e),int(m0_pts),float(m1s),float(m1e),int(m1_pts),float(exposure)
        
        counters = np.zeros((z_nb_points,m1_pts,m0_pts))
        m2_pos = []
        for i in np.arange(z_nb_points):
            h5path_counters = "/%i.1/measurement/"%(i+1)
            print(osp.join(h5path_counters,counter),i)
            counters[i,:,:] = h5f[osp.join(h5path_counters,counter)][()].reshape(m1_pts,m0_pts)
            h5path_positioner = "/%i.1/instrument/positioners/"%(i+1)
            m2_pos.append(h5f[osp.join(h5path_positioner,slowest_mot.name)][()])
    
    # use EZ method, cross correlate consecutive sxdms    
    shifts = []
    shifts.insert(0, np.array([0,0]))
    for i in np.arange(z_nb_points):
        sh, err, dphase = registration.phase_cross_correlation(counters[i-1,:,:], counters[i,:,:])
        shifts.append(sh + shifts[i-1])
            
    etashift_arr = np.array(shifts)

    # now fit them to get the shift in each dimension
    x = np.arange(z_nb_points+1) #etashift_arr[:,0]
    y = etashift_arr[:,0]

    m0, b = np.polyfit(x, y, 1)
    # TODO port this to the logbook
    #plt.plot(x,y)
    #plt.plot(x, m0*x + b)
    
    x = np.arange(z_nb_points+1) #etashift_arr[:,0]
    y = etashift_arr[:,1]

    m1, b = np.polyfit(x, y, 1)
    # TODO port this to the logbook
    #plt.plot(x,y)
    #plt.plot(x, m1*x + b)
    
    #m pixels per deg - convert
    calc_offset_correction_factor = (m0 * umperdegree[0],m1 * umperdegree[1]) 
    print("Suggested drift corrected SXDM:")
    outs = ""
    outs+=f"asxdm_3D({fast_mot.name},{xmin},{xmax},{x_nb_points},{slow_mot.name},"
    outs+=f"{ymin},{ymax},{y_nb_points},{expo_time},{slowest_mot.name},{zmin},{zmax},"
    outs+=f"{z_nb_points},offset_correction_factor = {calc_offset_correction_factor},"
    outs+=f"latency_time = {latency_time},save_flag = True,frames_per_file={frames_per_file})"
    print(outs)
    
   
def sxdm_calc_contrast(scan_no, counter, filename = None, contrast_model = "diff"):
    """
    return the contrast of an sxdm scan
    """
    
    if filename is None: 
        globals_dict = current_session.env_dict
        filename = globals_dict["SCAN_SAVING"].filename
                
    # scans in hdf5 file                   
    sxdm_scans = id01h5.get_scans_title_str_hdf5(filename, "sxdm")
    #negative index pulls from the list in hdf5 file like np slicing
    if scan_no<0:
        scan_no = sxdm_scans[scan_no]
                
    counter, title = get_SXDM(scan_no, counter, filename)    
    m0name,m0s,m0e,m0_pts,m1name,m1s,m1e,m1_pts,exposure = get_SXDM_params4mtitle(title)
    counter = counter.reshape(m1_pts,m0_pts)
    
    return image.contrast(counter, contrast_model)

    #TODO port data to elogbook

def sxdm_plot(sxdmScanObj,counter_name):
    """
    defunt 20220217
    """
    cnt_data = sxdmScanObj.get_data(counter_name)
    fm_channel = sxdmScanObj.scan_info["fast_motor"]
    sm_channel = sxdmScanObj.scan_info["slow_motor"]
    fm_data =  sxdmScanObj.get_data(fm_channel)
    sm_data =  sxdmScanObj.get_data(sm_channel)    
    # miserable hack for the points
    fm, fms, fmf, fmpts, sm, sms, smf, smpts, exp1 = sxdmScanObj.scan_info["title"].strip().split()[1:]
    plt.pcolormesh(fm_data.reshape(int(fmpts),int(smpts)),sm_data.reshape(int(fmpts),int(smpts)),cnt_data.reshape(int(fmpts),int(smpts)))
    plt.show()
 
#current_session.env_dict["alignDefaultCounter"] = "someROI"
## aligning on sxdm datsets - it was recommended not to use the SCANS[-1] 
# approach to keep the Bliss shell clean of heavy computation >4000 points    
    
def hdf5handler(filename,path):
    """
    open / pull all data / close when retrieving anything from hdf5
    this avoids potential file lock problems
    """
    # TODO check if it is more efficient to load all from a single file open/close
    #data_dict = {}
    with h5.File(filename,'r') as h5file:
        data = h5file[path][()]
    return data

    
def sxdm_align(channels = ["",], filename = None, nb = -1,  mynorm = 'linear', move_motors = True, vmin=None, vmax=None):
    """
    channels: the counters to plot
    filename: hdf5 filename
    nb: scan number (accepts list -n nomenclature)
    
    TODO: counters in hdf5 do not match counters in measurement group e.g _sum
    
    note found an upper limit on memory - ask SEB where this comes from 
    needa check if sxdm is more than 1000 points should use the hdf5 file.
    
    TODO this uses the input values not the raw adc for plotting
    pcolormesh could be used here??
    
    TODO make channels a counters list or MG or something more sensible
    the counter channels names varies between hdf5 and SCANS object which is nasty
    
    TODO Add check for other motor positions - to make sure positioners havent changed
    
    TODO     #toggle button for log/linear + slide bar for colorbar?

    """


    # load all of the counters
    if filename is None: 
        globals_dict = current_session.env_dict
        filename = globals_dict["SCAN_SAVING"].filename
        # we are working with the deque SCANS / scan number in memory
        #m0name,m0s,m0e,m0_pts, m1name,m1s,m1e,m1_pts,x,y,counters,channels_dict = sxdmdatahandler_fast(nb, channels)
        #channels_list = list(channels_dict.keys())
        
    if filename is not None: # we are working with hdf5
        
        # scans in hdf5 file                   
        sxdm_scans = id01h5.get_scans_title_str_hdf5(filename, "sxdm")
        
        #negative index pulls from the list in hdf5 file like np slicing
        if nb<0:
            nb = sxdm_scans[nb]
             
        print("\nfilename: %s, scan_no: %s\n"%(filename,nb))
        h5path_counters = "/%i.1/measurement/"%nb
        h5path_title = "/%i.1/title"%nb
        
        with h5.File(filename,'r') as h5f:
            channels_list = [key for key in h5f[h5path_counters].keys()]
            scan_title = h5f[h5path_title][()].decode()
            if scan_title.count("sxdm")!=1:
                print("warning not sxdm data")
            m0name,m0s,m0e,m0_pts, m1name,m1s,m1e,m1_pts,exposure = scan_title.strip().split()[1:]
            m0s,m0e,m0_pts,m1s,m1e,m1_pts,exposure = float(m0s),float(m0e),int(m0_pts),float(m1s),float(m1e),int(m1_pts),float(exposure)
            m0name,m1name = m0name.strip(), m1name.strip() 
            
            if m1_pts==1:
                _sxdm_align1D(channels = channels, filename = filename, nb = nb,  mynorm = mynorm, move_motors = move_motors)
                return
            
            channel_keys={}
            if channels==[]:
                channels = channels_list
                
            for channel in channels:
                if channels_list.count(channel):
                    try:
                        channel_keys[channel]=channel
                    except:
                        print("channel:%s not found"%channel) 

            if len(channel_keys)==0:
                print("#################\n Available channels: ")
                for item in channels_list:
                    print(item)
                print("#################\n")
                
            counters = np.zeros((len(channel_keys),m1_pts,m0_pts))
            for i,channel in enumerate(channel_keys):
                try:
                    counters[i,:,:] = h5f[h5path_counters][channel][()].reshape(m1_pts,m0_pts)                
                except:
                    print("Failed for channel: %s"%channel)
            channel_list = list(channel_keys)

    # look at them

    try:    
        fig = pl.figure(figsize=(9,6))
        ax = fig.add_subplot(111)
        fig.suptitle(" File: %s \n Scan: %i"%(filename,nb))
        tracker = GenericIndexTracker(ax, data = counters, norm=mynorm,exit_onclick=True,imshow_kw={"vmin":vmin,"vmax":vmax})
        ax.set_xlabel(m0name)
        ax.set_ylabel(m1name)
        tracker.set_extent(m0s,m0e,m1s,m1e)
        tracker.set_axes_properties(title=channels)
        pngfn = "/tmp/test.png"
        pl.savefig(pngfn)
        #elogbook_send_file(pngfn)
        pl.show()
        
        if move_motors:
            #print(tracker.motion)
            umv(*tracker.motion)
    except:
        if len(channel_keys)==0:
            print("#################\n Please select a channel frome the list above and provide it as an argument")
            print("  e.g sxdm_align([\"%s\"])"%channel_list[0])
        else:
            print("An unhandled error occurred contact leake@esrf.fr")
        

def sxdm_inspect(channels = ["",], filename = None, nb = -1,  mynorm = 'linear', positionsGroup = True):
    """
    channels: the counters to plot
    filename: hdf5 filename
    nb: scan number (accepts list -n nomenclature)
    
    TODO: counters in hdf5 do not match counters in measurement group e.g _sum
    
    note found an upper limit on memory - ask SEB where this comes from 
    needa check if sxdm is more than 1000 points should use the hdf5 file.
    
    TODO this uses the input values not the raw adc for plotting
    pcolormesh could be used here??
    
    TODO make channels a counters list or MG or something more sensible
    the counter channels names varies between hdf5 and SCANS object which is nasty
    
    TODO Add check for other motor positions - to make sure positioners havent changed
    
    TODO     #toggle button for log/linear + slide bar for colorbar?

    """


    # load all of the counters
    if filename is None: 
        globals_dict = current_session.env_dict
        filename = globals_dict["SCAN_SAVING"].filename
        # we are working with the deque SCANS / scan number in memory
        #m0name,m0s,m0e,m0_pts, m1name,m1s,m1e,m1_pts,x,y,counters,channels_dict = sxdmdatahandler_fast(nb, channels)
        #channels_list = list(channels_dict.keys())
        
    if filename is not None: # we are working with hdf5
        
        # scans in hdf5 file                   
        sxdm_scans = id01h5.get_scans_title_str_hdf5(filename, "sxdm")
        
        #negative index pulls from the list in hdf5 file like np slicing
        if nb<0:
            nb = sxdm_scans[nb]
             
        print("\nfilename: %s, scan_no: %s\n"%(filename,nb))
        h5path_counters = "/%i.1/measurement/"%nb
        h5path_title = "/%i.1/title"%nb
        
        with h5.File(filename,'r') as h5f:
            channels_list = [key for key in h5f[h5path_counters].keys()]
            scan_title = bytes(h5f[h5path_title][()])
            if scan_title.count(b"sxdm")!=1:
                print("warning not sxdm data")
            m0name,m0s,m0e,m0_pts, m1name,m1s,m1e,m1_pts,exposure = scan_title.strip().split()[1:]
            m0s,m0e,m0_pts,m1s,m1e,m1_pts,exposure = float(m0s),float(m0e),int(m0_pts),float(m1s),float(m1e),int(m1_pts),float(exposure)
            m0name,m1name = m0name.strip(), m1name.strip() 
        
            channel_keys={}
            if channels==[]:
                channels = channels_list
                
            for channel in channels:
                if channels_list.count(channel):
                    try:
                        channel_keys[channel]=channel
                    except:
                        print("channel:%s not found"%channel) 

            if len(channel_keys)==0:
                print("#################\n Available channels: ")
                for item in channels_list:
                    print(item)
                print("#################\n")
                
            counters = np.zeros((len(channel_keys),m1_pts,m0_pts))
            for i,channel in enumerate(channel_keys):
                try:
                    counters[i,:,:] = h5f[h5path_counters][channel][()].reshape(m1_pts,m0_pts)                
                except:
                    print("Failed for channel: %s"%channel)
            channel_list = list(channel_keys)

    # look at them
    try:    
        fig = pl.figure(figsize=(9,6))
        ax = fig.add_subplot(111)
        fig.suptitle(" File: %s \n Scan: %i"%(filename,nb))
        tracker = GenericIndexTracker(ax, data = counters, norm=mynorm,exit_onclick=False)
        ax.set_xlabel(m0name)
        ax.set_ylabel(m1name)
        tracker.set_extent(m0s,m0e,m1s,m1e)
        tracker.set_axes_properties(title=channels)
        pl.show()
        
    except:
        print("An error occurred")
    
    if positionsGroup:
        # place in a positions group
        instance_counter = 0
        # check env dict if the group exists - offer the option to clean it or add to it
        globals_dict = current_session.env_dict
        
        try:
            if globals_dict['positionsGroup_sxdm_inspect']:
                print("positionsGroup_sxdm_inspect exists.. [space_bar] to overwrite or any other key to append")
                ans = click.getchar(echo=False)
                if ans == " ":
                    globals_dict['positionsGroup_sxdm_inspect'].parameters.purge()
                    globals_dict['positionsGroup_sxdm_inspect'] = bl_posInst.ID01Positions("sxdm_inspect",tracker.POI_list[0][0][0],tracker.POI_list[0][0][2])
                else:
                    instance_counter = len(globals_dict['positionsGroup_sxdm_inspect'].parameters.instances)-2 # 2 defaults
                    print(instance_counter)
        except:    
            globals_dict['positionsGroup_sxdm_inspect'] = bl_posInst.ID01Positions("sxdm_inspect",tracker.POI_list[0][0][0],tracker.POI_list[0][0][2])
        
        for i,item in enumerate(tracker.POI_list):
            item = item[0]
            globals_dict['positionsGroup_sxdm_inspect'].switch("p%i"%(i+instance_counter))
            # hard coded could be neater TODO
            globals_dict['positionsGroup_sxdm_inspect'].set_axis_position(item[0],item[1])
            globals_dict['positionsGroup_sxdm_inspect'].set_axis_position(item[2],item[3])
        
    return tracker.POI_list


def auto_alignSXDM_COM(scan_no, counter, filename=None):
    """
    COM based on actual motor positions in the scan
    
    TODO find the next scan number in BLISS
    """
    #TODO
    globals_dict = current_session.env_dict
    if filename is None: 
        filename = globals_dict["SCAN_SAVING"].filename
    
    hdf5_hotfix = True # added to catch an error searching for the hdf5 file before it is written
    while hdf5_hotfix:
        try:
            counter, title = get_SXDM(scan_no, counter, filename)
            m0name,m0s,m0e,m0_pts,m1name,m1s,m1e,m1_pts,exposure = get_SXDM_params4mtitle(title)
            counter = counter.reshape(m1_pts,m0_pts)[3:-3,3:-3]
            #COM = center_of_mass(counter) # assumes uniform grid

            counter_m0, title = get_SXDM(scan_no, m0name+"_position", filename)
            counter_m1, title = get_SXDM(scan_no, m1name+"_position", filename)
            counter_m0 = counter_m0.reshape(m1_pts,m0_pts)[3:-3,3:-3]
            counter_m1 = counter_m1.reshape(m1_pts,m0_pts)[3:-3,3:-3]
            m0 = (counter_m0*counter).sum()/counter.sum()
            m1 = (counter_m1*counter).sum()/counter.sum()

            umv(globals_dict[m0name],m0,globals_dict[m1name],m1)
            hdf5_hotfix = False
        except:
            print(f"waiting for hdf5 file: {filename}")
        
    return m0,m1
    
    
def auto_alignSXDM_CC(scan0_nb,counter0_name,scan1_nb,counter1_name,filename=None):
    """
    autocorrelation - use get_KMAP function
    """
    #TODO
    
    def get_shift_between_images_CC(image0, image1, upsample_factor=1000):
        """
        calculate the sub pixel shift between two images using sckit-image
        """
        cc, error, diffphase = registration.phase_cross_correlation(image0,image1, upsample_factor=upsample_factor)
        return cc,error,diffphase
        
    counter0, title0 = get_SXDM(scan0_nb, counter0_name, filename)
    m0name,m0s,m0e,m0_pts,m1name,m1s,m1e,m1_pts,exposure = get_SXDM_params4mtitle(title0)
    counter0 = counter0.reshape(m1_pts,m0_pts)

    counter1, title1 = get_SXDM(scan1_nb, counter1_name, filename)
    m0name,m0s,m0e,m0_pts,m1name,m1s,m1e,m1_pts,exposure = get_SXDM_params4mtitle(title1)
    counter1 = counter1.reshape(m1_pts,m0_pts)  
    
    cc,error,diffphase = get_shift_between_images_CC(counter0, counter1, upsample_factor=1000)  
    
    # TODO add calculated shift in normalised units
    return cc,error,diffphase


def auto_findPeaksSXDM(scan_no, counter, filename=None, min_distance_pxls=1,prefix="",threshold = 0.5, smooth=False,smooth_nn=1.2, verbose=False):
    """
    analyse a 2D sxdm - pick some peaks and return their positions
    threshold <1 is a multiplication of max 
    threshold >1 it is an absolute threshold

    output is in intensity order, brightest first.
    """
    #TODO
    if filename is None: 
        globals_dict = current_session.env_dict
        filename = globals_dict["SCAN_SAVING"].filename

    scan = id01h5.openScan(filename,scan_no)

    print(scan_no, counter, filename,prefix)

    if verbose:
        print(scan.getAllCountersList())

    data = scan.getRoiData(counter) 
    
    if threshold >1: 
        data[data < threshold] = 0

    elif threshold <=1:
        data[data < data.max()*threshold] = 0


    if smooth:
        data = gaussian_filter(data, smooth_nn, mode='nearest')
    # hack from stack overflow
    #peaks = detect_peaks(data)

    # skimage professional implementation
    peaks = peak_local_max(data)

    motors2check = ["pix","piy","piz"]
    positions_dict = {}
    for motor in motors2check:
        try:
            motpos = scan.getRoiData(motor+"_position")
            positions_dict[motor] = motpos
        except:
            pass

    positions_dict_pi ={}
    list_motors=[]
    for motor in positions_dict.keys():
        positions_dict_pi[motor] = [positions_dict[motor][x,y] for x,y in peaks]
        list_motors.append(motor)
    
    intensities = [data[x,y] for x,y in peaks] 

    # TODO return a motion list? or MP points list
    if verbose:
        plt.figure()
        plt.imshow(data)
        plt.plot(peaks[:,1],peaks[:,0],'ro')
        plt.colorbar()
        plt.show()
    
    return positions_dict_pi,peaks, intensities

def detect_peaks(image):
    """
    Takes an image and detect the peaks usingthe local maximum filter.
    Returns a boolean mask of the peaks (i.e. 1 when
    the pixel's value is the neighborhood maximum, 0 otherwise)
    """

    # define an 8-connected neighborhood
    neighborhood = generate_binary_structure(2,2)

    #apply the local maximum filter; all pixel of maximal value 
    #in their neighborhood are set to 1
    local_max = maximum_filter(image, footprint=neighborhood)==image
    #local_max is a mask that contains the peaks we are 
    #looking for, but also the background.
    #In order to isolate the peaks we must remove the background from the mask.

    #we create the mask of the background
    background = (image==0)

    #a little technicality: we must erode the background in order to 
    #successfully subtract it form local_max, otherwise a line will 
    #appear along the background border (artifact of the local maximum filter)
    eroded_background = binary_erosion(background, structure=neighborhood, border_value=1)

    #we obtain the final mask, containing only peaks, 
    #by removing the background from the local_max mask (xor operation)
    detected_peaks = local_max ^ eroded_background

    return detected_peaks




def get_SXDM(scan_no, counter, filename=None,prefix=""):
    """
    return a previously measure KMAP for use in the BLISS terminal
    """
    #TODO
    if filename is None: 
        globals_dict = current_session.env_dict
        filename = globals_dict["SCAN_SAVING"].filename
    #import pdb  # TODO file locking issue????
    #pdb.set_trace()
    h5path_counter = "/%s%i.1/measurement/%s"%(prefix,scan_no,counter)
    h5path_title = "/%s%i.1/title"%(prefix,scan_no) 

    try:
        with h5.File(filename,'r') as h5f:
            counter_data = h5f[h5path_counter][()]
    except:
        print("... %s does not exist ..."%h5path_counter) 
            
    try:
        with h5.File(filename,'r') as h5f:
            title_data = h5f[h5path_title][()]
    except:
        print("... %s does not exist ..."%h5path_title)
        
    if title_data.count("sxdm") == 0:
        raise ValueError("Scan %i is not a sxdm dataset"%scan_no)
            
    return counter_data, title_data

def get_SXDM_detIms(scan_no, counter, roi ,filename=None,prefix=""):
    """
    return a previously measured SXDM map for use in the BLISS terminal
    """
    #TODO
    if filename is None: 
        globals_dict = current_session.env_dict
        filename = globals_dict["SCAN_SAVING"].filename
    #import pdb  # TODO file locking issue????
    #pdb.set_trace()
    h5path_counter = "/%s%i.1/measurement/%s"%(prefix,scan_no,counter)
    h5path_title = "/%s%i.1/title"%(prefix,scan_no) 

    try:
        with h5.File(filename,'r') as h5f:
            counter_data = h5f[h5path_counter][:,roi["Ymin"]:roi["Ymax"],roi["Xmin"]:roi["Xmax"]][()]
    except:
        print("... %s does not exist ..."%h5path_counter) 
            
    try:
        with h5.File(filename,'r') as h5f:
            title_data = h5f[h5path_title][()]
    except:
        print("... %s does not exist ..."%h5path_title)
        
    if title_data.count("sxdm") == 0:
        raise ValueError("Scan %i is not a sxdm dataset"%scan_no)
            
    return counter_data, title_data
   
    
def get_SXDM_params4mtitle(title):
    m0name,m0s,m0e,m0_pts, m1name,m1s,m1e,m1_pts,exposure = title.strip().split()[1:]
    m0s,m0e,m0_pts,m1s,m1e,m1_pts,exposure = float(m0s),float(m0e),int(m0_pts),float(m1s),float(m1e),int(m1_pts),float(exposure)
    m0name,m1name = m0name.strip(), m1name.strip() 
    return m0name,m0s,m0e,m0_pts,m1name,m1s,m1e,m1_pts,exposure
    #TODO properly collection
                    
def get_SXDM_detector_average(scan_no, detObjName, filename=None):
    """
    produce a detector average image and stick it in the datafile somehow
    """
    
    if filename is None: 
        globals_dict = current_session.env_dict
        filename = globals_dict["SCAN_SAVING"].filename
        
    # TODO find detector used automatically
    counter, title = get_SXDM(scan_no, detObjName, filename)
    
    # TODO optimise with MP and porting to a faster computer
    return counter.sum(0), title     

    
def reanalyse_SXDM_pickroi(scan, counter, roi=None, filename=None, fig_width=8, norm="log"):
    """
    based on a user defined ROI reanalyse, 
    pickroi from a detector average image
    roi: {"Xmin":0,"Xmax":1,"Ymin":0,"Ymax":1]}
    """
    det_avg, title = get_SXDM_detector_average(scan, counter, filename = filename)
    if roi==None:
        roi = ID01utils.user_pickroi(det_avg)
    
    # pull the hdf5 data but only the slice
    imStack, title_data = get_SXDM_detIms(scan, counter, roi) 
    #[roi["Xmin"]:roi["Xmax"],roi["Ymin"]:roi["Ymax"]]
    counter_data = imStack.sum(axis=(1,2))
    
    m0name,m0s,m0e,m0_pts,m1name,m1s,m1e,m1_pts,exposure = get_SXDM_params4mtitle(title_data)
    
    m0_pos, title = get_SXDM(scan, m0name+"_position", filename,prefix="")
    m1_pos, title = get_SXDM(scan, m1name+"_position", filename,prefix="")
       
    fig, ax = plt.subplots(figsize=(fig_width,fig_width))                
    ax.pcolormesh(m0_pos.reshape(m0_pts,m1_pts), m1_pos.reshape(m0_pts,m1_pts), counter_data.reshape(m0_pts,m1_pts), cmap='jet',norm=cnorm(counter_data,norm))
    
    ax.set_aspect('equal')

    ax.set_xlabel('{} [um]'.format(m0name))
    ax.set_ylabel('{} [um]'.format(m1name))
    fig.tight_layout()
    pl.show()
    
def reanalyse_SXDM():
    """
    based on a user defined ROI reanalyse 
    """
    #TODO
    # provide lima_pickroi type command to the dteector avergae of a sxdm map
    # slice the hdf5 file using the values from the previous output
    pass


##================================================
## PB the following function disappeared this morning 2022-04-01 08:37
## from this module 
#def get_scans_title_str_hdf5(filename, string):
    #"""
    #return a list of scans whose title contains <string> from an hdf5 <filename>
    #"""
    #print("Available %s scans in hdf5 file:"%string)
    
    #with h5.File(filename,'r') as h5f:
        #scans = [key for key in h5f.keys()]
        #scans = list(map(float,scans))
        #scans = list(map(int,scans))
        #scans.sort()
        #scansList = []
        #for scan in scans:
            #if h5f["%i.1/title"%scan][()].count(string):
                #print("%i ... %s"%(scan,h5f["%i.1/title"%scan][()]))
                #scansList.append(scan)
    #return scansList




def get_piezo_deltas(filename = None, nb = -1,mynorm = 'linear',stds=2):
    """

    show the deltas for the piezo over the sxdm scan

    filename: hdf5 filename
    nb: scan number (accepts list -n nomenclature)
    

    """


    # load all of the counters
    if filename is None: 
        globals_dict = current_session.env_dict
        filename = globals_dict["SCAN_SAVING"].filename
        # we are working with the deque SCANS / scan number in memory
        #m0name,m0s,m0e,m0_pts, m1name,m1s,m1e,m1_pts,x,y,counters,channels_dict = sxdmdatahandler_fast(nb, channels)
        #channels_list = list(channels_dict.keys())
        
    if filename is not None: # we are working with hdf5
        
        # scans in hdf5 file                   
        sxdm_scans = id01h5.get_scans_title_str_hdf5(filename, "sxdm")
        
        #negative index pulls from the list in hdf5 file like np slicing
        if nb<0:
            nb = sxdm_scans[nb]
             
        print("\nfilename: %s, scan_no: %s\n"%(filename,nb))
        
        scan = id01h5.openScan(filename, nb)
        breakpoint()
        posdict = scan.getSXDM_MotorsPosition()
        scan_mots = scan.getScanMotorNames()
        m0_name, m1_name = scan_mots
        m0_s, m0_e, m0_pts = scan.command.split()[2:5]
        m0_s=float(m0_s)
        m0_e=float(m0_e)
        m0_pts= int(m0_pts)

        m1_s, m1_e, m1_pts = scan.command.split()[6:9]
        m1_s=float(m1_s)
        m1_e=float(m1_e)
        m1_pts= int(m1_pts)

        m0,m1 = np.meshgrid(np.linspace(m0_s,m0_e,m0_pts),np.linspace(m1_s,m1_e,m1_pts))

        plt.figure(figsize=(12,12))
        plt.subplot(2,2,1)
        print(m0_name, m1_name)
        print(posdict)
        print(posdict[m0_name],posdict[m1_name])
        plt.scatter(posdict[m0_name],posdict[m1_name],2)
        ax=plt.gca()
        ax.invert_yaxis()
        plt.title(f"scan positions {m0_name} / {m1_name}")
        plt.ylabel = m1_name
        plt.xlabel = m0_name
        plt.subplot(2,2,3)
        plt.title(f"{m0_name} deltas")
        deltas = m0-posdict[m0_name]
        std = deltas.std()
        mean = deltas.mean()
        plt.imshow(deltas,vmin = mean-std*stds,vmax = mean+std*stds)
        plt.colorbar()
        plt.subplot(2,2,2)
        plt.title("histogram")
        plt.xlabel = "deltas"
        plt.ylabel = "int"
        plt.hist(deltas.flatten(),bins='auto')
        plt.subplot(2,2,4)
        plt.title( f"{m1_name} deltas")
        deltas = m1-posdict[m1_name]
        std = deltas.std()
        mean = deltas.mean()
        plt.imshow(deltas,vmin = mean-std*stds,vmax = mean+std*stds)        
        plt.colorbar()   
        plt.subplot(2,2,2)
        plt.hist(deltas.flatten(),bins='auto')
        plt.show()    


def differentialSXDM(filepath,nb0,nb1,roi_name):
    scan0 = id01h5.openScan(filepath,nb0)
    map0 = scan0.getRoiData(roi_name)

    scan1 = id01h5.openScan(filepath,nb1) 
    map1 = scan1.getRoiData(roi_name)


    plt.imshow(map0-map1)
    tmppath = filepath.split("RAW_DATA")[-1]
    plt.title(f"{tmppath}: {roi_name} scan number {nb0} - {nb1}")
    ax= plt.gca()
    ax.invert_yaxis()
    plt.show()
