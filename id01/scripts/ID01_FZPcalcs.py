# -*- coding: utf-8 -*-

# ID01 BLISS git repo
#       authors:
#         Steven Leake, leake@esrf.fr
#
#       Created at: 11. Apr 16:22:00 CET 2023
#
#----------------------------------------------------------------------
# Description: 
#    FZP calculation tools for ID01 - using X-ray data booklet  https://xdb.lbl.gov/Section4/Sec_4-4.html
# 
#

import numpy as np

def focal_spot_limit(outer_zone_width):
    return 1.22*outer_zone_width

def NA(wavelength, outer_zone_width):
    return wavelength/(2*outer_zone_width)

def DoF(wavelength, outer_zone_width):
    return wavelength/(2*(NA(wavelength, outer_zone_width)**2))

def focal_length(wavelength, diameter, outer_zone_width, order):
    return (( diameter*outer_zone_width ) / ( order*wavelength ) )

def calc_FZP(energy, diameter, outer_zone_width, order, source_size = (60e-6,15e-6),distance2source=118.5):
    """
    energy: keV
    diameter: um
    outer_zone_width: nm
    order: integer 
    """

    wavelength = 12.39842/energy*1e-10 
    outer_zone_width = outer_zone_width*1e-9
    diameter = diameter*1e-6

    print(f"wavelength[Angstroms]: {wavelength}")

    print(f"focal length [mm]: {focal_length(wavelength, diameter, outer_zone_width, order)*1e3}")

    print(f"NA : {NA(wavelength,outer_zone_width)}")

    print(f"Depth of Focus (plus/minus) [um]: {DoF(wavelength, outer_zone_width)/1e-6}")


    print(f"Estimate beam size at sample (H,V) [nm]: ")

    FZP_beam_size = (focal_spot_limit(outer_zone_width)/1e-9,focal_spot_limit(outer_zone_width)/1e-9)
    print(f"  FZP limit : {FZP_beam_size[0]}, {FZP_beam_size[1]}")

    geometry_beam_size = (source_size[0]/(distance2source/focal_length(wavelength, diameter, outer_zone_width, order))/1e-9,source_size[1]/(distance2source/focal_length(wavelength, diameter, outer_zone_width, order))/1e-9)
    print(f"  geometric limit:  {geometry_beam_size[0]}, {geometry_beam_size[1]}")
    
    anticipated_beam_size = np.array(FZP_beam_size)

    if FZP_beam_size[0]<geometry_beam_size[0]:
        print(f"You will be limited by geometry in the horizontal: {geometry_beam_size[0]}")
        anticipated_beam_size[0] = geometry_beam_size[0]

    if FZP_beam_size[1]<geometry_beam_size[1]:
        print(f"You will be limited by geometry in the horizontal: {geometry_beam_size[1]}")
        anticipated_beam_size[1] = geometry_beam_size[1]

    print(f"... expected beam size (H,V), [nm]: {anticipated_beam_size[0]}, {anticipated_beam_size[1]}")

    source_horiz = 60e-6

    
def FZP_check_geometry_vs_lens_limit(energies,diameter,outer_zone_width,order,source_horiz, source_vert):
    """
    compare the geometry imposed beam shaping to the lens limit
    """
    wavelengths = (12.39842/energies)*1e-10
    focals = focal_length(wavelengths,diameter, outer_zone_width, order)

    demag= 118.5/focals

    smallest_Hbeam = source_horiz/demag*1e9
    smallest_Vbeam = source_vert/demag*1e9

    plt.figure(figsize = (12,6))
    plt.plot(energies, smallest_Hbeam, "g", label="geometry limit (H)")
    plt.plot(energies, smallest_Vbeam, label="geometry limit (V)")

    plt.axhline(focal_spot_limit(outer_zone_width)*1e9, c='r',label="diffraction/optic limit")
    plt.xlabel("Energy (keV)")
    plt.ylabel("beam_size (nm)")
    plt.legend()
    plt.show()