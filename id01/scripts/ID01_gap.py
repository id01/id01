from bliss import current_session
from bliss.config.beacon_object import BeaconObject
import numpy as np

class IDgaps(BeaconObject):

    def __init__(self,name,config):
        
        super().__init__(config,name,share_hardware=False)

    def calcGap(self,IDmne, nrj, order, gapoff=0,verbose=True):
        """
        use calibrated polynomial to calculate gaps
        """

        valid_IDs = ["u35a","u35b","u27b","u27c"]
        if valid_IDs.count(IDmne.lower()) ==0:
            print(f"{IDmne} not valid .. should be one of:",valid_IDs)
        pars = self.config.get("outputs")[IDmne]

        tab_order = pars["order"]

        equnrj = nrj* (tab_order/order)

        equnrjmin = pars["nrjmin"]        
        equnrjmax = pars["nrjmax"]

        if equnrjmin > 0 and equnrj < equnrjmin:
            print(f" Energy {nrj} keV too small for order {order}\n")
            return
        if equnrjmax > 0 and equnrj > equnrjmax:
            print(f" Energy {nrj} keV too large for order {order}\n")
            return
        
        gap = 0
        x = equnrj-pars["x0"]

        for i in reversed(np.linspace(1,pars["ppars"],pars["ppars"])):
            try: 
                value = pars["p%i"%i]
                gap*=x
                gap+=value
            except:
                pass

        if gapoff == 0:
            gapoff=pars["offset"]

        gap = gap+gapoff

        mingap = pars["gapmin"]
        maxgap = pars["gapmax"]

        if gap < mingap or gap > maxgap:
            print(f"Calculated gap {gap:.4f} is out of range ({mingap}...{maxgap})")

        if verbose:
            print(f"Calculated gap {gap:.4f}mm for harmonic {order} at {nrj} keV")
        else:
            return gap 
