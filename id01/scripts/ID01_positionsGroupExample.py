import id01.scripts.ID01_positionInstances as bl_posInst
import id01.scripts.ID01_beamlineParameters as bl_params

from bliss.config.settings import ParametersWardrobe

bl_posInst.find_redis_wardrobes()

bl_posInst.load_positions_wardrobes()

#positionsGroup_KB = bl_posInst.ID01Positions("KB")
#positionsGroup_FZP = bl_posInst.ID01Positions("FZP")

# load positions from the redis database 
 
#positionsGroup_FZP = ParametersWardrobe('stevenSim:positionsGroup:FZP')


#TODO
#a=PositionInstances(instance_name="CRL") 

# once a.parameters  <<< this is a wardrobe and all usual functionality applies
a.parameters.instances
a.parameters.switch("")


#pull groups from redis i.e. when reloading a session
load_wardrobes_positions(config.get('stevenSim')) 

# alternatively 
path = "/users/opid01/blisstests"
a.save(path)
a.parameters.to_file(path)
a.parameters.from_file(path)


### TODO
# make wardorbe functionality + switch work on the POsitionInstance - not obvious for me anyway
