from bliss import current_session, global_map, setup_globals
from bliss.common.axis import Axis
from bliss.common.standard import move
from bliss.shell.standard import umv
from bliss.shell.cli.user_dialog import UserChoice, UserYesNo
from bliss.shell.cli.pt_widgets import BlissDialog, button_dialog
from bliss.config.settings import scan
from bliss.config.settings import ParametersWardrobe
from bliss.config.static import Config

import logging
logger = logging.getLogger(__name__)

#debugon(bl_posInst.logger)
#lslog()
#bl_posInst.
logger.setLevel(logging.CRITICAL)
#bl_posInst.logger.setLevel(logging.WARNING)
#bl_posInst.logger.setLevel(logging.DEBUG)
#bl_posInst.logger.setLevel(logging.CRITICAL)
#bl_posInst.logger.setLevel(logging.ERROR)


# any ID01 scripts import are explicit! NO from ..id01.. import .... please
import id01.scripts.ID01_beamlineParameters as bl_params
import datetime
from os.path import join, exists
import pdb
import click
import numpy as np
import matplotlib.pyplot as pl

# TODO need to force offsets to be created upon spawning.

# TODO loadall - needs to work on yml files
# TODO run a test script and define a setup script
# TODO how to incorporate the hexapod reference - depends on how the hexapod reference is defined
# TODO order of motor moves if any, i.e. a problem with motors moving simultaneously. from PB

def positionsGroupMetadata(savepath = "/users/opid01/delete/", loadpath = "/users/opid01/delete/"):
	"""
	define all positionsGroup related metadata
    savepath = directory to save positionsGroups to
    loadpath = directory to load positionsGroups from    
	"""
	current_session.env_dict['positionsGroupMetadata'] = ParametersWardrobe("positionsGroup")
	positionsGroup = current_session.env_dict['positionsGroupMetadata']
	positionsGroup.add("savepath",savepath)
	positionsGroup.add("loadpath",loadpath)


def find_redis_wardrobes (session_name = current_session.name):
    """
    Find the names of all wardrobes defined in redis. 
    """
    positionsGroup_list = []
    for item in scan(f"parameters:{session_name}:positionsGroup*"):
        if len(item.split(":"))==4:
            #print(f"found a group to intialise: {item.split(':')[3]}")
            if positionsGroup_list.count(item.split(':')[3])==0:
                # add the positionsGroupget
                try:
                    cmd = f"{session_name}:positionsGroup:{item.split(':')[3]}"
                    #print(cmd)
                    logger.info(cmd)
                    positionsGroup_list.append(item.split(':')[3])
                except Exception:
                    #print("doh!mmage could not add this group from redis: {item}")
                    logger.info("doh!mmage could not add this group from redis: {item}")
    logger.info("positionsGroups spawned from redis :")             
    logger.info(positionsGroup_list) 
    return positionsGroup_list

def load_positions_wardrobes (session_name = current_session.name): 
    """
    Spawn all wardrobes defined in redis as ID01positions objects (inherents from ParametersWardrobe).
    You can now call all the usual wardrobe functionality
    """
    positionsGroup_list = find_redis_wardrobes (session_name)
    #print(positionsGroup_list)
    for item in positionsGroup_list:
        try:
            current_session.env_dict[f'positionsGroup_{item}'] = ID01Positions(item)
            logger.info(f" ... intialised : {item}")
        except Exception:
            logger.info("doh!mmage could not add this group from redis{item}")
                    
    return positionsGroup_list     
    
def positionsGroup_saveall (savepath = "./", file_name = "", prefix = '', suffix = ""):
    """
    Save all positions defined in redis, 
    user defined filename or 
    timestamped prefix + instance_name + suffix.yml
    """
    positionsGroup_list = find_redis_wardrobes (current_session.name)
    prefix = datetime.datetime.now().strftime('%Y%m%d_')
    for item in positionsGroup_list:
        try:
            PG_instance = current_session.env_dict[f'positionsGroup_{item}'] 
            PG_instance.save_instances(savepath, file_name, prefix, suffix)
        except Exception:
            logger.info(f"doh!mmage could not add '{item}' group from redis")
                    

def positionsGroup_loadyml (fns):
    """
    Load all positions defined in a yml file,
    
    TODO
    """
    positionsGroup_list = find_redis_wardrobes (session_name)
    for item in positionsGroup_list:
        try:
            PG_instance = current_session.env_dict[f'positionsGroup_{item}'] 
            PG_instance.load_instances(loadpath, file_name)
        except Exception:
            logger.info("doh!mmage could not add this group from redis{item}")
                       
    
def positionsGroup_showall (session_name = current_session.name):
    """
    show all positionsGroup defined in redis
    """
    positionsGroup_list = find_redis_wardrobes (session_name)

    for item in positionsGroup_list:
        try:
            logger.info(f"###########################################")
            logger.info(f"#####      positionsGroup_{item}     ")
            logger.info(f"###########################################")

            current_session.env_dict[f'positionsGroup_{item}'].show()
        except Exception:
            logger.info("doh!mmage could not show this group {item}")
                    
    

def positionsGroup_add(instance_name, *axis_list):
    """
    Add a positionsGroup_{instance_name} to the current session 
    """

    if f'positionsGroup_{instance_name}' in current_session.env_dict:
        dlg = UserYesNo(label=f"positionsGroup_{instance_name} exists, overwrite? Yes/No?")
        #display(dlg, title='wardrobe manager')
        if dlg:
            current_session.env_dict[f'positionsGroup_{instance_name}'] = ID01Positions(instance_name,*axis_list)
        
    else:
        logger.info(f"{instance_name} does not exist ... spawning")
        current_session.env_dict[f'positionsGroup_{instance_name}'] = ID01Positions(instance_name,*axis_list)
        logger.info( f" ... initialised object : positionsGroup_{instance_name}  ")


class ID01Positions(bl_params.ID01Parameters):
    """
    Class to define all ID01 saved motor group positions. (borrowed ideas from ID26)
    
    You will create a ParametersWardrobe object which will act as the container for a series of positions.
    


    Usage:

    - **positionsGroup_{instance_name} = ID01Positions ('instance_name' , mot1, mot2, mot3 ...)**
      loads as 'positionsGroup_instance' session object any previously initialised Positions instances with the name 'positionsGroup_instance', or create it if it is the first time it is instantiated.
       - if a list of axes is passed as arguments, slots are prepared for that set of axes in the redis database. 
       - if no axis is passed, slots are prepared for all the motors of the session.
    
    - **positionsGroup_{instance_name}.switch (<position_name>)**
  
    - **positionsGroup_{instance_name}.set_pos (<axis list>)**
      stores in 'positionsGroup_{instance_name}' object (attached to 'positionsGroup_{instance_name}' in redis) the current positions of the axis in the set. 
    
    - **positionsGroup_{instance_name}.set_pos_manual (<axis list>)**
      launches a dialog window to enter positions to be stored in 'positionsGroup_{instance_name}'. Position can be None 
    
    - **positionsGroup_{instance_name}.move (mot0, mot1)**
      move all axes listed to the position that was stored (if any) for them in 'positionsGroup_{instance_name}'
          Note: the offsets instance, can be toggled positionsGroup_{instance_name}.offset_on()/off(), 
                 no trace when saving to file, use wisely.
                  
    - **positionsGroup_{instance_name}.move_all ()**
      move all axes of the set to the position that was stored (if any) for them in 'positionsGroup_{instance_name}'. 
          Note: the offsets instance, can be toggled positionsGroup_{instance_name}.offset_on()/off(), 
                 no trace when saving to file, use wisely.
                 
    - **positionsGroup_{instance_name}.save/.load (directory, suffix='')**
      stores positions read from a previously saved file (see .save() below)
      - directory (path) must be specified
      - a suffix can be given, the filename is : {session}_positionsGroup_{suffix}.yml 

    The following functions act on all the position instances existing in redis database. The behaviour will be the same whatever is the session object calling it.
    
    - **positionsGroup_{instance_name}.show (invert=True)**
      shows all positions sets stored among their different existing instances in redis database. It might happen that some instances are no longer useful. **note that this is the user responsibility to clear unused instances** to free redis memory with the next command.
    
    - **positionsGroup_{instance_name}.instance_add/remove ('wanted/unwanted_instance_name')**
      add/remove specified instance from the database.

    - **positionsGroup_{instance_name}.axis_add/remove  (mot3, mot4, mot5)**
      add some axis slots. do not forget to assign them some positions after. (.set_pos_manual). 

    Example:

    positionsGroup_Sample = bl_posInst.ID01Positions("Sample")
    positionsGroup_Sample.show(invert=True)
    positionsGroup_Sample.instance_add("pos1")
    positionsGroup_Sample.set_pos_manual()
    positionsGroup_Sample.move_all()
    positionsGroup_Sample.instance_add("pos2")
    positionsGroup_Sample.instance_add("pos3")
    positionsGroup_Sample.instance_remove("pos1")
    positionsGroup_Sample.switch("pos2")
    bl_posInst.positionsGroupMetadata(savepath = "/users/opid01/delete/", loadpath = "/users/opid01/delete/")
    bl_posInst.find_redis_wardrobes()
    bl_posInst.load_positions_wardrobes()
    bl_posInst.positionsGroup_saveall()
    bl_posInst.positionsGroup_showall()
    
    """
    def __init__(self, instance_name, *axis_list):
        """
        instance_name = name of the group of motors
        *axis_list = list of motors to add to the group
        """
        #param_name = current_session.name + "_positions" 
        #tweak for definig multiple groups in a session
        param_name = current_session.name + ":positionsGroup:" + instance_name
        
        positions_dict = {}
        self.axis_objdict = {}
        self.axis_objlist = []
        self.applyoffsets=True

        # workaround for loading just the axes in the list in redis
        if len(axis_list) is 0:
            try:
                tmppars = bl_params.ID01Parameters(param_name)  # this doesnt work yet
                tmp_axis_list = list(tmppars.parameters.to_dict().keys())
                axis_list=[]
                session_axis_list = list(global_map.get_axes_iter())
                for axis in tmp_axis_list: # check if exists in session
                    for session_axis in session_axis_list:
                        if session_axis.name == axis:
                            axis_list.append(session_axis)
            except Exception:
                axis_list = list(global_map.get_axes_iter())
        
        for axis in axis_list:
            if isinstance(axis, Axis):
                positions_dict[axis.name] = None
                self.axis_objdict[axis.name] = axis
                self.axis_objlist.append(axis)
            else:
                logger.info(f"{axis} not an Axis in current session.")
                logger.info("hint: do not use string names for axis list, but objects.")

        #print(param_name,positions_dict)

        logger.info(f"initialising ...")

        super().__init__(param_name, positions_dict)

        logger.info(f"axis names: {list(map(lambda x: x, positions_dict))}")
        logger.info(f"done. {len(axis_list)} axis slots by {len(self.parameters.instances)} instances.")
        #init the offsets
        offset_dict={}
        for axis in axis_list:
            offset_dict[axis.name]=0

        self.set_axis_offsets(offset_dict)
        
        # set up the hexapod pose parameter if required
        self.hexaObj_dict = dict()
        shexa1_mots = ["thx","thy","thz", "rhx", "rhy","rhz"]
        shexa2_mots = ["tobx","toby","tobz", "robx", "roby","robz"]
        for axis in axis_list:
            if shexa1_mots.count(axis.name):
                self.parameters.add("shexa1_user_coord",None)
                self.parameters.add("shexa1_object_coord",None)
                self.hexaObj_dict["shexa1"] = current_session.env_dict["shexa1"]
                break # save sone time 
        for axis in axis_list:
            if shexa2_mots.count(axis.name):
                self.parameters.add("shexa2_user_coord",None)
                self.parameters.add("shexa2_object_coord",None)
                self.hexaObj_dict["shexa2"] = current_session.env_dict["shexa2"]
                break # save sone time 
        
        self.parameters.add("last_set_pos",None)
        
    def _purge(self):
        """                                
        remove all existing instances of current bliss session saved positions from redis .
        make sure thats what you want before proceeding ...                               
        """
        self.parameters.purge()
        
    def get_param_name(self,):
        return self.param_name


    def get_axis_list(self):
        return list(self.parameters.to_dict().keys())
        
    def get_position_list(self):
        return dict(self.parameters.to_dict())
    
    def set_pos(self, query_user = True):
        """
        assign current positions - if offsets are enabled
        """
        if query_user:
            ans = click.confirm(f"Do you want to overwrite the position for '{self.parameters.current_instance}'?")
            print(ans)
            #breakpoint()
            if not ans:
                print(" ... you chose no ... exiting ...")
                return
        
        ####breakpoint()

        # update offsets
        tmp_current_instance = self.parameters.current_instance
        self.switch("offsets")
        offsets_dict = self.parameters.to_dict(export_properties=False)
        self.switch(tmp_current_instance)

        offsets_set = False
        for key, offset in offsets_dict.items():
            if offset!=0.0 and key != "last_set_pos":
                offsets_set=True

        if offsets_set and self.applyoffsets:
            print("OFFSETS are enabled and not zero!!! beware")

        #if axis_list == ():
        #    axis_list=self.get_axis_list()

        parameters_dict = self.parameters.to_dict(export_properties=True)
        ret = {}
        if len(self.axis_objlist):
            for axis in self.axis_objlist:
                if isinstance(axis, Axis) and axis.name in parameters_dict:
                    if self.applyoffsets:
                        ret[axis.name] = axis.position-offsets_dict[axis.name]
                    else:
                        ret[axis.name] = axis.position
            for hexaObj in self.hexaObj_dict.keys():
                user_coord, obj_coord = self.hexaObj_dict[hexaObj].get_origin()
                ret[hexaObj+"_user_coord"] = user_coord
                ret[hexaObj+"_object_coord"] = obj_coord
                #if axis.name.split("_")[0] in self.hexaObj_dict.keys():
                #    user_coord, obj_coord = self.hexaObj_dict[axis.name.split("_")[0]].get_origin()
                #    ret[axis.name+"user_coord"] = user_coord
                #    ret[axis.name+"object_coord"] = obj_coord
            ret["last_set_pos"] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        else:
            for name in self.parameters.to_dict(export_properties=False):
                if name in current_session.object_names:
                    obj = getattr(setup_globals, name)
                    if isinstance(obj, Axis):
                        if self.applyoffsets:
                            ret[name] = obj.position-offsets_dict[name]
                        else:
                            ret[name] = obj.position
                #elif name.split("_")[0] in self.hexaObj_dict.keys():
                #    print(name)
                #    user_coord, obj_coord = self.hexaObj_dict[name.split("_")[0]].get_origin()
                #    ret[name.split("_")[0]+"_user_coord"] = user_coord
                #    ret[name.split("_")[0]+"_object_coord"] = obj_coord
                     
        logger.info(f"updating {self.parameters.current_instance} with {ret}")

        parameters_dict.update(ret)
        self.parameters.from_dict(parameters_dict)

    def move(self, *axis_list):
        """
        move motors to stored positions
        """
        if self.parameters.current_instance == "offsets" or self.parameters.current_instance == "accuracy":
            logger.info(f"Instance ({self.parameters.current_instance}) reserved. please switch to a different instance.")
        else:
            tmp_current_instance = self.parameters.current_instance
            # update offsets
            self.switch("offsets")
            offsets_dict = self.parameters.to_dict(export_properties=False)
            self.switch(tmp_current_instance)
            
            motion = ()
            positions_dict = self.parameters.to_dict()
            for axis in axis_list:
                if (
                    isinstance(axis, Axis)
                    and axis.name in positions_dict
                    and positions_dict[axis.name] is not None
                ):
                    if self.applyoffsets:
                        motion += (axis, float(positions_dict[axis.name])+float(offsets_dict[axis.name]))
                        logger.warning(".... offsets are enabled ....")
                    else:
                        motion += (axis, float(positions_dict[axis.name]))

            if len(axis_list) is 0:
                logger.info(
                    "You must specify the list of axis you {'pix': 45.0, 'piy': 8.666702299999997, 'piz': 10.0}want to move, or use .move_all()"
                )
            else:
                if len(motion):
                    
                    # check hexapods here
                    if len(self.hexaObj_dict.keys()):
                        for key in self.hexaObj_dict.keys():
                            user_coord, obj_coord = self.hexaObj_dict[key].get_origin()
                            if (positions_dict[key+"_user_coord"] != user_coord and positions_dict[key+"_user_coord"] is not None) or (positions_dict[key+"_object_coord"] != obj_coord and positions_dict[key+"_object_coord"] is not None):
                               self.hexaObj_dict[key].set_origin(positions_dict[key+"_user_coord"],positions_dict[key+"_object_coord"])
                               for ax in self.hexaObj_dict[key].axes.keys():
                                   self.hexaObj_dict[key].axes[ax].sync_hard()
                               print(" WARNING discrepancy in hexapod reference frames: \n  now   ",positions_dict[key+"_user_coord"],positions_dict[key+"_object_coord"])
                               print(" was ...",user_coord,obj_coord)      
                                             
                    umv(*motion)
                    #move(*motion)
                else:
                    logger.info("nothing to move...")

    def move_all(self):
        """
        move all motors to stored positions
        """
        if self.parameters.current_instance == "offsets" or self.parameters.current_instance == "accuracy":
            logger.info(f"Instance ({self.parameters.current_instance}) reserved. switch to a different instance.")
        else:
            tmp_current_instance = self.parameters.current_instance
            # update offsets
            self.switch("offsets")
            offsets_dict = self.parameters.to_dict(export_properties=False)
            self.switch(tmp_current_instance)
            
            motion = ()
            positions_dict = self.parameters.to_dict(export_properties=False)
            for name in positions_dict:
                try:
                    print(name)
                    axis = current_session.config.get(name)
                    
                    if isinstance(axis,Axis):
                        if positions_dict[name] is not None: # only moves if motors are defined - otherwise it does not move
                            if self.applyoffsets:
                                motion += (axis, float(positions_dict[name])+float(offsets_dict[name]))
                                print(axis.name,float(positions_dict[name])+float(offsets_dict[name]))
                                logger.warning(".... offsets are enabled ....")
                            else:
                                motion += (axis, float(positions_dict[name]))
                                print(axis.name,float(positions_dict[name]))
                except Exception:
                    print("%s not in config .. skipping .. "%name)
            if len(motion):
                
                # check hexapods here
                if len(self.hexaObj_dict.keys()):
                    for key in self.hexaObj_dict.keys():
                        user_coord, obj_coord = self.hexaObj_dict[key].get_origin()
                        if (positions_dict[key+"_user_coord"] != user_coord and positions_dict[key+"_user_coord"] is not None) or (positions_dict[key+"_object_coord"] != obj_coord and positions_dict[key+"_object_coord"] is not None):
                            self.hexaObj_dict[key].set_origin(positions_dict[key+"_user_coord"],positions_dict[key+"_object_coord"])
                            for ax in self.hexaObj_dict[key].axes.keys():
                                self.hexaObj_dict[key].axes[ax].sync_hard()
                            print(" WARNING discrepancy in hexapod reference frames: \n        ",positions_dict[key+"_user_coord"],positions_dict[key+"_object_coord"])
                            print(" now ...",user_coord,obj_coord)                   
                
                print(motion)
                umv(*motion)            
                #move(*motion)
            else:
                logger.info("nothing to move...")

    def set_axis_position(self, axis, new_position=None):
        """
        assign a position to one particular axis (before move() call for instance)
        can also be done with setup() in an interactive way
        """
        parameters_dict = self.parameters.to_dict(export_properties=True)
        ret = {}
        if isinstance(axis, Axis) and axis.name in parameters_dict:
            if new_position is not None:
                ret[axis.name] = float(new_position)
                parameters_dict.update(ret)
                self.parameters.from_dict(parameters_dict)
            else:
                return parameters_dict[axis.name] 
        else:
            logger.info(f"{axis.name if isinstance(axis, Axis) else axis} not \
	                    an axis in the saved positions list, sorry.")
            
    def set_axis_accuracy (self, accuracy_dict = {}):
        """
        accuracies define the theoretical tolerance on position, i.e. 
        if you are at a specific position or not.
        """
        tmp_instance_name = self.parameters.current_instance
        
        if len(accuracy_dict.keys()) != len(self.axis_objdict.keys()):
            logger.info(f"accuracy_dict ({len(accuracy_dict.keys())}) is not the same as axes_list ({len(self.axis_objdict.keys())})")
            logger.info(self.axis_objdict.keys())
            for key in self.axis_objdict.keys():
                accuracy_dict[key] = 0.005
		
        self.switch("accuracy")
        self.parameters.from_dict(accuracy_dict)
        self.show()
        self.switch(tmp_instance_name)
        self.show()
    
    def get_axis_offsets (self, instance_name=None):
        """
        calculate the difference between an instance and the current motor positions
        """
        if instance_name is not None:
            tmp_instance_name = self.parameters.current_instance
            self.switch(instance_name)
        saved_pos = self.parameters.to_dict()
        offsets_dict={}
        for item in self.axis_objdict.keys():               
            axis = current_session.env_dict[item]
            offsets_dict[item] = axis.position - float(saved_pos[item])
	    
        if instance_name is not None:
            self.switch(tmp_instance_name)
        return offsets_dict

    def get_axis_offsets_between_groups (self, instance_name=None, instance_name1=None):
        """
        calculate the difference between two instances (instance1-instance)
        """

        if instance_name is None or instance_name1 is None:
            print("Please provide two valid instances")
            return

        tmp_instance_name = self.parameters.current_instance

        if instance_name is not None: 
            self.switch(instance_name)
            saved_pos = self.parameters.to_dict()
        if instance_name is not None:
            self.switch(instance_name1)
            saved_pos1 = self.parameters.to_dict()

        offsets_dict={}
        for item in self.axis_objdict.keys():               
            axis = current_session.env_dict[item]
            offsets_dict[item] = float(saved_pos1[item]) - float(saved_pos[item])
	    
        self.switch(tmp_instance_name)
        return offsets_dict

    def plot_trajectory(self,motors_list, instances_list):
        """
        for a series of instances plot the trajectory of a defined list of motors
        """
        tmp_instance_name = self.parameters.current_instance
        
        recarrdtypes = dict()
        dtypes = []
        for motor in motors_list:
            recarrdtypes[motor] = "<f8"
            dtypes.append((motor,"<f8"))

        dtypes.append(("instance_name","|S30"))
        data = np.recarray((len(instances_list),),dtype=dtypes)

        for ii,instance_name in enumerate(instances_list):
            self.switch(instance_name)
            saved_pos = self.parameters.to_dict()
            req_data = []
            for motor in motors_list:
                req_data.append(saved_pos[motor])
            
            req_data.append(instance_name)
            data[ii] = tuple(req_data)


        self.switch(tmp_instance_name)

        pl.figure(figsize=(16,8))
        for motor in motors_list:
            pl.plot(data["instance_name"],data[motor],"o-",label = motor)
        pl.legend(loc=1)
        pl.show()

        return data

    def set_axis_offsets (self, offsets_dict =  {}):
        """
        offsets define the theoretical tolerance on position, i.e. 
        if you are at a specific position or not.
        """

        if not isinstance(offsets_dict,dict):
            raise TypeError(f"Only a dictionary is allowed : you sent {offsets_dict}")
        
        tmp_instance_name = self.parameters.current_instance
        if len(offsets_dict.keys())!=len(list(self.axis_objdict.keys())):
            logger.info("offsets_dict ({len(offsets_dict.keys())}) is not the same as axes_list {len(self.axis_objdict.keys())}")
            logger.info(self.axis_objdict.keys())
            for key in self.axis_objdict.keys():
                offsets_dict[key] = 0.00

        self.switch("offsets")
        self.parameters.from_dict(offsets_dict)
        #self.show()
        self.switch(tmp_instance_name)
        #self.show()

    def offset_on(self,):
        self.applyoffsets = True
        

    def offset_off(self,):
        self.applyoffsets = False


    def axis_add(self, *axis_list):
        """
        add new axis to the set 
        """
        for axis in axis_list:
            if isinstance(axis, Axis):
                self.parameters.add(axis.name, None)
                self.axis_objdict[axis.name] = axis
                
                tmp_instance_name = self.parameters.current_instance
                self.switch("offsets")
                offsets_dict = self.parameters.to_dict()
                offsets_dict[axis.name]=0
                self.parameters.from_dict(offsets_dict)
                self.switch(tmp_instance_name)
                logger.info(f"{axis.name} added, offset=0 aded")
            else:
                logger.info(f"{axis} not an axis")
        if len(axis_list) is 0:
            logger.info("Nothing added, please specify axis you want to add")

    def axis_remove(self, *axis_list):
        """
        remove axis from the set
        """
        for axis in axis_list:
            if isinstance(axis, Axis):
                self.parameters.remove("." + axis.name)
                self.axis_objdict.pop(axis.name)
                logger.info(f"{axis.name} removed")
            else:
                logger.info(f"{axis} not an axis")
        if len(axis_list) is 0:
            logger.info(
                "Nothing removed, please specify axis you want to be removed from the instances"
            )

    def instance_remove(self, instance_name):
        """
        remove instance 
        """
        
        if instance_name in self.parameters.instances:
            self.parameters.remove(instance_name)
            logger.info(f"{instance_name} removed.")
        else:
            logger.info(f"{instance_name} unknown instance ")


    def instance_add(self, instance_name):
        """
        add instance 
        """
               
        if instance_name in self.parameters.instances:
            logger.warning(f"{instance_name} exists ... overwriting...")
            self.switch(instance_name)
            self.set_pos()
        else:
            logger.info(f"\"{instance_name}\" unknown instance ")
            self.switch(instance_name)
            self.set_pos(query_user=False)   


        logger.info(f"{instance_name} added.")


    def save_instances(self, directory, file_name = None, prefix='', suffix=''):
        """
        saves to disk a yml files with instances content
        directory must be specified
        """
    
        if self.param_name != None and len(directory)>1:
            if exists(directory):
                file_name = prefix + self.param_name + suffix +'.yml'
                file_full_path = join(directory, file_name)
                self.parameters.to_file(file_full_path,*self.parameters.instances) 
                print (f'Saving all instances of {self.param_name} in: {file_name}') 
            else:
                print (f"Path ('{directory}') doesn't exist, please try again ...")
            
    def load_instances(self, file_name,*instances):
        """
        loads from a previously saved file 
        directory must be specified
        old1.load (directory)
        loads only position for instance referenced by the calling object. (old1 in the example)  
        """
        if len(instances)==0:
            instances = self.parameters.instances
        for instance_name in instances:
            self.switch(instance_name)

            if self.param_name != None and exists(file_name):
                self.parameters.from_file(file_name,self.parameters.current_instance) 
                print (f"loading '{self.parameters.current_instance}' instance of {self.param_name}") 
            else:
                print (f"Path ('{file_name}') doesn't exist, please try again ...")


    def switch(self,instance_name=None):
        """
        Switch between instances of positionsGroup object
        """
        if instance_name != self.parameters.current_instance:
            # add test for None
            adict = self.get_position_list()
            no_keys = len(adict.keys())
            _ii=0
            for key in adict.keys():
                if adict[key] is None:
                    _ii+=1

            if _ii == no_keys:
                print("##########################")
                print(f"WARNING all positions in instance \"{self.parameters.current_instance}\" are None - maybe switch back and do a \"set_pos()\"")
                print("##########################")
                #ans = click.confirm(f"All positions are \"None\" are you sure you want to switch before setting the positions (i.e. set_pos)?")
            
            try:
                if str.isdigit(instance_name[0]):
                    print("WARNING instances must start with letters..")
                    instance_name = "pos_"+instance_name
                self.parameters.switch(instance_name)
                print(f"... instance {self.parameters.current_instance} is SELECTED...")
            except Exception:
                logger.info("doh!mmage  ({instance_name}) does not exist: ")
                logger.info(self.parameters.instances)

"""
    def switch(self,instance_name=None):
        try:
            if str.isdigit(self.instance_name[0]):
                print("WARNING instances must start with letters..")
                self.instance_name = "pos_"+self.instance_name
            self.parameters.switch(instance_name)
            print(f"... instance {instance_name} is SELECTED...")
        except:
            logger.info("doh!mmage  ({instance_name}) does not exist: ")
            logger.info(self.parameters.instances)
"""
