from bliss import current_session
import os
from id01.process.h5utils import get_scans_title_str_hdf5, openScan
from id01.process.interactive import GenericIndexTracker, cnorm
import sys

import matplotlib.pyplot as plt
import matplotlib.patches as patches
import numpy as np
import fabio
#
def reroi_scan(filename, nb):
    """
    TODO
    """
    if filename is None: 
        globals_dict = current_session.env_dict
        _SCAN_SAVING = globals_dict["SCAN_SAVING"]
        filename = _SCAN_SAVING.filename

    if nb < 0 :
        nbs = get_scans_title_str_hdf5(filename,"")
        scan_number = nbs[nb]

        filename 
        # look up last scan and check for scan type to decide on data analysis pipeline
        pass


    # look for the acc_frame image
    datadir = os.path.join(_SCAN_SAVING.get_path(),"scan"+_SCAN_SAVING.scan_number_format%scan_number)
    fns = os.listdir(datadir)

    acc_frame_found = False
    for fn in fns:
        if fn.count("accframe"):
            # load it
            edffile = fabio.open(os.path.join(datadir,fn))
            acc_frame_found = True

    if not acc_frame_found:
        # load the data from the hdf5
        print("loading hdf5 not implemented")

    # subplot the 2D image with interaction a la lima_pickroi
    data = edffile.data
    #breakpoint()
    fig = plt.figure(figsize=(12,12))
    ax = fig.add_subplot(111)
    #norm=self._norm(data[self.ind]))
    print(data.max())
    ax.imshow(data, interpolation="nearest", origin="upper")
    #annotate = PScanTracker.Annotate(ax, specsession, exit_onclick=False,
    #                                 rectangle_onclick=True)
    annotate = GenericIndexTracker(ax,
                                   norm = "log",
                                   exit_onclick = False,
                                   rectangle_onclick = True)
    plt.title("Integrated scan images")
    plt.show()  
      
    if annotate.y0==annotate.y1 or  annotate.x0==annotate.x1:
      print("..WARNING: two positions were the same")
      sys.exit()

    xpos = np.sort([annotate.x0, annotate.x1])
    ypos = np.sort([annotate.y0, annotate.y1])

    Xmin, Xmax = xpos.round().astype(int)
    Ymin, Ymax = ypos.round().astype(int)

    #print(Xmin, Xmax )
    #print(Ymin, Ymax )

    # subplot the result on the right subplot
    _scan = openScan(filename,scan_number)
    rawdata_shape = _scan.getImages().shape
    if len(rawdata_shape) == 2:
        data_newroi = _scan.getImages()[Ymin:Ymax,Xmin:Xmax].sum(axis=(0,1))
        print(f"integrated intensity was : {data_newroi}")
    if len(rawdata_shape) == 3:
        data_newroi = _scan.getImages()[:,Ymin:Ymax,Xmin:Xmax].sum(axis=(1,2))
        mot_pos = _scan.motor


        fig = plt.figure(figsize=(12,6))

        ax = plt.subplot(1,2,1)
        im = plt.imshow(data, interpolation="nearest", origin="upper", norm=cnorm(data, "log", None, None))
        cb = plt.colorbar(im)

        rect = patches.Rectangle((Xmin, Ymin), Xmax-Xmin, Ymax-Ymin, linewidth=1, edgecolor='r', facecolor='none')
        ax.add_patch(rect)


        plt.subplot(1,2,2)
        plt.plot(mot_pos,data_newroi)
        plt.xlabel(_scan.motor_name)
        plt.ylabel("ROI intensity")
        plt.show()
    else:
        print("not implemented for this scan")
    # extra  -  add pick new roi button


def spawn_acc_frame_watcher():
    """
    TODO
    spawn a 2dplot of the accumulated frame intensity
    """
    pass