# -*- coding: utf-8 -*-

# ID01 BLISS git repo
#       authors:
#         Steven Leake, leake@esrf.fr
#
#       Created at: 19. January 23:17:00 CET 2022
#
#----------------------------------------------------------------------
# Description: 
#    functions to spawn data analysis - on SLURM / other
#    output to be sent to elogbook / integrated into metadata somehow?
#
# TODO:
#   TODO write them PTYCHO
#   TODO write them BCDI
#   TODO write them KMAP 
#   TODO write them flatfield
#   TODO write them beamline frequencies
#----------------------------------------------------------------------

# build a live analysis structure
import os
import os.path as osp
from bliss import current_session
import id01.process.slurm_client as slurm

class ID01_auto_analysis():
    """
    ID01 auto analysis - provide automated routines for data reduction
    
    #try using the slurm REST https://confluence.esrf.fr/display/AAWWK/Slurm+REST from Wout
    
    targets:
    #   TODO write them PTYCHO
    #   TODO write them BCDI
    #   TODO write them KMAP  
    """
    
    def __init__(self, email = "leake@esrf.fr", ):
        
        self.email = email
        self.url = "http://slurm-api.esrf.fr:6820"  # slurm 

        self.SCAN_SAVING = current_session.env_dict["SCAN_SAVING"]
                
        self.set_analysis_dir_tree()
        
    def set_token(self, token):
        """
        set the token used for launching a batch job
        """
        self.token = token
    
    def get_token(self,):
        """
        log in to cluster-access and get yourself a token
        """
        self.token = None
        
    def set_analysis_dir_tree(self,):
        
        self.base_path = osp.join(self.SCAN_SAVING.base_path, self.SCAN_SAVING.proposal_name, self.SCAN_SAVING.beamline, "analysis") 
        if not osp.isdir(self.base_path):
            os.mkdir(self.base_path)
        
        self.auto_base_path = osp.join(self.base_path, "auto")
        if not osp.isdir(self.auto_base_path):
            os.mkdir(self.auto_base_path)

    def set_sample_dir_tree(self,scan_number):
        
        tmp_path = osp.join(self.auto_base_path, self.SCAN_SAVING.collection_name)
        if not osp.isdir(tmp_path):
            os.mkdir(tmp_path)
            
        tmp_path = osp.join(tmp_path, self.SCAN_SAVING.collection_name+"_"+self.SCAN_SAVING.dataset_name)
        if not osp.isdir(tmp_path):
            os.mkdir(tmp_path)   
            
        tmp_path = osp.join(tmp_path, self.SCAN_SAVING.dataset_name) #self.SCAN_SAVING.scan_number_format%scan_number)
        if not osp.isdir(tmp_path):
            os.mkdir(tmp_path)   
                         
        self.saving_path = tmp_path


    def analyse_BCDI_pynx(self, scan_number, pynx_script=None):
        self.set_sample_dir_tree(scan_number)
        script = self.make_SLURM_batch_script_mpi(pynx_script)
        breakpoint()
        # send the batch script to slurm
        #slurm.sync_job(self.url, self.token, script)
        slurm.async_job(self.url, self.token, script, std_out=self.saving_path, std_err=self.saving_path)

    def make_SLURM_batch_script_mpi(self,pynx_script=None):
        email_alert = self.email
        data_path = ""
        out_path = self.saving_path
        if pynx_script is None:
            pynx_script = "pynx-id01cdi.py data=/data/id01/inhouse/leake/data_analysis/20201130_BCDI_PtNi_diffBeams/60_60/concat.cxi support_threshold=0.25,0.5 rebin=1,2,2 nb_run=100 nb_run_keep=10 auto_center_resize psf=gaussian,0.5,10 detwin=True algorithm=\"ER**50,(Sup*RAAR**100)**3,(Sup*HIO**100)**3\""

        
        batch_script = "#!/bin/bash\n"
        #batch_script += "#SBATCH --nodes=2\n"
        #batch_script += "#SBATCH --ntasks=4\n"
        #batch_script += "#SBATCH --partition=p9gpu\n"
        #batch_script += "#SBATCH --gres=gpu:2\n"
        #batch_script += "#SBATCH --time=20\n"
        #batch_script += "#SBATCH --cpus-per-task=8\n"
        #batch_script += "#SBATCH --mem-per-cpu=4G\n"
        #batch_script += "#SBATCH --output=%x-%j.out\n"

        #batch_script += "###SBATCH --export=NONE\n"

        batch_script += "# The following is only useful for some weird 'cannot allocate memory in static TLS block' error\n"
        batch_script += "# If you need your own PATH, then remove the --export=NONE and try without the next two lines\n"
        batch_script += "export LD_PRELOAD=/lib/powerpc64le-linux-gnu/libgomp.so.1\n"
        batch_script += "export PATH=/bin:/usr/bin:/sbin:/usr/sbin:/usr/local/sbin:/usr/local/bin:/opt/slurm/bin:/opt/slurm/ebin\n"


        batch_script += "env | grep CUDA\n"
        batch_script += "scontrol --details show jobs $SLURM_JOBID |grep RES\n"

        batch_script += f"echo `date`  | mail -s \" SLURM JOB $SLURM_JOBID has started on scan {self.saving_path}\" {email_alert}\n"

        batch_script += f"cd {out_path}\n"
        
        batch_script += f"mpiexec  -n $SLURM_NTASKS /sware/exp/pynx/devel.p9/bin/{pynx_script} mpi=run \n"
        # -mca plm_rsh_agent rsh  # doesnt fix it
        batch_script += f"echo `date`  | mail -s \" SLURM has finished job: $SLURM_JOBID on scan {self.saving_path}!! \" {email_alert}\n"
        
        print(batch_script)
        return batch_script

    def make_SLURM_batch_script(self,pynx_script=None):
        email_alert = self.email
        data_path = ""
        out_path = self.saving_path
        if pynx_script is None:
            pynx_script = "pynx-id01cdi.py data=/data/id01/inhouse/leake/data_analysis/20201130_BCDI_PtNi_diffBeams/60_60/concat.cxi support_threshold=0.25,0.5 rebin=1,2,2 nb_run=100 nb_run_keep=10 auto_center_resize psf=gaussian,0.5,10 detwin=True algorithm=\"ER**50,(Sup*RAAR**100)**3,(Sup*HIO**100)**3\""

        
        batch_script = "#!/bin/bash\n"
        #batch_script += "#SBATCH --ntasks=1\n"
        #batch_script += "#SBATCH --partition=p9gpu\n"
        #batch_script += "#SBATCH --gres=gpu:1\n"
        #batch_script += "#SBATCH --time=20\n"
        #batch_script += "#SBATCH --mem-per-cpu=4G\n"

        batch_script += "###SBATCH --export=NONE\n"

        batch_script += "# The following is only useful for some weird 'cannot allocate memory in static TLS block' error\n"
        batch_script += "# If you need your own PATH, then remove the --export=NONE and try without the next two lines\n"
        batch_script += "#export LD_PRELOAD=/lib/powerpc64le-linux-gnu/libgomp.so.1\n"
        batch_script += "#export PATH=/bin:/usr/bin:/sbin:/usr/sbin:/usr/local/sbin:/usr/local/bin:/opt/slurm/bin:/opt/slurm/ebin\n"


        batch_script += "env | grep CUDA\n"
        batch_script += "scontrol --details show jobs $SLURM_JOBID |grep RES\n"

        batch_script += f"echo `date`  | mail -s \" SLURM JOB $SLURM_JOBID has started on scan {self.saving_path}\" {email_alert}\n"

        batch_script += f"cd {out_path}\n"
        
        batch_script += f"mpiexec -n $SLURM_NTASKS /sware/exp/pynx/devel.p9/bin/{pynx_script} mpi=run \n"

        batch_script += f"echo `date`  | mail -s \" SLURM has finished job: $SLURM_JOBID on scan {self.saving_path}!! \" {email_alert}\n"
        
        print(batch_script)
        return batch_script







# from id01.scripts.ID01_analysis import *                                                                            
# a=ID01_auto_analysis()                                                                                              
# token="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2NTAxOTI3NDksImlhdCI6MTY1MDE4OTE0OSwic3VuIjoibGVha2UifQ.vl4JljMA9DXoT4hF_Nk2YhkXeSfzBMiuMCwF5YyH3-0"
# a.set_token(token)                                                                                                  
# a.analyse_BCDI_pynx(10)  





###### FLATFIELD

##TODO - multiple detector output - example - not clear how to do it
##TODO - ask the question for overwrite rather than skipping i.e if a mistake is made 
##TODO - delete a scan from an hdf5 file cleanly - use 'del' function
##TODO - varying count rates between images
#"""

## make a flatfield of Eiger data
## generate the data
#from id01lib import id01h5
#import glob

#sample = 'align' # name of top level h5 entry
#h5file = 'align.h5' # output file
#imgdir = None#'/data/visitor/hc3211/id01/mpx/e17089/'

##speclist = glob.glob(specdir+'e16014.spec')
#specfile = '/data/id01/inhouse/IHR/EBScomm0306/%s.spec'%sample # source file
#scanno = ("29.1",) # None for all, must be tuple i.e. ("1.1",) for single scanno

#with id01h5.ID01File(h5file,'a') as h5f: # the safe way
    #s = h5f.addSample(sample)
    #s.importSpecFile(specfile,
                     #numbers=scanno,
                     #verbose=True,
                     #imgroot=imgdir,
                     #overwrite=False, # skip if scan exists
                     #compr_lvl=6)
#"""

#import id01.process.flatfield import Flatfieldv2

#import h5py as h5
#import sys
#import numpy as np

#data=h5.File('EBScomm0306.h5','r')
##mask=h5.File('/data/id01/inhouse/leake/beamReconstructions/EigerMask.h5','r')['/image_data'].value

##create a flatfield
#ff_path = ''

#ff_data0 = data['/flatfield/29.1/data_0'][:,:,:]

#"""
## remove bad frames, eiger got some bad packets
#remove=[3,25,26,44,54,55,60,66,73,76,77,90,91,92,94,96,102,116,123,131,132,136,137,142,152,157,165,180,181,189,192]

#ff_data0 = np.r_[data['/align/19.1/measurement/image_0/data'][:,:,:],\
      #data['/align/21.1/measurement/image_0/data'][:,:,:],\
      #data['/align/23.1/measurement/image_0/data'][:,:,:],\
      #data['/align/25.1/measurement/image_0/data'][:,:,:],\
      #data['/align/27.1/measurement/image_0/data'][:,:,:]]
    
#ff_data0=np.delete(ff_data0,remove,axis=0)
##ff_monitor = data[u'align/16.1/measurement/exp1'][:]
#ff_data0=np.delete(ff_data0,np.arange(0,42,1),axis=0)
#ff_data0=np.delete(ff_data0,[54,87,88,89],axis=0)

#ff_data0[ff_data0>=1000000]=0
#"""

#ff_path=''

#ff_init = Flatfieldv2(ff_data0, ff_path,detector='mpx4', tolerance=98, auto = False) #,mask=mask,auto = False)
##ff_data1 = h5.get_scan_images(h5fn,2)[:]
##ff_data2 = h5.get_scan_images(h5fn,3)[:]
##ff_data3 = h5.get_scan_images(h5fn,4)[:]
##ff_init = Flatfield(np.r_[ff_data0,ff_data1,ff_data2,ff_data3], ff_path,auto = True)

##apply a monitor
##ff_init.apply_monitor2data(ff_monitor)

#ff_init.calc_I_bar_sigma()

## catch some hot pixels / dead pixels
#ff_init.dead_pixel_mask = np.invert((np.isnan(ff_init.I_bar)) | \
						#(np.isnan(np.sum(ff_init.data_dev,axis=0))) | \
						#(ff_init.I_bar>(np.median(np.round(ff_init.I_bar))*100))) 
#ff_init.tot_mask_cxi[np.invert(ff_init.dead_pixel_mask)] += 128  # BAD PIXEL


## plot the integrated intensity in the detector as a function of image
#ff_init.apply_mask2data(np.invert(ff_init.dead_pixel_mask))

#ff_init.plot_int_det_cts(mask=ff_init.dead_pixel_mask)
#ff_init.scale_data(mask=ff_init.dead_pixel_mask)
#ff_init.plot_int_det_cts(mask=ff_init.dead_pixel_mask)

## mask_1: take only those pixels whose count rate lies within the user defined min/max count rate
##ff_init.set_I_min_max()
#print("set I min/max")

#ff_init.I_lims=[30,15000]
#ff_init.mask_1 = ((ff_init.I_bar>= ff_init.I_lims[0]) & \
                #(ff_init.I_bar <= ff_init.I_lims[1])) #& \                (ff_init.dead_pixel_mask == False)

#ff_init.plot_bad_pxl_mask(ff_init.mask_1,id='1')

#ff_init.apply_mask2data(np.invert(ff_init.mask_1))


#'''
#print("plot bad pixel mask")
## mask_2 based on acceptable counting rates
#ff_init.make_mask_2()
#print("made mask 2")
#ff_init.plot_bad_pxl_mask(ff_init.mask_2,id='2')
## mask_3 based on tolerance ~ 98%
#ff_init.mask_3 = ff_init.set_tolerance()
#print("set tolerance")
#ff_init.plot_bad_pxl_mask(ff_init.mask_3,id='3')

#ff_init.final_mask([ff_init.mask_3,ff_init.mask_2,ff_init.mask_1])
#'''
#ff_init.tot_mask=ff_init.dead_pixel_mask
#ff_init.tot_mask=ff_init.mask_1
#print("final mask")
#ff_init.plot_bad_pxl_mask(ff_init.tot_mask,id='final')

## look at the standard deviation across the detector
#ff_init.apply_mask2data(np.invert(ff_init.tot_mask))
#ff_init.plot_SD_image()
#print("plot SD image")
#ff_init.gen_ff()
#print("generate ff")
#ff_init.plot_ff()
#print("plot ff")
#ff_init.plot_rnd_ff_im()
#print("plot rnd ff im")
#ff_init.plot_worst_pixel()
#print("plot worst pixel")


##ff_init.calc_ff_ID01()
#ff_init.make_ff_h5()
#ff_init.make_ff_cxi()

##ff,ff_unc = ff_init.read_ff_h5()


