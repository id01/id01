import numpy as np
from numpy import pi
from bliss.controllers.motor import CalcController
from bliss.common.axis import Axis as AxisBase

import logging
import transformations as tfs
#logging.basicConfig(level=logging.DEBUG)
#logging.basicConfig(level=logging.WARNING)
#logging.debug('This will get logged')
#logging.warning('This will get logged')

#_log = logging.getLogger("id01.scripts.ID01_lab_frame")
#The log level can be set in the bliss session after import logging
#and defining _log as in the above command.
#_log.setLevel("DEBUG")


class Labframe(CalcController):
    """
    lab frame calc motor, works with piezo stage pix/piy

    raw calculation

    TODO - no longer valid due to coordinate system change
    """

    # todo consider motor sign in case of piezo change, currently configure for usual pix +ve outboard and piy +ve upstream
    def __init__(self,config,*args,**kwargs):
        super().__init__(config,*args,**kwargs)

            
    def calc_from_real(self, real_positions):

        real_pix = real_positions["real_pix"]
        real_piy = real_positions["real_piy"]
        real_phi = real_positions["real_phi"]
        #breakpoint()

        calc_pos_lab_x = real_pix*np.cos(real_phi*pi/180.) + real_piy*np.sin(real_phi*pi/180.)
        calc_pos_lab_y = real_piy*np.cos(real_phi*pi/180.) - real_pix*np.sin(real_phi*pi/180.)

        return {"lab_x2": calc_pos_lab_x,
                "lab_y2": calc_pos_lab_y}
    
    def calc_to_real(self, positions_dict):
        posdict_labx = positions_dict["lab_x2"]
        posdict_laby = positions_dict["lab_y2"]
        phi_axis = self._tagged["real_phi"][0]

        real_posx = posdict_labx*np.cos(phi_axis.position*pi/180.)-posdict_laby*np.sin(phi_axis.position*pi/180.)
        real_posy = posdict_laby*np.cos(phi_axis.position*pi/180.)+posdict_labx*np.sin(phi_axis.position*pi/180.)
        return {"real_pix": real_posx,"real_piy": real_posy}
            
# TODO rewrite these function as matrices to add additional rotation axes. need phi to work to test this....


class Labframe3axes(CalcController):
    """
    
    lab frame calc motor, works with piezo stage pix/piy/piz
    the lab frame is defined RH with the +ve downstream

    20230320 - tested

    """

    # todo consider motor sign in case of piezo change, currently configure for usual pix +ve outboard and piy +ve upstream
    def __init__(self,config,*args,**kwargs):
        super().__init__(config,*args,**kwargs)

            
        self.origin, self.xaxis, self.yaxis, self.zaxis = [0, 0, 0], [1, 0, 0], [0, 1, 0], [0, 0, 1]
        #self.Rx = tfs.rotation_matrix(eta, xaxis)
        #self.Ry = tfs.rotation_matrix(eta, yaxis)
        #self.Rz = tfs.rotation_matrix(phi, zaxis)

        #self.Rxz = tfs.concatenate_matrices

    def phi(self, angle):
        return tfs.rotation_matrix(-angle, self.zaxis)

    def mu(self, angle):
        return tfs.rotation_matrix(-angle, self.zaxis)

    def eta(self, angle):
        return tfs.rotation_matrix(-angle, self.yaxis)
    
    def totRot(self,angphi,angeta): #angmu,
        #return tfs.concatenate_matrices(self.mu(angmu),self.eta(angeta),self.phi(angphi))
        return tfs.concatenate_matrices(self.eta((angeta/180.)*np.pi),self.phi((angphi/180.)*np.pi))

    def calc_from_real(self, real_positions):
        #breakpoint()

        real_pix = real_positions["real_pix"]
        real_piy = real_positions["real_piy"]
        real_piz = real_positions["real_piz"]

        real_phi = real_positions["real_phi"]
        real_eta = real_positions["real_eta"]
        #breakpoint()

        Rxz = self.totRot(real_phi,real_eta)
        pos = np.array(np.matrix(f"{real_pix};{real_piy};{real_piz};0"))
        #print(f"inv: {Rxz} \n pos: {pos},combined: {Rxz@pos}")

        calc_pos_lab_x, calc_pos_lab_y, calc_pos_lab_z, qrt = Rxz@pos

        return {"lab_x": calc_pos_lab_x[0],
                "lab_y": calc_pos_lab_y[0],
                "lab_z": calc_pos_lab_z[0]}
    
    def calc_to_real(self, positions_dict):
        posdict_labx = positions_dict["lab_x"]
        posdict_laby = positions_dict["lab_y"]
        posdict_labz = positions_dict["lab_z"]

        phi_axis = self._tagged["real_phi"][0]
        eta_axis = self._tagged["real_eta"][0]

        Rxz_inv = tfs.inverse_matrix(self.totRot(phi_axis.position,eta_axis.position))
        pos = np.array(np.matrix(f"{posdict_labx};{posdict_laby};{posdict_labz};0"))

        #print(f"inv: {Rxz_inv} \n pos: {pos},combined: {Rxz_inv@pos}")


        real_posx, real_posy, real_posz, qrt = Rxz_inv@pos
        
        return {"real_pix": real_posx,"real_piy": real_posy,"real_piz": real_posz}
            