# -*- coding: utf-8 -*-

# ID01 BLISS git repo
#       authors:
#       Roberto Homs-Regojo  
#       Documentation >> Steven Leake, leake@esrf.fr
#
#       Created at: 05. Dec 16:32:00 CET 2021
#
#----------------------------------------------------------------------
# Description: 
#    functions to execute scanning x-ray diffraction microscopy
#
# TODO:
#    Seb is going to rewrite the kmap macros to resemble the PRESET style
#    used in other scan methods to define the prepare/start/stop functions
#     
#
# FUNCTIONS:
#    akmap - absolute KMAP  << TESTED 20210102
#    dkmap - relative KMAP  << TESTED 20210102
#    akmap_lut - 1D + multiple slow motors  >> UNTESTED
#----------------------------------------------------------------------

import sys
import numpy as np
import time
from bliss import current_session
import pdb

#from bliss.common.scans.scan_info import ScanInfoFactory
from bliss.scanning.scan_info import ScanInfo

import logging

_log = logging.getLogger("bliss.scripts.kmap")

SUFIX_POSITION = "_position"

from bliss.scanning.acquisition.motor import (
    MeshTrajectoryMaster,
    MotorMaster,
    VariableStepTriggerMaster,
)


from bliss.scanning.acquisition.calc import CalcChannelAcquisitionSlave
from bliss.scanning.acquisition.musst import (
    MusstAcquisitionSlave,
    MusstAcquisitionMaster,
)
from bliss.scanning.acquisition.counter import IntegratingCounterAcquisitionSlave
from bliss.scanning.acquisition.mca import McaAcquisitionSlave

from bliss.scanning.acquisition.ct2 import (
    CT2AcquisitionMaster,
    CT2CounterAcquisitionSlave,
)
from bliss.controllers.ct2.device import AcqMode as P201AcqMode
from bliss.scanning.chain import AcquisitionChain
from bliss.scanning.scan import Scan
from bliss.scanning.scan import is_zerod
from bliss.scanning.acquisition.lima import LimaAcquisitionMaster
from bliss.common.cleanup import cleanup, axis as cleanup_axis
from bliss import setup_globals
from contextlib import contextmanager
from .cscantools import *

from bliss.scanning.scan import Scan, StepScanDataWatch, DataWatchCallback

from bliss.common.measurementgroup import _get_counters_from_measurement_group
from bliss.scanning.toolbox import ChainBuilder
from bliss.controllers.lima.lima_base import Lima
from bliss.controllers.ct2.client import CT2Controller
from bliss.controllers.mca.base import TriggerMode, BaseMCA

from id01.scripts import musst_tools

# =========================================================
# =========================================================
global kmap_dict
global PRESET
kmap_dict = dict()
PRESET=[]

def setup(_kmap_dict):
    global kmap_dict
    kmap_dict = _kmap_dict
    _log.warning(f"\n... kmap_dict: [{kmap_dict}]")


def add_scan_preset(ps):
    PRESET.append(ps)
# =========================================================
# Conversion object for pi encoder position
# =========================================================

class _Musst2PiConversion:
    def __init__(self,scale,offset,channel_name=""):
        self._scale = scale
        self._offset = offset
        self._channel_name = channel_name
        
    def __call__(self,data):
        return_data =  (data * (10. / 0x7fffffff)) / self._scaling - self._offset
        print(f"Calculate {self._channel_name} channel input data {data} output {return_data}")
        return return_data

    
# =========================================================
# AKMAP
# =========================================================
def akmap(
    fast_mot,
    xmin,
    xmax,
    x_nb_points,
    slow_mot,
    ymin,
    ymax,
    y_nb_points,
    expo_time,
    latency_time=0.,
    save_flag=True,
    frames_per_file=None,
):
    """
    Two dimensional continuous scan of PI motors (two out of pix/piy/piz)
    Absolute positions.
    Data is collected on the outbound motion of the fast axis
    
    Usage:
    akmap
       <X fast motor> <Xmin> <Xmax> <X nr points>
       <Y slow motor> <Ymin> <Ymax> <Y nr lines>
       <sampling time (s)>
       
    """
    global kmap_dict
    fnId = "akamp"
    measurement = setup_globals.ACTIVE_MG
    enabled_counters_name = measurement.enabled
    _log.debug(
        f"\n... ENTRY: [{fnId}]"
        f"\n... measurement: [{measurement}]"
        f"\n... enabled_counters_name: [{enabled_counters_name}]"
    )

    chain = AcquisitionChain(parallel_prepare=True)

    top_master = MeshTrajectoryMaster(
        fast_mot, xmin, xmax, x_nb_points, slow_mot, ymin, ymax, y_nb_points, expo_time
    )
    top_master.set_event_position(fast_mot, xmin)

    musst_device = kmap_dict["musst"]
    _log.warning(f"\n... musst_device: {musst_device}")

    cams = kmap_dict.get("cam", [])

    # for now abort musst program (workaround)
    gate_width = 1e-6
    gate_width = 1e-4
    readout = 1e-4
    for cam_dict in cams:
        cam = cam_dict["device"]
        _log.debug(f"\n... cam: [{cam.name}] set gate_width for basler")
        if cam.camera_type == "Basler":
            gate_width = 100e-6
            print("--- FOUND BASLER")
            break

    musst_device.ABORT
    
    musst_device.TMRCFG="10MHZ"
    factor_scan_total_time = 1.05
    
    #scan_total_time = (expo_time + latency_time) * x_nb_points * x_nb_points
    scan_total_time = expo_time * x_nb_points * x_nb_points * factor_scan_total_time

    musst_tools.check_max_timer(musst_device, scan_total_time)
    
    c_fast, c_slow = musst_device.get_channel_by_names(fast_mot.name, slow_mot.name)
    _log.debug(
        f"\n... c_fast[{c_fast}] c_slow[{c_slow}] fast_mot[{fast_mot.name}] slow_mot[{slow_mot.name}]"
        f"\n... c_fast[{c_fast.channel_id}] c_slow[{c_slow.channel_id}] fast_mot[{fast_mot.name}] slow_mot[{slow_mot.name}]"
    )

    store_list = ["time", "raw_adc3"]
    encoder_channel = [
        "raw_%s_adc" % name
        for cid, name in sorted(
            ((c_fast.channel_id, fast_mot.name), (c_slow.channel_id, slow_mot.name))
        )
    ]
    store_list += encoder_channel + ["raw_z_adc"]

    _log.debug(
        f"\n... encoder_channel: [{encoder_channel}]"
        f"\n... store_list: [{store_list}]"
    )

    # print(f"=== store_list: {store_list}")
    # store_list: ['time', 'raw_adc3', 'raw_pix_adc', 'raw_piy_adc', 'raw_z_adc']
    # STORELIST    TIMER      ADC_3        ADC_X          ADC_Y         ADC_Z

    timer_factor = musst_device.get_timer_factor()

    _gate_width = int(np.ceil((expo_time - readout) * timer_factor))
    _sampling_period = int(np.ceil(expo_time * timer_factor))

    vars = {
        "NPOINTS_LINE": x_nb_points,
        "NLINES": y_nb_points,
        "SAMPLING_PERIOD": _sampling_period,
        "GATE_WIDTH": _gate_width,
    }
    _log.debug(f"\n... [{fnId}] > MUSST vars[{vars }]")

    musst_acq = MusstAcquisitionMaster(
        musst_device,
        program="kmap_simple.mprg",
        program_start_name="KMAP",
        # program_abort_name = 'CLEANUP',ne 369,
        vars=vars,
    )
    musst_slave = MusstAcquisitionSlave(musst_device, store_list=store_list)
    chain.add(musst_acq, musst_slave)

    chain.add(top_master, musst_acq)
    top_master_musst = musst_acq

    # Piezo position calculation
    for motor_name, source_name, musst_channel in zip(
        (fast_mot.name, slow_mot.name), encoder_channel, (c_fast, c_slow)
    ):
        _log.debug(
            f"\n... motor_name[{motor_name}] source_name[{source_name}] musst_channel[{musst_channel}]"
        )

        scaling, offset = musst_channel.switch.scaling_and_offset
        dest_name = "%s%s" % (motor_name, SUFIX_POSITION)
        conversion = _Musst2PiConversion(scaling,offset,channel_name=dest_name)
        top_master.add_external_channel(
            musst_slave,
            source_name,
            dtype=np.float,
            rename=dest_name,
            conversion=conversion,
        )

    nb_points = x_nb_points * y_nb_points
    _log.debug(
        f"\n... expo_time[{expo_time}] latency_time[{latency_time}] nb_points[{nb_points}]"
    )

    # for kmap x_nb_points = total nr of points (only "one line")
    detectors_2d, mcas = _add_detectors(
        chain,
        # top_master,
        top_master_musst,
        enabled_counters_name,
        nb_points,
        expo_time,
        latency_time,
        frames_per_file,
        # save_flag,
        x_nb_points=nb_points,
        xmap_save_spectra=True,
    )

    fast_mot_name = fast_mot.name + SUFIX_POSITION
    slow_mot_name = slow_mot.name + SUFIX_POSITION

    scan_info = ScanInfo()
    #breakpoint()
    scan_info.update({
        "title": f"akmap( {fast_mot.name}, {xmin}, {xmax}, {x_nb_points}, {slow_mot.name}, {ymin}, {ymax}, {y_nb_points}, {expo_time} )",
        "technique":{
            "dim0":x_nb_points, 
            "dim1":y_nb_points
            },
        "data_dim": 2,
        "fast_motor": fast_mot_name,
        "slow_motor": slow_mot_name,
    })

    # ======= SCATTER PLOT
    # Specify the same group for all this channels (axis or values)
    scan_info.set_channel_meta(
        #fast_mot.name,
        fast_mot_name,
        # This is the fast axis
        axis_id=0,
        # In forth direction only
        axis_kind="forth",
        # The grid have to be specified
        start=xmin,
        stop=xmax,
        axis_points=x_nb_points,
        # Optionally the full number of points can be specified
        points=x_nb_points * y_nb_points,
    )
    scan_info.set_channel_meta(
        slow_mot_name,
        #slow_mot.name,
        axis_id=1,
        axis_kind="forth",
        start=ymin,
        stop=ymax,
        axis_points=y_nb_points,
        points=x_nb_points * y_nb_points,
        
    )

    # Request a specific scatter to be displayed
    #scan_info.add_scatter_plot(x=fast_mot.name, y=slow_mot.name)
    scan_info.add_scatter_plot(x=fast_mot_name, y=slow_mot_name)
    # ======= scatter plot / end


    # set_scan_watch_callbacks(on_new_scan, on_scan_point, on_scan_end)
    display = KmapCScanDisplay(
        detectors_2d, mcas, nb_points, channel_name="musst1_master:raw_z_adc"
    )
    # display = StepScanDataWatch()

    _log.warning(f"\n... chain tree\n[{chain._tree}]")
    scan = Scan(chain, name="kmap", scan_info=scan_info, data_watch_callback=display)
    #for ps in PRESET: 
    #    scan.add_preset(ps)

    multiplexer = kmap_dict["multiplexer"]

    _log.warning(
        f"\n... multiplexer [{multiplexer.name}]"
        f"\n... musst_device [{musst_device.name}]"
        f"\n... scan.run()"
    )
    with _multiplexer(multiplexer, musst_device):
        try:
            scan.run()
        finally:
            # delete eiger file for now
            if "eiger" in enabled_counters_name:
                eiger_cam = setup_globals.eiger
                e = eiger_cam._get_proxy("Eiger")
                e.deletememoryfiles()
    return scan


# =========================================================
# DKMAP
# =========================================================
def dkmap(
    fast_mot,
    xmin,
    xmax,
    x_nb_points,
    slow_mot,
    ymin,
    ymax,
    y_nb_points,
    expo_time,
    latency_time=0.,
    save_flag=True,
    frames_per_file=None,
):
    """
    Two dimensional continuous scan of PI motors (two out of pix/piy/piz)
    Relative positions.
    Data is collected on the outbound motion of the fast axis
    
    Usage:
    akmap
       <X fast motor> <-deltaX> <+deltaX> <X nr points>
       <Y slow motor> <-deltaY> <+deltaY> <Y nr lines>
       <sampling time (s)>
       
    """
    xmin += fast_mot.position
    xmax += fast_mot.position
    ymin += slow_mot.position
    ymax += slow_mot.position
    with cleanup(fast_mot, slow_mot, restore_list=(cleanup_axis.POS,)):
        return akmap(
            fast_mot,
            xmin,
            xmax,
            x_nb_points,
            slow_mot,
            ymin,
            ymax,
            y_nb_points,
            expo_time,
            latency_time,
            save_flag,
            frames_per_file=frames_per_file,
        )


# =========================================================
# AKMAP_LUT
#
# kmap.akmap_lut(
#               fluoby,-0.04,0.04,81,
#               0.05,
#               nnz,np.linspace(-0.04,+0.04,10),
#               nnp4,np.linspace(10,20,10))
# kmap.akmap_lut(
#                fluoby,29.8,30.2,100,
#                0.005,
#                fluobx,np.linspace(24,25,10))
# kmap.akmap_lut(
#                ustry,16.0,18.0,100,
#                0.005,
#                ustrz,np.linspace(24,25,10))
# =========================================================
def akmap_lut(
    fast_mot, xmin, xmax, x_nb_points, expo_time, *slow_motors_positions, **kwargs
):
    """
    One dimensional continuous scan of PI motors (two out of pix/piy/piz)
    for any number of slow motors 1D,2D,3D....
    Data is collected on the outbound motion of the fast axis
    
    Usage:
    akmap
       <X fast motor> <Xmin> <Xmax> <X nr points>
       <Y slow motor> <Ymin> <Ymax> <Y nr lines>....
       <N slow motor> <Nmin> <Nmax> <N nr lines>
       <sampling time (s)>
       
    """
    global kmap_dict
    latency_time = kwargs.pop("latency_time", 0.)
    save_flag = kwargs.pop("save_flag", True)
    frames_per_file = kwargs.pop("frames_per_file", None)

    # get positions to move back after scan
    start_motors = [fast_mot]
    start_positions = [float(fast_mot.position)]
    for mot in slow_motors_positions[::2]:
        start_motors.append(mot)
        start_positions.append(float(mot.position))

    print("started the scan at :")
    print([x.name for x in start_motors])
    print(start_positions)

    if kwargs:
        raise ValueError("akmap_lut: keys %r are not allowed" % list(kwargs.keys()))

    measurement = setup_globals.ACTIVE_MG
    enabled_counters_name = measurement.enabled

    chain = AcquisitionChain(parallel_prepare=True)

    ICEPAP_MODE = fast_mot.controller.__class__.__name__ == "Icepap"

    if not ICEPAP_MODE:
        fast_mot.controller.output_position_gate(fast_mot, xmin, xmax)
        undershoot = 0.1  # piezo
        undershoot_start_margin = 0
    else:
        undershoot = None
        undershoot_start_margin = 10 / fast_mot.steps_per_unit

    fast_master = MotorMaster(
        fast_mot,
        xmin,
        xmax,
        x_nb_points * expo_time,
        undershoot=undershoot,
        undershoot_start_margin=undershoot_start_margin,
    )

    # *slow_motors_positions
    # ustrz, array([6.3       , 6.30102041, 6.30204082, 6.30306122, ...])

    top_master = VariableStepTriggerMaster(
        *slow_motors_positions, broadcast_len=x_nb_points
    )

    y_nb_points = top_master.npoints

    # print(f"--- x_nb_points[{x_nb_points}] y_nb_points[{y_nb_points}]")

    chain.add(top_master, fast_master)

    musst_device = kmap_dict["musst"]
    # musst_device = setup_globals.musst

    # for now abort musst program (workaround)
    gate_width = 100e-6

    _log.debug(f"\n... musst_device: {musst_device}")
    musst_device.ABORT

    # - name: umusst
    #   channels:
    #     - label: ustry
    #       type: encoder
    #       channel: 2

    musst_ch_obj_fast = musst_device.get_channel_by_name(fast_mot.name)
    # print(f"=== musst_ch_obj_fast mode[{musst_ch_obj_fast.mode_str}] value[{musst_ch_obj_fast.value}] ch[{musst_ch_obj_fast.channel_id}] status[{musst_ch_obj_fast.status_string}]")

    timer_factor = musst_device.get_timer_factor()

    if not ICEPAP_MODE:
        # ===== NOT ICEPAP MODE
        store_list = ["time", "raw_adc3"]
        source_name = "raw_%s_adc" % fast_mot.name
        store_list.append(source_name)
        store_list += ["raw_y_adc", "raw_z_adc"]

        vars = [
            {
                "NPOINTS_LINE": x_nb_points,
                "NLINES": 1,
                "SAMPLING_PERIOD": int(np.ceil(expo_time * timer_factor)),
                "GATE_WIDTH": int(np.ceil(gate_width * timer_factor)),
            }
        ]

        _log.debug(f"\n... MUSST (no icepap) vars[{vars}]")

        vars += [{}] * y_nb_points
        musst_acq = MusstAcquisitionSlave(
            musst_device,
            program="kmap_simple.mprg",
            program_start_name="KMAP",
            # program_abort_name = 'CLEANUP',
            store_list=store_list,
            vars=vars,
        )
    else:
        # ===== ICEPAP MODE
        enc_name = "enc_%s" % fast_mot.name
        enc_start = int(xmin * fast_mot.steps_per_unit)

        # _get_musst_template -> cscantools
        # template_replacement[{'$MOTOR_CHANNEL$': 'CH2', '$DATA_ALIAS$': 'ALIAS DATA1 = CH1\n', '$DATA_STORE$': 'DATA1 '}]
        # template_replacement = _get_musst_template((enc_name, musst_ch_obj_fast.channel_id), ("ch1", 1))

        template_replacement = _get_musst_template(
            (enc_name, musst_ch_obj_fast.channel_id)
        )

        # _get_musst_store_list -> cscantools
        #  store_list[['timer', 'enc_ustry']]
        store_list = _get_musst_store_list((enc_name, musst_ch_obj_fast.channel_id))

        vars = [
            {
                "POSSTART": enc_start,
                "TIMEDELTA": int((expo_time + gate_width) * timer_factor),
                "SCANMODE": 0,
                "NPULSES": x_nb_points,
                "GATEWIDTH": int(np.ceil(expo_time * timer_factor)),
            }
        ]
        _log.debug(f"\n... MUSST (icepap) vars[{vars}]")
        vars += [{}] * y_nb_points
        _log.debug(f"\n... MUSST (icepap) vars[{vars}]")
        musst_acq = MusstAcquisitionSlave(
            musst_device,
            program="contscan.mprg",
            program_start_name="CONTSCAN",
            # program_abort_name = 'CONTSCAN_CLEAN',
            store_list=store_list,
            program_template_replacement=template_replacement,
            vars=vars,
        )

    chain.add(fast_master, musst_acq)

    # Piezo position calculation
    if not ICEPAP_MODE:
        scaling, offset = musst_ch_obj_fast.switch.scaling_and_offset
        dest_name = "%s%s" % (fast_mot.name, SUFIX_POSITION)
        conversion = lambda data: (data * (10. / 0x7fffffff)) / scaling - offset
        fast_master.add_external_channel(
            musst_acq,
            source_name,
            dtype=np.float,
            rename=dest_name,
            conversion=conversion,
        )
    else:
        # synchronize the encoder
        musst_ch_obj_fast.value = fast_mot.position * fast_mot.steps_per_unit
        calc_up_counter = "%s_up" % fast_mot.name
        mean_pos = CScanMusstChanTrigCalc(
            enc_name, _step_per_unit(fast_mot), fast_mot.name
        )
        calc_device = CalcChannelAcquisitionSlave(
            "mean_pos", (musst_acq,), mean_pos, mean_pos.acquisition_channels
        )
        chain.add(fast_master, calc_device)
        dest_name = "%s%s" % (fast_mot.name, SUFIX_POSITION)
        fast_master.add_external_channel(calc_device, calc_up_counter, rename=dest_name)

    # Timer calculation

    nb_points = x_nb_points * y_nb_points

    detectors_2d, mcas = _add_detectors(
        chain,
        fast_master,
        enabled_counters_name,
        nb_points,
        expo_time,
        latency_time,
        frames_per_file,
        # save_flag,
        x_nb_points=x_nb_points,
        xmap_save_spectra=True,
    )

    fast_mot_name = fast_mot.name + SUFIX_POSITION
    slow_mot_name = "axis:" + slow_motors_positions[0].name
    ymax = max(slow_motors_positions[1])
    ymin = min(slow_motors_positions[1])

    ystart = slow_motors_positions[1][0]
    ystop = slow_motors_positions[1][-1]
    
    scan_info = ScanInfo()

    scan_info.update({
        "title": f"akmap_lut( {fast_mot.name}, {xmin}, {xmax}, {x_nb_points}, {slow_motors_positions[0].name}, {ymin}, {ymax}, {y_nb_points}, {expo_time} )",
        "technique":{
            "dim0":x_nb_points, 
            "dim1":y_nb_points
            },
        "data_dim": 2,
    })

    # ======= SCATTER PLOT
    #factory = ScanInfoFactory(scan_info)
    # Specify the same group for all this channels (axis or values)
    scan_info.set_channel_meta(
        fast_mot_name,
        # This is the fast axis
        axis_id=0,
        # In forth direction only
        axis_kind="forth",
        # The grid have to be specified
        start=xmin,
        stop=xmax,
        axis_points=x_nb_points,
        # Optionally the full number of points can be specified
        points=x_nb_points * y_nb_points,
    )
    scan_info.set_channel_meta(
        slow_mot_name,
        axis_id=1,
        axis_kind="forth",
        #start=ymin,
        #stop=ymax,
        start=ystart,
        stop=ystop,
        axis_points=y_nb_points,
        points=x_nb_points * y_nb_points,
    )

    # Request a specific scatter to be displayed
    scan_info.add_scatter_plot(x=fast_mot_name, y=slow_mot_name)
    # ======= scatter plot / end

    ## add display p201
    # print(f"=== LUT detectors_2d[{detectors_2d}]")
    # print(f"=== LUT mcas[{mcas}]")
    # print(f"=== LUT nb_points[{nb_points}]")

    display = KmapCScanDisplay(detectors_2d, mcas, nb_points, channel_name=dest_name)
    # TODO

    scan = Scan(
        chain, name="kmap_lut", scan_info=scan_info, data_watch_callback=display
    )

    multiplexer = kmap_dict["multiplexer"]

    with _multiplexer(multiplexer, musst_device):
        try:
            scan.run()
        finally:

            ## returning to strat pos
            for mot, pos in zip(start_motors, start_positions):
                mot.move(pos)

            print("moved back to:")
            print([x.name for x in start_motors])
            print(start_positions)

            # delete eiger file for now
            if "eiger" in enabled_counters_name:
                print("DELETE EIGER files")
                eiger_cam = setup_globals.eiger
                e = eiger_cam._get_proxy("Eiger")
                e.deletememoryfiles()

    return scan


# ==================================================================
# ==================================================================
def _add_detectors(
    chain,
    top_master,
    enabled_counters_name,
    nb_points,
    expo_time,
    latency_time,
    frames_per_file,
    xmap_save_spectra=False,
    x_nb_points=None,
):

    bla = (
        f"\n... add_detectors\n  chain[{chain}]\n  top_master[{top_master}]\n  enabled_counters_name[{enabled_counters_name}]"
        f"\n  nb_points[{nb_points}]\n  expo_time[{expo_time}]"
        f"\n  latency_time[{latency_time}]\n  frames_per_file[{frames_per_file}]\n  xmap_save_spectra[{xmap_save_spectra}]"
        f"\n  x_nb_points[{x_nb_points}]"
    )

    _log.debug(bla)

    measurement = setup_globals.ACTIVE_MG

    enabled_device_name = set(x.split(":")[0] for x in enabled_counters_name)

    # print(f"=== enabled_counters_name[{enabled_counters_name}]")
    # print(f"=== enabled_device_name[{enabled_device_name}]")

    # this function will add enabled detectors for a kmap like
    detectors_2d = set()
    mcas = set()

    # Introspect counters with the scanning ToolBox
    counters = _get_counters_from_measurement_group(measurement)
    builder = ChainBuilder(counters)

    # ==================================================================
    # camera
    # ==================================================================
    l_camera = kmap_dict.get("camera", [])
    if l_camera == None:
        l_camera = []
    
    cam2params = {camera_params["device"]: camera_params for camera_params in l_camera}
    for node in builder.get_nodes_by_controller_type(Lima):

        device = node.controller
        camera_params = cam2params.get(device)
        if camera_params:

            assert device.name in enabled_device_name

            _log.warning(
                f"\n... with CAMERA enabled_device_name[{device.name}] mg_name[{measurement.name}]"
            )

            extra_latency = camera_params.get("extra_latency", 0)
            saving_format = camera_params.get("saving_format", "HDF5BS")
            acq_trigger_mode = camera_params.get(
                "acq_trigger_mode", "EXTERNAL_TRIGGER_MULTI"
            )
            print(expo_time, device.proxy.latency_time, latency_time, extra_latency)
            cam_expo_time = (
                expo_time - device.proxy.latency_time - latency_time - extra_latency
            )
            device_type = device.camera_type
            acq_mode = "SINGLE"
            prepare_once = True
            start_once = True

            if frames_per_file is None:
                frames_per_file = nb_points

            ctrl_params = {
                "saving_format": saving_format,
                "saving_frame_per_file": frames_per_file,
            }

            lima_params = {
                "acq_nb_frames": nb_points,
                "acq_expo_time": cam_expo_time,
                "acq_mode": acq_mode,
                "acq_trigger_mode": acq_trigger_mode,
                "wait_frame_id": range(x_nb_points - 1, nb_points, x_nb_points),
                "prepare_once": prepare_once,
                "start_once": start_once,
            }

            node.set_parameters(acq_params=lima_params, ctrl_params=ctrl_params)

            chain.add(top_master, node)

            detectors_2d.add(device)

            _log.warning(
                f"\n... with CAMERA: LimaAcquisitionMaster()"
                f"\n... device.name[{device.name}] device.type[{device.camera_type}] saving_format[{saving_format}] frames_per_file[{frames_per_file}]"
                f"\n... nb_points[{nb_points}] cam_expo_time[{cam_expo_time}] acq_mode[{acq_mode}] acq_trigger_mode[{acq_trigger_mode}]"
                f"\n... prepare_once[{prepare_once}] start_once[{start_once}]"
            )

    # ==================================================================
    # P201
    # ==================================================================
    l_p201 = kmap_dict.get("p201", [])
    if l_p201 == None:
        l_p201 = []


    for node in builder.get_nodes_by_controller_type(CT2Controller):

        p201 = node.controller
        if not (p201 in l_p201):
            _log.warning(f"\n... p201Ctrl [{p201.name}] in MG is not in kmap_dict.get('p201') [{l_p201}]")
            continue 

        print(f"... adding p201 [{p201.name}]")

        node.set_parameters(
            acq_params={
                "npoints": nb_points,
                "acq_mode": P201AcqMode.ExtTrigMulti,
                "acq_expo_time": expo_time - 10e-6,
            }
        )

        for child_node in node.children:
            child_node.set_parameters(acq_params={"count_time": expo_time - 10e-6})

        chain.add(top_master, node)

        _log.debug(f"\n... counters[{p201.counters}]" f"\\n... [expo_time[{expo_time}]")

    # ==================================================================
    # XMAP
    # ==================================================================
    l_mca = kmap_dict.get("mca", [])
    if l_mca == None:
        l_mca = []

    for node in builder.get_nodes_by_controller_type(BaseMCA):

        mca = node.controller
        if not (mca in l_mca):
            _log.warning(f"\n... mcaCtrl [{mca.name}] in MG is not in kmap_dict.get('mca') [{l_mca}]")
            continue 

        print(f"... adding mca [{mca.name}]")

        mca_params = {}
        mca_params["npoints"] = nb_points
        mca_params["trigger_mode"] = McaAcquisitionSlave.GATE

        if not xmap_save_spectra:
            node._counters = [
                c for c in node._counters if c.name.find("spectrum") == -1
            ]

        #breakpoint()
        node.set_parameters(acq_params=mca_params)
        chain.add(top_master, node)

        mcas.add(mca)


    print(f"... PRINT BUILDER:")
    builder.print_tree(not_ready_only=False)

    print(f"... detectors [{detectors_2d}]\n... mcas [{mcas}]")

    return detectors_2d, mcas


# =========================================================
# =========================================================
@contextmanager
def _multiplexer(multiplexer, musst_device):

    fnId = "_multiplexer"
    _log.debug(f"\n... ENTRY [{fnId}]")
    musst_fs_close = kmap_dict["musst_fs_close"]
    multiplexer_sw = kmap_dict["multiplexer_sw"]

    started = time.time()
    # musst_device.putget(musst_fs_close)  # Force shutter to be closed

    # set multiplexer to the initial state
    bla = ""
    for cmd, state_1, state_2 in multiplexer_sw:
        st = state_1
        bla += f"\n... mpx.sw(1): {cmd} -> {st}"
        multiplexer.switch(cmd, st)
    # multiplexer.switch("shutter", "MUSST_GATE_BACK", synchronous=True)
    _log.debug(bla)

    try:
        _log.debug("\n... BEFORE YIELD")
        yield
        _log.debug("\n... AFTER YIELD")
    finally:
        _log.debug("\n... FINALLY")
        # set multiplexer to the ifinal state
        bla = ""
        for cmd, state_1, state_2 in multiplexer_sw:
            st = state_2
            bla += f"\n... mpx.sw(2): {cmd} -> {st}"
            multiplexer.switch(cmd, st)
        _log.debug(bla)

        # musst_device.putget(musst_fs_close)  # Force shutter to be closed
    print(("Took: %s seconds" % (time.time() - started)))
    _log.debug(f"\n... EXIT [{fnId}]")


# =========================================================
# =========================================================
def kmap_musst_configure(musst_dev):
    cfg_list = ("ENC", "CNT", "ADC +-10V", "ADC +-10V", "ADC +-10V", "ADC +-10V")
    for i in range(0, 6):
        cmd = "#CHCFG CH%d %s" % (i + 1, cfg_list[i])
        musst_dev.putget(cmd)
    print(musst_dev.INFO)


# =========================================================
# display
# =========================================================


class KmapCScanDisplay(DataWatchCallback):
    def __init__(self, detectors, mcas, nb_points, channel_name=None):
        self.nb_points = nb_points
        self.detectors = detectors
        self.mcas = mcas
        self.detector_info = dict()
        self.channel_name = channel_name
        self.last_display = ""
        self.display_all = dict()

    def on_state(self, state):
        return True

    def on_scan_end(self, scan_info):
        """
        Called at the end of the scan
        """
        print((self.last_display))
        # print("\n*** on_scan_end")
        pass

    def on_scan_new(self, scan, scan_info):
        """
        Called when scan is starting
        """
        # print("*** on_scan_new")
        pass

    def on_scan_data(self, data_events, nodes, scan_info):
        """
        Called when new data are emitted by the scan.  This method should
        raise en exception to stop the scan.  All exception will
        bubble-up exception the **StopIteration**.  This one will just
        stop the scan.
        
        This callback is called when new data is emitted.

        data_events --  a dict with Acq(Device/Master) as key and a set of signal as values
        nodes -- a dict with Acq(Device/Master) as key and the associated data node as value
        scan_info -- dictionnary which contains the current scan state
        """
        # print("\n*** on_scan_data\n")
        done = False
        # for det in self.detectors:
        # curr_image = det.proxy.last_image_ready
        # display_list.append(
        # "{0} {1}/{2}".format(det.name, curr_image + 1, self.nb_points)
        # )
        for mca in self.mcas:
            try:
                curr_point = mca._proxy.get_current_pixel()
                self.display_all[mca.name] = "{0} {1}/{2}".format(
                    mca.name, curr_point, self.nb_points
                )
                done = True
            except:
                pass

        for acq_device, events in list(data_events.items()):
            # print("\n\n+++++++++++\n",list(data_events.items()))
            data_node = nodes.get(acq_device)
            # print(f"\n===name[{data_node.name}] \n...db_name[{data_node.db_name}] \n...info[{data_node.info}] \n...type[{data_node.type}]  \n...len[{len(data_node.shape)}] \n...parent[{data_node.parent}]")
            if is_zerod(data_node):
                # print(f"... IS_ZERO self.channel_name[{self.channel_name}]  data_node.name[{data_node.name}] ")
                # print(f"... self.channel_name [{self.channel_name}]   data_node.name [{data_node.name}]")
                if self.channel_name == data_node.name:
                    point_number = len(data_node)
                    self.display_all[data_node.name] = "%s %d/%d" % (
                        data_node.name,
                        point_number,
                        self.nb_points,
                    )
                    done = True
                    # print(f"\n===name[{data_node.name}] \n...db_name[{data_node.db_name}] \n...info[{data_node.info}] \n...type[{data_node.type}] \n...parent[{data_node.parent}]")
            # else:
            # print("... IS_NOT ZERO")

            for det in self.detectors:
                det_name = f"{det.name}:image"
                if det_name == data_node.name:
                    curr_image = det.proxy.last_image_ready
                    self.display_all[
                        det_name
                    ] = f"det[{det.name}] img[{curr_image+1}/{self.nb_points}]"
                    # print(f"\n===name[{data_node.name}] \n...db_name[{data_node.db_name}] \n...info[{data_node.info}] \n...type[{data_node.type}] \n...parent[{data_node.parent}]")
                    done = True

        display = ""
        if done:
            # print(self.display_all)
            print("+++", end=" ")
            for i in self.display_all:
                display += self.display_all[i] + "   "
            print(display + "\r", end=" ")
            # print(" ")
            sys.stdout.flush()
        self.last_display = display
