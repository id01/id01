# -*- coding: utf-8 -*-

# ID01 BLISS git repo
#       authors:
#         Steven Leake, leake@esrf.fr
#
#       Created at: 4. Jul 13:42:00 CET 2022
#
#----------------------------------------------------------------------
# Description: 
#       motion hooks for ID01 motors
#       
#
# TODO:
#      TODO apply a cage of limits
#----------------------------------------------------------------------


from bliss.common.standard import SoftAxis
from bliss import current_session, setup_globals, global_map
import gevent
from bliss.common.hook import MotionHook
import click


class diff_det_arm_Hook(MotionHook):
    """diffractometer detector arm object motion hook"""

    def __init__(self, name, config):
        self.config = config
        self.name = name
        super().__init__()

    def pre_move(self, motion_list):
        self.check_collision_limits(motion_list)    

    def post_move(self, motion_list):
        pass

    def check_collision_limits(self,motion_list):
        """
        eta <=delta  with a 1 degree tolerance 
        """
        # convert to real units and make a dict
        target_pos_dict_siunits = {}
        for motion in motion_list:
            target = motion.axis.dial2user(motion.target_pos/motion.axis.steps_per_unit)
            name = motion.axis.name
            target_pos_dict_siunits[name] = target


        # do something sensible to test the collision limits

        motors = list(target_pos_dict_siunits.keys())
        eta_spos = current_session.env_dict["eta"].position
        delta_spos = current_session.env_dict["delta"].position

        if motors.count("eta") == 0 and motors.count("delta") != 0:
            # delta must be greater than eta
            if target_pos_dict_siunits["delta"] < eta_spos - self.config['tolerance']:
                raise ValueError(f"...delta must be greater than eta [{eta_spos - self.config['tolerance']}]...")

        if motors.count("delta") == 0 and motors.count("eta") != 0:
            # eta must be less than delta
            if target_pos_dict_siunits["eta"] > delta_spos + self.config['tolerance']:
                raise ValueError(f"...eta must be less than delta [{delta_spos + self.config['tolerance']}]...")

        if motors.count("eta") != 0 and motors.count("delta") != 0:
            # delta must greater than eta at all times
            if target_pos_dict_siunits["eta"] > target_pos_dict_siunits["delta"] + self.config['tolerance']:
                raise ValueError(f"...eta must be less than delta [{delta_spos + self.config['tolerance']}]...")