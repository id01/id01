# ID01 BLISS git repo
#       authors:
#         Peter Boesecke, boesecke@esrf.fr
#
#       Created at: 24. Oct 17:22:00 CET 2022
#
#----------------------------------------------------------------------
# Description:
#       class RbsValve()
#       Module for positioning the rotative beamstop (rbs) valves
#
# History:
#   2022-10-25 PB creation
#   2022-10-27 PB issafe, show, safecnt
#   2022-10-31 PB rbsvalve_dict, 
#                 because only strings are read from the config file
#                 num() and mne() accept mnemonics, integers and 
#                 integer strings
#   2022-11-02 PB positions, dump, remove and label corrected for 
#                 string numbers
#
#----------------------------------------------------------------------

import numpy as np

from bliss.config import static
from bliss.setup_globals import *
from bliss.common import session

import time

import logging
#logging.basicConfig(level=logging.DEBUG)
#logging.basicConfig(level=logging.WARNING)
#logging.debug('This will get logged')
#logging.warning('This will get logged')

_log=logging.getLogger("id01.scripts.ID01_rbsvalve")
#The log level can be set in the bliss session after import logging
#and defining _log as in the above command.
#_log.setLevel("DEBUG")

class RbsValve():

    def __init__(self,rbsvalve_dict=None):

        if rbsvalve_dict is None:
            self.default()
        else:
            self.rbsvalve_dict=rbsvalve_dict
            ###print("self.rbsvalve_dict={}".format(self.rbsvalve_dict))

        wago_name = self.rbsvalve_dict.get("wago_name")

        if wago_name is not None:
            self.control=static.get_config().get(wago_name)


    def default(self):

        self.rbsvalve_dict={} 

        #self.add(1,'None','self.control.set("bs1",1)','self.control.set("bs1",0)','self.control.get("bs1")')
        #self.add(2,'circular BS + diode','self.control.set("bs2",1)','self.control.set("bs2",0)','self.control.get("bs2")')
        #self.add(3,'rectangular BS','self.control.set("bs3",1)','self.control.set("bs3",0)','self.control.get("bs3")')
        #self.add(4,'big circular BS','self.control.set("bs4",1)','self.control.set("bs4",0)','self.control.get("bs4")')

        VALVE={}

        VALVE[1]="bs1"
        VALVE[2]="bs2"
        VALVE[3]="bs3"
        VALVE[4]="bs4"

        #VALVE["bs1"] = "1"
        mne="bs1"
        VALVE[mne] = {}
        VALVE[mne]["num"] = 1
        VALVE[mne]["label"] = None
        VALVE[mne]["cmd_in"] = 'self.control.set("bs1",1)'
        VALVE[mne]["cmd_out"] = 'self.control.set("bs1",0)'
        VALVE[mne]["cmd_position"] = 'self.control.get("bs1")'

        #VALVE["bs2"] = "2"
        mne="bs2"
        VALVE[mne] = {}
        VALVE[mne]["num"] = 2
        VALVE[mne]["label"] = 'circular BS + diode'
        VALVE[mne]["cmd_in"] = 'self.control.set("bs2",1)'
        VALVE[mne]["cmd_out"] = 'self.control.set("bs2",0)'
        VALVE[mne]["cmd_position"] = 'self.control.get("bs2")'

        mne="bs3"
        #VALVE["bs3"] = "3"
        VALVE[mne] = {}
        VALVE[mne]["num"] = 3
        VALVE[mne]["label"] = 'rectangular BS'
        VALVE[mne]["cmd_in"] = 'self.control.set("bs3",1)'
        VALVE[mne]["cmd_out"] = 'self.control.set("bs3",0)'
        VALVE[mne]["cmd_position"] = 'self.control.get("bs3")'

        mne="bs4"
        #VALVE["bs4"] = "4"
        VALVE[mne] = {}
        VALVE[mne]["num"] = 4
        VALVE[mne]["label"] = 'big circular BS'
        VALVE[mne]["cmd_in"] = 'self.control.set("bs4",1)'
        VALVE[mne]["cmd_out"] = 'self.control.set("bs4",0)'
        VALVE[mne]["cmd_position"] = 'self.control.get("bs4")'

        self.rbsvalve_dict["VALVE"]=VALVE
        self.rbsvalve_dict["wago_name"]='wcid01saxs1'


    def valverange(self):

        VALVE=self.rbsvalve_dict["VALVE"]
        keymin=None
        keymax=None
        for key in VALVE:
            try:
                key=int(key)
                if keymin is None:
                    keymin=key
                if keymax is None:
                    keymax=key

                if key<keymin:
                   keymin=key
                elif key>keymax:
                   keymax=key
            except:
                pass

        if keymax is not None:
            keymax+=1

        return(range(keymin,keymax))


    def dump(self):
        """
        Dumps the current rbsvalve setup
        _valveadd(mne,label,cmd_in,cmd_out,cmd_position)
         mne        = list_item(RBSVALVE,rbsvalvenum)        # rbsvalve_mne(rbsvalvenum)
         label      = list_getpar(RBSVALVE,mne,"label")      # rbsvalve_label(mne)
         cmd_in     = list_getpar(RBSVALVE,mne,"cmd_in")     # rbsvalve_cmdin(mne)
         cmd_out    = list_getpar(RBSVALVE,mne,"cmd_out")    # rbsvalve_cmdout(mne)
         cmd_position = list_getpar(RBSVALVE,mne,"cmd_position") # rbsvalve_cmdposition(mne)
        """

        VALVE=self.rbsvalve_dict["VALVE"]
        for num in self.valverange():
           mne = VALVE.get(num)
           if mne is None:
               mne = VALVE.get("{}".format(num))
           if num is not None:
               label = VALVE[mne]["label"]
               if label is None:
                   label="None"
               cmd_in = VALVE[mne]["cmd_in"]
               cmd_out = VALVE[mne]["cmd_out"]
               cmd_position = VALVE[mne]["cmd_position"]
               print("add({},'{}','{}','{}','{}')".format(num,label,cmd_in,cmd_out,cmd_position))


    def add(self,num,label,cmd_in,cmd_out,cmd_position):

        VALVE=self.rbsvalve_dict["VALVE"]

        mne = "bs{}".format(num)
        # better, clean VALVE[num] and VALVE[mne]
        VALVE[num]=mne
        VALVE[mne]={}
        VALVE[mne]["num"]=num
        if label == 'None': # label is a string, which can be 'None'
            VALVE[mne]["label"]=None
        else:
            VALVE[mne]["label"]=label
        VALVE[mne]["cmd_in"]=cmd_in
        VALVE[mne]["cmd_out"]=cmd_out
        VALVE[mne]["cmd_position"]=cmd_position

    def remove(self,num):
        """
        Remove valve num from the configuration
        """
        VALVE=self.rbsvalve_dict["VALVE"]

        mne = VALVE.get(num)
        if mne is None:
            num="{}".format(num)
            mne = VALVE.get(num)
        if mne is not None:
           VALVE[mne].clear()
           del(VALVE[num])


    def num(self,mne):
        """
        Returns the valve number
        mne can be a mnemonic, an integer or an integer string
        """
        VALVE=self.rbsvalve_dict["VALVE"]

        # try first if mne is an integer string
        try:
            mne=int(mne)
        except:
            pass

        num=None
        if isinstance(mne,int):

            valve=VALVE.get(mne)

            if valve is None:
               valve=VALVE.get("{}".format(mne))

            # because mne is an integer or integer string it is already the number
            if valve is not None:
                num=mne

        else:
            if mne is not None:
                valve=VALVE.get(mne)
                if valve is not None:
                    # because mne is a string the number is stored in element "num"
                    num=valve.get("num")

        return(num)

    def mne(self,num):
        """
        Returns the valve mne
        mne can be a mnemonic, an integer or an integer string
        """
        VALVE=self.rbsvalve_dict["VALVE"]

        num=self.num(num)

        mne=VALVE.get(num)
        if mne is None:
            num="{}".format(num)
            mne=VALVE.get(num)

        return(mne)
        
    def label(self,mne):
        """
        Return the label as string
        """
        VALVE=self.rbsvalve_dict["VALVE"]

        mne=self.mne(mne) # mne is in VALVE! or None
        label=None

        if mne is not None:
            label=VALVE[mne].get("label")

        if label is None:
            label='None'
        
        return(label)

    def position(self,mne):
        """
        Returns the position (position) of the valve.
        mne can be the number or the mnemonic
        """
        VALVE=self.rbsvalve_dict["VALVE"]

        mne=self.mne(mne)

        cmd_position=VALVE[mne]["cmd_position"]
        cur_position=eval(cmd_position)

        return(cur_position)


    def positions(self):
        """
        Returns the current positions of all valves in a dictionary
        """
        VALVE=self.rbsvalve_dict["VALVE"]

        POSITIONS={}
        for num in self.valverange():

            mne=VALVE.get(num)
            if mne is None:
                mne=VALVE.get("{}".format(num))
            if mne is not None:
                POSITIONS[mne]=self.position(mne)

        return(POSITIONS)


    def move(self,mne,pos):
        """
        Moves the valve to pos out|in : 0|1
        mne can be the valve mnemonic or the valve number
        If mne is None all valves are moved to pos.
        No value returned
        """

        if mne is None:
            # Move all valves to pos
            for num in self.valverange():
                self._move(num,pos)
        else:
            # Move the specified valve to pos
            self._move(mne,pos)


    def _move(self,mne,pos):
        """
        Moves the valve to pos out|in : 0|1 position
        mne can be the valve mnemonic or the valve number
        No value returned
        """
        VALVE=self.rbsvalve_dict["VALVE"]

        # Move a single valve according to pos
        mne=self.mne(mne)

        if mne is not None:
            if pos is not None:
                if pos==0:
                    # deactivate the valve
                    if "cmd_out" in VALVE[mne]:
                        cmd = VALVE[mne]["cmd_out"]
                        if cmd is not None:
                            eval(cmd)

                elif pos==1:
                    # activate the valve 
                    if "cmd_in" in VALVE[mne]:
                        cmd = VALVE[mne]["cmd_in"]
                        if cmd is not None:
                            eval(cmd)

    def moveall(self,positions):
        """
        Moves all valves to the position in the dictionary positions
        """
        if positions is None:
            positions={}
        for mne in positions:
            positions[mne]=self.move(mne,positions[mne])
        return(positions)

    def issafe(self,valve,mode=0):
        """
        Checks, whether valve is a valid parameter.
        If mode is 0 and if valve describes a valid beamstop with a valid label,
        i.e. not 0, None, 'None', '' or 'empty', the returned value is 1, otherwise 0
        If mode is 1 its label is not checked.
        """
        VALVE=self.rbsvalve_dict["VALVE"]

        is_valid=False

        try:
            # check for integer number
            ibs=int(valve)
            if ibs in self.valverange():
                # when reading from config files integers are converted to strings
                if ibs in VALVE:
                    mne=VALVE[ibs]
                else:
                    mne=VALVE["{}".format(ibs)]
                is_valid=True
        except:
            if valve in VALVE:
                mne=valve
                is_valid=True

        if is_valid and mode==0:
            label=VALVE[mne].get('label')
            if label is not None:
                if isinstance(label,str):
                    if len(label)>1 and label!='None' and label.upper()!="EMPTY" and label!="0":
                        is_valid=True
                else:
                    is_valid=False
            else:
                is_valid=False

        return(is_valid)


    def safecnt(self,positions=None,mode=0):
        """
        Counts the number of non-empty ("safe") beamstops in positions
        """

        if positions is None:
            positions=self.positions()

        # count the number of inserted and safe beamstops
        cnt=0
        for key in positions:
            #  if inserted ...
            if self.issafe(key,mode) and positions[key]==1:
                cnt+=1

        return(cnt)


    def show(self,prvalpos=0,positions=None):
        """
        prvalpos 0: print labels of available beamstops
        prvalpos 1: print labels of available beamstops and valve positions
        """
        print("  valve position: ")

        if positions is None:
            positions=self.positions()

        for mne in positions:
            if prvalpos:
                pos=positions[mne]
                if pos<0:
                    position_string="(unknown)"
                elif pos==1:
                    position_string="On"
                elif pos==0:
                    position_string="Off"
                else:
                    position_string="On?"
            else:
                position_string=" "

            print("    valve {:7} : {:25} {:}".format(mne,self.label(mne),position_string))
        

