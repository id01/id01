# -*- coding: utf-8 -*-
# ID01 BLISS git repo
#       authors:
#         Peter Boesecke, boesecke@esrf.fr
#
#       Created at: 26. Sep 18:36:00 CET 2022
#
#----------------------------------------------------------------------
# Description:
#       class Sxrot(CalcController)
#       
#   Sxrot defines a pseudo motor using two rotational encoders. 
#   The active encoder is the one with changing positions
#
# History:
#   2022-09-26 PB Sxrot(CalcController)
#   2022-10-04 SP assuring atomic encoder reading in
#                 /home/blissadm/local/id01/id01/controllers/nd287.py
#   2022-10-10 PB radarstatus, savetoyml
#   2022-10-11 PB limitswitchstatus
#   2023-01-17 PB class Axis -> CalcAxis
#
#----------------------------------------------------------------------

import numpy as np
from bliss.controllers.motor import CalcController
from bliss.common.axis import CalcAxis as AxisBase
from bliss.config import static

import copy

import logging
#from bliss.common.logtools import *
#from bliss.common.logtools import disable_user_output

#logging.basicConfig(level=logging.DEBUG)
#logging.basicConfig(level=logging.WARNING)
#logging.debug('This will get logged')
#logging.warning('This will get logged')

_log = logging.getLogger("id01.scripts.ID01_sxrot")
#The log level can be set in the bliss session after import logging
#and defining _log as in the above command.
#_log.setLevel("DEBUG")

class Sxrot(CalcController):
    """
    Moving motor arc [mm] in degrees
    """
    def __init__(self,config,*args,**kwargs):
        super().__init__(config,*args,**kwargs)

        self._config=config

        Params={}
        Params["ARCTAG"]='arc'             # [mm] arc motor tag
        Params["ROTTAG"]='rot'             # [deg] rot motor tag
        self.Params=Params

        self.ENC_PREVIOUS=None

        self.EPS=1e-32

        self.nd287=static.get_config().get('nd287')
        self.radar=static.get_config().get('wcid01saxs1')

        # do not cache _set_position
        ##self.axis_settings.persistent_setting['_set_position'] = False

        if 'sxrot_dict' in config:
            self.sxrot_dict=config['sxrot_dict']
        else:
            self.default()
            self.savetoyml()

        # save current encoder positions
        self.save_encoders()

        self.ACTIVE='X2' # currently used encoder (X2 is default)

    def default(self):
        '''
        Set sxrot motor and encoder parameters to default values
        '''

        # The encoder and motor configurations must be specified for each rotation center ("X1" or "X2") in sxrot_dict.
        # Info: motor limits can be set to inifinity with motor.limits=(float("-inf"),float("inf"))
        # The movement of the motor ('sxarc') must be calibrated in millimeters:
        #     sxarc.steps_per_unit=15.835 
        #     sxarc.sign=1 (cw)
        self.sxrot_dict={}

        #X1
        Params={}
        Params["motor"]="sxarc"                 # motor is just a name, the motor is selected  by its tag
        Params["motor_radius"]=6269.5           # [mm] distance of motor wheel from rotation center in mm
        Params["sign"] = 1                      # [deg]
        Params["offset"] = 0                    # [deg]
        Params["slope"] = 17.92309891           #  
        Params["enc_raw_cmd"] = "self.nd287.read_pos(1)"
        Params["enc_raw_homeswitch"] = 102.759  # [mm]
        Params["enc_raw_changerotationaxis"] = Params["enc_raw_homeswitch"]
        Params["enc_radius"] = 349.8            # [mm] distance of encoder from rotation center in mm

        # encoder positioning limits
        Params["enc_raw_min"] = 95.428          # minimum raw position limit
        Params["enc_raw_max"] = 660.7           # maximum raw position limit
        Params["enc_resolution"] = 0.02         # maximum raw position limit

        self.sxrot_dict["X1"]=Params

        #X2
        Params={}
        Params["motor"]="sxarc"
        Params["motor_radius"]=8319.5           # [mm] distance of motor wheel from rotation center in mm
        Params["sign"] = -1                     # [deg]
        Params["offset"] = 0                    # [deg]
        Params["slope"] = -2.69675850           # 
        Params["enc_raw_cmd"] = "self.nd287.read_pos(2)"
        Params["enc_raw_homeswitch"] = 6094.282 # [mm]
        Params["enc_raw_changerotationaxis"] = Params["enc_raw_homeswitch"]
        Params["enc_radius"] = 3085             # [mm] distance of encoder from rotation center in mm

        # encoder positioning limits
        Params["enc_raw_min"] = 990             # minimum raw position limit
        Params["enc_raw_max"] = 6133.3          # maximum raw position limit
        Params["enc_resolution"] = 0.02         # resolution limit

        self.sxrot_dict["X2"]=Params


    def save_encoders(self):
        """ save current encoder positions """

        # save current encoder positions
        self.ENC_PREVIOUS={}               # previous encoder reading
        for center in self.sxrot_dict:
            enc_raw = eval(self.sxrot_dict[center]["enc_raw_cmd"])
            self.ENC_PREVIOUS[center]=enc_raw

        self.ACTIVE='X2'                   # currently used encoder

    def if_OK_encoder(self,value,center):
        """
        Returns True, if minimum <= value <= maximum, otherwise False
        """
        minimum = self.sxrot_dict[center]["enc_raw_min"]
        maximum = self.sxrot_dict[center]["enc_raw_max"]
        
        if ( minimum <= value ) and ( value <= maximum ):
          return(True)
        else:
          Return(False)

    def detect_changing_encoders(self):
        """
        Returns the encoders that differ from ENC_PREVIOUS.
        """
        changing={}
        if not self.ENC_PREVIOUS is None:
            enc_previous=copy.deepcopy(self.ENC_PREVIOUS)
            for center in enc_previous:
                # sometimes timeout error occur here
                # they are not important and ignored
                try:
                    if center in self.sxrot_dict:
                        enc_raw = eval(self.sxrot_dict[center]["enc_raw_cmd"])
                        enc_resolution = self.sxrot_dict[center]["enc_resolution"]
                        diff=enc_raw-enc_previous[center]
                        if abs(diff)>enc_resolution:
                            changing[center]=diff
                        
                except:
                    pass

        return(changing)

    def _radarstatus(self):
        """
        Returns the current status of the safety radars in a tuple
        (fu,fm,fd,bu,bm,bd): 1: not tripped, 0: tripped
        """

        fu=int(self.radar.get("i_fu_status"))
        fm=int(self.radar.get("i_fm_status"))
        fd=int(self.radar.get("i_fd_status"))

        bu=int(self.radar.get("i_bu_status"))
        bm=int(self.radar.get("i_bm_status"))
        bd=int(self.radar.get("i_bd_status"))

        return(fu,fm,fd,bu,bm,bd)

    def radarstatus(self,trippedonly=False):
        """
        Returns the current status of the safety radars in a string (1:OK, 0:tripped)
        """

        (fu,fm,fd,bu,bm,bd) = self._radarstatus()

        tripped_ccw = not (fu&fm&fd)
        tripped_cw  = not (bu&bm&bd)

        if (tripped_ccw):
            ccw_string = "ccw radar tripped"
        else:
            ccw_string = "ccw radar not tripped"

        if (tripped_cw):
            cw_string = "cw radar tripped"
        else:
            cw_string = "cw radar not tripped"

        if trippedonly:
            str=""
            if (tripped_ccw):
                str=str+"{}:".format(ccw_string)
                if fu!=1:
                    ##str=str+" fu={}".format(fu)
                    str=str+" fu"
                if fm!=1:
                    ##str=str+" fm={}".format(fm)
                    str=str+" fm"
                if fd!=1:
                    ##str=str+" fd={}".format(fd)
                    str=str+" fd"
            if (tripped_cw):
                str=str+"{}:".format(cw_string)
                if bu!=1:
                    ##str=str+" bu={}".format(bu)
                    str=str+" bu"
                if bm!=1:
                    ##str=str+" bm={}".format(bm)
                    str=str+" bm"
                if bd!=1:
                    ##str=str+" bd={}".format(bd)
                    str=str+" bd"

        else:
            str="{}: fu={},fm={},fd={}, {}: bu={},bm={},bd={}".format(ccw_string,fu,fm,fd,cw_string,bu,bm,bd)

        return(str)

    def _limitswitchstatus(self):
        """
        Return the current hardware status of the limit switches in a tuple
        (neg_lim_status,pos_lim_status)
        """

        ARCTAG = self.Params["ARCTAG"]

        arc_motor=self._tagged[ARCTAG][0]

        neg_lim_status=arc_motor.hw_state.LIMNEG
        pos_lim_status=arc_motor.hw_state.LIMPOS

        return(neg_lim_status,pos_lim_status)

    def limitswitchstatus(self,trippedonly=False):
        """
        Return the current hardware status of the limit switches in a string (0 is OK, otherwise tripped)
        """

        (neg_lim_status, pos_lim_status) = self._limitswitchstatus()

        if (neg_lim_status):
            neg_string = "negative limit tripped"
        else:
            neg_string = "negative limit not tripped"

        if (pos_lim_status):
            pos_string = "positive limit tripped"
        else:
            pos_string = "positive limit not tripped"

        if trippedonly:
            str=""
            if (neg_lim_status):
                str=str+"{}".format(neg_string)
            if (pos_lim_status):
                if (len(str)>0):
                    str=str+", "
                str=str+"{}".format(pos_string)
        else:
            str="{}, {}".format(neg_string,pos_string)

        if (neg_lim_status and pos_lim_status):
            str+=" --- CHECK HUTCH SEARCH ---"

        return(str)

        
    def calc_from_real(self, level1_positions):
        '''
        Calculate current rot from arc encoder
        '''
        _log.debug("  calc_from_real ({}) BEGIN".format(level1_positions))

        changing_encoders = self.detect_changing_encoders()
        number_of_changing_encoders = len(changing_encoders)
        if ( number_of_changing_encoders > 1 ):
            _log.warning(self,'WARNING: {} encoders are changing simultaneously {}'.format(number_of_changing_encoders,changing_encoders))
            pass
        elif len(changing_encoders) == 1:
            self.ACTIVE=list(changing_encoders.keys())[0]

        X = self.ACTIVE

        ARCTAG = self.Params["ARCTAG"]
        ROTTAG = self.Params["ROTTAG"]

        arc_mm = level1_positions[ARCTAG]

        enc_raw_homeswitch = self.sxrot_dict[X]["enc_raw_homeswitch"]
        enc_raw_per_deg = self.sxrot_dict[X]["enc_radius"]*np.pi/180.0  # [mm/deg] enc_radius * Pi()/180.0
        sign = self.sxrot_dict[X]["sign"]
        offset = self.sxrot_dict[X]["offset"]

        enc_raw = eval(self.sxrot_dict[X]["enc_raw_cmd"])

        #enc_dial [deg] = (enc_raw-enc_raw_homeswitch)/enc_raw_per_deg
        #enc_user [deg] = enc_dial*sign+offset
        enc_dial = (enc_raw-enc_raw_homeswitch)/enc_raw_per_deg
        enc_user = enc_dial*sign+offset
        
        rot_deg = enc_user

        calc_level2_positions={ROTTAG:rot_deg}

        # save current encoder positions
        self.save_encoders()

        _log.debug("  calc_from_real END ({})".format(calc_level2_positions))

        return calc_level2_positions

    def calc_to_real(self, level2_positions):
        '''
        Calculate arc from current rot
        '''
        _log.debug("  calc_to_real BEGIN ({})".format(level2_positions))

        X = self.ACTIVE

        ARCTAG = self.Params["ARCTAG"]
        ROTTAG = self.Params["ROTTAG"]

        rot_deg = level2_positions[ROTTAG]

        arc_motor=self._tagged[ARCTAG][0]
        current_arc = arc_motor.position

        enc_raw_homeswitch = self.sxrot_dict[X]["enc_raw_homeswitch"]
        enc_raw_per_deg = self.sxrot_dict[X]["enc_radius"]*np.pi/180.0  # [mm/deg] enc_radius * Pi()/180.0
        sign = self.sxrot_dict[X]["sign"]
        offset = self.sxrot_dict[X]["offset"]
        slope = self.sxrot_dict[X]["slope"]

        # enc_dial [deg] = (enc_user-offset)/sign
        enc_dial_dest = (rot_deg-offset)/sign

        # enc_raw_dest [mm] = enc_dial_dest*enc_raw_per_deg+enc_raw_homeswitch
        enc_raw_dest = enc_dial_dest*enc_raw_per_deg+enc_raw_homeswitch

        enc_raw = eval(self.sxrot_dict[X]["enc_raw_cmd"])

        delta_enc = enc_raw_dest - enc_raw

        delta_arc = delta_enc*slope

        arc_mm = current_arc + delta_arc

        calc_level1_positions={ARCTAG:arc_mm}

        # save current encoder positions
        self.save_encoders()

        _log.debug("  calc_to_real END ({})".format(calc_level1_positions))

        return calc_level1_positions

    def savetoyml(self):
        '''
        Save current sxrot parameters to yml file
        '''
        self._config['sxrot_dict']=self.sxrot_dict
        self._config.save()


class CalcAxis(AxisBase):
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        # calculated motor positions do not need to be buffered
        # => disable buffering
        self.settings.disable_cache("_set_position") #disable target pos
        self.settings.disable_cache("position") #disable current pos
        self.settings.disable_cache("state") #disbale the state



