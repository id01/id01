from bliss.config.beacon_object import BeaconObject
from bliss.config import settings
import click
import numpy as np

class Camview(BeaconObject):
    """
    Class instance to hold all parameters related to the camview controller
    """

    POI0 = BeaconObject.property_setting('POI0',default= (0,0))
    POI1 = BeaconObject.property_setting('POI1',default= (0,0))
    COR = BeaconObject.property_setting('COR',default= (0,0))    
                
    def __init__(self,name,config):
        """
        Initialise beacon objects 
        """
                                    
        BeaconObject.__init__(self, config,
                                    name=name,
                                    share_hardware=False)
                                    


    @BeaconObject.property(default=(0,0))
    def POI0(self):
        return self.POI0   
        
    @POI0.setter
    def POI0(self, value):
        self.POI0 = value    
    
    @BeaconObject.property(default=(0,0))
    def POI1(self):
        return self.POI1   
        
    @POI1.setter
    def POI1(self, value):
        self.POI1 = value    

    @BeaconObject.property(default=(0,0))
    def COR(self):
        return self.COR  
        
    @COR.setter
    def COR(self, value):
        self.COR = value    
