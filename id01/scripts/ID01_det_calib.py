# -*- coding: utf-8 -*-

# ID01 BLISS git repo
#       authors:
#         Steven Leake, leake@esrf.fr
#
#       Created at: 19. Feb 09:00:00 CET 2021
#
#----------------------------------------------------------------------
# Description: 
#    locate extents of 2D detector
#    calibrate detector distance ( & tilts) with a mesh scan
#
# TODO:
#    add xrayutilities tilt functionality?
#    protect a detector object?
#----------------------------------------------------------------------

import os
import sys
import time
import collections
import datetime
import click
import matplotlib.pyplot as pl

import numpy as np
#from skimage.feature import register_translation
from skimage.registration import phase_cross_correlation
from scipy.ndimage.fourier import fourier_shift
from scipy.ndimage.measurements import center_of_mass
from bliss.common.scans import ct, sct
from bliss.shell.standard import umv, umvr
from bliss.common.scans import amesh
from bliss import current_session

from bliss.config.beacon_object import BeaconObject



from id01.scripts.ID01_limamask import id01_limamask, id01_limamask_mask_pixel, id01_limamask_unmask_pixel
from id01.process.interactive import *
from id01.scripts.ID01_utils import ANSI
from id01.scripts.ID01_utils import limaroiID01_direct_beam
from id01.scripts.ID01_utils import interactive_two_theta_plot



ScanRange = collections.namedtuple("ScanRange", ['name', 'start', 'stop', 'numpoints'])

class DetectorCalibration(BeaconObject):
    """
    Detector calibration class
    
    the way the beacon object inherits is not obvious
    the 'config'

    this function will define:
    distance: sample to detector
    beam_center_x: as you see it in the flint window 
    beam_center_y: as you see it in the flint window
    """
    #beacon objects for persistent data retention i.e. after ctrl+D
    distance = BeaconObject.property_setting('distance',default=0)
    #det_dist_COM = BeaconObject.property_setting('det_dist_COM',default=0)    
    #det_dist_CC = BeaconObject.property_setting('det_dist_CC',default=0)
    beam_center_x = BeaconObject.property_setting('beam_center_x',default=0) 
    beam_center_y = BeaconObject.property_setting('beam_center_y',default=0) 
    mask = BeaconObject.property_setting('mask',default=0) 
    pxlperdeg = BeaconObject.property_setting('pxl_per_deg',default=0) 
    #pixel_mask = BeaconObject.property_setting('pixel_mask',default=0) 
    # move pixels2mask_userdefined here how does beacon deal with lists?
    # TODO other critical info
    
    
    
        
    def __init__(self, name, config, debug = False): #det_obj, 
        
        super().__init__(config, name, share_hardware=False)
        
        
        self.debug = debug
        #self.det_obj = det_obj
        self.scan_mode = None
        #self.pxlperdeg = None   ## TODO pxlperdeg not maintained after restart!!!
        #self.mask = self.pixel_mask # intialise to the previous calculated mask if not defined.
        #self.pixel_mask = 0
        
        self.det_dist_CC = 0
        self.det_dist_COM = 0
        #self.beam_center_x = 0
        #self.beam_center_y = 0
        self.pixels2mask_userdefined = []

    def _debug_exec_command(self,command):
        if self.debug:
            print(command)
        else:
            eval(command)
            
    def get_sct_images(self,exposure=1):
        """
        return an array from the desired detector ct
        """
        scan_obj = sct(exposure, self.det_obj, run=False)  # TODO make work with saving + add catches
        scan_obj.run()
        image = scan_obj.get_data()['{}:image'.format(self.det_obj.name)].as_array()

        self.last_image = image
        return self.last_image

    def get_scan_images(self, scan_obj):
        """
        return an array from the desired detector and scan
        """
        imageid = '{}:image'.format(self.det_obj.name)
        try:
            self.last_image = scan_obj.get_data()[imageid].as_array()
        except:
            print("probably raise a KeyError: {}".format(imageid))
        return self.last_image

    def get_beam_position(self,image=None, roi_name=None, goto = "com"):
        """
        Return center of mass of intensity on a 2D detector
        roi_name = use an ROI in case of high intensity elsewhere
        """
        if image is None:
            det_image = self.get_sct_images(self.det_obj)
        else:
            det_image = image

        if roi_name != None:
            roi = dict(self.det_obj.roi_counters.items())[roi_name]
            x,y,h,w = roi.x,roi.y,roi.height,roi.width
            
            det_image = det_image[y:y+w,x:x+h]
            if goto == 'com':
                com = np.array(center_of_mass(det_image))
                com = com+np.array([y,x])
            elif goto == "max":
                max = np.unravel_index(det_image.argmax(),det_image.shape)
                max = max+np.array([y,x])
        else:
            if goto =='com':
                com = np.array(center_of_mass(det_image))
            elif goto == 'max':
                max = np.unravel_index(det_image.argmax(),det_image.shape)
        
        if goto == 'com':
            return com
        elif goto == 'max':
            return max
            
    def get_shift_between_images_CC(self, det_image0, 
                                          det_image1, 
                                          upsample_factor=1000):
        """
        calculate the sub pixel shift between two images using sckit-image
        """
        cc, error, diffphase = phase_cross_correlation( det_image0, 
                                                        det_image1, 
                                                        upsample_factor = upsample_factor)
        
        if cc[0]>0:
            cc[0]=det_image0.shape[0]-cc[0]
        if cc[1]>0:
            cc[1]=det_image0.shape[1]-cc[1]        
        return cc,error,diffphase
        
    def get_shift_between_images_COM(self, det_image0, det_image1):
        """
        estimate the pixels per degree from a single motor movement
        """
        com0 = np.array(center_of_mass(det_image0))
        com1 = np.array(center_of_mass(det_image1))
        return com0-com1
        
    def get_calibration_pixperdeg(self, mot_obj=None, angle=0.1, pxlsize = 55e-6, dim=0):
        """
        estimate the pixels per degree from a single motor movement
        TODO dimension agnostic needs motor definitions to be right!
        """
        
        if mot_obj.name=="delta":
        
            com0 = self.get_beam_position()
            #command = 'umvr({},{})'.format(mot_obj.name,angle)
            #self._debug_exec_command(command)
            umvr(mot_obj,angle)
            com1 = self.get_beam_position()
            
            pxlperdeg = abs((com0[dim]-com1[dim])/angle)  # TODO  check directions
            self.pxlperdeg = pxlperdeg
            return pxlperdeg, self.get_detector_distance(pxlperdeg, pxlsize)
        else:
            print("unknown motor - use delta until further notice")
        
    def get_detector_distance(self,pxlperdeg, pxlsize):
        #np.array((det_obj.image.height, det_obj.image.width))
        return (pxlperdeg*pxlsize)/np.tan(np.deg2rad(1.0))
        
    def pxl2motpos(self, trgt_pxl, pxlperdeg, ref_pxl=[0,0], ref_ang=[0,0]):
        '''
            set the origin of the detector at 0,0 get the angles
            find all positions relative to this
            ref_ang = (delta, nu)
        '''
        trgt_pxl = np.array(trgt_pxl)
        ref_pxl  = np.array(ref_pxl)
        ref_ang  = np.array(ref_ang)
        pxlperdeg = np.array(pxlperdeg)

        diffpxl = trgt_pxl - ref_pxl
        trgt_ang = ref_ang + diffpxl/pxlperdeg
        #print(diffpxl,trgt_ang,ref_ang,diffpxl/pxlperdeg)
        return trgt_ang
        
    def get_detector_calibration_scan_limits(self,  mot_objs = (None,None), 
                                                    no_pixels=np.array([50,50]), 
                                                    pxlperdeg=np.array([0,0])):
        """
        find limits in pixels and motor units of detector based on a border defined 
        by a number of pixels from the edge of the detector in each axis 
        """

        detsize_pxl = np.array((self.det_obj.image.height, self.det_obj.image.width))
        
        # always check the beam is on the detector, in case of user error
        com_beam_pxl = self.get_beam_position()
        com_beam_mot = np.array([0,0])
        if mot_objs[0] is None:
            com_beam_mot[1] = mot_objs[1].position
        elif mot_objs[1] is None:
            com_beam_mot[0] = mot_objs[0].position
        else:
            com_beam_mot = np.array([mot_objs[0].position,mot_objs[1].position])
        
        detsize_mot = (detsize_pxl)/pxlperdeg
        scan_TL_pxl = no_pixels
        scan_BR_pxl = detsize_pxl-no_pixels
        scan_TL_mot = self.pxl2motpos(scan_TL_pxl,pxlperdeg,com_beam_pxl,com_beam_mot)
        scan_BR_mot = self.pxl2motpos(scan_BR_pxl,pxlperdeg,com_beam_pxl,com_beam_mot)
        
        return scan_TL_pxl,scan_BR_pxl,scan_TL_mot,scan_BR_mot
        
    def get_calibration_scan_type(self, mot_objs = (None,None), 
                                        standard_mot_limits = ([-2,2],[-2,2]),
                                        no_pixels = np.array([50,50]),
                                        pxlperdeg = np.array([0,0]),
                                        intervals = [4,4],
                                        exposure = 0.25 ):
        """
        based on the motor limits see what scans are possible for calibration
        returns mode = 0/1/2 (mesh/single motor/other single motor) in case of collisions

        """
        
        # combine limits
        if mot_objs[0] is not None:
            mot0_lims = np.array(mot_objs[0].limits)
        else:
            mot0_lims = np.array([0,0])
  
        """
        if mot0_lims[0] < standard_mot_limits[0][0]:
            mot0_lims[0] = standard_mot_limits[0][0]  

        if mot0_lims[1] > standard_mot_limits[0][1]:
            mot0_lims[1] = standard_mot_limits[0][1]  
        """
                  
        if mot_objs[1] is not None:
            mot1_lims = np.array(mot_objs[1].limits)
        else:
            mot1_lims = np.array([0,0])    
        """
        if mot1_lims[0] < standard_mot_limits[1][0]:
            mot1_lims[0]=standard_mot_limits[1][0]  

        if mot1_lims[1] > standard_mot_limits[1][1]:
            mot1_lims[1] = standard_mot_limits[1][1]  
        """
            
        print(mot0_lims,mot1_lims)

        # compare to the detector field of view
        TL_pxl,BR_pxl,TL_mot,BR_mot = self.get_detector_calibration_scan_limits(mot_objs,
                                                                                no_pixels,
                                                                                pxlperdeg)
                                                                                
        print(TL_pxl,BR_pxl,TL_mot,BR_mot)
        
        #combine limits of detector field of view
        # mapping here - be careful
        
        if mot0_lims[0] < TL_mot[0]:
            mot0_lims[0] =  TL_mot[0]
        if mot1_lims[1] > BR_mot[1]:
            mot1_lims[1] =  BR_mot[1]   
        if mot0_lims[1] > BR_mot[0]:
            mot0_lims[1] =  BR_mot[0]
        if mot1_lims[0] < TL_mot[1]:
            mot1_lims[0] =  TL_mot[1]  
        
                                
        scan_start = np.array([mot0_lims[0],mot1_lims[0]])
        scan_end = np.array([mot0_lims[1],mot1_lims[1]])
        print("pixels range: ",TL_pxl,BR_pxl)
        print("motors range limits: m0: ",mot0_lims,"m1: ",mot1_lims)
        
        # this gets messy if you want to make it generic TODO add motor sign.
        
        if mot_objs[0]==None:
            self.scan_mode = 2 # scan motor 1 only
            command = "ascan({},{},{},{},{},{})".format("mot_objs[1]",scan_start[1],scan_end[1],intervals[1],exposure,self.det_obj.name)
        if mot_objs[1]==None:
            self.scan_mode = 1 # scan motor 2 only
            command = "ascan({},{},{},{},{},{})".format("mot_objs[0]",scan_start[0],scan_end[0],intervals[0],exposure,self.det_obj.name)
        if mot_objs[0] is not None and mot_objs[1] is not None:
            self.scan_mode = 0 # mesh is possible 
            command = "amesh({},{},{},{},{},{},{},{},{},{})".format("mot_objs[0]",scan_start[0],scan_end[0],intervals[0],"mot_objs[1]",scan_start[1],scan_end[1],intervals[1],exposure,"self.det_obj")
        delta_mot = scan_end-scan_start
        return command, delta_mot
        
        
    def run_detector_calibration(self, mot_objs = (None,None),
                                       angle = 0.1, 
                                       no_pixels = [50,50], 
                                       pixperdeg = [0,0],
                                       intervals = [3,3],
                                       exposure = 0.25, 
                                       beam_size = [10,10]):
        """
        make a rough calibration
        launch a mesh/line scan 
        """
        return_motion=[]
        
        if mot_objs[0] == None and mot_objs[1] == None:
            print("please define your motors (delta,nu)")
            exit()
        
        for obj in mot_objs:
            return_motion.append(obj)
            return_motion.append(obj.position)
            
        if self.scan_mode is None:
            print("scan_mode is not set consider get detector limits")
            exit  # TODO test this works
        pixperdeg, det_dist = self.get_calibration_pixperdeg(mot_obj=mot_objs[0],angle=angle)
        #print(pixperdeg)
        #breakpoint()
        standard_mot_limits = (mot_objs[0].limits,mot_objs[1].limits)
        command,delta_pos = self.get_calibration_scan_type( mot_objs,
                                                            no_pixels=[50,50], 
                                                            standard_mot_limits=standard_mot_limits, 
                                                            pxlperdeg=np.array([pixperdeg,pixperdeg]),
                                                            intervals = intervals,
                                                            exposure = exposure)
                                                            
        # TODO from first and last image of scan - calculate everything 
        scan_obj = eval(command) #self._debug_exec_command(command)

        if not self.debug:
            # calc the detector distance COM / CC in each case
            images = scan_obj.get_data()['image'].as_array()
            image0, image1 = images[0,:,:],images[-1,:,:]
        
            CC_shift, error, diffphase = self.get_shift_between_images_CC(image0, image1)
            COM_shift = self.get_shift_between_images_COM(image0, image1)
            
            #pos = scan_obj.get_data()['axis:{}'.format()]
            #delta_pos = start_pos-end_pos
            pxlperdegCOM = COM_shift/delta_pos 
            pxlperdegCC = CC_shift/delta_pos
            print("here")
            print(delta_pos,CC_shift,COM_shift) 
            detsize_pxl = np.array((self.det_obj.image.height, self.det_obj.image.width))

            det_dist_CC = self.get_detector_distance(pxlperdegCC,55e-6)
            det_dist_COM = self.get_detector_distance(pxlperdegCOM,55e-6)
            
            # TODO set the metadata parameters
            print("CC: %.3f \nCOM: %.3f"%(det_dist_CC[0],det_dist_COM[0]))
            
            self.det_dist_CC = det_dist_CC[0]
            self.det_dist_COM = det_dist_COM[0]
            
        
        for obj in mot_objs:
            if obj is not None:
                umv(obj,0)
            
        # finding central pixel
        COM = self.get_beam_position()
        self.beam_center_x = COM[1]
        self.beam_center_y = COM[0]   

        limaroiID01_direct_beam(self.det_obj,beam_pos = [self.beam_center_x,self.beam_center_y],beam_size = beam_size)
        
        self.timestamp = (datetime.datetime.now().isoformat())     
        
        self.set_metadata()
        # move back to start
        umv(*return_motion)
        
        
        
    def find_PoNI(self, ):
        """
        find the point of normal incidence on the detector
        """
        
        # TODO define the detector pixel size - this is metadata
        # get the detector pixel size
        # get the detector limits 
        # get the current beam position
        # and all detector motors + directions?
        # estimate if the direct beam will hit the detector or not
        
        # if within the bounds go to zero and take an image.
        # otherwise just estimate based on the base detector calibration
        #ct
        PoNI = center_of_mass(det_image)
        
        #calculate central pixel from first image i.e. when the beam was on the detector
        #cen_x = com_start[1]+_mot_mpxy_start*(1/55e-3)-_mot_nu_start*pixperdeg  # opposite sign due to detector flip
        #cen_y = com_start[0]-_mot_mpxz_start*(1/55e-3)+_mot_del_start*pixperdeg
        
        # add a calculation if the direct beam will not hit the detector at zeroed motor positions.
        return PoNI
        
    def set_metadata(self, ):
        """
        update the metadata
        """
        #try:
        #    metaObj = current_session.env_dict["metadata"+self.det_obj.name]
        #
        #except:
        #    print("metadata"+self.det_obj.name+" does not exist please check the configuration/setup for your session")
            
        self.distance = self.det_dist_CC
        #self.meta_obj.beam_center_x = self.beam_center_x
        #self.meta_obj.beam_center_y = self.beam_center_y
        self.pixel_mask = self.mask
        
        self.show_det_calibration()
        
    def move_beam_to_pixel(self, target_pixel, motObjs = (None,None), mynorm = 'log', move_motors=True, roi_name=None, goto="com"):
        """
        move the beam to a desired pixel on the detector
        """

            
        # show image of the detector and ask if this is right beam position
        #  if not select it yourself + click on it and select it
        det_image = self.get_sct_images(self.det_obj)
        current_pos = self.get_beam_position(det_image)
        
        if  motObjs[0].name=="delta" and motObjs[1].name=="nu":
            #breakpoint()
            #pxlperdeg = (self.pxlperdeg[0],self.pxlperdeg[0]*(np.cos(np.radians(motObjs[0].position))))
            pxlperdeg = (self.pxlperdeg,self.pxlperdeg*(np.cos(np.radians(motObjs[0].position))))
        else:
            pxlperdeg = self.pxlperdeg
            
        delta_angs = (target_pixel-self.get_beam_position(det_image,roi_name=roi_name,goto=goto))/pxlperdeg
        
        motObj0_pos = motObjs[0].position+delta_angs[0]
        motObj1_pos = motObjs[1].position+delta_angs[1]
        if move_motors==True:
            umv(motObjs[0],motObj0_pos, motObjs[1],motObj1_pos)
            det_image = self.get_sct_images(self.det_obj)
            
        return motObj0_pos,motObj1_pos
        
    def pick_beam_position(self, motObjs=(None,None),mynorm="log",move_motors=True):
        """
        take an image and allow the user to pick the beam position
        from a pop up window
        """
        # TODO
        # add a question to check the calibration is correct before moving?
        self.show_det_calibration()
        ans = click.confirm("\nAre you happy with the current detector calibration?")
        if ans:
            pass
        else:
            exit 
            
        # show image of the detector and ask if this is right beam position
        #  if not select it yourself + click on it and select it
        det_image = self.get_sct_images(self.det_obj)
        try:    
            fig = pl.figure(figsize=(9,6))
            ax = fig.add_subplot(111)
            #fig.suptitle(" File: %s \n Scan: %i"%(filename,nb))
            #breakpoint()
            tracker = GenericIndexTracker(ax, data = det_image, norm=mynorm,exit_onclick=True)
            ax.set_xlabel(motObjs[1].name)
            ax.set_ylabel(motObjs[0].name)
            plt.gca().invert_yaxis()
            pl.show()
            
            #TODO calculated the motor in 
            trgt = (tracker.POI_list[0][0][3],tracker.POI_list[0][0][1])
            
            trgt_angs = self.move_beam_to_pixel(trgt,(motObjs[0],motObjs[1]),move_motors=move_motors)
            
            if not move_motors:
                print(f"pixel position in angular coords ({motObjs[0]},{motObjs[1]}): ({trgt_angs[0]},{trgt_angs[1]})")
      
        except:
            print("An error occurred")
        # take an exposure to confirm
        # make a test based on the limits and ask the question?
            pixel_mask = BeaconObject.property_setting('pixel_mask',default=0) 

    def get_pixelangles(self,exposure=1):
        interactive_two_theta_plot(exposure=exposure)
        
    
    def spawn_flatfield_calibration(self,):
        """
        Make a typical flatfield scan
        return the flatfield
        """
        # import id01lib or use a virtual environment and spawn it on another computer

        # do a CT - check the average count rate

        # assume 100K is a good value (0.3% error) - estimate the timescan required for that
        #loopscan()
        pass

    def set_flatfield_calibration(self,):
        """
        Make a typical flatfield scan
        return the flatfield
        """
        #self.meta_obj.flatfield
        
        #loopscan
        pass
    
    def id01_limamask(self,):
        # TODO written in id01limamask (id01_limamask)
        self.mask = 1-id01_limamask(self.det_obj)  # inverted array i.e. 1s represent bad pixels
        #self.pixel_mask = self.mask
        #for xy in self.pixels2mask_userdefined:
        #    self.mask = id01_limamask_unmask_pixel(self.det_obj,xy[0],xy[1])
        #self.set_metadata()
        
    def id01_limamaskoff(self,):
        self.detObj.processing.use_mask = False
        
    def id01_limamaskon(self,):
        self.detObj.processing.use_mask = True   

    def id01_limamask_maskpixel(self,list_tuple_xy=[]):
        for xy in list_tuple_xy:
            self.mask = id01_limamask_mask_pixel(self.det_obj,xy[1],xy[0])
            self.pixels2mask_userdefined.append(xy)

    def id01_limamask_unmaskpixel(self,list_tuple_xy=[]):
        for xy in list_tuple_xy:
            self.mask = id01_limamask_unmask_pixel(self.det_obj,xy[1],xy[0])
            try:
                self.pixels2mask_userdefined.remove(xy)
            except:
                print("xy pos not in list")    

    def show_det_calibration(self,):
        print(f"### CALIBRATION COMPLETE ###")
        distance = ANSI.color_text("90",f"{self.distance:03f}")
        print(f"distance: {distance}")
        cenpix = ANSI.color_text("90",f"{self.beam_center_x:03f},{self.beam_center_y:03f}")

        print(f"self.beam_center_x,y: {cenpix}")
        print(f"pixel mask is set? {self.det_obj.processing.use_mask}")
        fn = ANSI.color_text("90",'/scanno/instrument/detname/')
        print(f".... saved to hdf5 file see '{fn}'")
        print(f"############################")
  

        
"""
* from id01.scripts.ID01_det_calib import DetectorCalibration                                │ 
* a=DetectorCalibration()                                                                    │ 
* b=a.get_calibration_scan_type(mpx1x4,mot_objs=(delta,nu),pxlperdeg=np.array([277,277]))    │ 
* from id01.scripts.ID01_det_calib import DetectorCalibration                                │ 
* a=DetectorCalibration()                                                                    │ 
* b=a.get_calibration_scan_type(mpx1x4,mot_objs=(delta,nu),pxlperdeg=np.array([277,277]))    │ 
* umv(delta,0,nu,0)                                                                          │ 
* a.run_detector_calibration(mpx1x4,(delta,nu))                                              │ 
* a=DetectorCalibration()                                                                    │ 
* from id01.scripts.ID01_det_calib import DetectorCalibration                                │ 
* a=DetectorCalibration()                                                                    │ 
* b=a.get_calibration_scan_type(mpx1x4,mot_objs=(delta,nu),pxlperdeg=np.array([277,277]))    │ 
* a.run_detector_calibration(mpx1x4,(delta,nu))                                              │ 
* last_error                                        
"""
