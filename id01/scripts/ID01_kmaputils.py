# -*- coding: utf-8 -*-

# ID01 BLISS git repo
#       authors:
#         Steven Leake, leake@esrf.fr
#
#       Created at: 20. May 09:00:00 CET 2021
#
#----------------------------------------------------------------------
# Description: 
#    functions for extending / interacting with kmap scans
#    straight to daquiri? or use id01sware for basic easy to use stuff
#
#
# TODO:
#  alignpix %BR%
#  alignpiy %BR%        <<<< https://gitlab.esrf.fr/id01/id01/-/issues/1
#  alignpiz %BR%
#----------------------------------------------------------------------
from bliss.shell.standard import *
from bliss.common.cleanup import cleanup, axis as cleanup_axis
from bliss import current_session, setup_globals

import numpy as np
import h5py as h5
import sys
import os.path as osp

from skimage import  registration
from skimage.registration import phase_cross_correlation
from skimage.feature import peak_local_max
#from scipy.ndimage.fourier import fourier_shift
from scipy.ndimage.measurements import center_of_mass
from id01.scripts.kmap import akmap, dkmap
from id01.process.interactive import *
import matplotlib.pyplot as pl

from id01.scripts.ID01_utils import elogbook_send_file

# id01sware utils

# TODO: the following macros do not exist yet / an issue is open on gitlab for flint interaction / daiquiri
# started work on PScanTracker_bliss

#  pscan_align%BR%
#  pscan_align1D%BR%
#  pscan_align2D%BR%
#  pscan_autoalign1D%BR%
#  pscan_autoalign2D%BR%
#  pscan_pickroi%BR%
#  pscan_inspect%BR%
#  pscan_inspect1D%BR%

def pizero(*axislist,suppress_umv=False):
    """
    pizero(pix,piy,piz)
    """
    motion = ()
    delta_motion = ()
    
    axis_dict = {"pix":current_session.env_dict["thx"],
                 "piy":current_session.env_dict["thy"],
                 "piz":current_session.env_dict["thz"]}
                 
    for axis in axislist:
        lims = axis.limits
        stroke = lims[1]-lims[0]
        cen_stroke = np.array(lims).sum()/2
        motion += (axis, cen_stroke)
        delta_motion += (axis_dict[axis.name], (axis.position-cen_stroke)/1000) # hexpaod hard coded
    
    if len(motion) and not suppress_umv:
        umv(*motion)
        
    if suppress_umv:
        return motion, delta_motion
        
def pizerohex(*axislist):
    """
    like a motor stack, move the hexapod to offset the PI to move 
    the beam to the center of the PI stroke
    i.e. maintaining alignment on the sample
    """
    
    motion, delta_motion = pizero(*axislist,suppress_umv=True)

    if len(delta_motion):
        umv(*motion)
        umvr(*delta_motion)
        
        # TODO dont hard code the hexapod consider piezo strokes signs etc.
        
def pisetstroke(strokelist, *axislist):
    """
    pisetstroke([10,20,20],pix,piy,piz) 
    """
    
    # TODO pull values directly from PI controller
    # TODO allow non zero min
    # error 
    if len(strokelist) != len(axislist):
        print("strokelist ({len(strokelist)}) != axislist ({len(axislist)})")
        exit
    for axis,stroke in zip(axislist,strokelist):
        axis.limits = (0,float(stroke))       
    
def peak_optimise(axis,start_step = 0.5, end_step = 0.1,counter = None):
    # TODO need to pull the current plotselect counter if counter = None
    # TODO adapt the autofocus function from id01sware for this purpose
    pass
    
def dbeam_caustic_profile(axis, contrast_function, start_step = 0.5, end_step = 0.1):
    """
    run a series of scans and optimise based on the max intensity
    run a scan move 0.5mm
    run a scan
    """
    # TODO exploit peak optimise for a non linear search trajectory
    
    # TODO how to handle the geometry with generic macros.

def kmap_x2D():
    """
    move hexapod through several positions and make maps
    use surface coordinates (not lab coordinates)
    """
    # TODO in abs coords << this should use the sample stack
    # TODO use the stroke of the PI - requires this to be saved somewhere.
    pass
    
def dkmap_x2D():
    """
    move hexapod through several positions and make maps
    use surface coordinates (not lab coordinates)
    """
    # TODO in abs coords << this should use the sample stack
    # TODO use the stroke of the PI - requires this to be saved somewhere.
    pass

def kmap_3D(fast_mot,
    xmin,
    xmax,
    x_nb_points,
    slow_mot,
    ymin,
    ymax,
    y_nb_points,
    expo_time,
    slowest_mot,
    zmin,
    zmax,
    z_nb_points,
    offset_correction_factor=(0,0),
    latency_time=0.,
    save_flag=True,
    frames_per_file=None):
    
    """
    Usage:
    akmap
       <X fast motor> <Xmin> <Xmax> <X nr points>
       <Y slow motor> <Ymin> <Ymax> <Y nr lines>
       <sampling time (s)>
       <Z slow motor> <Zmin> <Zmax> <Z nr 2Dmaps>
       <offset_correction_factor>

    offset_correction_factor - microns per degree
    """
    
    # invoke metadata newdataset
    # TODO invoke KMAP metadata
    newdataset()
    
    z_step = (zmax-zmin)/(z_nb_points-1)
    zpos_start = np.linspace(zmin,zmax,z_nb_points)
    zpos_deltas = zpos_start-zpos_start.mean()
    xpos_start = zpos_deltas*offset_correction_factor[0]+(xmin+xmax)/2.
    xstroke = xmax-xmin
    ypos_start = zpos_deltas*offset_correction_factor[1]+(ymin+ymax)/2.
    ystroke = ymax-ymin

    for zpos,ypos,xpos in zip(zpos_start,ypos_start,xpos_start):
        umv(fast_mot,xpos,slow_mot,ypos,slowest_mot, zpos)
        #umvr(fast_mot, float(xpos) + z_step*offset_correction_factor)
        #umvr(slow_mot, float(ypos) + z_step*offset_correction_factor)
        #umvr(slowest_mot, float(zpos) + z_step*offset_correction_factor)
        
        dkmap(fast_mot, -xstroke/2., xstroke/2., x_nb_points, slow_mot, -ystroke/2., ystroke/2., \
              y_nb_points, expo_time, latency_time, \
              save_flag=save_flag, frames_per_file=frames_per_file)
    
    #TODO port necessary preanalysis to the elogbook
    
    # close dataset
    enddataset()

def dkmap_3D(fast_mot,
    xmin,
    xmax,
    x_nb_points,
    slow_mot,
    ymin,
    ymax,
    y_nb_points,
    expo_time,
    slowest_mot,
    zmin,
    zmax,
    z_nb_points,
    offset_correction_factor=(0,0),
    latency_time=0.,
    save_flag=True,
    frames_per_file=None,):

    xmin += fast_mot.position
    xmax += fast_mot.position
    ymin += slow_mot.position
    ymax += slow_mot.position
    zmin += slowest_mot.position
    zmax += slowest_mot.position
    
    #TODO cleanup adds up to the end of the scan and then all of the moves are made
    with cleanup(fast_mot, slow_mot, slowest_mot, restore_list=(cleanup_axis.POS,)):
        return kmap_3D(
            fast_mot,
            xmin,
            xmax,
            x_nb_points,
            slow_mot,
            ymin,
            ymax,
            y_nb_points,
            expo_time,
            slowest_mot,
            zmin,
            zmax,
            z_nb_points,
            offset_correction_factor=offset_correction_factor,
            latency_time=latency_time,
            save_flag=save_flag,
            frames_per_file=frames_per_file,
        )
    
def kmap_guess_offset_correction_factor(fast_mot,
            xmin,
            xmax,
            x_nb_points,
            slow_mot,
            ymin,
            ymax,
            y_nb_points,
            expo_time,
            slowest_mot,
            zmin,
            zmax,
            z_nb_points,
            offset_correction_factor=(0,0),
            latency_time=0.,
            save_flag=True,
            frames_per_file=None, 
            counter=None,
            filename = None):
    """    
    # make a series of kmaps and extract the roi of interest and populate an array with 2D images
    kmap_3D(fast_mot, 
            xmin,
            xmax,
            x_nb_points,
            slow_mot,
            ymin,
            ymax,
            y_nb_points,
            expo_time,
            slowest_mot,
            zmin,
            zmax,
            z_nb_points,
            offset_correction_factor,
            latency_time,
            save_flag,
            frames_per_file)
    """
    
    #TODO split this function, one function to calculate offsets

    degperZpoint= (zmax-zmin)/(z_nb_points-1)
    umperdegree = [((xmax-xmin)/(x_nb_points-1))/degperZpoint, ((ymax-ymin)/(y_nb_points-1))/degperZpoint]
    
    

    if filename is None: 
        globals_dict = current_session.env_dict
        filename = globals_dict["SCANS"][-1].scan_info["filename"]    
        scan_nb = globals_dict["SCANS"][-1].scan_info["scan_nb"]    

    h5path_title = "/%i.1/title"%scan_nb # to check for kmap
    with h5.File(filename,'r') as h5f:
        scan_title = h5f[h5path_title][()]
        m0name,m0s,m0e,m0_pts, m1name,m1s,m1e,m1_pts,exposure = scan_title.split("(")[-1].split(")")[0].split(",")
        m0s,m0e,m0_pts,m1s,m1e,m1_pts,exposure = float(m0s),float(m0e),int(m0_pts),float(m1s),float(m1e),int(m1_pts),float(exposure)
        
        counters = np.zeros((z_nb_points,m1_pts,m0_pts))
        m2_pos = []
        for i in np.arange(z_nb_points):
            h5path_counters = "/%i.1/measurement/"%(i+1)
            print(osp.join(h5path_counters,counter),i)
            counters[i,:,:] = h5f[osp.join(h5path_counters,counter)][()].reshape(m1_pts,m0_pts)
            h5path_positioner = "/%i.1/instrument/positioners/"%(i+1)
            m2_pos.append(h5f[osp.join(h5path_positioner,slowest_mot.name)][()])
    
    # use EZ method, cross correlate consecutive kmaps    
    shifts = []
    shifts.insert(0, np.array([0,0]))
    for i in np.arange(z_nb_points):
        sh, err, dphase = registration.phase_cross_correlation(counters[i-1,:,:], counters[i,:,:])
        shifts.append(sh + shifts[i-1])
            
    etashift_arr = np.array(shifts)

    # now fit them to get the shift in each dimension
    x = np.arange(z_nb_points+1) #etashift_arr[:,0]
    y = etashift_arr[:,0]

    m0, b = np.polyfit(x, y, 1)
    # TODO port this to the logbook
    #plt.plot(x,y)
    #plt.plot(x, m0*x + b)
    
    x = np.arange(z_nb_points+1) #etashift_arr[:,0]
    y = etashift_arr[:,1]

    m1, b = np.polyfit(x, y, 1)
    # TODO port this to the logbook
    #plt.plot(x,y)
    #plt.plot(x, m1*x + b)
    
    #m pixels per deg - convert
    calc_offset_correction_factor = (m0 * umperdegree[0],m1 * umperdegree[1]) 
    print("Suggested drift corrected kmap:")
    outs = ""
    outs+=f"kmap_3D({fast_mot.name},{xmin},{xmax},{x_nb_points},{slow_mot.name},"
    outs+=f"{ymin},{ymax},{y_nb_points},{expo_time},{slowest_mot.name},{zmin},{zmax},"
    outs+=f"{z_nb_points},offset_correction_factor = {calc_offset_correction_factor},"
    outs+=f"latency_time = {latency_time},save_flag = True,frames_per_file={frames_per_file})"
    print(outs)
    
    
def kmap_calc_contrast(no_scans,counter):
    kmaps_std_sum = ()
    Z_pos = ()
    for i in np.arange(1,no_scans+1):
        tmp_SCAN = SCANS[no_scans+1-i]
        kmaps_std_sum += (np.std((tmp_SCAN.get_data()[counter])).sum(),)
        z_pos += (tmp_scan.scan_info['positioners']['positioners_start'][slowest_mot.name],)
    
    #TODO https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.triang.html
    # extract peak and move there. or suggest to move there.
    
    #port data to elogbook

import matplotlib.pyplot as plt

def kmap_plot(kmapScanObj,counter_name):
    
    cnt_data = kmapScanObj.get_data(counter_name)
    fm_channel = kmapScanObj.scan_info["fast_motor"]
    sm_channel = kmapScanObj.scan_info["slow_motor"]
    fm_data =  kmapScanObj.get_data(fm_channel)
    sm_data =  kmapScanObj.get_data(sm_channel)    
    # miserable hack for the points
    fm, fms, fmf, fmpts, sm, sms, smf, smpts, exp1 = kmapScanObj.scan_info["title"].split(",")
    plt.pcolormesh(fm_data.reshape(int(fmpts),int(smpts)),sm_data.reshape(int(fmpts),int(smpts)),cnt_data.reshape(int(fmpts),int(smpts)))
    plt.show()
 
current_session.env_dict["alignDefaultCounter"] = "someROI"

# TODO convert these functions to a specific scan number!
def alignCEN(counter = current_session.env_dict["alignDefaultCounter"],scanObj=None):
    """
    Return the motor position corresponding to the center of the fwhm of the last scan.
    """
    # set / use default scan counter
    #if counter is not None:
    #    alignDefaultCounter = counter

    # default last scan unless explicit
    if scanObj == None:
        globals_dict = current_session.env_dict
        scanObj = globals_dict["SCANS"][-1]
    
    scanObj.goto_cen(counter)
    
def alignCOM(counter = current_session.env_dict["alignDefaultCounter"],scanObj=None):
    """
    Return position of scanned motor at maximum of <counter> of last scan.
    """
    # set / use default scan counter
    #if counter is not None:
    #    alignDefaultCounter = counter

    # default last scan unless explicit
    if scanObj == None:
        globals_dict = current_session.env_dict
        scanObj = globals_dict["SCANS"][-1]
    
    scanObj.goto_com(counter)
    
def alignMAX(counter = current_session.env_dict["alignDefaultCounter"],scanObj=None):
    """
    Return center of mass of last scan according to <counter>.
    """

    # set / use default scan counter
    #if counter is not None:
    #    alignDefaultCounter = counter

    # default last scan unless explicit
    if scanObj == None:
        globals_dict = current_session.env_dict
        scanObj = globals_dict["SCANS"][-1]
    
    scanObj.goto_peak(counter)


        

## aligning on kmap datsets - it was recommended not to use the SCANS[-1] 
# approach to keep the Bliss shell clean of heavy computation >4000 points    
    
def hdf5handler(filename,path):
    """
    open / pull all data / close when retrieving anything from hdf5
    this avoids potential file lock problems
    """
    # TODO check if it is more efficient to load all from a single file open/close
    #data_dict = {}
    with h5.File(filename,'r') as h5file:
        data = h5file[path][()]
    return data
    
"""
def kmapdatahandler(nb=-1):
"""
#nb: scan number <0  in deque units (-1 is the last scan) > 0 in scan number

#loads all data
"""
    
    # find the  last kmap scan
    globals_dict = current_session.env_dict

    kmap_scans = []
    kmap_scans_nb = {}
    for i in np.arange(len(globals_dict["SCANS"]))[::-1]:
        idx = i-len(globals_dict["SCANS"])
        if globals_dict["SCANS"][idx].scan_info["title"].count("kmap"):
            kmap_scans.append(idx)
            kmap_scans_nb["%i"%idx] = globals_dict["SCANS"][idx].scan_info["scan_nb"]
            
    print("Available kmap scans in deque : scan nb  :", kmap_scans_nb)        

    if nb < 0:
        scan_nb = globals_dict["SCANS"][nb].scan_info["scan_nb"]
        tmp_scan = globals_dict["SCANS"][nb]
        #import pdb
        #pdb.set_trace()
        if tmp_scan.scan_info["title"].count("kmap") == 0:
            print("... rolling back to last kmap measured ...")
            last_kmap_idx = list(kmap_scans_nb.keys())[0]
            tmp_scan = globals_dict["SCANS"][int(last_kmap_idx)]
            print("... scan nb: %i"%kmap_scans_nb[last_kmap_idx])
            
    if nb > 0:    
        kmap_nb_scans = {v: k for k, v in kmap_scans_nb.items()}
        tmp_scan = globals_dict["SCANS"][int(kmap_nb_scans[nb])]
        
    if not tmp_scan.scan_info["title"].count("kmap"):
        msg = "... this scan is not a kmap..."
        print(msg)
      
    else:
        # need x positions, y positions and counters
        m0name,m0s,m0e,m0_pts, m1name,m1s,m1e,m1_pts,exposure = tmp_scan.scan_info["title"].split("(")[-1].split(")")[0].split(",")
        m0s,m0e,m0_pts,m1s,m1e,m1_pts,exposure = float(m0s),float(m0e),int(m0_pts),float(m1s),float(m1e),int(m1_pts),float(exposure)
        m0name,m1name = m0name.strip(), m1name.strip()
        
        # all counters are found in channels
        channels_dict = tmp_scan.scan_info["channels"].copy()
        m0dname = channels_dict[m0name+'_position']['display_name']
        m1dname = channels_dict[m1name+'_position']['display_name']
        # remove positions from counters
        del channels_dict[m0name+'_position']
        del channels_dict[m1name+'_position']
        # remove any raw positions / image data
        delkeys = []
        for item in channels_dict.keys():
            if item.count("raw") or item.count("image") or item.count("time"):
                delkeys.append(item)
        for item in delkeys:
            del channels_dict[item]
        
        # build an array for the counters
        #np.zeros()
        x = tmp_scan.get_data(m0dname).reshape((m1_pts,m0_pts))
        y = tmp_scan.get_data(m1dname).reshape((m1_pts,m0_pts))
        counters = np.zeros((len(channels_dict),x.shape[0],x.shape[1]))
        for i,item in enumerate(channels_dict.keys()):
            try:
                counters[i,:] = tmp_scan.get_data(item).reshape((m1_pts,m0_pts))
            except:
                print(item,tmp_scan.get_data(item).shape) 
        # todo return the labels
        return m0name,m0s,m0e,m0_pts, m1name,m1s,m1e,m1_pts,x,y,counters,channels_dict

def kmapdatahandler_fast(nb=-1, channels=[]):
"""
#nb: scan number <0  in deque units (-1 is the last scan) > 0 in scan number

#channels to load
"""
    
    # find the  last kmap scan
    globals_dict = current_session.env_dict

    kmap_scans = []
    kmap_scans_nb = {}
    for i in np.arange(len(globals_dict["SCANS"]))[::-1]:
        idx = i-len(globals_dict["SCANS"])
        if globals_dict["SCANS"][idx].scan_info["title"].count("kmap"):
            kmap_scans.append(idx)
            kmap_scans_nb["%i"%idx] = globals_dict["SCANS"][idx].scan_info["scan_nb"]
            
    print("Available kmap scans in deque : scan nb  :", kmap_scans_nb)        

    if nb < 0:
        scan_nb = globals_dict["SCANS"][nb].scan_info["scan_nb"]
        tmp_scan = globals_dict["SCANS"][nb]
        #import pdb
        #pdb.set_trace()
        if tmp_scan.scan_info["title"].count("kmap") == 0:
            print("... rolling back to last kmap measured ...")
            last_kmap_idx = list(kmap_scans_nb.keys())[0]
            tmp_scan = globals_dict["SCANS"][int(last_kmap_idx)]
            print("... scan nb: %i"%kmap_scans_nb[last_kmap_idx])
            
    if nb > 0:    
        kmap_nb_scans = {v: k for k, v in kmap_scans_nb.items()}
        tmp_scan = globals_dict["SCANS"][int(kmap_nb_scans[nb])]
        
    if not tmp_scan.scan_info["title"].count("kmap"):
        msg = "... this scan is not a kmap..."
        print(msg)
      
    else:
        # need x positions, y positions and counters
        m0name,m0s,m0e,m0_pts, m1name,m1s,m1e,m1_pts,exposure = tmp_scan.scan_info["title"].split("(")[-1].split(")")[0].split(",")
        m0s,m0e,m0_pts,m1s,m1e,m1_pts,exposure = float(m0s),float(m0e),int(m0_pts),float(m1s),float(m1e),int(m1_pts),float(exposure)
        m0name,m1name = m0name.strip(), m1name.strip()
        
        # all counters are found in channels
        channels_dict = tmp_scan.scan_info["channels"].copy()
        
        channel_keys={}
        for channel in channels:
            try:
                channel_keys[channel]=channels_dict[channel]['display_name']
            except:
                print("channel:%s not found"%channel)
        
        
        
        m0dname = channels_dict[m0name+'_position']['display_name']
        m1dname = channels_dict[m1name+'_position']['display_name']
        
        # build an array for the counters
        x = tmp_scan.get_data(m0dname).reshape((m1_pts,m0_pts))
        y = tmp_scan.get_data(m1dname).reshape((m1_pts,m0_pts))
        
        counters = np.zeros((len(channel_keys),m1_pts,m0_pts))
        for i,item in enumerate(channel_keys):
            try:
                counters[i,:] = tmp_scan.get_data(item).reshape((m1_pts,m0_pts))
            except:
                print(item,tmp_scan.get_data(item).shape) 
        # todo return the labels
        return m0name,m0s,m0e,m0_pts,m1name,m1s,m1e,m1_pts,x,y,counters,channel_keys


def pscan_align(channels = [], filename = None, nb = -1,  mynorm = 'linear'):
#"""
#channels: the counters to plot
#filename: hdf5 filename
#nb: scan number

#note found an upper limit on memory - ask SEB where this comes from 
#needa check if kmap is more than 1000 points should use the hdf5 file.

#rename this
#lazy version of pscan_align
#this uses the input values not the raw adc for plotting
#pcolormesh could be used here??
#make channels a counters list or MG or something more sensible
#the counter channels names varies between hdf5 and SCANS object which is nasty
"""
    
    # load all of the counters
    if filename is None: # we are working with the deque SCANS / scan number in memory
        m0name,m0s,m0e,m0_pts, m1name,m1s,m1e,m1_pts,x,y,counters,channels_dict = kmapdatahandler(nb)
        channels_list = list(channels_dict.keys())
        
    if nb > 0 and filename is not None: # we are working with hdf5
        # TODO
            
        h5path_counters = "/%i.1/measurement/"%nb
        h5path_title = "/%i.1/title"%nb # to check for kmap
        with h5.File(filename,'r') as h5f:
            channels_list = [key for key in h5f[h5path_counters].keys()]
            scan_title = h5f[h5path_title][()]
            if scan_title.count("kmap")!=1:
                print("warning not kmap data")
            m0name,m0s,m0e,m0_pts, m1name,m1s,m1e,m1_pts,exposure = scan_title
        
            counters = np.zeros((len(channels_list),m0_pts,m1_pts))
            for i,channel in enumerate(channels_list):
                idx = channels_list.index(channel) 
                counters[i,:,:] = h5f[h5path_counters][channel][()].reshape(m0_pts,m1_pts)                
    
    # select some of the counters
    if len(channels) != 0:
        # check if channels exist in counters
        tmp_ch_list = []
        for item in channels:
            if not channels_list.count(item):
                print("channel %s not in channel_dict"%item)
            else:
                tmp_ch_list.append(item)
        print(tmp_ch_list)
        tmp_channels_list = list(channels_dict.keys())        
        tmp_counters = np.zeros((len(tmp_ch_list),counters.shape[1],counters.shape[2]))
        for i,channel in enumerate(tmp_ch_list):
            idx = tmp_channels_list.index(channel) 
            tmp_counters[i,:,:] = counters[idx,:,:]
        
        counters = tmp_counters
        channels = tmp_ch_list
        if len(channels) == 0:
            outs = ""
            outs += "....................\n"
            outs += "choose from the available channels:\n"
            for item in channels_dict.keys():
                outs += item+"\n"
            outs +="....................\n"
            raise ValueError("none of the channels were found\n"+outs)
    else:
        channels = channels_list
        
    # look at them
    try:    
        fig = pl.figure()
        ax = fig.add_subplot(111)
        tracker = GenericIndexTracker(ax, data = counters, norm=mynorm,exit_onclick=True)
        ax.set_xlabel(m0name)
        ax.set_ylabel(m1name)
        tracker.set_extent(m0s,m0e,m1s,m0e)
        tracker.set_axes_properties(title=channels)
        pl.show()
    except:
        print("An error occurred")
"""

def sxdm_align(channels = [], filename = None, nb = -1,  mynorm = 'linear'):
    """
    channels: the counters to plot
    filename: hdf5 filename
    nb: scan number (accepts list -n nomenclature)
    
    TODO: counters in hdf5 do not match counters in measurement group e.g _sum
    
    note found an upper limit on memory - ask SEB where this comes from 
    needa check if kmap is more than 1000 points should use the hdf5 file.
    
    TODO this uses the input values not the raw adc for plotting
    pcolormesh could be used here??
    
    TODO make channels a counters list or MG or something more sensible
    the counter channels names varies between hdf5 and SCANS object which is nasty
    
    TODO Add check for other motor positions - to make sure positioners havent changed
    
    """


    # load all of the counters
    if filename is None: 
        globals_dict = current_session.env_dict
        filename = globals_dict["SCAN_SAVING"].filename
        # we are working with the deque SCANS / scan number in memory
        #m0name,m0s,m0e,m0_pts, m1name,m1s,m1e,m1_pts,x,y,counters,channels_dict = kmapdatahandler_fast(nb, channels)
        #channels_list = list(channels_dict.keys())
        
    if filename is not None: # we are working with hdf5
        
        # scans in hdf5 file                   
        kmap_scans = get_scans_title_str_hdf5(filename, "kmap")
        
        #negative index pulls from the list in hdf5 file like np slicing
        if nb<0:
            nb = kmap_scans[nb]
             
        print("\nfilename: %s, scan_no: %s\n"%(filename,nb))
        h5path_counters = "/%i.1/measurement/"%nb
        h5path_title = "/%i.1/title"%nb
        
        with h5.File(filename,'r') as h5f:
            channels_list = [key for key in h5f[h5path_counters].keys()]
            scan_title = h5f[h5path_title][()]
            if scan_title.count("kmap")!=1:
                print("warning not kmap data")
            m0name,m0s,m0e,m0_pts, m1name,m1s,m1e,m1_pts,exposure = scan_title.split("(")[-1].split(")")[0].split(",")
            m0s,m0e,m0_pts,m1s,m1e,m1_pts,exposure = float(m0s),float(m0e),int(m0_pts),float(m1s),float(m1e),int(m1_pts),float(exposure)
            m0name,m1name = m0name.strip(), m1name.strip() 
        
            channel_keys={}
            if channels==[]:
                channels = channels_list
                
            for channel in channels:
                if channels_list.count(channel):
                    try:
                        channel_keys[channel]=channel
                    except:
                        print("channel:%s not found"%channel) 

            if len(channel_keys)==0:
                print("#################\n Available channels: ")
                for item in channels_list:
                    print(item)
                print("#################\n")
                
            counters = np.zeros((len(channel_keys),m1_pts,m0_pts))
            for i,channel in enumerate(channel_keys):
                try:
                    counters[i,:,:] = h5f[h5path_counters][channel][()].reshape(m1_pts,m0_pts)                
                except:
                    print("Failed for channel: %s"%channel)
            channel_list = list(channel_keys)

    # look at them
    try:    
        fig = pl.figure(figsize=(9,6))
        ax = fig.add_subplot(111)
        fig.suptitle(" File: %s \n Scan: %i"%(filename,nb))
        tracker = GenericIndexTracker(ax, data = counters, norm=mynorm,exit_onclick=True)
        ax.set_xlabel(m0name)
        ax.set_ylabel(m1name)
        tracker.set_extent(m0s,m0e,m1s,m1e)
        tracker.set_axes_properties(title=channels)
        pngfn = "/tmp/test.png"
        pl.savefig(pngfn)
        elogbook_send_file(pngfn)
        pl.show()
    except:
        print("An error occurred")
        
def alignKMAP(*counters): 
    """
    spawn a 2D plot, mouse wheel to scroll all counters
    toggle button for log/linear + slide bar for colorbar?
    """
     # is MG a counters list? can it be handled like that?
    #if counters not set take active MG?
    
    if scanObj == None:
        globals_dict = current_session.env_dict
        scanObj = globals_dict["SCANS"][-1]   
        
    fn = scanObj.filename
    scan_nb = scanObj.scan_nb
    counters_path = f"{scan_nb}/measurement/%s"
    
    
    # build array for counters
    #tmp_data = np.zeros(len(counters))
    #for counter in counters:
    #    hdf5handler(fn,counters_path%counter.name)
    

def inspectSXDM(*counters): 
    """
    spawn a 2D plot, mouse wheel to scroll all counters
    toggle button for log/linear + slide bar for colorbar?
    output a list of positions that can be used as targets
    """
    #TODO
    pass

def auto_alignSXDM_COM(scan_no, counter, filename=None):
    """
    COM based on actual motor positions in the scan
    
    TODO find the next scan number in BLISS
    """
    #TODO
    if filename is None: 
        globals_dict = current_session.env_dict
        filename = globals_dict["SCAN_SAVING"].filename
        
    counter, title = get_SXDM(scan_no, counter, filename)
    m0name,m0s,m0e,m0_pts,m1name,m1s,m1e,m1_pts,exposure = get_SXDM_params4mtitle(title)
    counter = counter.reshape(m1_pts,m0_pts)
    #COM = center_of_mass(counter) # assumes uniform grid

    counter_m0, title = get_SXDM(scan_no, m0name+"_position", filename)
    counter_m1, title = get_SXDM(scan_no, m1name+"_position", filename)
    counter_m0 = counter_m0.reshape(m1_pts,m0_pts)
    counter_m1 = counter_m1.reshape(m1_pts,m0_pts)
    m0 = (counter_m0*counter).sum()/counter.sum()
    m1 = (counter_m1*counter).sum()/counter.sum()
    
    umv(globals_dict[m0name],m0,globals_dict[m1name],m1)
    
    return m0,m1
    
def auto_alignSXDM_CC(scan0_nb,counter0_name,scan1_nb,counter1_name,filename=None):
    """
    autocorrelation - use get_KMAP function
    """
    #TODO
    
    def get_shift_between_images_CC(image0, image1, upsample_factor=1000):
        """
        calculate the sub pixel shift between two images using sckit-image
        """
        cc, error, diffphase = phase_cross_correlation(image0,image1, upsample_factor)
        return cc,error,diffphase
        
    counter0, title0 = get_SXDM(scan0_nb, counter0_name, filename)
    m0name,m0s,m0e,m0_pts,m1name,m1s,m1e,m1_pts,exposure = get_SXDM_params4mtitle(title0)
    counter0 = counter0.reshape(m1_pts,m0_pts)

    counter1, title1 = get_SXDM(scan1_nb, counter1_name, filename)
    m0name,m0s,m0e,m0_pts,m1name,m1s,m1e,m1_pts,exposure = get_SXDM_params4mtitle(title1)
    counter1 = counter1.reshape(m1_pts,m0_pts)  
    
    cc,error,diffphase = get_shift_between_images_CC(counter0, counter1, upsample_factor=1000)  
    
    # TODO add calculated shift in normalised units
    return cc,error,diffphase

def auto_findPeaksSXDM(scan_no, counter, filename=None, min_distance_pxls=1,prefix=""):
    """
    analyse a 2D kmap - pick some peaks and return their positions
    """
    #TODO
    if filename is None: 
        globals_dict = current_session.env_dict
        filename = globals_dict["SCAN_SAVING"].filename
    print(scan_no, counter, filename,prefix)
        
    counter, title = get_SXDM(scan_no, counter, filename=filename, prefix=prefix)
    m0name,m0s,m0e,m0_pts,m1name,m1s,m1e,m1_pts,exposure = get_SXDM_params4mtitle(title)
    counter = counter.reshape(m1_pts,m0_pts)
    #COM = center_of_mass(counter) # assumes uniform grid
    peaks = peak_local_max(counter,min_distance=min_distance_pxls)
    counter_m0, title = get_SXDM(scan_no, m0name+"_position", filename,prefix=prefix)
    counter_m1, title = get_SXDM(scan_no, m1name+"_position", filename,prefix=prefix)
    counter_m0 = counter_m0.reshape(m1_pts,m0_pts)
    counter_m1 = counter_m1.reshape(m1_pts,m0_pts)
    m0 = (counter_m0*counter).sum()/counter.sum()
    m1 = (counter_m1*counter).sum()/counter.sum()
    
    #umv(globals_dict[m0name],m0,globals_dict[m1name],m1)
    # TODO return a motion list? or MP points list
    return peaks


def get_SXDM(scan_no, counter, filename=None,prefix=""):
    """
    return a previously measure KMAP for use in the BLISS terminal
    """
    #TODO
    if filename is None: 
        globals_dict = current_session.env_dict
        filename = globals_dict["SCAN_SAVING"].filename
    import pdb  # TODO file locking issue????
    pdb.set_trace()
    h5path_counter = "/%s%i.1/measurement/%s"%(prefix,scan_no,counter)
    h5path_title = "/%s%i.1/title"%(prefix,scan_no) 

    try:
        with h5.File(filename,'r') as h5f:
            counter_data = h5f[h5path_counter][()]
    except:
        print("... %s does not exist ..."%h5path_counter) 
            
    try:
        with h5.File(filename,'r') as h5f:
            title_data = h5f[h5path_title][()]
    except:
        print("... %s does not exist ..."%h5path_title)
        
    if title_data.count("kmap") == 0:
        raise ValueError("Scan %i is not a kmap dataset"%scan_no)
            
    return counter_data, title_data
    
def get_SXDM_params4mtitle(title):
    m0name,m0s,m0e,m0_pts, m1name,m1s,m1e,m1_pts,exposure = title.split("(")[-1].split(")")[0].split(",")
    m0s,m0e,m0_pts,m1s,m1e,m1_pts,exposure = float(m0s),float(m0e),int(m0_pts),float(m1s),float(m1e),int(m1_pts),float(exposure)
    m0name,m1name = m0name.strip(), m1name.strip() 
    return m0name,m0s,m0e,m0_pts,m1name,m1s,m1e,m1_pts,exposure
    #TODO properly collection
                    
def get_SXDM_detector_average(scan_no, counter, filename=None):
    """
    produce a detector average image and stick it in the datafile somehow
    """
    if filename is None: 
        globals_dict = current_session.env_dict
        filename = globals_dict["SCAN_SAVING"].filename
        
    counter, title = get_SXDM(scan_no, counter, filename)  
    # TODO save this to the output file  
    return counter.sum(axis=0)
    
def reanalyse_SXDM_pickroi(scan_no, counter, filename=None):
    """
    based on a user defined ROI reanalyse, 
    pickroi from a detector average image
    """
    det_avg = get_SXDM_detector_average(scan_no, counter, filename=None)
    #TODO    
    pass
    
def reanalyse_SXDM():
    """
    based on a user defined ROI reanalyse 
    """
    #TODO
    pass

def sxdm_stitch(scans = {}, channels=[],monitor=None, gamma = 0.5, norm = 'log', cmap = "jet"):
    """
    code from EZ modified by SL
    scans: dictionary {filename:scans_list,}
    channel: list of counters to plot
    monitor: counter name for monitor
    norm: log/power else linear
    """
    from silx.io import specfile
    from id01lib.plot import Normalize  # TODO
    from id01lib import image
    import pylab as pl
    from matplotlib import colors
    from matplotlib.ticker import LogFormatter
    from scipy import ndimage
     
    import collections
    import argparse
    import re
     
    _motordef = dict(pix="adcY",
                     piy="adcX",
                     piz="adcZ")
     
    _printmotors = ['eta', 'phi', 'delta', 'nu', 'thx', 'thy', 'thz']
     
    plotcols = channels
     
    formatter = None
    if norm=="log":
        _norm = lambda vmin, vmax: colors.LogNorm(vmin, vmax)
    elif norm=="power":
        _norm = lambda vmin, vmax: Normalize.Normalize(vmin=vmin, vmax=vmax, stretch="power", exponent=gamma, clip=False)
        formatter = LogFormatter(10, labelOnlyBase=False)
    else:
        _norm = lambda vmin, vmax: colors.Normalize(vmin, vmax)
     
    plotdata = collections.defaultdict(list)
    VMIN = collections.defaultdict(lambda:  pl.inf)
    VMAX = collections.defaultdict(lambda: -pl.inf)
    print(scans.items())
    for fname, scanno in scans.items():
     
        sf = specfile.SpecFile(fname)
        if not scanno:
            scanno = sf.keys()
        for num in scanno:
            try:
                sc = sf[num]
            except:
                break
            h = sc.scan_header_dict["S"].split()
            print(sc.number, h)
            motor1 = h[2]
            motor2 = h[6]
            arraysizey = int(h[5])
            arraysizex = int(h[9])
            pscan_shape = arraysizex, arraysizey
     
            if 1:
                x = -sc.data_column_by_name("adcY")/1000 + sc.motor_position_by_name("thx")
            else:
                x = sc.data_column_by_name("adcY")/1000 + sc.motor_position_by_name("thx")
     
            y = sc.data_column_by_name("adcX")/1000 + sc.motor_position_by_name("thy")
            I0 = sc.data_column_by_name(monitor) if monitor in sc.labels else 1.
            for ii, col in enumerate(plotcols):
                roidata = sc.data_column_by_name(col)/I0
                try:
                    x = x.reshape(pscan_shape)
                except:
                    continue
                y = y.reshape(pscan_shape)
                roidata = roidata.reshape(pscan_shape)
                vmin, vmax = image.percentile_interval(roidata[roidata>0])
                VMIN[col] = min(vmin, VMIN[col])
                VMAX[col] = max(vmax, VMAX[col])
     
                plotdata[col].append((x, y, roidata))
     
    for ii, col in enumerate(plotdata):
        fig = pl.figure(col)
        if not fig.axes:
            ax = fig.add_subplot(111)
        else:
            ax = fig.axes[0]
        norm = _norm(VMIN[col], VMAX[col])
        for (x,y,roidata) in plotdata[col]:
            im = ax.pcolormesh(x, y, roidata, cmap=cmap, norm=norm)
        ax.set_aspect('equal', adjustable="box")
        ax.set_title(col)


def get_scans_title_str_hdf5(filename, string):
    """
    return a list of scans whose title contains <string> from an hdf5 <filename>
    """
    print("Available %s scans in hdf5 file:"%string)
    
    with h5.File(filename,'r') as h5f:
        scans = [key for key in h5f.keys()]
        scans = list(map(float,scans))
        scans = list(map(int,scans))
        scans.sort()
        scansList = []
        for scan in scans:
            if h5f["%i.1/title"%scan][()].count(string):
                print("%i ... %s"%(scan,h5f["%i.1/title"%scan][()]))
                scansList.append(scan)
    return scansList
