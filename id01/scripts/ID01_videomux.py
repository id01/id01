# ID01 BLISS git repo
#       authors:
#         Peter Boesecke, boesecke@esrf.fr
#
#       Created at: 24. Oct 14:36:00 CET 2022
#
#----------------------------------------------------------------------
# Description:
#       class VideoMux()
#
# History:
#   2022-10-24  PB from nano3:/users/blissadm/local/spec/macros/ID01videomux.mac
#   2022-11-14  SL VideoMux(): config.get("vm")
#               lid01nano1:/users/blissadm/local/beamline_configuration/eh/videomux.yml
#               - plugin: bliss
#                 package: id01.scripts.ID01_videomux
#                 class: VideoMux
#                 name: vm
#
#----------------------------------------------------------------------

from bliss.comm import tcp

import logging
#logging.basicConfig(level=logging.DEBUG)
#logging.basicConfig(level=logging.WARNING)
#logging.debug('This will get logged')
#logging.warning('This will get logged')

_log = logging.getLogger("id01.scripts.ID01_videomux")
#The log level can be set in the bliss session after import logging
#and defining _log as in the above command.
#_log.setLevel("DEBUG")

class VideoMux():
    """
    model SMV116p:
    The serial line of the multiplexer must be 
    configured for:
    1200 bps, 1 start bit, 8 data bits, 
    1 stop bit, no parity
    (e.g. SDEV_1 = TACO:id01/ser012_rp/23 1200 raw)
    The pin assignments of the RJ11 remote in connector are 
    1,6: unused, 2,5: GND, 3:RXD, 4:TXD

    SERIAL        DEVICE <>TYPE  <>BAUD  <>MODE
    0  YES  lid012:28322 SOCKET    1200  raw
    REM: 28322 is using rocket port 23 (22+1)
    
    VIDEO_MUX_SL_DEVID="id01/ser012_rp/23"
    """

    def __init__(self,name,config,model="SMV116p"):
        """
        RS-232 remote commands consists of three ASCII characters. All commands
        begin with the forward slash "/" character. The two characters following
        the slash identify the command.
        The RS-232 remote connection requires two wires. One wire connects to the 
        RS-232 pin (pin25) and the other connects to a ground pin (pin13, 14 or 15).
        The communication protocol supported by the multiplexer is as follows:
        Data rate:  1200 bps
        Characters: 8
        Parity:     None
        Stop bits:  1
        The RS-232 remote pin is an input only.

        Problem reported by DFC in 2004:
        This problem is related with some low level of positive voltage (about 0.1V) in the TX pin of the multiplexer. is value is taken by the RP as a noisly long space (continuos break), with the result of a lot of 0xff and similar (see the histogram).
        then the RX channel of the RP is "receiving" a continuos stream of wrong chars and associated erros (out of frame,buffer overflow (4KB), ....)
        the flush solution proposed, solves partially the problem (reset errors and empty the buffer)
        disconnecting the RX channel of the RP from the TX channel of the multiplexer (NOT USED), solves complete the problem, and it is not required the flush action.

        More info in id01 wiki: 
        NOTES on SIMPLEX VIDEO MULTIPLEXER SMV116p ID01
        http://wikiserv.esrf.fr/id01/index.php/NOTES_on_SIMPLEX_VIDEO_MULTIPLEXER_SMV116p_ID01`
        """
        


        assert model=="SMV116p","ERROR: unsupported model {}".format(model)
        if model == "SMV116p":
            self.name = model
            self.VM_CMD_STR={}
            self.VM_END_STR ="\r\n"    # end of each command

            self.VM_CMD_STR[0]=0       # dummy

            self.VM_CMD_STR[1]="/01"   # Camera 1
            self.VM_CMD_STR[2]="/02"   # ...
            self.VM_CMD_STR[3]="/03"
            self.VM_CMD_STR[4]="/04"
            self.VM_CMD_STR[5]="/05"
            self.VM_CMD_STR[6]="/06"
            self.VM_CMD_STR[7]="/07"
            self.VM_CMD_STR[8]="/08"
            self.VM_CMD_STR[9]="/09"
            self.VM_CMD_STR[10]="/10"
            self.VM_CMD_STR[11]="/11"
            self.VM_CMD_STR[12]="/12"
            self.VM_CMD_STR[13]="/13"
            self.VM_CMD_STR[14]="/14"
            self.VM_CMD_STR[15]="/15"
            self.VM_CMD_STR[16]="/16"  # Camera 16

            self.VM_CMD_STR[17]="/AT"  # Function
            self.VM_CMD_STR[18]="/FZ"  # Full Screen / Zoom
            self.VM_CMD_STR[19]="/PP"  # PIP / up arrow
            self.VM_CMD_STR[20]="/22"  # 2x2 / down arrow
            self.VM_CMD_STR[21]="/33"  # 3x3 / left arrow
            self.VM_CMD_STR[22]="/44"  # 4x4 / right arrow
            self.VM_CMD_STR[23]="/LV"  # Live
            self.VM_CMD_STR[24]="/TP"  # Tape
            self.VM_CMD_STR[25]="/SQ"  # Seq (Sequence)
            self.VM_CMD_STR[26]="/SL"  # Sel (Select)

            self.VM_CMD_STR[27]="/00"  # dummy

            self._sock = tcp.Socket("lid012",28322)
        
        self.config = config
        super().__init__()

    def _cmd(self,num):

        _mystring = "{}{}".format(self.VM_CMD_STR[num],self.VM_END_STR)
        _mystring = _mystring.encode()

        ##print(_mystring)
        
        ## ser_put(VIDEO_MUX_SL,_mystring)

        self._sock.write(_mystring) 


    def cmd(self,num=None):
        if num in range(1,27):
            self._cmd(num)
        else:
            print("    Choose video multiplexer outputs")
            for no in range(1,17):
                if no%4==1:
                    print("                ",end="")
                print("{} camera {} ".format(no,no),end="")
                if (no%4==0):
                    print("")

            #    print("                17: function (motion setup)")
            #    print("                18: vcr view")
            #    print("                19: sequence / up arrow")
            print("                20: 2 x 2 / down arrow")
            print("                21: 3 x 3 / left arrow")
            print("                22: 4 x 4 / right arrow")
            print("                23: live")
            #    print("                24: tape / play")
            #    print("                25: record\n")
            print("                26: toggle main <-> call")
            #    print("                27:")

            print("                 0: EXIT")

            # setting to live mode (for using main and call outputs of videomux)
            self._cmd(23)




