"""
  AUTHOR:  2019-10-04 Peter Boesecke(PB), ESRF
  HISTORY: 2019-10-04 V0.5 PB
  HISTORY: 2021-06-09 V0.6 PB isotime(), wut(), wat() added
"""

from bliss.setup_globals import *
from bliss.shell.standard import *
from bliss.common.standard import (
    iter_counters,
    iter_axes_state,
    iter_axes_state_all,
    iter_axes_position,
    iter_axes_position_all,
    sync,
    info,
    reset_equipment,
)  # noqa: F401

def chg_dial(mot, dial):
  """
  chg_dial(i, u) - Sets the dial position of motor i to u by changing the contents of the
       controller registers.  Returns -1 if not configured for motor i or if the motor is
       protected, unusable or moving, else returns 0.

  chg_dial(i, s) - Starts motor i on a home or limit search, according to the value of s, as
       follows: 

       "home+" - move to home switch in positive direction.
       "home-" - move to home switch in negative direction.
       "home" - move to home switch in positive direction if current dial position is less than
            zero, otherwise move to home switch in negative direction.
       "lim+" - move to limit switch in positive direction.
       "lim-" - move to limit switch in negative direction.

       Positive and negative direction are with respect to the dial position of the motor. 
       (Not all motor controllers implement the home or limit search feature.)  
       For changing the motor controller after the end of the search use chg_dial(i, u).
       Returns -1 if not configured for motor i or if the motor is protected, 
       unusable or moving, else returns 0.
  """
  #### CHECK
  ####if mot is not configured, protected, unusable or moving:
  ####  return(-1)

  ### mot.wait becomes superfluous if the motor is not moving
  ### in spec it was not possible waiting for a specific motor
  ### it could be done here
  mot.wait_move()

  if type(dial) in (bytes, str):
    if dial == "lim+":
      mot.hw_limit(1, False)
    elif dial == "lim-": 
      mot.hw_limit(-1, False)
    elif dial == "home+": 
      mot.home(1, False)
    elif dial == "home-": 
      mot.home(-1, False)
    elif dial == "home": 
      if mot.dial < 0:
        mot.home(1, False)
      else:
        mot.home(-1, False)
    else:
      print(f"dial parameter [{dial}] is NOT KNOWN\n")        
      return(-1)

  else:
    _offset = mot.offset
    _sign = mot.sign
    # the next command can change implicitly mot.offset
    mot.dial = dial
    # update the user position calculated with the initial offset
    # possible problem, if the so calculated user position is outside limits
    mot.position = _sign * dial + _offset

  return(0)


def set_lim(mot, u, v):
  """
  set_lim(i, u, v) - Sets the low and high dial limits of motor i.  It doesn't matter which order
       the limits, u and v, are given.  Returns -1 if not configured for motor i or if the motor
       is protected, unusable or moving, else returns 0.
  """

  #### CHECK
  ####if mot is not configured, protected, unusable or moving:
  ####  return(-1)

  ### mot.wait becomes superfluous if the motor is not moving
  ### in spec it was not possible waiting for a specific motor
  ### it could be done here
  mot.wait_move()

  if u < v:
     lower = mot.dial2user(u) 
     upper = mot.dial2user(v)
  else:
     lower = mot.dial2user(v)
     upper = mot.dial2user(u) 

  mot.limits = (lower, upper)


def set_lm(mot, low_user, high_user):
  """
  set_lm - set lower and upper software limits

  The set_lm macro is used to establish the low_limit and high_limit for one motor in units of the
  user positions. The function set_lim(), used by the set_lm macro, establishes the limits in 
  terms of dial positions.
  """

  u = mot.user2dial(low_user)
  v = mot.user2dial(high_user)
  set_lim(mot, u, v)
  

def set_offset(mot, offset):
  """
  set_offset(mot, offset) - Sets the offset of the motor
  """

  #### CHECK
  ####if mot is not configured, protected, unusable or moving:
  ####  return(-1)

  ### mot.wait becomes superfluous if the motor is not moving
  ### in spec it was not possible waiting for a specific motor
  ### it could be done here
  mot.wait_move()

  mot.position=mot.user2dial(mot.position)*mot.sign+offset

def chg_offset(mot, user):
  """
  chg_offset(i, u) - Sets offset (determining user angle) of motor i to u.
  Returns -1 if not configured for motor i or if the motor is unusable or
  moving, else returns 0.
  """

  mot.position = user

  return(0)


from datetime import datetime
import time

def isotime():
    """
    Returns the current time as an isotime string
    """

    # There is now simple local time offset string available in datetime without
    # using tzinfo
    return("{}{}".format(datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%f'),time.strftime('%z')))



from bliss import setup_globals
from bliss.common.utils import safe_get, ErrorWithTraceback
from bliss import global_map

_MAX_COLS = wa.__globals__['_MAX_COLS']
_ERR = wa.__globals__['_ERR']
_print_errors_with_traceback = wa.__globals__["_print_errors_with_traceback"]
_tabulate = wa.__globals__["_tabulate"]

def wu(**kwargs):
    """
    Displays all positions (Where All) as user values (in the configured units)
    """
    print("Current Positions: user")
    print("                   dial")

    max_cols = kwargs.get("max_cols", _MAX_COLS)

    header, pos, dial = [], [], []
    ####tables = [(header, pos, dial)]
    tables = [(header, pos)]
    errors = []

    data = iter_axes_position_all(**kwargs)
    for axis_name, axis_unit, position, dial_position in data:
        if len(header) == max_cols:
            header, pos, dial = [], [], []
            ####tables.append((header, pos, dial))
            tables.append((header, pos))

        axis_label = axis_name
        if axis_unit:
            axis_label += "[{0}]".format(axis_unit)

        header.append(axis_label)
        pos.append(position)
        dial.append(dial_position)

        if _ERR in [str(position), str(dial_position)]:
            errors.append((axis_label, dial_position))

        _print_errors_with_traceback(errors, device_type="motor")

    for table in tables:
        print("")
        print(_tabulate(table))


def wut(**kwargs):
    """
    Displays the time and all positions (Where User) in user units
    """
    print(isotime())
    wu(**kwargs)


def wat(**kwargs):
    """
    Displays the time and all positions (Where All) in user and dial units
    """
    print(isotime())
    wa(**kwargs)

