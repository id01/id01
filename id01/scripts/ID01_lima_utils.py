from bliss.controllers.lima import roi as lima_roi
from bliss import setup_globals as SG
from bliss import current_session

import id01.scripts.ID01_utils as lutils
import numpy as np

cosd = lambda alpha : np.cos(np.radians(alpha))


def lima_set_arcRoi(detObj,roiName,cenx,ceny,radInner,radOuter,startAng,endAng):
    """
    define an arcRoi for a detector
    """
    detObj.roi_counters.set(roiName,lima_roi.ArcRoi(cenx,ceny,radInner,radOuter,startAng,endAng))

def lima_set_Roi(detObj,roiName,cenx,ceny,width,height,mode = None):
    """
    define a standard Roi for a detector
    """
    posx = cenx-width/2
    posy = ceny-width/2

    if mode==None:
        detObj.roi_counters.set(roiName,lima_roi.Roi(posx,posy,width,height))
    else:
        #breakpoint()
        #detObj.roi_counters.set(roiName,lima_roi.RoiProfile(posx,posy,width,height,mode=mode))
        #TODO doesnt accept this due to missing roi_values - not clear why as Seb
        pass



def twoThetaCalc(lattice_parameter, nrj):
    """
    lattice paramter: metres
    nrj: keV
    """
    wavelength = (12.3984/nrj)*1e-10
    return np.arcsin(wavelength/(2*lattice_parameter))/np.pi*180


def plot_powder_rings(detObj,dict_ids_lattice_pars,dict_ids_widths,nrj,deltaMne="delta",nuMne="nu",buffer_edge_pixel=0.1,plot_verbose=False):
    """
    goal is to draw circular ROIs on the detector to facilitate powder ring identification
    buffer_edge_pixel = 0.1, is the amount to clip the angle definitions of the arcRoi should it collide with outside of LIMAs capability to capture arcRois
    """

    # add test to make sure detector in measurement group
    globals_dict = current_session.env_dict
    if detObj.name == "mpx1x4" or detObj.name == "mpxgaas" or detObj.name == "simcam1":
        detcalibObj = globals_dict["detcalib_"+detObj.name]
        metadataObj = globals_dict["metadata_"+detObj.name]
        #exec(f"detcalibObj = SG.detcalib_{detObj.name}")
    else:
        try:
            exec(f"detcalibObj =  SG.detcalib_{detObj.name}_huber")
        except:
            exec(f"detcalibObj = SG.detcalib_{detObj.name}_bigpipe")

    
    pxl_size = np.array([metadataObj.x_pixel_size,metadataObj.y_pixel_size])
    image_size = np.array([detObj.image.width,detObj.image.height])

    #two_theta_array = lutils.create_two_theta_array(np.zeros(image_size),deltaMne=deltaMne,nuMne=nuMne)
    beam_cen = np.array([detcalibObj.beam_center_x,detcalibObj.beam_center_y])


    for id,latt in dict_ids_lattice_pars.items():
        #breakpoint()
        twoTheta = twoThetaCalc(latt, nrj)

        solid_angle = (np.arctan((image_size*pxl_size)/detcalibObj.distance)/np.pi)*180
        pxlperdeg = image_size/solid_angle
        #pxl_array = two_theta_array/pxlperdeg
        # convert

        delta = globals_dict[deltaMne].position
        nu = globals_dict[nuMne].position
        
        pxlperdeg = np.array((pxlperdeg[0]*cosd(delta), pxlperdeg[1]))

        # detector top left DTL vector from reciprocal space origin RSO, 
        # central pixel CP vector (y axis inverted) from DTL, 
        # BC vector, central pixel from RS0
        
        CPv = beam_cen
        BCv = np.array([nu*pxlperdeg[0],delta*pxlperdeg[1]])*np.array([-1,1]) # nu flipped
        DTLv = -CPv*np.array([1,-1]) + BCv

        # central pixel to RS0 radius
        radius_beam_cen = np.sum(BCv**2)**0.5
        det_twoTheta = np.arccos(np.cos(delta*np.pi/180)*np.cos(nu*np.pi/180))*180/np.pi
        pxlperdeg_radius = radius_beam_cen/det_twoTheta
        radius = twoTheta*pxlperdeg_radius

        radius = radius_beam_cen
        radInner,radOuter = radius - (dict_ids_widths[id]*pxlperdeg_radius)/2., radius + (dict_ids_widths[id]*pxlperdeg_radius)/2.

        print(pxlperdeg,delta,nu,DTLv,CPv,BCv,radius_beam_cen,det_twoTheta,pxlperdeg_radius,radius)
        #(x-h)**2+(y-k)**2=r**2 for a circle at center h,k

        # we have a circle centered at RS0
        # find the intersection with a square at DTLv 
        # with xmin,xmax,ymin,ymax of the detector
        xmin,xmax,ymin,ymax = [DTLv[0],DTLv[0]+image_size[0],DTLv[1],DTLv[1]-image_size[1]] # flipped y axis
        # round to nearest integer pixel
        xmin,xmax,ymin,ymax = int(xmin),int(xmax),int(ymin), int(ymax)

        #find intersection of circle and edges of the detector
        # define the detector edges using corners
        # RS0 is te zero coordinate

        det_extents_lines = [[(xmin,ymin),(xmin,ymax)],[(xmin,ymin),(xmax,ymin)],[(xmax,ymin),(xmax,ymax)],[(xmin,ymax),(xmax,ymax)]]
        cenPix_direct_beam = np.array([0,0])

        print(det_extents_lines)

        int_pts_inner = []
        for line_pts in det_extents_lines:
            int_pts_inner += circle_line_segment_intersection(cenPix_direct_beam, radInner, line_pts[0], line_pts[1],full_line=False)

            print(cenPix_direct_beam, radInner, line_pts[0], line_pts[1])
            try:
                print(int_pts_inner[-1])
            except:
                pass
        int_pts_outer = []
        for line_pts in det_extents_lines:
            int_pts_outer += circle_line_segment_intersection(cenPix_direct_beam, radOuter, line_pts[0], line_pts[1],full_line=False)

            print(cenPix_direct_beam, radOuter, line_pts[0], line_pts[1])
            try:
                print(int_pts_outer[-1])
            except:
                pass

        #in pixel coordinates in the detector frame define the detector and the circles, also define the angle around the beam, zero being horizontal right handed along the beam
        
        #breakpoint()
        if len(int_pts_inner)<2 or len(int_pts_outer)<2:
            print("ROI does not intersect the detector")
            return
        
        # convert intercepts to the direct beam coordinate system, i.e. + pxl_beam_cen_array_corner
        #x,y = pxl_beam_cen_array_corner[0]+x,pxl_beam_cen_array_corner[1]-y

        if plot_verbose:
            import matplotlib.pyplot as plt
            from matplotlib.patches import Rectangle

            fig, ax = plt.subplots()
            [ax.plot(x,y,'bo') for x,y in int_pts_inner]
            #ax.add_patch(Rectangle((DTLv[0], DTLv[1]), 516, 516))
            #ax.add_patch(plt.Circle((0, 0), radius, color='r', alpha=0.5))

            [ax.plot(x,y,'bx') for x,y in int_pts_outer]
            ax.add_patch(Rectangle((DTLv[0], DTLv[1]-516), 516, 516))
            ax.add_patch(plt.Circle((0, 0), radius, color='r', alpha=0.5))
            ax.set_ylim((-1500,1500))
            ax.set_xlim((-1500,1500))
            #ax.invert_yaxis()
            plt.show()

        # detector theta definition start on the x axis and rotates clockwise
        angles_inner = [(np.arctan2(x,y)/np.pi)*180+270 for x,y in int_pts_inner]
        angles_outer = [(np.arctan2(x,y)/np.pi)*180+270 for x,y in int_pts_outer]

        angles_inner.sort()
        angles_outer.sort()

        if np.abs(angles_inner[0]-angles_inner[1]) > np.abs(angles_outer[0]-angles_outer[1]):
            #use outer
            angles = angles_outer
        else:
            angles = angles_inner
        for angle in angles:
            if angle > 360.0:
                angles.remove(angle)
                angles+=[angle-360]

        angles.sort()

        # buffer edge angles 
        angles_cropped=[round_up(angles[0],1)+buffer_edge_pixel,round_down(angles[1],1)-buffer_edge_pixel]
        #locate the intersection of the lines of the exterior of the detector  with the circles radInner/radOuter

        # make logic tests to choose the parameters for the arcRoi
        # TODO account for when detector is close and ROI doesnt collide with the edges of the detector.

        # setting Arc ROI relative to the top left corner of the detector
        print(f"lima_set_arcRoi({detObj.name},\'{id}\',{CPv[0]-BCv[0]},{CPv[1]-BCv[1]*-1},{radInner},{radOuter},{angles_cropped[0]},{angles_cropped[1]})")
        lima_set_arcRoi(detObj,id,CPv[0]-BCv[0],CPv[1]-BCv[1]*-1,radInner,radOuter,angles_cropped[0],angles_cropped[1])

import math

def round_up(n, decimals=0): 
    multiplier = 10 ** decimals 
    return math.ceil(n * multiplier) / multiplier

def round_down(n, decimals=0):
    multiplier = 10 ** decimals
    return math.floor(n * multiplier) / multiplier

def circle_line_segment_intersection(circle_center, circle_radius, pt1, pt2, full_line=True, tangent_tol=1e-9):
    """ Find the points at which a circle intersects a line-segment.  This can happen at 0, 1, or 2 points.

    :param circle_center: The (x, y) location of the circle center
    :param circle_radius: The radius of the circle
    :param pt1: The (x, y) location of the first point of the segment
    :param pt2: The (x, y) location of the second point of the segment
    :param full_line: True to find intersections along full line - not just in the segment.  False will just return intersections within the segment.
    :param tangent_tol: Numerical tolerance at which we decide the intersections are close enough to consider it a tangent
    :return Sequence[Tuple[float, float]]: A list of length 0, 1, or 2, where each element is a point at which the circle intercepts a line segment.

    Note: We follow: http://mathworld.wolfram.com/Circle-LineIntersection.html
    """

    (p1x, p1y), (p2x, p2y), (cx, cy) = pt1, pt2, circle_center
    (x1, y1), (x2, y2) = (p1x - cx, p1y - cy), (p2x - cx, p2y - cy)
    dx, dy = (x2 - x1), (y2 - y1)
    dr = (dx ** 2 + dy ** 2)**.5
    big_d = x1 * y2 - x2 * y1
    discriminant = circle_radius ** 2 * dr ** 2 - big_d ** 2

    if discriminant < 0:  # No intersection between circle and line
        return []
    else:  # There may be 0, 1, or 2 intersections with the segment
        intersections = [
            (cx + (big_d * dy + sign * (-1 if dy < 0 else 1) * dx * discriminant**.5) / dr ** 2,
             cy + (-big_d * dx + sign * abs(dy) * discriminant**.5) / dr ** 2)
            for sign in ((1, -1) if dy < 0 else (-1, 1))]  # This makes sure the order along the segment is correct
        if not full_line:  # If only considering the segment, filter out intersections that do not fall within the segment
            fraction_along_segment = [(xi - p1x) / dx if abs(dx) > abs(dy) else (yi - p1y) / dy for xi, yi in intersections]
            intersections = [pt for pt, frac in zip(intersections, fraction_along_segment) if 0 <= frac <= 1]
        if len(intersections) == 2 and abs(discriminant) <= tangent_tol:  # If line is tangent to circle, return just one point (as both intersections have same location)
            return [intersections[0]]
        else:
            return intersections