# ID01 BLISS git repo
#       authors:
#         Peter Boesecke, boesecke@esrf.fr
#
#       Created at: 20. Apr 14:36:00 CET 2022
#
#----------------------------------------------------------------------
# Description:
#       class DoubleAxisRotationMm(DoubleAxisRotation)
#
# History:
#   2022-05-23  PB DoubleAxisRotationMm(DoubleAxisRotation)
#
#----------------------------------------------------------------------

import numpy
from bliss.controllers.motor import CalcController
from bliss.common.axis import Axis as AxisBase
from id01.scripts.optics.ID01_doubleaxisrotation  import *

import logging
#logging.basicConfig(level=logging.DEBUG)
#logging.basicConfig(level=logging.WARNING)
#logging.debug('This will get logged')
#logging.warning('This will get logged')

_log = logging.getLogger("id01.scripts.optics.ID01_doubleaxisrotation_mm")
#The log level can be set in the bliss session after import logging
#and defining _log as in the above command.
#_log.setLevel("DEBUG")

class DoubleAxisRotationMm(DoubleAxisRotation):
    """
    ID01 multilayer monochromator (mm)
    """
    def __init__(self,config,*args,**kwargs):
        super().__init__(config,*args,**kwargs)

        if 'Params' in config:
            self.Params=config['Params']
        else:
            self.default()
            self.savetoyml()

    def default(self):
        '''
        Set ID01 mm doubleaxis rotation parameters to default values
        '''
        # The translation motors of each device must be defined as
        # tagged motors in <device>.yml and the calculated motors must be
        # specified for each <device> in stripe_dict, e.g.
        # stripe_dict["translation1"]='axis1' for using the gap
        # motor specified in <device>.yml:
        #  - name: $<axis1>
        #    tags: real translation1
        #  - name: $<axis2>
        #    tags: real translation1
        # ...
        #  - name: tr
        #    tags: translation
        #  - name: rt
        #    tags: rotation
        # ...

        Params={}
        Params["A"]=1.5  # m
        Params["B"]=0.385922  # m
        Params["TRCOR"]=0.0 # mm
        Params["RTCOR"]=0.0 # mrad
        Params["T1TAG"]='translation1' # t1 motor tag
        Params["T2TAG"]='translation2' # t2 motor tag
        Params["TRTAG"]='translation'  # translation motor tag
        Params["RTTAG"]='rotation'     # rotation motor tag
        self.Params=Params
