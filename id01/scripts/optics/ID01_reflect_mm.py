#       authors:
#         Peter Boesecke, boesecke@esrf.fr
#
#       Created at: 20. Apr 14:36:00 CET 2022
#
#----------------------------------------------------------------------
# Description:
#       class ReflectMm(Reflect)
#
# History:
#   2022-05-23  PB ReflectMm(Reflect)
#
#----------------------------------------------------------------------

import numpy
from bliss.controllers.motor import CalcController
from bliss.common.axis import Axis as AxisBase
from id01.scripts.optics.ID01_reflect  import * 

import logging
#logging.basicConfig(level=logging.DEBUG)
#logging.basicConfig(level=logging.WARNING)
#logging.debug('This will get logged')
#logging.warning('This will get logged')

_log = logging.getLogger("id01.scripts.optics.ID01_reflect_mm")
#The log level can be set in the bliss session after import logging
#and defining _log as in the above command.
#_log.setLevel("DEBUG")

class ReflectMm(Reflect):
    """
    ID01 multilayer monochromator (mm)
    """
    def __init__(self,config,*args,**kwargs):
        super().__init__(config,*args,**kwargs)
        '''
        The bragg parameters of each stripe must be defined
        in reflect_dict. The following parameters are needed:
        coating, thickness1_nm, material1, thickness2_nm, material2, type
        '''

        if 'reflect_dict' in config and 'opmode' in config and 'NRJTAG' in config \
          and 'refract_data' in config:
            self.reflect_dict=config['reflect_dict']
            self.refract.data=config['refract_data']
            self.NRJTAG=config['NRJTAG']
            self.opmode=config['opmode']
        else:
            self.default()
            self.savetoyml()

    def default(self):
        '''
        Set ID01 mm reflection parameters to default values
        '''

        # for selecting a specific nrj motor with ID01_nrj
        # all nrj-motors must have different names and tags
        self.NRJTAG='mmnrj'            # mmnrj motor tag

        # The bragg parameters of each stripe must be specified in reflect_dict.
        self.reflect_dict={}

        #2nm0
        Stripe={}
        Stripe["coating"]="Ni96V4_2nm0"       # coating material (optional)
        Stripe["thickness1_nm"]=1.98873*0.5   # thickness_nm of layer 1 in nm (d_spacing, if crystal)
        Stripe["material1"]="Ni96V4_2nm0"     # layer 1 material (refract.py)
        Stripe["thickness2_nm"]=1.98873*0.5   # thickness_nm of layer 2 in nm (here, only a single layer)
        Stripe["material2"]="B4C_2nm0"        # layer 2 material (refract.py)
        Stripe["type"]="multilayer"           # monochromator type
        self.reflect_dict["2nm0"]=Stripe

        #2nm5
        Stripe={}
        Stripe["coating"]="Ni96V4_2nm5"       # coating material (optional)
        Stripe["thickness1_nm"]=2.5117*0.47   # thickness_nm of layer 1 in nm (d_spacing, if crystal)
        Stripe["material1"]="Ni96V4_2nm5"     # layer 1 material (refract.py)
        Stripe["thickness2_nm"]=2.5117*0.53   # thickness_nm of layer 2 in nm (here, only a single layer)
        Stripe["material2"]="B4C_2nm5"        # layer 2 material (refract.py)
        Stripe["type"]="multilayer"           # monochromator type
        self.reflect_dict["2nm5"]=Stripe

        # The refract parameters of each material must be specified with
        # self.refract.setup(Z,A[g/mol],rho[g/cm3]).
        self.refract.data={}  # remove unused parameters
        self.refract.setup("Ni96V4_2nm0",2780,5838.01,6.75)
        self.refract.setup("B4C_2nm0",26,55.254,2.9)
        self.refract.setup("Ni96V4_2nm5",2780,5838.01,7.25)
        self.refract.setup("B4C_2nm5",26,55.254,3.2)

        self.opmode=0 # 0: adjust gap if nrj changes
                      # 1: just calculate nrj from alpha
                      # do not move alpha when nrj is changed
