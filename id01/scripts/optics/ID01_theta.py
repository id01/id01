# -*- coding: utf-8 -*-

# ID01 BLISS git repo
#       authors:
#         Peter Boesecke, boesecke@esrf.fr
#
#       Created at: 29. Apr 12:36:00 CET 2022
#
#----------------------------------------------------------------------
# Description:
#       class Theta(CalcController)
#
# History:
#   2022-04-29  PB Theta(CalcController)
#   2023-01-17 PB class Axis -> CalcAxis
#
#----------------------------------------------------------------------

import numpy
from bliss.controllers.motor import CalcController
from bliss.common.axis import CalcAxis as AxisBase

import logging
#logging.basicConfig(level=logging.DEBUG)
#logging.basicConfig(level=logging.WARNING)
#logging.debug('This will get logged')
#logging.warning('This will get logged')

_log = logging.getLogger("id01.scripts.optics.ID01_theta")
#The log level can be set in the bliss session after import logging
#and defining _log as in the above command.
#_log.setLevel("DEBUG")

class Theta(CalcController):
    """
    Duplicating motor alpha [mrad] as theta[deg]
    """
    def __init__(self,config,*args,**kwargs):
        super().__init__(config,*args,**kwargs)

        Params={}
        Params["ALFTAG"]='alpha'             # [mrad] alpha motor tag
        Params["THETAG"]='theta'             # [deg] theta motor tag
        self.Params=Params

        self.EPS=1e-32

        # do not cache _set_position
        ##self.axis_settings.persistent_setting['_set_position'] = False

    def calc_from_real(self, level1_positions):
        '''
        Calculate theta from alpha
        '''
        _log.debug("  calc_from_real ({}) BEGIN".format(level1_positions))

        ALFTAG = self.Params["ALFTAG"]
        THETAG = self.Params["THETAG"]

        alpha_mrad = level1_positions[ALFTAG]

        theta_deg = alpha_mrad*(0.180/numpy.pi)

        calc_level2_positions={THETAG:theta_deg}

        _log.debug("  calc_from_real END ({})".format(calc_level2_positions))

        return calc_level2_positions

    def calc_to_real(self, level2_positions):
        '''
        Calculate alpha from nrj
        '''
        _log.debug("  calc_to_real BEGIN ({})".format(level2_positions))

        ALFTAG = self.Params["ALFTAG"]
        THETAG = self.Params["THETAG"]

        theta_deg = level2_positions[THETAG]

        alpha_mrad = theta_deg*(numpy.pi/0.180)

        calc_level1_positions={ALFTAG:alpha_mrad}

        _log.debug("  calc_to_real END ({})".format(calc_level1_positions))

        return calc_level1_positions

class CalcAxis(AxisBase):
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        # calculated motor positions do not need to be buffered
        # => disable buffering
        self.settings.disable_cache("_set_position") #disable target pos
        self.settings.disable_cache("position") #disable current pos
        self.settings.disable_cache("state") #disbale the state



