# ID01 BLISS git repo
#       authors:
#         Peter Boesecke, boesecke@esrf.fr
#
#       Created at: 20. Apr 14:36:00 CET 2022
#
#----------------------------------------------------------------------
# Description:
#       class BeamOffset(CalcController)
#
# History:
#   2022-04-20 PB BeamOffset(CalcController)
#   2022-06-16 PB dumping doubleaxisrotation setup if existing
#   2023-01-17 PB class Axis -> CalcAxis
#
#----------------------------------------------------------------------

import numpy
from bliss.controllers.motor import CalcController
from bliss.common.axis import CalcAxis as AxisBase

import logging
#logging.basicConfig(level=logging.DEBUG)
#logging.basicConfig(level=logging.WARNING)
#logging.debug('This will get logged')
#logging.warning('This will get logged')

_log = logging.getLogger("id01.scripts.optics.ID01_beamoffset")
#The log level can be set in the bliss session after import logging
#and defining _log as in the above command.
#_log.setLevel("DEBUG")

class BeamOffset(CalcController):
    """
    The beam offset of two reflecting parallel surfaces is calculated from
    the 'gap' between the surfaces and for the current inclination angle 'rotation'
    (measured relative to the incident beam):
    
             gap[mm]: gap between the surfaces
      rotation[mrad]: incidence angle with respect to the incident beam

    The active stripe is determined from the position of the 'selection' motor.

    Parameters
              GAPCOR[mm] : gap_mm=gap+GAPCOR
            ALFCOR[mrad] : alpha_mrad=rotation+ALFCOR

    The motors 'beamoffset' [mrad] and 'alpha' [mrad] are calculated from the real motors 
    'gap' and 'rotation' in the following way using the parameters of the active stripe:

         alpha[mrad] = rotation+ALFCOR
      beamoffset[mm] = 2.0*(gap+GAPCOR)*cos(alpha*0.001)

    The real motors gap and rotation are calculated from 'beamoffset' and 'alpha' 
    in the following way using the parameters of the active stripe:

      rotation[mrad] = alpha-ALFCOR
             gap[mm] = beamoffset/(2.0*cos(alpha*0.001))-GAPCOR

    """
    def __init__(self,config,*args,**kwargs):
        super().__init__(config,*args,**kwargs)
        '''
        The following parameters must be defined for each stripe:
        ts_min, ts_in, ts_max, GAPCOR, ALFCOR, GAPTAG, ROTTAG, BOFTAG, ALFTAG
        The following parameter is optional:
        ROFTAG
        '''
        self._config=config

        self.SELTAG='selection'
        self.LISTMODE="auto"
        self.beamoffset_dict={}
        self.opmode=0                        # 0: normal, 1: do not move gap with rotation
        self.EPS=1e-32

        # do not cache _set_position
        ##self.axis_settings.persistent_setting['_set_position'] = False

        # The real and calculated motors must be defined as tagged motors 
        # in <device>.yml.
        # motors specified in <device>.yml:
        #  - name: $<axis1>
        #    tags: real gap
        #
        #  - name: $<axis2>
        #    tags: real rotation
        #
        #  - name: $<axis3>
        #    tags: real selection
        # ...
        #  - name: alpha
        #    tags: alpha
        #
        #  - name: beamoffset
        #    tags: beamoffset
        # ...

        ### The motors are defined as tagged motors in <device>.yml
        ###DCM
        ##Stripe={}
        ##Stripe["ts_min"]=-10.            # minimum ts position
        ##Stripe["ts_in"]=3.5              # ts insertion position
        ##Stripe["ts_max"]=25              # maximum ts position
        ##Stripe["GAPCOR"]=0.0             # [mm] actual gap = A[gap]|gap + gap_offset
        ##Stripe["ALFCOR"]=4.546582701     # [mrad] alpha_mrad = rotation_mrad+ALFCOR (th_offset_deg=0.2605_deg)
        ##Stripe["GAPTAG"]='gap'           # gap value in mm or motor tag
        #                                  # if gap is a string, it will be used as motor mnemonic
        ##Stripe["ROTTAG"]='rotation'
        ##Stripe["BOFTAG"]='beamoffset'
        ##Stripe["ALFTAG"]='alpha'
        ##self.beamoffset_dict["dcm"]=Stripe
        ###CC
        ##Stripe={}
        ##Stripe["ts_min"]=25.             # minimum ts position
        ##Stripe["ts_in"]=47.              # ts insertion position
        ##Stripe["ts_max"]=50.             # maximum ts position
        ##Stripe["GAPCOR"]=0.0             # [mm] actual gap = A[gap]|gap + gap_offset in mm
        ##Stripe["ALFCOR"]=4.546582701     # [mrad] alpha_mrad = rotation_mrad+ALFCOR (th_offset_deg=0.314983_deg)
        ##Stripe["GAPTAG"]=8.5             # gap value in mm or motor tag
        ##Stripe["ROTTAG"]='rotation'
        ##Stripe["BOFTAG"]='beamoffset'
        ##Stripe["ALFTAG"]='alpha'
        ##self.beamoffset_dict["cc"]=Stripe
        ###...

    def get_current_stripe(self):
        '''
        Returns the name of the first found stripe with ts position between
        ts_min and ts_max.
        '''
        selection_motor_position=self._tagged[self.SELTAG][0].position
        ####_log.debug("      selection_motor_position={}".format(selection_motor_position))
        return self.get_stripe_name(selection_motor_position)

    def get_stripe_name(self, selection_motor_position):
        _log.debug("  get_stripe_name BEGIN")
        stripe_name=None

        if (self.LISTMODE == "auto"):
            for key in self.beamoffset_dict:
                Stripe=self.beamoffset_dict[key]
                ts_min=Stripe["ts_min"]
                ts_max=Stripe["ts_max"]
                if (ts_min < selection_motor_position) and (selection_motor_position <= ts_max):
                    stripe_name=key
                    _log.debug("      stripe_name={}".format(stripe_name))
                    break
        else:
            stripe_name=self.LISTMODE

        _log.debug("  get_stripe_name END ({})".format(stripe_name))
        return stripe_name

    def go_to_stripe(self, stripe_name=None):
        '''
        Moves the monochromator to stripe_name
        '''

        SELTAG=self.SELTAG

        if (stripe_name is not None) and (stripe_name in self.beamoffset_dict):
            Stripe=self.beamoffset_dict[stripe_name]
            ts_in=Stripe["ts_in"]

            ts_motor=self._tagged[SELTAG][0]

            ## input("{}.move({})".format(ts_motor.name,ts_in))  #++++++++++

            ts_motor.move(ts_in)
        else:
            Stripes=self.stripes()
            print("Available stripes: ",end='')
            sep=""
            for key in Stripes:
               print('{}"{}"'.format(sep,key),end='')
               sep=", "
            print("")

        print("Current stripe is {}".format(self.get_current_stripe()))

    def stripes(self):
        stripe_list=[]
        for key in self.beamoffset_dict:
            Stripe=self.beamoffset_dict[key]
            stripe_list.append(key)
        return(stripe_list)

    def now(self):
        print(self.get_current_stripe())

    def listmode(self,mode=None):
        """
        Select the stripe used for calculations, default is 'auto'.
        """
        if mode is not None:
            if mode in self.beamoffset_dict:
                self.LISTMODE=mode
            else:
                self.LISTMODE="auto"
        mode=self.LISTMODE
        return(mode)

    def mode(self,opmode=None):
        '''
        0: keep beamoffset constant when moving alpha (default)
        1: do not fix beamoffset, do not change gap
        '''
        stripe_name=self.get_current_stripe()
        if stripe_name is None:
            opmode=-1 # undefined
        else:
            Stripe=self.beamoffset_dict[stripe_name]

            try:
                gap=float(Stripe["GAPTAG"])
                opmode=2
            except ValueError:
                opmode=self._mode(opmode)
                gap=None

        print("The beamoffset mode is {}".format(opmode))
        if opmode is 1:
            print("Moving alpha does not keep the beamoffset. The beamoffset cannot be moved.")
        elif opmode is 0:
            print("Moving alpha keeps the beamoffset. The beamoffset can be moved.")
        elif opmode is 2:
            print("The gap is not motorized. It is {} mm.".format(gap))
        else:
            print("Unknown stripe, unknown gap")

    def _mode(self,opmode=None):
        if opmode is not None:
            self.opmode=opmode
        return(self.opmode)

    def show(self):
        print("SELTAG : '{}'".format(self.SELTAG))
        for stripe in self.beamoffset_dict:
            Stripe = self.beamoffset_dict[stripe]
            print("{}:".format(stripe))
            for key in Stripe:
                print("    {:10} : {}".format(key,Stripe[key]))

    def dump(self,motor=None):
        motor_name="motor" if motor is None else motor.name
        print("{}.controller.SELTAG='{}'".format(motor_name,self.SELTAG))
        rotation_motor_dumpstr=set()
        rotation_motor_name=''
        for stripe in self.beamoffset_dict:
            # dump beamoffset config
            print('{}.controller.beamoffset_dict["{}"]={}'.format(motor_name,stripe,self.beamoffset_dict[stripe]))
            # dump doubleaxisrotation config, if used
            ROTTAG=self.beamoffset_dict[stripe]['ROTTAG']
            rotation_motor=self._tagged[ROTTAG][0]
            rotation_motor_name=rotation_motor.name
            # if the rotation motor has a dumpstr function the output string must be printed
            if hasattr(rotation_motor.controller,'dumpstr'):
                rotation_motor_dumpstr.add(rotation_motor.controller.dumpstr(rotation_motor_name))

        for element in rotation_motor_dumpstr:
            print(element)

        if len(rotation_motor_dumpstr)>0:
            print('#{}.controller.savetoyml()'.format(rotation_motor_name))

        print("{}.controller.mode({})".format(motor_name,self._mode()))
        print('{}.controller.listmode("{}")'.format(motor_name,self.listmode()))

        print('#{}.controller.savetoyml()'.format(motor_name))
        print('#{}.controller.go_to_stripe("{}")'.format(motor_name,self.get_current_stripe()))
        

    def calc_from_real(self, level1_positions):
        '''
        Calculate the pseudo motors 'alpha' [mrad], 'beamoffset' [mm] and 'reflectionoffset' [mm]
        from level1_positions 'rotation' and 'gap'.
           alpha[mrad] = rotation+ALFCOR
        beamoffset[mm] = 2.0*(gap+GAPCOR)*cos(alpha*0.001)
        reflectionoffset[mm] = (gap+GAPCOR)/tan(alpha*0.001)
        reflectionoffset is only calculated if ROFTAG is defined in Stripe
        '''
        _log.debug("  calc_from_real BEGIN ({})".format(level1_positions))

        calc_level2_positions={}

        stripe_name=self.get_current_stripe()
        if stripe_name is not None:
            Stripe=self.beamoffset_dict[stripe_name]

            GAPCOR_mm   = Stripe["GAPCOR"]
            ALFCOR_mrad = Stripe["ALFCOR"]
            GAPTAG      = Stripe["GAPTAG"]
            ROTTAG      = Stripe["ROTTAG"]
            BOFTAG      = Stripe["BOFTAG"]
            ALFTAG      = Stripe["ALFTAG"]
            if "ROFTAG" in Stripe:
                ROFTAG = Stripe["ROFTAG"]
            else:
                ROFTAG = None

            _log.debug("    GAPCOR_mm ={}, ALFCOR_mrad={}, GAPTAG={}, ROTTAG={}, BOFTAG={}, ALFTAG={}, ROFTAG={}".\
              format(GAPCOR_mm,ALFCOR_mrad,GAPTAG,ROTTAG,BOFTAG,ALFTAG,ROFTAG))

            #Get input positions
            try:
                gap = float(GAPTAG)
            except ValueError:
                gap = level1_positions[GAPTAG]
            rotation = level1_positions[ROTTAG]

            gap_mm = gap + GAPCOR_mm

            alpha_mrad = rotation+ALFCOR_mrad
            beamoffset_mm  = 2.0*gap_mm*numpy.cos(alpha_mrad*0.001)

            if ROFTAG is None:
                calc_level2_positions={BOFTAG:beamoffset_mm,ALFTAG:alpha_mrad}
            else:
                tanalpha = numpy.tan(alpha_mrad*0.001)
                if abs(tanalpha) > self.EPS:
                    reflectionoffset_mm = gap/tanalpha
                    calc_level2_positions={BOFTAG:beamoffset_mm,ALFTAG:alpha_mrad,ROFTAG:reflectionoffset_mm}
                else:
                    calc_level2_positions={BOFTAG:beamoffset_mm,ALFTAG:alpha_mrad}


        _log.debug("  calc_from_real END ({})".format(calc_level2_positions))

        return calc_level2_positions

    def calc_to_real(self, level2_positions):
        '''
        Calculate the motors 'gap' [mm] and 'rotation' [mrad]
        from the level2_positions alpha' and 'beamoffset'

          rotation[mrad] = alpha-ALFCOR
                 gap[mm] = beamoffset/(2.0*cos(alpha*0.001))-GAPCOR
        '''
        _log.debug("  calc_to_real BEGIN ({})".format(level2_positions))

        calc_level1_positions={}

        stripe_name=self.get_current_stripe()
        if stripe_name is not None:
            Stripe=self.beamoffset_dict[stripe_name]

            GAPCOR_mm   = Stripe["GAPCOR"]
            ALFCOR_mrad = Stripe["ALFCOR"]
            GAPTAG      = Stripe["GAPTAG"]
            ROTTAG      = Stripe["ROTTAG"]
            BOFTAG      = Stripe["BOFTAG"]
            ALFTAG      = Stripe["ALFTAG"]

            _log.debug("    GAPCOR_mm ={}, ALFCOR_mrad={}, GAPTAG={}, ROTTAG={}, BOFTAG={}, ALFTAG={}".\
              format(GAPCOR_mm,ALFCOR_mrad,GAPTAG,ROTTAG,BOFTAG,ALFTAG))

            #Get input positions
            alpha_mrad = level2_positions[ALFTAG]
            beamoffset_mm = level2_positions[BOFTAG]

            _log.debug("    beamoffset_mm={}, alpha_mrad={}".format(beamoffset_mm,alpha_mrad))

            rotation = alpha_mrad-ALFCOR_mrad

            try:
                gap = float(GAPTAG)
                gap_motor = None
            except ValueError:
                gap_motor = self._tagged[GAPTAG][0]
                gap = None

            # 0: normal, otherwise do not adjust gap to rotation
            _log.debug("        opmode is {}".format(self.opmode))
            if (self.opmode is 0) and (gap_motor is not None): # then adjust the gap:
                _log.debug("        moving the gap")
                gap = beamoffset_mm/(2.0*numpy.cos(alpha_mrad*0.001))-GAPCOR_mm
                calc_level1_positions={ROTTAG:rotation,GAPTAG:gap}
            else:
                _log.debug("        fixed gap")
                calc_level1_positions={ROTTAG:rotation}

        _log.debug("  calc_to_real END ({})".format(calc_level1_positions))

        return calc_level1_positions

    def savetoyml(self):
        '''
        Save current ID01 mir beamoffset parameters to yml file
        '''
        self._config['beamoffset_dict']=self.beamoffset_dict
        self._config.save()


class CalcAxis(AxisBase):
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        # calculated motor positions do not need to be buffered
        # => disable buffering
        self.settings.disable_cache("_set_position") #disable target pos
        self.settings.disable_cache("position") #disable current pos
        self.settings.disable_cache("state") #disbale the state

    def dump(self):
        self.controller.dump(self)

    def show(self):
        self.controller.show()

    def listmode(self,mode=None):
        return(self.controller.listmode(mode))

    def mode(self,opmode=None):
        self.controller.mode(opmode)

    def now(self):
        self.controller.now()

    def go(self,stripe_name=None):
        self.controller.go_to_stripe(stripe_name)

