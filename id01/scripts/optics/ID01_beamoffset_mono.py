#       authors:
#         Peter Boesecke, boesecke@esrf.fr
#
#       Created at: 20. Apr 14:36:00 CET 2022
#
#----------------------------------------------------------------------
# Description:
#       class BeamOffsetMono(BeamOffsetMono)
#
# History:
#   2022-05-16  PB BeamOffsetMono(BeamOffsetMono)
#   2022-06-16  PB ALFCOR->4.784209279 mrad
#
#----------------------------------------------------------------------

import numpy
from bliss.controllers.motor import CalcController
from bliss.common.axis import Axis as AxisBase
from id01.scripts.optics.ID01_beamoffset  import * 

import logging
#logging.basicConfig(level=logging.DEBUG)
#logging.basicConfig(level=logging.WARNING)
#logging.debug('This will get logged')
#logging.warning('This will get logged')

_log = logging.getLogger("id01.scripts.optics.ID01_beamoffset_mono")
#The log level can be set in the bliss session after import logging
#and defining _log as in the above command.
#_log.setLevel("DEBUG")

class BeamOffsetMono(BeamOffset):
    """
    ID01 double crystal monochromator (mono)
    """
    def __init__(self,config,*args,**kwargs):
        super().__init__(config,*args,**kwargs)

        if 'beamoffset_dict' in config:
            self.beamoffset_dict=config['beamoffset_dict']
        else:
            self.default()
            self.savetoyml()

    def default(self):
        '''
        Set ID01 mono beamoffset parameters to default values
        '''

        # replacing the default stripe definitions
        self.beamoffset_dict={}

        # The real and calculated motors must be defined as tagged motors 
        # in <device>.yml.
        # motors specified in <device>.yml:
        #  - name: $<axis1>
        #    tags: real gap
        #
        #  - name: $<axis2>
        #    tags: rotation
        #
        #  - name: $<axis3>
        #    tags: selection
        # ...
        #  - name: <alpha>
        #    tags: alpha
        #
        #  - name: <beamoffset>
        #    tags: beamoffset
        # ...

        # replacing the default stripe definitions
        self.beamoffset_dict={}
        # The motors are defined as tagged motors in <device>.yml
        #DCM
        #monolistadd stripe ts_min ts_in ts_max coating d_spacing/nm filling material1 material2 th_offset/deg
        #monolistadd dcm -10 3.5 25 Si 0.313521 1 Si Si 0.274115 dcm2ty 0 Si111
        Stripe={}
        Stripe["ts_min"]=-10.            # minimum ts position
        Stripe["ts_in"]=3.5              # ts insertion position
        Stripe["ts_max"]=25              # maximum ts position
        Stripe["GAPCOR"]=0.0             # [mm] actual gap = A[gap]|gap + gap_offset
        Stripe["ALFCOR"]=4.784209279     # [mrad] actual alpha = rotation_mrad+ALFCOR (th_offset_deg=0.274115_deg)
        Stripe["GAPTAG"]='gap'           # gap value in mm or motor tag
        #                                # if gap is a string, it will be used as motor mnemonic
        Stripe["ROTTAG"]='rotation'
        Stripe["BOFTAG"]='beamoffset'
        Stripe["ALFTAG"]='alpha'
        self.beamoffset_dict["dcm"]=Stripe
        #CC
        #monolistadd stripe ts_min ts_in ts_max coating d_spacing/nm filling material1 material2 th_offset/deg
        #monolistadd cc 25 47 50 Si 0.313567 1 Si Si 0.314983 8.5 0 Si111
        Stripe={}
        Stripe["ts_min"]=25.             # minimum ts position
        Stripe["ts_in"]=47.              # ts insertion position
        Stripe["ts_max"]=50.             # maximum ts position
        Stripe["GAPCOR"]=0.0             # [mm] actual gap = A[gap]|gap + gap_offset in mm
        Stripe["ALFCOR"]=5.497490438     # [mrad] actual alpha = rotation_mrad+ALFCOR (th_offset_deg=0.314983_deg)
 
        Stripe["GAPTAG"]=8.5             # gap value in mm or motor tag
        Stripe["ROTTAG"]='rotation'
        Stripe["BOFTAG"]='beamoffset'
        Stripe["ALFTAG"]='alpha'
        self.beamoffset_dict["cc"]=Stripe
        #...

