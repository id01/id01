# -*- coding: utf-8 -*-

# ID01 BLISS git repo
#       authors:
#         Peter Boesecke, boesecke@esrf.fr
#
#       Created at: 29. Apr 12:36:00 CET 2022
#
#----------------------------------------------------------------------
# Description:
#       class Nrj(CalcController)
#
# History:
#   2022-05-23  PB Nrj(CalcController)
#                  Link either mononrj or mmnrj with the motor nrj.
#   2022-06-02  PB after changing nrj-axis clear previously buffered positions
#
#----------------------------------------------------------------------

import numpy
from bliss.controllers.motor import CalcController
from bliss.common.axis import CalcAxis as AxisBase
from bliss.config import static

import logging
#logging.basicConfig(level=logging.DEBUG)
#logging.basicConfig(level=logging.WARNING)
#logging.debug('This will get logged')
#logging.warning('This will get logged')

_log = logging.getLogger("id01.scripts.optics.ID01_nrj")
#The log level can be set in the bliss session after import logging
#and defining _log as in the above command.
#_log.setLevel("DEBUG")


class Nrj(CalcController):
    """
    Using a tagged motor as nrj motor. The motor can be chosen
    with self.active(tag), e.g. if the bliss motor is called 
    'nrj' the command nrj.active('mononrj') links it with 'mononrj'
    and the command nrj.active('mmnrj') links it with 'mmnrj'.
    For selecting a specific nrj-motor nrj motors must have different tags,
    e.g. mononrj, mmnrj and mirnrj.
    """
    def __init__(self,config,*args,**kwargs):
        super().__init__(config,*args,**kwargs)

        Params={}
        Params["ACTIVETAG"]='mononrj'  # [keV] input nrj motor 
                                       # mononrj, mmnrj, mirnrj
        Params["NRJTAG"]='nrj'         # [keV] output nrj motor

        self.Params=Params

        self.EPS=1e-32

        # do not cache _set_position
        ##self.axis_settings.persistent_setting['_set_position'] = False

    def calc_from_real(self, level1_positions):
        '''
        Get nrj from ACTIVETAG
        '''
        _log.debug("  calc_from_real ({}) BEGIN".format(level1_positions))

        ACTIVETAG = self.Params["ACTIVETAG"]
        NRJTAG = self.Params["NRJTAG"]

        nrj_keV = level1_positions[ACTIVETAG]

        calc_level2_positions={NRJTAG:nrj_keV}

        _log.debug("  calc_from_real END ({})".format(calc_level2_positions))

        return calc_level2_positions

    def calc_to_real(self, level2_positions):
        '''
        Get nrj from NRJTAG
        '''
        _log.debug("  calc_to_real BEGIN ({})".format(level2_positions))

        # for selecting a specific nrj motor
        # all nrj-motors must have different names and tags
        ACTIVETAG = self.Params["ACTIVETAG"]
        NRJTAG = self.Params["NRJTAG"]

        nrj_keV = level2_positions[NRJTAG]

        calc_level1_positions={ACTIVETAG:nrj_keV}

        _log.debug("  calc_to_real END ({})".format(calc_level1_positions))

        return calc_level1_positions

    def dump(self):

        # for selecting a specific nrj motor 
        # all nrj-motors must have different names and tags
        ACTIVETAG = self.Params["ACTIVETAG"]
        NRJTAG = self.Params["NRJTAG"]

        out=self._tagged[NRJTAG][0]
        active=self._tagged[ACTIVETAG][0]

        print('{}.controller.Params["ACTIVETAG"]="{}"'.format(out.name,active.name))

    def active(self,activetag=None):
        # for selecting a specific nrj motor 
        # all nrj-motors must have different names and tags
        if activetag is not None:
            if self.Params["ACTIVETAG"] != activetag:
                self.Params["ACTIVETAG"]=activetag 
                # clear previously buffered positions
                NRJTAG = self.Params["NRJTAG"]
                out=self._tagged[NRJTAG][0]
                self.axes[out.name].settings.clear("_set_position")

        ACTIVETAG = self.Params["ACTIVETAG"]

        return(ACTIVETAG)
            
       
class CalcAxis(AxisBase):
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        # calculated motor positions do not need to be buffered
        # => disable buffering
        self.settings.disable_cache("_set_position") #disable target pos
        self.settings.disable_cache("position") #disable current pos
        self.settings.disable_cache("state") #disbale the state

    def dump(self):
        self.controller.dump()

    def active(self,activetag=None):
        if activetag is None:
            print("mononrj | mmnrj | mirnrj")
        return(self.controller.active(activetag))


