#       authors:
#         Peter Boesecke, boesecke@esrf.fr
#
#       Created at: 20. Apr 14:36:00 CET 2022
#
#----------------------------------------------------------------------
# Description:
#       class ReflectMirror(Reflect)
#
# History:
#   2022-05-18  PB ReflectMirror(Reflect)
#
#----------------------------------------------------------------------

import numpy
from bliss.controllers.motor import CalcController
from bliss.common.axis import Axis as AxisBase
from id01.scripts.optics.ID01_reflect  import * 

import logging
#logging.basicConfig(level=logging.DEBUG)
#logging.basicConfig(level=logging.WARNING)
#logging.debug('This will get logged')
#logging.warning('This will get logged')

_log = logging.getLogger("id01.scripts.optics.ID01_reflect_mir")
#The log level can be set in the bliss session after import logging
#and defining _log as in the above command.
#_log.setLevel("DEBUG")

class ReflectMirror(Reflect):
    """
    ID01 double mirror (mir)
    """
    def __init__(self,config,*args,**kwargs):
        super().__init__(config,*args,**kwargs)
        '''
        The bragg parameters of each stripe must be defined
        in reflect_dict. The following parameters are needed:
        coating, thickness1_nm, material1, thickness2_nm, material2, type
        '''

        if 'reflect_dict' in config and 'opmode' in config and 'NRJTAG' in config \
          and 'refract_data' in config:
            self.reflect_dict=config['reflect_dict']
            self.refract.data=config['refract_data']
            self.NRJTAG=config['NRJTAG']
            self.opmode=config['opmode']
        else:
            self.default()
            self.savetoyml()

    def default(self):
        '''
        Set ID01 mir reflection parameters to default values
        '''
        self.opmode=1 # 1: just calculate nrj from alpha
                      # do not move alpha when nrj is changed

        # for selecting a specific nrj motor with ID01_nrj
        # all nrj-motors must have different names and tags
        self.NRJTAG='mirnrj'            # mirnrj motor tag

        # The refract parameters of each material must be specified with
        # self.refract.setup(Z,A[g/mol],rho[g/cm3]).
        self.refract.data={}  # remove unused parameters
        self.refract.setup("Si",14,28.086,2.33)
        self.refract.setup("W",74,183.92,19.3)
        self.refract.setup("Rh",45,102.92,12.44)
        self.refract.setup("Pt",78,195.09,21.37)
        self.refract.setup("W_7nm0",74,183.92,19.3)
        self.refract.setup("B4C_7nm0",26,55.254,3.2)

        # The bragg parameters of each stripe must be specified in reflect_dict.
        self.reflect_dict={}

        #Si
        Stripe={}
        Stripe["coating"]="Si"           # coating material (optional)
        self.reflect_dict["Si"]=Stripe
        #WB4C
        Stripe={}
        Stripe["coating"]="W"            # coating material (optional)
        Stripe["thickness1_nm"]=7.0*0.5  # thickness_nm of layer 1 in nm (d_spacing, if crystal)
        Stripe["material1"]="W_7nm0"     # layer 1 material (refract.py)
        Stripe["thickness2_nm"]=7.0*0.5  # thickness_nm of layer 2 in nm (here, only a single layer)
        Stripe["material2"]="B4C_7nm0"   # layer 2 material (refract.py)
        Stripe["type"]="multilayer"      # monochromator type
        self.reflect_dict["WB4C"]=Stripe
        #Rh
        Stripe={}
        Stripe["coating"]="Rh"           # coating material (optional)
        self.reflect_dict["Rh"]=Stripe
        #Pt
        Stripe={}
        Stripe["coating"]="Pt"           # coating material (optional)
        self.reflect_dict["Pt"]=Stripe

