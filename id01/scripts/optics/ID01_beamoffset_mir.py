#       authors:
#         Peter Boesecke, boesecke@esrf.fr
#
#       Created at: 20. Apr 14:36:00 CET 2022
#
#----------------------------------------------------------------------
# Description:
#       class BeamOffsetMirror(BeamOffset)
#
# History:
#   2022-05-16  PB BeamOffsetMirror(BeamOffset)
#   2022-05-24  PB config from/to device yml file
#
#----------------------------------------------------------------------

import numpy
from bliss.controllers.motor import CalcController
from bliss.common.axis import Axis as AxisBase
from id01.scripts.optics.ID01_beamoffset  import * 

import logging
#logging.basicConfig(level=logging.DEBUG)
#logging.basicConfig(level=logging.WARNING)
#logging.debug('This will get logged')
#logging.warning('This will get logged')

_log = logging.getLogger("id01.scripts.optics.ID01_beamoffset_mir")
#The log level can be set in the bliss session after import logging
#and defining _log as in the above command.
#_log.setLevel("DEBUG")

class BeamOffsetMirror(BeamOffset):
    """
    ID01 double mirror (mir)
    """
    def __init__(self,config,*args,**kwargs):
        super().__init__(config,*args,**kwargs)

        if 'beamoffset_dict' in config:
            self.beamoffset_dict=config['beamoffset_dict']
        else:
            self.default()
            self.savetoyml()

    def default(self):
        '''
        Set ID01 mir beamoffset parameters to default values
        '''

        # default stripe definitions
        self.beamoffset_dict={}
        # The motors are defined as tagged motors in <device>.yml
        #Si
        Stripe={}
        Stripe["ts_min"]=-24.9           # minimum ts position
        Stripe["ts_in"]=-24.0            # ts insertion position
        Stripe["ts_max"]=-14.0           # maximum ts position
        Stripe["GAPCOR"]=0.0             # [mm] actual gap = A[gap]|gap + gap_offset
        Stripe["ALFCOR"]=0.0             # [mrad] actual alpha = rotation_mrad+ALFCOR
        Stripe["GAPTAG"]='gap'           # gap value in mm or motor tag
        #                                # if gap is a string, it will be used as motor mnemonic
        Stripe["ROTTAG"]='rotation'
        Stripe["BOFTAG"]='beamoffset'
        Stripe["ALFTAG"]='alpha'
        self.beamoffset_dict["Si"]=Stripe
        # WB4C
        # mirlistadd stripe ts_min ts_in ts_max [coating [d_spacing/nm filling material1 material2 th_offset/de
        # mirlistadd WB4C -12.3 -10 -1.5 W 7 0.5 W_7nm0 B4C_7nm0 0
        Stripe={}
        Stripe["ts_min"]=-12.3           # minimum ts position
        Stripe["ts_in"]=-10.0            # ts insertion position
        Stripe["ts_max"]=-1.5            # maximum ts position
        # the d_spacing parameters should be defined in BraggMonoMirror...
        Stripe["GAPCOR"]=0.0             # [mm] actual gap = A[gap]|gap + gap_offset
        Stripe["ALFCOR"]=0.0             # [mrad] actual alpha = rotation_mrad+ALFCOR
        Stripe["GAPTAG"]='gap'           # gap value in mm or motor tag
        #                                # if gap is a string, it will be used as motor mnemonic
        Stripe["ROTTAG"]='rotation'
        Stripe["BOFTAG"]='beamoffset'
        Stripe["ALFTAG"]='alpha'
        self.beamoffset_dict["WB4C"]=Stripe
        # Rh
        # mirlistadd stripe ts_min ts_in ts_max
        # mirlistadd Rh 0 2 11 Rh
        Stripe={}
        Stripe["ts_min"]=0.0             # minimum ts position
        Stripe["ts_in"]=2.0              # ts insertion position
        Stripe["ts_max"]=11.0            # maximum ts position
        Stripe["GAPCOR"]=0.0             # [mm] actual gap = A[gap]|gap + gap_offset
        Stripe["ALFCOR"]=0.0             # [mrad] actual alpha = rotation_mrad+ALFCOR
        Stripe["GAPTAG"]='gap'           # gap value in mm or motor tag
        #                                # if gap is a string, it will be used as motor mnemonic
        Stripe["ROTTAG"]='rotation'
        Stripe["BOFTAG"]='beamoffset'
        Stripe["ALFTAG"]='alpha'
        self.beamoffset_dict["Rh"]=Stripe
        # Pt
        # mirlistadd Pt 12.3 24 24.4 Pt
        Stripe={}
        Stripe["ts_min"]=12.3            # minimum ts position
        Stripe["ts_in"]=24.0             # ts insertion position
        Stripe["ts_max"]=24.4            # maximum ts position
        Stripe["GAPCOR"]=0.0             # [mm] actual gap = A[gap]|gap + gap_offset
        Stripe["ALFCOR"]=0.0             # [mrad] actual alpha = rotation_mrad+ALFCOR
        Stripe["GAPTAG"]='gap'           # gap value in mm or motor tag
        #                                # if gap is a string, it will be used as motor mnemonic
        Stripe["ROTTAG"]='rotation'
        Stripe["BOFTAG"]='beamoffset'
        Stripe["ALFTAG"]='alpha'
        self.beamoffset_dict["Pt"]=Stripe
