# -*- coding: utf-8 -*-

# ID01 BLISS git repo
#       authors:
#         Steven Leake, leake@esrf.fr
#         Peter Boesecke, boesecke@esrf.fr
#
#       Created at: 30. Mar 12:36:00 CET 2022
#
#----------------------------------------------------------------------
# Description: 
#       class Reflect(CalcController)
#
# This module calculates the (Bragg) nrj from 
# from the (Bragg) incidence angle alpha and vice versa.
# The calculations are done in calc_from_real()
# and calc_to_real() using the functions
# mm_dispersion_nrj() and mm_dispersion_alpha()
# of the module refract.py.
#
# History:
#   2022-05-19 PB Reflect(CalcController)
#   2022-05-25 PB reflectshow
#   2022-06-16 PB printing sync() at the end of dump()
#   2022-09-13 PB reflectshow,_reflect_str:
#                  using parameter nrj_current_keV
#   2023-01-17 PB class Axis -> CalcAxis
#
#----------------------------------------------------------------------

import numpy
from bliss.controllers.motor import CalcController
from bliss.common.axis import CalcAxis as AxisBase
from bliss.config import static

import logging
#logging.basicConfig(level=logging.DEBUG)
#logging.basicConfig(level=logging.WARNING)
#logging.debug('This will get logged')
#logging.warning('This will get logged')

_log = logging.getLogger("id01.scripts.optics.ID01_reflect")
#The log level can be set in the bliss session after import logging
#and defining _log as in the above command.
#_log.setLevel("DEBUG")

#nrj and angle calculation functions
from id01.process.refract import *

class Reflect(CalcController):
    """
    A bragg monochromator selects the 'nrj' by adjusting the incidence angle 'alpha'.

    ALFTAG='alpha' [mrad]
    NRJTAG='nrj'   [keV]

    The motor 'alpha' is either a real motor or a calculated motor, e.g. based
    on the class BeamOffset.

    The name of the currently active stripe is determined with
    self.get_current_stripe(), which is based on
    alpha_motor.controller.get_current_stripe()
    """
    def __init__(self,config,*args,**kwargs):
        super().__init__(config,*args,**kwargs)
        '''
        The bragg parameters of each stripe must be defined
        in reflect_dict. The following parameters are needed:
        coating, thickness1_nm, material1, thickness2_nm, material2, type
        The materials must be defined in Refract().
        '''

        self._config=config

        # do not cache _set_position
        ##self.axis_settings.persistent_setting['_set_position'] = False

        # The motor of the incidence angle alpha and the 
        # nrj motor are defined as tagged motors in mono.yml,
        # e.g. reflect_dict["dcm"]["alpha"]='alpha' for using
        #  - name: $alpha
        #    tags: real alpha

        self.ALFTAG='alpha'             # alpha motor tag
        self.NRJTAG='nrj'               # nrj motor tag
        self.refract=Refract()
        self.EPS=1e-32
        self.opmode=0                   # 0: normal, 1: just calculate nrj from alpha
                                        # do not move alpha when nrj is changed

        # The bragg parameters of each stripe must be specified in reflect_dict.
        self.reflect_dict={}

        # The refract parameters of each material must be specified with 
        # self.refract.setup(material,Z,A[g/mol],rho[g/cm3]).

	#get the energy motor
        ##self._nrj = config.get('energy_axis')  # what is energy_axis? used where? obviously replaced with nrj_current=static.get_config().get('nrj') ???

    def calc_from_real(self, level1_positions):
        '''
        Calculate nrj from alpha
        '''
        _log.debug("  calc_from_real ({}) BEGIN".format(level1_positions))

        ALFTAG = self.ALFTAG
        NRJTAG = self.NRJTAG

        alpha_motor=self._tagged[ALFTAG][0]
        mono_stripe=alpha_motor.controller.get_current_stripe()

        calc_level2_positions = {}

        alpha_mrad = None
        if mono_stripe is not None:

          #Get position of real motors
          alpha_mrad = level1_positions[ALFTAG]

        nrj_keV = None
        if mono_stripe is not None and alpha_mrad is not None and abs(alpha_mrad) > self.EPS:
            Stripe=self.reflect_dict[mono_stripe]
            # calculate nrj_keV from alpha_mrad
            if "material1" in Stripe and "thickness1_nm" in Stripe and "material2" in Stripe and "thickness2_nm" in Stripe:
                material1=Stripe["material1"]
                thickness1=Stripe["thickness1_nm"]
                material2=Stripe["material2"]
                thickness2=Stripe["thickness2_nm"]
                # ...
                _log.debug("    {}.controller.refract.mm_dispersion_nrj({},{},{},{},{})".format("motor",material1, thickness1, material2, thickness2, alpha_mrad))
                nrj_keV=self.refract.mm_dispersion_nrj(material1, thickness1, material2, thickness2, alpha_mrad)
                _log.debug("        {} -> nrj_keV".format(nrj_keV))
            else:
                nrj_keV=-999.0
            
        if nrj_keV is not None:
            calc_level2_positions[NRJTAG] = nrj_keV

        _log.debug("  calc_from_real END ({})".format(calc_level2_positions))

        return calc_level2_positions
    
    def calc_to_real(self, level2_positions):
        '''
        Calculate alpha from nrj
        '''
        _log.debug("  calc_to_real BEGIN ({})".format(level2_positions))

        ALFTAG = self.ALFTAG
        NRJTAG = self.NRJTAG

        mono_stripe=self.get_current_stripe()

        calc_level1_positions = {}

        if self.opmode == 0:

            nrj_keV = None
            if mono_stripe is not None:
                nrj_keV = level2_positions[NRJTAG]

            alpha_mrad = None
            if mono_stripe is not None and nrj_keV is not None:
                # calculate alpha_mrad from nrj_keV
                material1=self.reflect_dict[mono_stripe]["material1"]
                thickness1=self.reflect_dict[mono_stripe]["thickness1_nm"]
                material2=self.reflect_dict[mono_stripe]["material2"]
                thickness2=self.reflect_dict[mono_stripe]["thickness2_nm"]
                # ...
                _log.debug("    {}.controller.refract.mm_dispersion_alpha({},{},{},{},{})".format("motor",material1, thickness1, material2, thickness2, nrj_keV))
                alpha_mrad=self.refract.mm_dispersion_alpha(material1, thickness1, material2, thickness2, nrj_keV)
                _log.debug("        {} -> alpha_mrad".format(alpha_mrad))

            if alpha_mrad is not None:
                calc_level1_positions[ALFTAG] = alpha_mrad

        elif self.opmode == 1:
            alpha_motor=self._tagged[ALFTAG][0]

            calc_level1_positions[ALFTAG] = alpha_motor.position

        _log.debug("  calc_to_real END ({})".format(calc_level1_positions))

        return calc_level1_positions

    def mode(self,opmode=None):
        '''
        0: calculate nrj from alpha and vice versa
        1: do not calculate real motor positions from alpha
           == just calculate nrj, do not move alpha when changing nrj
        '''
        opmode=self._mode(opmode)
        ALFTAG = self.ALFTAG
        NRJTAG = self.NRJTAG
        alpha_motor=self._tagged[ALFTAG][0]
        nrj_motor=self._tagged[NRJTAG][0]

        print("The operation mode is {}".format(opmode))
        if opmode is 1:
            print("Do not move {} when {} is changed.".format(alpha_motor.name,nrj_motor.name))
        elif opmode is 0:
            print("{} moves with {} and vice versa.".format(alpha_motor.name,nrj_motor.name))

    def _mode(self,opmode=None):
        if opmode is not None:
            self.opmode=opmode
        return(self.opmode)

    def show(self,motor=None):
        motor_name="motor" if motor is None else motor.name

        materials=set()
        for stripe_name in self.reflect_dict:
            print("  {}:".format(stripe_name))
            Stripe=self.reflect_dict[stripe_name]
            for param_name in Stripe:
                print("    {:25} = {}".format(param_name,Stripe[param_name]))
                if param_name=="coating" or param_name=="material" or param_name=="material1" or param_name=="material2" :
                    material=Stripe[param_name]
                    if material is not None:
                        materials.add(material)
        self.refract.show()
        

    def dump(self,motor=None):
        motor_name="motor" if motor is None else motor.name

        materials=set()
        for stripe_name in self.reflect_dict:
            Stripe=self.reflect_dict[stripe_name]
            for param_name in Stripe:
                if param_name=="coating" or param_name=="material" or param_name=="material1" or param_name=="material2" :
                    material=Stripe[param_name]
                    if material is not None:
                        materials.add(material)
        for material in materials:
            print("{}.controller.refract.{}".format(motor_name,self.refract.write(material)))

        print('{}.controller.ALFTAG="{}"'.format(motor_name,self.ALFTAG))
        print('{}.controller.NRJTAG="{}"'.format(motor_name,self.NRJTAG))
        for stripe in self.reflect_dict:
            print('{}.controller.reflect_dict["{}"]={}'.format(motor_name,stripe,self.reflect_dict[stripe]))

        print('#{}.controller.savetoyml()'.format(motor_name))

        ALFTAG=self.ALFTAG
        alpha_motor=self._tagged[ALFTAG][0]
        alpha_motor.controller.dump(alpha_motor)

        print("sync()")


    def now(self):
        print(self.get_current_stripe())

    def listadd(self,stripe_name,coating,thickness1_nm,material1,thickness2_nm,material2,mono_type):
        '''
        Adds stripe parameters to reflect_dict.
        Existing stripe parameters are deleted and then filled
        '''

        Stripe={}
        Stripe["coating"]=coating             # coating material (optional)
        Stripe["thickness1_nm"]=thickness1_nm # thickness_nm of layer1 in nm (d_spacing, if crystal)
        Stripe["material1"]=material1         # layer 1 material (refract.py)
        Stripe["thickness2_nm"]=thickness2_nm # thickness_nm of layer2 in nm (here, only a single layer)
        Stripe["material2"]=material2         # layer 2 material (refract.py)
        Stripe["type"]=mono_type              # monochromator type

        self.reflect_dict[stripe_name]=Stripe

    def get_current_stripe(self):
        '''
        Returns the name of the first found stripe with ts position between 
        ts_min and ts_max.
        '''
        ALFTAG=self.ALFTAG
        alpha_motor=self._tagged[ALFTAG][0]
        return alpha_motor.controller.get_current_stripe()
    
    def go_to_stripe(self, stripe_name=None):
        '''
        Moves the monochromator to stripe_name
        '''

        ALFTAG=self.ALFTAG
        alpha_motor=self._tagged[ALFTAG][0]

        alpha_motor.controller.go_to_stripe(stripe_name)

    def stripes(self):
        stripe_list=[]
        for key in self.reflect_dict:
            Stripe=self.reflect_dict[key]
            stripe_list.append(key)
        return(stripe_list)

    def reflectshow(self,stripe_name=None, alpha_current_mrad=None, nrj_current_keV=None):
        current_stripe_name=self.get_current_stripe
        print(self._reflect_str(0,stripe_name,alpha_current_mrad,nrj_current_keV))
        print(self._reflect_str(1,stripe_name,alpha_current_mrad,nrj_current_keV))
        if stripe_name is None:
            for stripe_name in self.stripes():
                print(self._reflect_str(2,stripe_name,alpha_current_mrad,nrj_current_keV))
        else:
            print(self._reflect_str(2,stripe_name,alpha_current_mrad,nrj_current_keV))



    def _reflect_str(self,mode,stripe_name,alpha_current_mrad=None, nrj_current_keV=None):
        '''
        mode 0: return formatted labels
        mode 1: return formatted values
        '''

        ALFTAG=self.ALFTAG
        alpha_motor=self._tagged[ALFTAG][0]
        NRJTAG=self.NRJTAG
        nrj_motor=self._tagged[NRJTAG][0]
        stripe=None

        mir_str=""

        current_stripe_name = self.get_current_stripe()
        if stripe_name is None:
            stripe_name = current_stripe_name

        if stripe_name is not None:
            if stripe_name in self.reflect_dict:
                stripe = self.reflect_dict[stripe_name]

        if stripe is not None:

            if "coating" in stripe:
                coating = stripe["coating"]
            else:
                coating = None
            if "thickness1_nm" in stripe:
                thickness1=stripe["thickness1_nm"]
            else:
                thickness1=None
            if "thickness2_nm" in stripe:
                thickness2=stripe["thickness2_nm"]
            else:
                thickness2=None
            if "material1" in stripe:
                material1 = stripe["material1"]
            else:
                material1 = None
            if "material2" in stripe:
                material2 = stripe["material2"]
            else:
                material2 = None

            if nrj_current_keV is None:
                nrj_current=static.get_config().get('nrj')
                nrj_current_keV=nrj_current.position
                nrj_current_active=nrj_current.controller.active()
            else:
                nrj_current_active="SetValue"

            if alpha_current_mrad is None:
                ALFTAG=self.ALFTAG
                alpha_current_mrad=self._tagged[ALFTAG][0].position

            nrj_critical_keV    = None
            alpha_critical_mrad = None
            nrj_bragg_keV = None

            # alphac(material, nrj)
            # nrjc(material, alpha)
            # mm_dispersion_nrj(material1, thickness1, material2, thickness2, alpha)

            if coating is not None:
                if alpha_current_mrad is not None:
                    if abs(alpha_current_mrad) > 0:
                        nrj_critical_keV = self.refract.nrjc(coating,alpha_current_mrad)
                if nrj_current_keV is not None:
                    if abs(nrj_current_keV) > 0:
                        alpha_critical_mrad = self.refract.alphac(coating,nrj_current_keV)
            if thickness1 is not None and thickness2 is not None and \
                material1 is not None and material2 is not None and alpha_current_mrad is not None:
                if abs(alpha_current_mrad)>0:
                    nrj_bragg_keV = self.refract.mm_dispersion_nrj(material1, thickness1, material2, thickness2, alpha_current_mrad)
            
            if (mode==0):
                mir_str="{:10s} {:17s} {:12s} {:11s} {:15s} {:13s} {:10s}".format(\
                  "stripe",\
                  "nrj({})/keV".format(nrj_current_active),\
                  "nrjCrit/keV",\
                  "alpha/mrad",\
                  "alphaCrit/mrad",\
                  "nrjBragg/keV",\
                  "")
                  #123456789ABCDE
            elif (mode==1):
                #  12345678901234567
                mir_str="{:10s} {:17s} {:12s} {:11s} {:15s} {:13s} {:10s}".format(\
                  "----------",\
                  "-----------------",\
                  "------------",\
                  "-----------",\
                  "---------------",\
                  "-------------",\
                  "")
            else:
                if stripe_name == current_stripe_name:
                    mark="selected"
                else:
                    mark=""
                x=lambda val: '-' if val is None else '{:10.3f}'.format(val)
                mir_str="{:10s} {:17s} {:12s} {:11s} {:15s} {:13s} {:10s}".format(\
                  stripe_name,\
                  x(nrj_current_keV),\
                  x(nrj_critical_keV),\
                  x(alpha_current_mrad),\
                  x(alpha_critical_mrad),\
                  x(nrj_bragg_keV),\
                  mark)
                  #123456789ABCDE

        return(mir_str)

    def savetoyml(self):
        '''
        Save current ID01 reflection parameters to yml file
        '''
        self._config['opmode']=self.opmode
        self._config['NRJTAG']=self.NRJTAG
        self._config['reflect_dict']=self.reflect_dict
        self._config['refract_data']=self.refract.data
        self._config.save()

class CalcAxis(AxisBase):
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        # calculated motor positions do not need to be buffered
        # => disable buffering
        self.settings.disable_cache("_set_position") #disable target pos
        self.settings.disable_cache("position") #disable current pos
        self.settings.disable_cache("state") #disable the state

    def mode(self,opmode=None):
        self.controller.mode(opmode)

    def dump(self):
        self.controller.dump(self)

    def show(self):
        self.controller.show(self)

    def now(self):
        self.controller.now()

    def go(self,stripe_name=None):
        self.controller.go_to_stripe(stripe_name)

    def reflectshow(self,stripe_name=None, alpha_current_mrad=None, nrj_current_keV=None):
        self.controller.reflectshow(stripe_name, alpha_current_mrad, nrj_current_keV)
