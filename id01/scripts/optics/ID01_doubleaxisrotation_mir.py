# ID01 BLISS git repo
#       authors:
#         Peter Boesecke, boesecke@esrf.fr
#
#       Created at: 20. Apr 14:36:00 CET 2022
#
#----------------------------------------------------------------------
# Description:
#       class DoubleAxisRotationMirror(DoubleAxisRotation)
#
# History:
#   2022-05-23  PB DoubleAxisRotationMirror(DoubleAxisRotation)
#   2022-05-24  PB config from/to device yml file
#
#----------------------------------------------------------------------

import numpy
from bliss.controllers.motor import CalcController
from bliss.common.axis import Axis as AxisBase
from id01.scripts.optics.ID01_doubleaxisrotation  import *

import logging
#logging.basicConfig(level=logging.DEBUG)
#logging.basicConfig(level=logging.WARNING)
#logging.debug('This will get logged')
#logging.warning('This will get logged')

_log = logging.getLogger("id01.scripts.optics.ID01_doubleaxisrotation_mir")
#The log level can be set in the bliss session after import logging
#and defining _log as in the above command.
#_log.setLevel("DEBUG")

class DoubleAxisRotationMirror(DoubleAxisRotation):
    """
    ID01 double mirror (mir)
    """
    def __init__(self,config,*args,**kwargs):
        super().__init__(config,*args,**kwargs)

        # The motor Params are kept in the device configuration file
        # <device>.yml, e.g. mir.yml:
        #   beamline_configuration/OH1/motion/mir.yml
        # updating with <motor_name>.controller.Params['A']=2.06
        # followed by <motor_name>.controller.Params.save(),
        # reading with <motor_name>.controller.Params['A']
        # The names of the motor axes can be chosen as needed.
        # The axes are selected with tags.
        #
        # <device>.yml:
        #
        # Params:
        #   A: 2.06              # 2.060 m
        #   B: 0.53              # 0.530 m
        #   TRCOR: 0.0           # 0.0 mm
        #   RTCOR: 0.0           # 0.0 mrad
        #   T1TAG: translation1  # 'translation1' t1 motor tag
        #   T2TAG: translation2  # 'translation2' t2 motor tag
        #   TRTAG: translation   # 'translation' translation motor tag
        #   RTTAG: rotation      # 'rotation' rotation motor tag
        #
        # motors specified in <device>.yml:
        # axes:
        # - name: mirty
        #   tags: translation
        #   unit: mm
        # - name: mirrz
        #   tags: rotation
        #   unit: mrad
        #
        # - name: $sim_mirty1
        #   tags: real translation1
        # - name: $sim_mirty2
        #   tags: real translation2

        if 'Params' in config:
            self.Params=config['Params']
        else:
            self.default()
            self.savetoyml()

    def default(self):
        '''
        Set ID01 mir doubleaxis rotation parameters to default values
        '''
        Params={}
        Params["A"]=2.060  # m
        Params["B"]=0.530  # m
        Params["TRCOR"]=0.0 # mm
        Params["RTCOR"]=0.0 # mrad
        Params["T1TAG"]='translation1' # t1 motor tag
        Params["T2TAG"]='translation2' # t2 motor tag
        Params["TRTAG"]='translation'  # translation motor tag
        Params["RTTAG"]='rotation'     # rotation motor tag
        self.Params=Params
