#       authors:
#         Peter Boesecke, boesecke@esrf.fr
#
#       Created at: 20. Apr 14:36:00 CET 2022
#
#----------------------------------------------------------------------
# Description:
#       class BeamOffsetMm(BeamOffset)
#
# History:
#   2022-05-16  PB BeamOffsetMm(BeamOffset)
#
#----------------------------------------------------------------------

import numpy
from bliss.controllers.motor import CalcController
from bliss.common.axis import Axis as AxisBase
from id01.scripts.optics.ID01_beamoffset  import * 

import logging
#logging.basicConfig(level=logging.DEBUG)
#logging.basicConfig(level=logging.WARNING)
#logging.debug('This will get logged')
#logging.warning('This will get logged')

_log = logging.getLogger("id01.scripts.optics.ID01_beamoffset_mm")
#The log level can be set in the bliss session after import logging
#and defining _log as in the above command.
#_log.setLevel("DEBUG")

class BeamOffsetMm(BeamOffset):
    """
    ID01 multilayer monochromator (mm)
    """
    def __init__(self,config,*args,**kwargs):
        super().__init__(config,*args,**kwargs)

        if 'beamoffset_dict' in config:
            self.beamoffset_dict=config['beamoffset_dict']
        else:
            self.default()
            self.savetoyml()

    def default(self):
        '''
        Set ID01 mm beamoffset parameters to default values
        '''

        # replacing the default stripe definitions
        self.beamoffset_dict={}
        # The motors are defined as tagged motors in <device>.yml

        #2nm0
        # mmlistadd stripe ts_min ts_in ts_max coating d_spacing/nm filling material1 material2 th_offset/deg
        # mmlistadd 2nm0 1 12 24 None 1.98873 0.5 Ni96V4_2nm0 B4C_2nm0 -0.005685
        Stripe={}
        Stripe["ts_min"]=1               # minimum ts position
        Stripe["ts_in"]=12               # ts insertion position
        Stripe["ts_max"]=24              # maximum ts position
        Stripe["GAPCOR"]=0.0             # [mm] actual gap = A[gap]|gap + gap_offset
        Stripe["ALFCOR"]=-0.099222       # [mrad] actual alpha = rotation_mrad+ALFCOR (th_offset_deg=-0.005685)
        Stripe["GAPTAG"]='gap'           # gap value in mm or motor tag
        #                                # if gap is a string, it will be used as motor mnemonic
        Stripe["ROTTAG"]='rotation'
        Stripe["BOFTAG"]='beamoffset'
        Stripe["ALFTAG"]='alpha'
        Stripe["ROFTAG"]='reflectionoffset' # optional calculation of reflectionoffset
        self.beamoffset_dict["2nm0"]=Stripe

        #2nm5
        # mmlistadd stripe ts_min ts_in ts_max coating d_spacing/nm filling material1 material2 th_offset/deg
        # mmlistadd 2nm5 -24 -12 -1 None 2.5117 0.47 Ni96V4_2nm5 B4C_2nm5 -0.005685 
        Stripe={}
        Stripe["ts_min"]=-24             # minimum ts position
        Stripe["ts_in"]=-12              # ts insertion position
        Stripe["ts_max"]=-1              # maximum ts position
        Stripe["GAPCOR"]=0.0             # [mm] actual gap = A[gap]|gap + gap_offset
        Stripe["ALFCOR"]=-0.099222       # [mrad] actual alpha = rotation_mrad+ALFCOR (th_offset_deg=-0.005685)
        Stripe["GAPTAG"]='gap'           # gap value in mm or motor tag
        #                                # if gap is a string, it will be used as motor mnemonic
        Stripe["ROTTAG"]='rotation'
        Stripe["BOFTAG"]='beamoffset'
        Stripe["ALFTAG"]='alpha' 
        Stripe["ROFTAG"]='reflectionoffset' # optional calculation of reflectionoffset
        self.beamoffset_dict["2nm5"]=Stripe

