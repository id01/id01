

def monotylim(mode=None):
    '''
    Calibrate MONO motor monoty at lim-
      mode -1: move to limit, do not change motor value
      move  0: move to limit, change dial value to 0
      mode  1: move to limit, change dial value to 0, set user value to default
    '''

    motor=sim_monoty

    if mode is None:
        print("\
    Calibrate MONO motor {} at lim-\n\
      mode -1: move to limit, do not change motor value\n\
      move  0: move to limit, change dial value to 0\n\
      mode  1: move to limit, change dial value to 0, set user value to default".format(motor.name))

    else:

        if mode>=-1:
            print("Moving {} to lim-".format(motor.name))
            chg_dial(motor,"lim-")
            motor.wait_move()

        if mode>=0:
            print("Setting dial limits of motor {}".format(motor.name))
            set_lim(motor,-0.50000,35.50000)

        if mode>=1:
            ##print("Setting user value of motor {} to default -22.0649".format(motor.name))
            ##motor.position=-22.0649
            pass

def dcm2tylim(mode):
    '''
    Calibrate DCM motor dcm2ty at lim+
      mode -1: move to limit, do not change motor value
      move  0: move to limit, change dial value to 0
      mode  1: move to limit, change dial value to 0, set user value to default
    '''

    motor=sim_dcm2ty

    if mode is None:
        print("\
    Calibrate DCM motor {} at lim+\n\
      mode -1: move to limit, do not change motor value\n\
      move  0: move to limit, change dial value to 0\n\
      mode  1: move to limit, change dial value to 0, set user value to default".format(motor.name))

    else:

        if mode>=-1:
            print("Moving {} to lim+".format(motor.name))
            chg_dial(motor,"lim+")
            motor.wait_move()

        if mode>=0:
            print("Changing dial value of {} to 0".format(motor.name))
            chg_dial(motor,0)
            set_lim(motor,-0.3,-24)

        if mode>=1:
            print("Setting user value of {} to 2.517".format(motor.name))
            motor.position=2.517
