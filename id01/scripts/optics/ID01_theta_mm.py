# -*- coding: utf-8 -*-

# ID01 BLISS git repo
#       authors:
#         Peter Boesecke, boesecke@esrf.fr
#
#       Created at: 29. Apr 12:36:00 CET 2022
#
#----------------------------------------------------------------------
# Description:
#       class ThetaMm(Theta)
#
# History:
#   2022-05-23  PB ThetaMm(Theta)
#
#----------------------------------------------------------------------

import numpy
from bliss.controllers.motor import CalcController
from bliss.common.axis import Axis as AxisBase
from id01.scripts.optics.ID01_theta  import *

import logging
#logging.basicConfig(level=logging.DEBUG)
#logging.basicConfig(level=logging.WARNING)
#logging.debug('This will get logged')
#logging.warning('This will get logged')

_log = logging.getLogger("id01.scripts.optics.ID01_theta_mm")
#The log level can be set in the bliss session after import logging
#and defining _log as in the above command.
#_log.setLevel("DEBUG")

class ThetaMm(Theta):
    """
    ID01 multilayer monochromator (mm)
    """
    def __init__(self,config,*args,**kwargs):
        super().__init__(config,*args,**kwargs)

