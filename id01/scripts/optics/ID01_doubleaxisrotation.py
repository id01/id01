# ID01 BLISS git repo
#       authors:
#         Peter Boesecke, boesecke@esrf.fr
#
#       Created at: 20. Apr 14:36:00 CET 2022
#
#----------------------------------------------------------------------
# Description:
#       class DoubleAxisRotation(CalcController)
#
# History:
#   2022-04-20 PB DoubleAxisRotation(CalcController)
#   2022-06-16 PB dumpstr() added
#   2023-01-17 PB class Axis -> CalcAxis
#
#----------------------------------------------------------------------

import numpy
from bliss.controllers.motor import CalcController
from bliss.common.axis import CalcAxis as AxisBase

import logging
#logging.basicConfig(level=logging.DEBUG)
#logging.basicConfig(level=logging.WARNING)
#logging.debug('This will get logged')
#logging.warning('This will get logged')

_log = logging.getLogger("id01.scripts.optics.ID01_doubleaxisrotation")
#The log level can be set in the bliss session after import logging
#and defining _log as in the above command.
#_log.setLevel("DEBUG")


class DoubleAxisRotation(CalcController):
    """
    A double axis rotation device uses two linear translations for adjusting inclination and translation:
      tr1[mm]: upstream translation
      tr2[mm]: downstream translation

    Parameters
               A[m] : longitudinal distance between tr1 and tr2 [m]
               B[m] : longitudinal distance of (virtual) rotation axis from axis tr1 [m]
          TRCOR[mm] : tr correction value
        RTCOR[mrad] : rt correction value
          T1TAG[mm] : t1 motor tag
          T2TAG[mm] : t2 motor tag
          TRTAG[mm] : translation motor tag
        RTTAG[mrad] : rotation motor tag

    The motors 'rt' [mrad] and 'tr' [mm] are calculated from the real motors 'tr1' and 'tr2':

      tr[mm]   = tr2[mm]*B[m]/A[m] + tr1[mm]*(A[m]-B[m])/A[m] + TRCOR[mm]
      rt[mrad] = atan(((tr2[mm]-tr1[mm])/A[m])*0.001)*1000 + RTCOR[mrad]

    The real motors tr1 [m] and tr2 [m] are calculated from 'tr' and 'rt':

       tr1[mm] = (tr[mm]-TRCOR[mm]) - B[m]*tan((rt[mrad]-RTCOR[mrad])*0.001)*1000
       tr2[mm] = (tr[mm]-TRCOR[mm]) + (A[m]-B[m])*tan((rt[mrad]-RTCOR[mrad])*0.001)*1000

    """
    def __init__(self,config,*args,**kwargs):
        super().__init__(config,*args,**kwargs)

        self._config=config

        # The translation motors of each device must be defined as
        # tagged motors in <device>.yml and the calculated motors must be
        # specified for each <device> in stripe_dict, e.g.
        # stripe_dict["translation1"]='axis1' for using the gap 
        # motor specified in <device>.yml:
        #  - name: $<axis1>
        #    tags: real translation1
        #  - name: $<axis2>
        #    tags: real translation1
        # ...
        #  - name: tr
        #    tags: translation
        #  - name: rt
        #    tags: rotation
        # ...

        self.EPS=1e-32

        # do not cache _set_position
        ##self.axis_settings.persistent_setting['_set_position'] = False

    def show(self):
        print("           A[m] : longitudinal distance between tr1 and tr2 [m]")
        print("           B[m] : longitudinal distance of (virtual) rotation axis from axis tr1 [m]")
        print("      TRCOR[mm] : tr correction value")
        print("    RTCOR[mrad] : rt correction value")
        print("          T1TAG : translation1 motor tag")
        print("          T2TAG : translation2 motor tag")
        print("          TRTAG : translation motor tag")
        print("          RTTAG : rotation motor tag")

        for param_name in self.Params:
            print("    {:25} = {}".format(param_name,self.Params[param_name]))

    def setup(self,A=None,B=None,TRCOR=None,RTCOR=None,T1TAG=None,T2TAG=None,TRTAG=None,RTTAG=None):
        """
               A[m] : longitudinal distance between tr1 and tr2 [m]
               B[m] : longitudinal distance of (virtual) rotation axis from axis tr1 [m]
          TRCOR[mm] : tr correction value
        RTCOR[mrad] : rt correction value
              T1TAG : translation1 motor tag
              T2TAG : translation2 motor tag
              TRTAG : translation motor tag
              RTTAG : rotation motor trag
        """
        if A is None or B is None or TRCOR is None or RTCOR is None or T1TAG is None or T2TAG is None or TRTAG is None or RTTAG is None:
            print("setup(A[m],B[m],TRCOR[mm],RTCOR[mrad],T1TAG,T2TAG,TRTAG,RTTAG)")
        else:
            self.Params["A"]=A
            self.Params["B"]=B
            self.Params["TRCOR"]=TRCOR
            self.Params["RTCOR"]=RTCOR
            self.Params["T1TAG"]=T1TAG
            self.Params["T2TAG"]=T2TAG
            self.Params["TRTAG"]=TRTAG
            self.Params["RTTAG"]=RTTAG

    def dumpstr(self,motor_name):
        """
        Returns the parameters in a string.
        """
        string="{}.controller.setup(A={},B={},TRCOR={},RTCOR={},T1TAG='{}',T2TAG='{}',TRTAG='{}',RTTAG='{}')".\
          format(motor_name,self.Params["A"],self.Params["B"],self.Params["TRCOR"],self.Params["RTCOR"],\
                            self.Params["T1TAG"],self.Params["T2TAG"],self.Params["TRTAG"],self.Params["RTTAG"])
        return(string)

    def dump(self,motor=None):
        motor_name="motor" if motor is None else motor.name
        print(self.dumpstr(motor_name))

    def calc_from_real(self, level0_positions):
        '''
        Calculate translation and rotation from translation1 and translation2
        level0_positions is a dictionary
        '''
        _log.debug("  calc_from_real BEGIN ({})".format(level0_positions))

        A_m=self.Params["A"]
        B_m=self.Params["B"]
        TRCOR_mm=self.Params["TRCOR"]
        RTCOR_mrad=self.Params["RTCOR"]
        T1TAG=self.Params["T1TAG"]
        T2TAG=self.Params["T2TAG"]
        TRTAG=self.Params["TRTAG"]
        RTTAG=self.Params["RTTAG"]
        _log.debug("    A_m={},B_m={},TRCOR_mm={},RTCOR_mrad={},T1TAG={},T2TAG={},TRTAG={},RTTAG={}".\
          format(A_m,B_m,TRCOR_mm,RTCOR_mrad,T1TAG,T2TAG,TRTAG,RTTAG))

        #Get input positions
        tr1_mm = level0_positions[T1TAG]
        tr2_mm = level0_positions[T2TAG]
 
        tr_mm   = tr2_mm*B_m/A_m + tr1_mm*(A_m-B_m)/A_m + TRCOR_mm
        rt_mrad = numpy.arctan(((tr2_mm-tr1_mm)/A_m)*0.001)*1000. + RTCOR_mrad

        calc_level1_positions={TRTAG:tr_mm,RTTAG:rt_mrad}

        _log.debug("  calc_from_real END ({})".format(calc_level1_positions))

        return calc_level1_positions

    def calc_to_real(self, level1_positions):
        '''
        Calculate translation1 and translation2 from translation and rotation,
        level1_positions is a dictionary
        '''
        _log.debug("  calc_to_real BEGIN ({})".format(level1_positions))

        A_m=self.Params["A"]
        B_m=self.Params["B"]
        TRCOR_mm=self.Params["TRCOR"]
        RTCOR_mrad=self.Params["RTCOR"]
        T1TAG=self.Params["T1TAG"]
        T2TAG=self.Params["T2TAG"]
        TRTAG=self.Params["TRTAG"]
        RTTAG=self.Params["RTTAG"]
        _log.debug("    A_m={},B_m={},TRCOR_mm={},RTCOR_mrad={},T1TAG={},T2TAG={},TRTAG={},RTTAG={}".\
          format(A_m,B_m,TRCOR_mm,RTCOR_mrad,T1TAG,T2TAG,TRTAG,RTTAG))

        #Get input positions
        tr_mm = level1_positions[TRTAG]
        rt_mrad = level1_positions[RTTAG]

        tan_rot=numpy.tan((rt_mrad-RTCOR_mrad)*0.001)
        tr1_mm = (tr_mm-TRCOR_mm) - B_m*tan_rot*1000.
        tr2_mm = (tr_mm-TRCOR_mm) + (A_m-B_m)*tan_rot*1000.

        calc_level0_positions={T1TAG:tr1_mm,T2TAG:tr2_mm}

        _log.debug("  calc_to_real END ({})".format(calc_level0_positions))

        return calc_level0_positions

    def savetoyml(self):
        '''
        Save current ID01 mm doubleaxis rotation parameters to yml file
        '''
        self._config['Params']=self.Params
        self._config.save()

class CalcAxis(AxisBase):
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        # calculated motor positions do not need to be buffered
        # => disable buffering
        self.settings.disable_cache("_set_position") #disable target pos
        self.settings.disable_cache("position") #disable current pos
        self.settings.disable_cache("state") #disbale the state

    def dump(self):
        self.controller.dump(self)

    def show(self):
        self.controller.show()

