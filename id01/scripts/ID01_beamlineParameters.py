from bliss.common.standard import info
from bliss.config.settings import ParametersWardrobe, QueueSetting, OrderedHashSetting, scan
from bliss.shell.cli.user_dialog import UserChoice, UserInput, UserIntInput, UserFloatInput, UserYesNo
from bliss.shell.cli.pt_widgets import BlissDialog

from collections import namedtuple
ROLES = "tx", "ty", "tz", "rx", "ry", "rz"
Pose = namedtuple("Pose", ROLES)

from os.path import join, exists
import datetime

def check_name (func):
    def wrapper (name):
        bli = name.split(':')
        if name.startswith('parameters:'):
            name = bli[0] +':'+ bli[1]
        else:
            name = 'parameters:' + bli[0]

        return func (name)
    return wrapper

@check_name
def purge_wardr (name):
    instances = QueueSetting(name)
    inst_names = instances.get()

    for instance in inst_names:
        pr = OrderedHashSetting("%s:%s" % (name, instance))
        pr.clear()
        instances.remove(instance)  # removing from Queue
    
    instances.clear()

@check_name    
def read_wardr (name):
    instances = QueueSetting(name)
    inst_names = instances.get()

    for instance in inst_names:
        print (instance)
        pr = OrderedHashSetting("%s:%s" % (name, instance))
        for k in pr.keys():
            print (k,pr[k])

def list_wardr (wildcard = '*'):
    for item in scan(f'parameters:{wildcard}'):
        print(item)

        
# TODO: not clear how to discriminate what is and what is not purgable
def del_wardr (wildcard = '*',interact = False):
    for item in scan(f'parameters:{wildcard}'):
        #print(item)
        if item.split(':')[-2]=='positionsGroup' or item.split(':')[-3]=='positionsGroup':
            wardr_name = ":".join(item.split(":")[1:])
            try:
                tmp = ParametersWardrobe(wardr_name)
                if interact:
                    dlg = UserYesNo(label=f"Delete {wardr_name} Yes/No?")
                    display(dlg, title='wardrobe manager')
                    if dlg:
                        tmp.purge()
                else:
                    tmp.purge()
                print(item)
            except Exception: 
                print(f"...could not purge {item}")
                #print(item)
                pass
            
def del_pos_wardr (wildcard = '*positionsGroup*'):
    for item in scan(f'parameters:{wildcard}'):
        wardr_name = ":".join(item.split(":")[1:])
        try:
            tmp = ParametersWardrobe(wardr_name)
            dlg = UserYesNo(label=f"Delete {wardr_name} Yes/No?")
            display(dlg, title='wardrobe manager')
            if dlg:
                tmp.purge()
            print(item)
        except Exception: 
            pass

def _dialog (title, parameters_dict, callbacks ={}):
    uilist = []
    for param in parameters_dict:

        validator = parameters_dict [param]
        if isinstance (validator, int):
            uilist.append([UserIntInput (param , param + ' : ' ,parameters_dict[param])])
        elif isinstance(validator, float):
            uilist.append([UserFloatInput (param , param + ' : ' ,parameters_dict[param])])
        else:
            uilist.append([UserInput (param , param + ' : ' ,parameters_dict[param])])
               
    ret = BlissDialog( uilist, title=title).show()

    if ret is not False:
            
        for param in parameters_dict:
            if isinstance(parameters_dict[param], list):
                ret[param]=eval(ret[param])
        for param in callbacks:
            if param in ret:
                if ret[param] != parameters_dict[param]:
                    ret[param] = callbacks[param].__call__(ret[param])
            else:
                ret[param] = callbacks[param].__call__(ret)
                
    return ret 



class ID01Parameters:
    """
    Base class to handle beamline persistant parameters.
    """

    def __init__(self, param_name, param_defaults=None, no_setup=[], callbacks = {}):
        """
        Reads the parameter set from Redis and add the default values when necessary
        """
        self.param_name = param_name
        self.instance_name = None
        self.param_setup = {}
        self.callbacks = callbacks


        if self.param_name != None:
            self.parameters = ParametersWardrobe(self.param_name)

        if param_defaults != None:
            for k,v in param_defaults.items():
                    nosetup = (k in no_setup)
                    callback = callbacks [k] if k in callbacks else None
                    self._add(k, v, nosetup = nosetup, callback = callback)


    def __info__ (self):
        string = info(self.parameters)
        string = self.param_name + '  ' + string if self.instance_name is None else string
        return string
            
    def _add (self, key, value, nosetup = False, callback = None):
        """
        utility function
        """
        if nosetup is True and key in self.param_setup:
            self.param_setup.pop(self.param_setup.index(key))
            
        if nosetup is False and key not in self.param_setup:
            self.param_setup[key]=value
            
        if key not in self.parameters.to_dict(export_properties=True):
            self.parameters.add (key, value)
        
        if callable(callback):
            self.callbacks[key]=callback
        else:
            if key in self.callbacks:
                self.callbacks.pop(key)
            
    def show(self,invert=True):
        if self.param_name != None:
            self.parameters.show_table(invert)
            

    def set_pos_manual(self):
        if self.param_name is None:
            return

        title = self.instance_name + ' Setup' if self.instance_name else self.param_name + ' Setup'
        parameters_dict = self.parameters.to_dict(export_properties=False)
        ret = _dialog (title, parameters_dict, self.callbacks)

        #convert string to float
        for k, v in ret.items():
            #allow strings
            try:
                ret[k] = float(v)
            except Exception:
                ret[k] = v

        #fix hexapod string to Pose
        for hexaObj in self.hexaObj_dict.keys():
            
            istr = ret[hexaObj+"_user_coord"]
            istr = istr.split("(")[1].split(")")[0]
            adict = dict()
            for item in istr.split(","):
                k,v = item.split("=")
                adict[k.strip()]=float(v)
            
            user_coord = Pose(adict["tx"],adict["ty"],adict["tz"],adict["rx"],adict["ry"],adict["rz"])

            ret[hexaObj+"_user_coord"] = user_coord

            istr = ret[hexaObj+"_object_coord"].split("(")[1].split(")")[0]
            adict = dict()
            for item in istr.split(","):
                k,v = item.split("=")
                adict[k.strip()]=float(v)
            
            object_coord = Pose(adict["tx"],adict["ty"],adict["tz"],adict["rx"],adict["ry"],adict["rz"])
            
            ret[hexaObj+"_object_coord"] = object_coord
            
        if ret is not False:
            parameters_dict.update(ret)
            self.parameters.from_dict(parameters_dict)


    def set_shexa_pose(self, shexaObj, all_instances=False):
        """
        manually define the pose - toggle all to set the pose for all positions in this positionsGroup
        """
        if self.param_name is None:
            return
        
        if all_instances: 
            instances = self.parameters.instances
        else:
            instances = [self.parameters.current_instance]

        #breakpoint()

        for instance in instances:
            #ignore some instances
            if instance!="default" and instance!="offsets":
                print(instance)
                self.switch(instance)

                parameters_dict = self.parameters.to_dict(export_properties=False)
                user, object = shexaObj.get_origin()

                parameters_dict[shexaObj.name+"_user_coord"] = user
                parameters_dict[shexaObj.name+"_object_coord"] = object

                self.parameters.from_dict(parameters_dict)



    def save_instance(self, directory, file_name = None, prefix = '', suffix = ''):
        """
        save in a file current instance of a parameter set         
        """
        
        
        if self.param_name != None and len(directory)>1:
            if exists(directory):
                if file_name == None or file_name == "":
                    if prefix == '':
                        prefix = datetime.datetime.now().strftime('%Y%m%d_')
                    file_name = prefix + self.param_name + suffix +'.yml'
                file_full_path = join(directory, file_name)
                self.parameters.to_file(file_full_path,self.parameters.current_instance) 
                print (f'Saving {self.parameters.current_instance} instance of {self.param_name} in: {file_name}') 
            else:
                print (f"Path ('{directory}') doesn't exist, please try again ...")
    
    def load_instance(self, directory, file_name):
        """
        loads from a previously saved file 
        directory must be specified
        loads only instance referenced by the calling object. 
        """
        if self.param_name != None and len(directory)>1:
            if exists(join(directory, file_name)):
                file_full_path = join(directory, file_name)
                self.parameters.from_file(file_full_path,self.parameters.current_instance) 
                print (f'loading {self.parameters.current_instance} instance of {self.param_name} in: {file_name}') 
            else:
                print (f"Path ('{directory}') doesn't exist, please try again ...")


    def copy (self, instance_name):
        """
        """
        #TODO does not work
        if instance_name in self.parameters.instances:
            instances = self.parameters._get_all_instances()
            copy_dict = instances [instance_name]
            self.parameters.from_dict(copy_dict)
        else:
            print ("Can't find instance '{0}' in parameters set {1}'" .format(self.instance_name, self.param_name)) 
           
            

    def _reset_parameters(self):
        """
        Deletes all parameters of the object from Redis
        to re-start with the default values
        """
        if self.param_name != None:
            parameters_dict = self.parameters.to_dict()
            for k in parameters_dict.items():
                self.parameters.remove("."+k[0])

        self.param_setup.clear()        
        self.callbacks.clear()        

        self.__init__(self.param_name)        

        
            

