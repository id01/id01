# -*- coding: utf-8 -*-

# ID01 BLISS git repo
#       authors:
#         Steven Leake, leake@esrf.fr
#
#       Created at: 20. May 09:00:00 CET 2021
#
#----------------------------------------------------------------------
# Description: 
#    functions for making detector masks

#
# TODO:
#    add logging
#    add fast shutter
#    add elogbook   
#    see all remaining TODOs
#----------------------------------------------------------------------
from bliss.common.scans import loopscan
from bliss import setup_globals
from bliss.controllers.lima.limatools import limatake


from os.path import exists
import fabio
import numpy as np

def check_usermask_file_exists( detObj, verbose = False):
    if detObj.name == "mpx1x4":
        detname="mpx4"
    elif detObj.name == "mpxgaas":
        detname="mpx22"
    elif detObj.name == "eiger2M":
        detname="eiger2M"        
    else:
        detname = detObj.name

    usermask_fn = f"/data/id01/inhouse/archive/spatcorr-files/{detname}/{detname}_user_bad_pixels.edf"
    hotmask_fn = f"/data/id01/inhouse/archive/spatcorr-files/{detname}/{detname}_hotmask.edf"

    if not exists(usermask_fn):
        mask = fabio.open(hotmask_fn).data
        emptymask = np.ones_like(mask)
        emptymask = emptymask.astype(np.uint8)
        tmp_edf = fabio.edfimage.EdfImage(emptymask)
        tmp_edf.save(usermask_fn)
    else:        
        if verbose:
            print(f"mask ({usermask_fn}) exists")
        

def id01_limamask(detObj,q=0.8,threshold=0.8):
    """
    function to create a mask for a detector
    """
    
    setup_globals.multiplexer_eh.switch("FS_CHANNEL","CLOSE")
    setup_globals
    detObj.processing.use_mask=False
    #scanObj = loopscan(21,0.1,detObj) # settings from SPEC id01limamask (frames,exposure_time)
    #tmp_image = scanObj.get_data()['{}:image'.format(detObj.name)].as_array()
    
    scanObj = limatake(0.1,50,detObj) # limatake to keep the shutter closed
    tmp_image = scanObj.get_data()['{}:image'.format(detObj.name)].as_array()

    #mask = np.where((tmp_image.std(axis=0)>10) + (tmp_image.mean(axis=0)>10),0,1) # data*mask for correct nomenclature
    
    mask = np.where(np.quantile(tmp_image,q,axis=0)>threshold,0,1)
    
    # TODO move this directly to LIMA
    #np.quantile(a, q, axis=None, out=None, overwrite_input=False, interpolation='linear', keepdims=False)
    # q is the quantile
    # q==0.5: median
    # 0<=q<=1: for a list of N numbers return the value that separates this list in two sub-lists, 
    # the first list containing N*q numbers that are smaller or equal than the returned value,
    # the second list containing N*(1-q) numbers that are larger than the returned value.
    # By using a suitable threshold and a suitable value for q, e.g. 0.3, pixels that exceed more than 30%
    # of the time the threshold will be masked.
    # If the threshold is 0 all pixels with at least 1 count in a fraction of q*N frames will be masked.  
    #mask = np.where((tmp_image>np.quantile(tmp_image,q,axis=0)),0,1) # data*mask for correct nomenclature
    # /sware/exp/saxs/edf/new/saxs/edfpack/avg.c
    # more ../src/saxs_average.c
     
    _id01_limamask_warning(mask,pc_tolerance = 10)
    # save the mask to a defined location (consistent with SPEC for now)
    if detObj.name == "mpx1x4":
        detname="mpx4"
    elif detObj.name == "mpxgaas":
        detname="mpx22"
    elif detObj.name == "eiger2M":
        detname="eiger2M"        
    else:
        detname = detObj.name
    mask = mask.astype(np.uint8)
    out_fn = f"/data/id01/inhouse/archive/spatcorr-files/{detname}/{detname}_hotmask.edf"
    ## EZ 20220421 - workaround because of inhouse not working
    #out_fn = f"/data/visitor/hc4759/id01/{detname}_hotmask.edf"
    
    # TODO: output file should have a timestamp and threshold energy.
    # to track detector evolution in a sensible way
    
    # if a user mask is defined add it to the calculated mask
    check_usermask_file_exists(detObj)
    usermask_fn = f"/data/id01/inhouse/archive/spatcorr-files/{detname}/{detname}_user_bad_pixels.edf"
    usermask = fabio.open(usermask_fn).data
    #workaround for weird dataformat for mask
    #tmp_arr = np.zeros((2,mask.shape[0],mask.shape[1]))
    #tmp_arr[0,:,:] =  mask
    tmp_arr = mask
    
    #combine user mask
    tmp_arr = np.logical_and(usermask,tmp_arr)
    tmp_arr1 = np.where(tmp_arr,1,0)
    tmp_edf = fabio.edfimage.EdfImage(tmp_arr1.astype(np.uint8))
    tmp_edf.save(out_fn)
    print(f"Saving file to {out_fn}")
    # update the detObj mask
    detObj.processing.use_mask=True                                                          
    # has to be edf at the moment < SEB has requested it hdf5 compatibility                                                                 
    detObj.processing.mask = out_fn    
     
    setup_globals.multiplexer_eh.switch("FS_CHANNEL","P201")
    
    _id01_limamask_analysis(tmp_arr1)
    
    return tmp_arr1

def id01_limamaskon(*detObjsList):
    """
    Turn on a hotpixel mask for all defined detObjs in list.
    """
    for detObj in detObjsList:
        detObj.processing.use_mask = True
        if not exists(detObj.processing.mask):
            print(f"File: {detObj.processing.mask} does not exist")
        elif not (detObj.processing.mask.endswith(".edf")):
            print(f"File format is not .edf")
        elif not (fabio.open(detObj.processing.mask).data.shape == (detObj.image.height, detObj.image.width)):
            print("Detector size ({fabio.open(detObj.processing.mask).data.shape})!= array from file {detObj.image.height} {detObj.image.width}")
        else:
            print("file is OK ... let's see ...")
            print(f"Limamask on {detObj}")
    
    
def id01_limamaskoff(*detObjsList):
    """
    Turn off the hotpixel mask for all defined detObjs in list
    """
    for detObj in detObjsList:
        detObj.processing.use_mask = False
        print(f"file is OK, mask activated for {detObj} let's see ...")

    
def _id01_limamask_warning(mask,pc_tolerance = 10):
    """
    if more than 10% of pixels are mask emmit a warning message
    """
    pxls_masked = (mask==0).sum()
    print(f"{pxls_masked/mask.size*100}% of pixels have been masked")
    if pxls_masked/mask.size > pc_tolerance/100.:
        print(f"BEWARE: {pxls_masked/mask.size*100:.2f}% > {pc_tolerance}% tolerance for masked pixels: {pxls_masked} / {mask.size}  ")

# def id01_limamask_mask_pixel(detObj,x,y):
#     """
#     mask a pixel which is not caught by the limamask macro
#     x,y - coordinate of pixel in flint
#     """
#     if detObj.name == "mpx1x4":
#         detname="mpx4"
#     if detObj.name == "mpxgaas":
#         detname="mpx22"
#     if detObj.name == "eiger2M":
#         detname="eiger2M"
        
#     mask_fn = f"/data/id01/inhouse/archive/spatcorr-files/{detname}/{detname}_hotmask.edf"
#     mask = fabio.open(mask_fn)
#     tmp_mask = mask
#     tmp_mask[y,x] = 0
#     tmp_edf = fabio.edfimage.EdfImage(tmp_mask)
#     tmp_edf.save(mask_fn)
#     return tmp_mask

# def id01_limamask_unmask_pixel(detObj,x,y):
#     """
#     mask a pixel which is not caught by the limamask macro
#     x,y - coordinate of pixel in flint
#     """
#     if detObj.name == "mpx1x4":
#         detname="mpx4"
#     if detObj.name == "mpxgaas":
#         detname="mpx22"
#     if detObj.name == "eiger2M":
#         detname="eiger2M"
        
#     mask_fn = f"/data/id01/inhouse/archive/spatcorr-files/{detname}/{detname}_hotmask.edf"
#     mask = fabio.open(mask_fn)
#     tmp_mask = mask
#     tmp_mask[y,x] = 1
#     tmp_edf = fabio.edfimage.EdfImage(tmp_mask)
#     tmp_edf.save(mask_fn)
#     return tmp_mask
    

def id01_limamask_mask_pixel(detObj,x,y):
    """
    mask a pixel which is not caught by the limamask macro
    x,y - coordinate of pixel in flint
    """
    if detObj.name == "mpx1x4":
        detname="mpx4"
    elif detObj.name == "mpxgaas":
        detname="mpx22"
    elif detObj.name == "eiger2M":
        detname="eiger2M"
    else:
        detname = detObj.name

    #TODO make this acccept lists of tuples

    hotmask_fn = f"/data/id01/inhouse/archive/spatcorr-files/{detname}/{detname}_hotmask.edf"    
    usermask_fn = f"/data/id01/inhouse/archive/spatcorr-files/{detname}/{detname}_user_bad_pixels.edf"
    mask = fabio.open(usermask_fn).data
    tmp_mask = mask
    tmp_mask[y,x] = 0
    tmp_edf = fabio.edfimage.EdfImage(tmp_mask.astype(np.uint8))
    tmp_edf.save(usermask_fn)

    newmask = update_mask(hotmask_fn,usermask_fn)

    return 1-newmask

def id01_limamask_unmask_pixel(detObj,x,y):
    """
    mask a pixel which is not caught by the limamask macro
    x,y - coordinate of pixel in flint
    """
    if detObj.name == "mpx1x4":
        detname="mpx4"
    elif detObj.name == "mpxgaas":
        detname="mpx22"
    elif detObj.name == "eiger2M":
        detname="eiger2M"
    else:
        detname = detObj.name

    #TODO make this acccept lists of tuples

    hotmask_fn = f"/data/id01/inhouse/archive/spatcorr-files/{detname}/{detname}_hotmask.edf"    
    usermask_fn = f"/data/id01/inhouse/archive/spatcorr-files/{detname}/{detname}_user_bad_pixels.edf"
    hmask = fabio.open(hotmask_fn).data
    umask = fabio.open(usermask_fn).data

    tmp_mask = hmask
    tmp_mask[y,x] = 1
    tmp_edf = fabio.edfimage.EdfImage(tmp_mask.astype(np.uint8))
    tmp_edf.save(hotmask_fn)

    tmp_mask = umask
    tmp_mask[y,x] = 1
    tmp_edf = fabio.edfimage.EdfImage(tmp_mask.astype(np.uint8))
    tmp_edf.save(usermask_fn)

    newmask = update_mask(hotmask_fn,usermask_fn)
    return 1-newmask

def update_mask(raw_mask_fn, mask2add_fn):
    raw_mask = fabio.open(raw_mask_fn).data
    mask2add = fabio.open(mask2add_fn).data

    tmp_arr = np.logical_and(raw_mask,mask2add)
    tmp_arr1 = np.where(tmp_arr,1,0)
    tmp_edf = fabio.edfimage.EdfImage(tmp_arr1.astype(np.uint8))
    tmp_edf.save(raw_mask_fn)
    return tmp_arr1


def _id01_limamask_analysis(mask):
    """
    function to summarise the result and pipe it to the elogbook
    """
    # TODO plot
    # TODO statistics
    pass
