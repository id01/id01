from bliss.shell.standard import mv, mvr, umv, umvr
from bliss.common.standard import SoftAxis
from bliss import current_session, setup_globals, global_map
from bliss.config.beacon_object import BeaconObject
import gevent
import click




class Zpyz_mot(object):

    _position = 0

    def set_motor(self,motobj0,motobj1,motobj2):
        # FZP is the master - everything else moves with it
        self._fzp = motobj0
        self._fzp.sync_hard()
        self._cs = motobj1
        self._cs.sync_hard()
        self._osa = motobj2
        self._osa.sync_hard()

    def get_position(self):
        return self._fzp.position

    def move(self, position):
        delta = position-self._fzp.position

        print(delta)
        umvr(self._fzp,delta,self._cs,delta,self._osa,delta)

def create_zpy(name,config):
    zpy = Zpyz_mot()
    zpy.set_motor(config.get('dep_axis0'),config.get('dep_axis1'),config.get('dep_axis2')) # current_session.env_dict["fs2y"])

    return SoftAxis( name, #'fs2y_motor', 
                     zpy, 
                     position='get_position',
                     move=zpy.move,
                     export_to_session=False)

def create_zpz(name,config):
    zpz = Zpyz_mot()
    zpz.set_motor(config.get('dep_axis0'),config.get('dep_axis1'),config.get('dep_axis2')) # current_session.env_dict["fs2y"])

    return SoftAxis( name, #'fs2y_motor', 
                     zpz, 
                     position='get_position',
                     move=zpz.move,
                     export_to_session=False)

class Zpx_mot(BeaconObject):

    _position = 0
    offset_axis1 = BeaconObject.property_setting('offset_axis1',default=0)
    offset_axis2 = BeaconObject.property_setting('offset_axis2',default=0)

    def set_motor(self,motobj0,motobj11,motobj12,motobj21,motobj22):
        # FZP is the master - everything else moves with it
        self._fzpx = motobj0
        self._fzpx.sync_hard()
        self._fzpy = motobj11
        self._csy = motobj12
        self._fzpz = motobj21
        self._csz = motobj22

    def set_offset(self,zpy_per_fzpx,zpz_per_fzpx):
        self.offset_axis1 = self.zpy_per_fzpx = zpy_per_fzpx
        self.offset_axis2 = self.zpz_per_fzpx = zpz_per_fzpx

    def get_offset(self,):
        return self.offset_axis1, self.offset_axis2

    def get_position(self):
        return self._fzpx.position

    def move(self, position):
        delta = position-self._fzpx.position

        # the cs and fzp are on the same carriage so should see the same offset, whereas the OSA remains stationary
        delta_y = self.offset_axis1*delta
        delta_z = self.offset_axis2*delta
        #print(delta,delta_y,delta_z)
        umvr(self._fzpx,delta,self._fzpy,delta_y,self._csy,delta_y,self._fzpz,delta_z,self._csz,delta_z)


def create_zpx(name,config):
    zpx = Zpx_mot(config)
    zpx.set_motor(config.get('dep_axis0'),config.get('dep_axis11'),config.get('dep_axis12'),config.get('dep_axis21'),config.get('dep_axis22')) # current_session.env_dict["fs2y"])
    #zpx.set_offset(config.get('offset_axis1'),config.get('offset_axis2'))
    # to get to this from a shell we can set it with zpx_motor.controller.obj.set_offset

    axis = SoftAxis( name, #'fs2y_motor', 
                     zpx, 
                     position='get_position',
                     move=zpx.move,
                     export_to_session=False)

    def set_offset(zpy_per_fzpx,zpz_per_fzpx):
        zpx.set_offset(zpy_per_fzpx,zpz_per_fzpx)
    axis.set_offset = set_offset
    def get_offset():
        print(zpx.get_offset())
    axis.get_offset = get_offset
    return axis


