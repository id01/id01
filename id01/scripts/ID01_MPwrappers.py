# -*- coding: utf-8 -*-

# ID01 BLISS git repo
#       authors:
#         Steven Leake, leake@esrf.fr
#
#       Created at: 15. Mar 12:36:00 CET 2022
#
#----------------------------------------------------------------------
# Description: 
#       limit searches for MP objects
#       when an encoder is not present use a limit switch to check positions
#       
#----------------------------------------------------------------------


from bliss.common.standard import SoftAxis
from bliss import current_session, setup_globals, global_map
from bliss.config.beacon_object import BeaconObject
import gevent
from bliss.common.hook import MotionHook
import click
import id01.scripts.specmot_utils as specutils

class MPmicro(object):

    _position = 0

    def set_motor(self,motobj):
        self._microy = motobj
        self._microy.sync_hard()

    def get_position(self):
        return self._microy.position

    def move(self, position):
        if position <= 10:
            # we need to do something special
            self.reset_position_on_limit_switch()
            self._microy.move(position)
        else:
            # just make the move precision is not important
            self._microy.move(position)
             
    def reset_position_on_limit_switch(self,):
            self._microy.hw_limit(-1)
            self._microy.dial = 0
            self._microy.limits = (-0.5,119)
            self._microy.position = 0

    

def create_pseudo_MPmicro(name,config):
    microMP = MPmicro()
    microMP.set_motor(config.get('dep_axis')) # current_session.env_dict["microy"])

    return SoftAxis( name, #'micro_motor', 
                     microMP, 
                     position='get_position',
                     move=microMP.move,
                     export_to_session=False)
                          
                        

class microMPHook(MotionHook):
    """microscope MP object motion hook"""

    def __init__(self, name, config):
        self.config = config
        self.name = name
        super().__init__()

    def pre_move(self, motion_list):
        self.check_collision_limits(motion_list)    

    def post_move(self, motion_list):
        pass

    def check_collision_limits(self,motion_list,verbose = False):

        #current microscope position
        micro_MP_pos = current_session.env_dict["micro_MP"].position

        if verbose:
            print(f"micro_MP is in position {micro_MP_pos}")
        for motion in motion_list:
            #breakpoint()
            if motion.axis.name != "eta":
                target_eta = self.axes["eta"].position
                target_microy = motion.axis.dial2user(motion.target_pos/motion.axis.steps_per_unit)

            else:
                target_microy = self.axes["microy"].position
                target_eta = motion.axis.dial2user(motion.target_pos/motion.axis.steps_per_unit)


            if target_microy < 10:
                target_MP = "screen_in"
            elif target_microy <40:
                target_MP = "screen_out"
            else:
                target_MP = "parked"

            config_pars = self.collisionLimits.config["collisionLimits"]

            # if we are outside of the defined limits do not move
            if verbose:
                print(f"Trying to move to {motion.axis.name} for {target_MP}")
            for axis in list(self.axes):
                axis = self.axes[axis]
                if verbose:
                    print(axis.position)

                if axis.name == "eta":
                    target = target_eta
                else:
                    target = target_microy

                for dict_n in config_pars:
                    if dict_n['axisname'].split("_")[-1] == axis.name and dict_n['axisname'].count(target_MP):
                        #print(dict_n)
                        #print(axis.name,target, dict_n['min'], dict_n['max'])

                        if axis.position < dict_n['min'] or axis.position > dict_n['max'] or target < dict_n['min'] or target > dict_n['max']:
                            raise ValueError(f"motor is outside of collision limits (",dict_n['min'],",",dict_n['max'],") defined for ({target_MP}) try changing the limits with micro_collision_limits.set_collision_limits()")  
                        else:
                            if verbose: 
                                print(f'Axis: {axis.name} is safe to move')     



class micro_collision_limits():#BeaconObject):
    """limits for micro_MP object"""
    #screen_in_microy = BeaconObject.property_setting('min',default=0)
    #max = BeaconObject.property_setting('max',default=0)
    #TODO: add settings to allow the config to be updated from inside Bliss and needs to be kept on a restart 

    def __init__(self, name, config, ):#path = ["collisionLimits"]):
        self.config = config
        self.name = name
        #self.collision_limits = dict()
        super().__init__()


    def set_collision_limits(self,motor_name="",value_min=0,value_max=0):
        self.get_collision_limits()
        #breakpoint()
        # TODO make this cleaner!!!
        if motor_name=="" and value_min==0 and value_max==0:
            if click.confirm('Do you want to change the settings?'):
                motor_name = click.prompt("enter the motor name ")
                value_min =  click.prompt("minimum ",default=-2.0) 
                value_max =  click.prompt("maximum ",default=90.0)
                print(f"{motor_name} limits set to ({value_min},{value_max})")
            
                
        outList = []
        for dict_n in self.config["collisionLimits"]:
            if dict_n['axisname'] == motor_name:
                print(f"Updating {dict_n['axisname']} ...")
                dict_n['min'] = value_min
                dict_n['max'] = value_max
                print(f"  {dict_n['axisname']}: ({dict_n['min']},{dict_n['max']})\n")
            outList.append(dict_n)
        
        self.config["collisionLimits"] = outList
        #print("NOW:")
        #self.get_collision_limits()


    def get_collision_limits(self,return_limits=False):
        print("Current limits imposed: ")
        for dict_n in self.config["collisionLimits"]:
            print(f"  {dict_n['axisname']}: ({dict_n['min']},{dict_n['max']})\n")
        
        if return_limits:
            return self.config["collisionLimits"]
                

    def enable_microscope(self,):
        self.set_collision_limits("screen_in_microy",-10,200)
        self.set_collision_limits("screen_out_microy",-10,200)
        self.set_collision_limits("screen_in_eta",-2,20)
        self.set_collision_limits("screen_out_eta",-10,200)
        print("...the microscope is enabled...")

    def disable_microscope(self,):
        self.set_collision_limits("screen_out_microy",120,121)
        self.set_collision_limits("screen_in_microy",120,121)
        self.set_collision_limits("screen_in_eta",-10,200)
        self.set_collision_limits("screen_out_eta",-10,200)
        print("...the microscope is disabled...")

    def disable_screen_in_microscope(self,):
        self.set_collision_limits("screen_in_microy",-1,1)
        print("...the microscope is disabled...")

    def disable_screen_out_microscope(self,):
        self.set_collision_limits("screen_out_microy",29,31)
        print("...the microscope is disabled...")







class MPfs2y(object):

    _position = 0

    def set_motor(self,motobj):
        self._fs2y= motobj
        self._fs2y.sync_hard()

    def get_position(self):
        return self._fs2y.position

    def move(self, position):
        if position >= 5: # TODO set value
            # we need to do something special
            self.reset_position_on_limit_switch()
            self._fs2y.move(position)
        else:
            # just make the move precision is not important
            self._fs2y.move(position)
             
    def reset_position_on_limit_switch(self,):
            self._fs2y.hw_limit(+1)
            self._fs2y.dial = 0
            self._fs2y.position = 5.625
            self._fs2y.limits = (-6,6)   # TODO set value

    

def create_pseudo_MPfs2y(name,config):
    fs2yMP = MPfs2y()
    fs2yMP.set_motor(config.get('dep_axis')) # current_session.env_dict["fs2y"])

    return SoftAxis( name, #'fs2y_motor', 
                     fs2yMP, 
                     position='get_position',
                     move=fs2yMP.move,
                     export_to_session=False)
                          
class MPfs2z(object):

    _position = 0

    def set_motor(self,motobj):
        self._fs2z= motobj
        self._fs2z.sync_hard()

    def get_position(self):
        return self._fs2z.position

    def move(self, position):
        print("hello")
        if position >= -1: # TODO set value
            # we need to do something special
            self.reset_position_on_limit_switch()
            self._fs2z.move(position)
        else:
            # just make the move precision is not important
            self._fs2z.move(position)
             
    def reset_position_on_limit_switch(self,):
            print("reset position on limit switch")
            self._fs2z.hw_limit(+1)
            self._fs2z.dial = 0
            self._fs2z.limits = (-119,0.5)  # TODO set value
            self._fs2z.position = 0

    

def create_pseudo_MPfs2z(name,config):
    fs2zMP = MPfs2z()
    fs2zMP.set_motor(config.get('dep_axis')) # current_session.env_dict["fs2z"])

    return SoftAxis( name, #'fs2z_motor', 
                     fs2zMP, 
                     position='get_position',
                     move=fs2zMP.move,
                     export_to_session=False)
                          




class MPfs1y(object):

    _position = 0

    def set_motor(self,motobj):
        self._fs1y= motobj
        self._fs1y.sync_hard()

    def get_position(self):
        return self._fs1y.position

    def move(self, position):
        if position > 7: 
            # we need to do something special
            self.reset_position_on_limit_switch()
            self._fs1y.move(position)
        else:
            # just make the move precision is not important
            self._fs1y.move(position)
             
    def reset_position_on_limit_switch(self,):
            self._fs1y.hw_limit(+1)
            self._fs1y.dial = 0
            self._fs1y.position = 7.662
            self._fs1y.limits = (-7.7,7.7)  

    

def create_pseudo_MPfs1y(name,config):
    fs1yMP = MPfs1y()
    fs1yMP.set_motor(config.get('dep_axis')) # current_session.env_dict["fs1y"])

    return SoftAxis( name, #'fs1y_motor', 
                     fs1yMP, 
                     position='get_position',
                     move=fs1yMP.move,
                     export_to_session=False)
                          
class MPfs1z(object):

    _position = 0

    def set_motor(self,motobj):
        self._fs1z= motobj
        self._fs1z.sync_hard()

    def get_position(self):
        return self._fs1z.position

    def move(self, position):
        print("hello")
        if position >= -1: 
            # we need to do something special
            self.reset_position_on_limit_switch()
            self._fs1z.move(position)
        else:
            # just make the move precision is not important
            self._fs1z.move(position)
             
    def reset_position_on_limit_switch(self,):
            print("reset position on limit switch")
            self._fs1z.hw_limit(+1)
            self._fs1z.dial = 0
            self._fs1z.limits = (-119,0.5)  
            self._fs1z.position = 0

    

def create_pseudo_MPfs1z(name,config):
    fs1zMP = MPfs1z()
    fs1zMP.set_motor(config.get('dep_axis')) # current_session.env_dict["fs1z"])

    return SoftAxis( name, #'fs1z_motor', 
                     fs1zMP, 
                     position='get_position',
                     move=fs1zMP.move,
                     export_to_session=False)



class MPatt11(object):

    _position = 0

    def set_motor(self,motobj):
        self._att11= motobj
        self._att11.sync_hard()

    def get_position(self):
        return self._att11.position

    def move(self, position):
        if position >= -5: 
            # we need to do something special
            self.reset_position_on_limit_switch()
            self._att11.move(position)
        else:
            # just make the move precision is not important
            self._att11.move(position)
             
    def reset_position_on_limit_switch(self,):
            print("reset position on limit switch")
            self._att11.hw_limit(+1)
            specutils.chg_dial(self._att11,0)
            self._att11.limits = (-119,0.5) 
            self._att11.position = 0

    

def create_pseudo_MPatt11(name,config):
    att11MP = MPatt11()
    att11MP.set_motor(config.get('dep_axis')) # current_session.env_dict["att11"])

    return SoftAxis( name, #'att11_motor', 
                     att11MP, 
                     position='get_position',
                     move=att11MP.move,
                     export_to_session=False)
                          
class MPatt12(object):

    _position = 0

    def set_motor(self,motobj):
        self._att12= motobj
        self._att12.sync_hard()

    def get_position(self):
        return self._att12.position

    def move(self, position):
        if position >= -5: 
            # we need to do something special
            self.reset_position_on_limit_switch()
            self._att12.move(position)
        else:
            # just make the move precision is not important
            self._att12.move(position)
             
    def reset_position_on_limit_switch(self,):
            print("reset position on limit switch")
            self._att12.hw_limit(+1)
            specutils.chg_dial(self._att12,0)
            self._att12.limits = (-119,0.5)  
            self._att12.position = 0

    

def create_pseudo_MPatt12(name,config):
    att12MP = MPatt12()
    att12MP.set_motor(config.get('dep_axis')) # current_session.env_dict["att12"])

    return SoftAxis( name, #'att12_motor', 
                     att12MP, 
                     position='get_position',
                     move=att12MP.move,
                     export_to_session=False)
            


class MPatt13(object):

    _position = 0

    def set_motor(self,motobj):
        self._att13= motobj
        self._att13.sync_hard()

    def get_position(self):
        return self._att13.position

    def move(self, position):
        if position >= -5: 
            # we need to do something special
            self.reset_position_on_limit_switch()
            self._att13.move(position)
        else:
            # just make the move precision is not important
            self._att13.move(position)
             
    def reset_position_on_limit_switch(self,):
            print("reset position on limit switch")
            self._att13.hw_limit(+1)
            specutils.chg_dial(self._att13,0)
            self._att13.limits = (-119,0.5)  
            self._att13.position = 0

    

def create_pseudo_MPatt13(name,config):
    att13MP = MPatt13()
    att13MP.set_motor(config.get('dep_axis')) # current_session.env_dict["att13"])

    return SoftAxis( name, #'att13_motor', 
                     att13MP, 
                     position='get_position',
                     move=att13MP.move,
                     export_to_session=False)
                          
class MPatt2(object):

    _position = 0

    def set_motor(self,motobj):
        self._att2= motobj
        self._att2.sync_hard()

    def get_position(self):
        return self._att2.position

    def move(self, position):
        if position >= 0: 
            self.reset_position_on_limit_switch()
            self._att2.move(position)
        else:
            # just make the move precision is not important
            self._att2.move(position)
             
    def reset_position_on_limit_switch(self,):
            print("reset position on limit switch")
            self._att2.hw_limit(-1)
            specutils.chg_dial(self._att2,0)
            sync(self._att2)
            self._att2.limits = (-105,1) 
            self._att2.position = 1

    

def create_pseudo_MPatt2(name,config):
    att2MP = MPatt2()
    att2MP.set_motor(config.get('dep_axis')) # current_session.env_dict["att2"])

    return SoftAxis( name, #'att2_motor', 
                     att2MP, 
                     position='get_position',
                     move=att2MP.move,
                     export_to_session=False)

class MPexp1(object):

    _position = 0

    def set_motor(self,motobj):
        self._exp1= motobj
        self._exp1.sync_hard()

    def get_position(self):
        return self._exp1.position

    def move(self, position):
        if position < 0.5: 
            # we need to do something special
            self.reset_position_on_limit_switch()
            self._exp1.move(position)
        else:
            # just make the move precision is not important
            self._exp1.move(position)
             
    def reset_position_on_limit_switch(self,):
            print("reset position on limit switch")
            self._exp1.hw_limit(+1)
            self._exp1.dial = 0
            self._exp1.limits = (-1,95)  
            self._exp1.position = 0

    

def create_pseudo_MPexp1(name,config):
    exp1MP = MPexp1()
    exp1MP.set_motor(config.get('dep_axis')) # current_session.env_dict["exp1"])

    return SoftAxis( name, #'exp1_motor', 
                     exp1MP, 
                     position='get_position',
                     move=exp1MP.move,
                     export_to_session=False)


class MPqbpmy(object):

    _position = 0

    def set_motor(self,motobj):
        self._qbpmy= motobj
        self._qbpmy.sync_hard()

    def get_position(self):
        return self._qbpmy.position

    def move(self, position):
        if position <= -5: 
            # we need to do something special
            self.reset_position_on_limit_switch()
            self._qbpmy.move(position)
        else:
            # just make the move precision is not important
            self._qbpmy.move(position)
             
    def reset_position_on_limit_switch(self,):
            self._qbpmy.hw_limit(-1)
            specutils.chg_dial(self._qbpmy,0)
            self._qbpmy.limits = (-6.75,6.75) 
            self._qbpmy.position = -6.25 # i.e. we are on the left of the stroke

    

def create_pseudo_MPqbpmy(name,config):
    qbpmyMP = MPqbpmy()
    qbpmyMP.set_motor(config.get('dep_axis')) # current_session.env_dict["qbpmy"])

    return SoftAxis( name, #'qbpmy_motor', 
                     qbpmyMP, 
                     position='get_position',
                     move=qbpmyMP.move,
                     export_to_session=False)

class MPqbpmz(object):

    _position = 0

    def set_motor(self,motobj):
        self._qbpmz= motobj
        self._qbpmz.sync_hard()

    def get_position(self):
        return self._qbpmz.position

    def move(self, position):
        if position >= -1: 
            # we need to do something special
            self.reset_position_on_limit_switch()
            self._qbpmz.move(position)
        else:
            # just make the move precision is not important
            self._qbpmz.move(position)
             
    def reset_position_on_limit_switch(self,):
            self._qbpmz.hw_limit(+1)
            specutils.chg_dial(self._qbpmz,0)
            self._qbpmz.limits = (-91,0.5) 
            self._qbpmz.position = 0

    

def create_pseudo_MPqbpmz(name,config):
    qbpmzMP = MPqbpmz()
    qbpmzMP.set_motor(config.get('dep_axis')) # current_session.env_dict["qbpmz"])

    return SoftAxis( name, #'qbpmz_motor', 
                     qbpmzMP, 
                     position='get_position',
                     move=qbpmzMP.move,
                     export_to_session=False)


class MPbv1h(object):

    _position = 0

    def set_motor(self,motobj):
        self._bv1h= motobj
        self._bv1h.sync_hard()

    def get_position(self):
        return self._bv1h.position

    def move(self, position):
        if position <= 1: 
            # we need to do something special
            self.reset_position_on_limit_switch()
            self._bv1h.move(position)
        else:
            # just make the move precision is not important
            self._bv1h.move(position)
             
    def reset_position_on_limit_switch(self,):
            self._bv1h.hw_limit(+1)
            specutils.chg_dial(self._bv1h,0)
            self._bv1h.limits = (-0.5,8) 
            self._bv1h.position = 0

    

def create_pseudo_MPbv1h(name,config):
    bv1hMP = MPbv1h()
    bv1hMP.set_motor(config.get('dep_axis')) # current_session.env_dict["qbpmz"])

    return SoftAxis( name, #'bv1_motor', 
                     bv1hMP, 
                     position='get_position',
                     move=bv1hMP.move,
                     export_to_session=False)