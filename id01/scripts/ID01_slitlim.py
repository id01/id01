# ID01 BLISS git repo
#       authors:
#         Peter Boesecke, boesecke@esrf.fr
#
#----------------------------------------------------------------------
# Description:
#       class SlitLim()
#
# History:
#   2022-12-06  PB SlitLim()
#      Defined as bliss object in beamline_configuration/CONTROLLERS/slitlim.yml
#                  - plugin: bliss
#                    package: id01.scripts.ID01_slitlim
#                    class: SlitLim
#                    name: slitlim
#
#                    slitlim_dict:
#                       ...
#   2023-01-16 PB Default parameters corrected
#
#----------------------------------------------------------------------

import numpy as np
from bliss.controllers.motor import CalcController
from bliss.common.axis import Axis as AxisBase
from bliss.config.settings import HashObjSetting

from bliss.setup_globals import *
from bliss.common import session
from bliss.config import static

from id01.scripts.specmot_utils import *

import time

import logging
#logging.basicConfig(level=logging.DEBUG)
#logging.basicConfig(level=logging.WARNING)
#logging.debug('This will get logged')
#logging.warning('This will get logged')

_log=logging.getLogger("id01.scripts.ID01_slitlim")
#The log level can be set in the bliss session after import logging
#and defining _log as in the above command.
#_log.setLevel("DEBUG")

class SlitLim():
    """
    SlitLim allows searching in parallel search switches for a 
    set of motors that can be defined for each slits in slitlim_dict.
    After arriving at these switches the motor values 
    can be changed to predefined positions for calibration.

    Updating/defining slitlim.slitlim_dict parameters
      All values are strings
    Set all parameters to default values:
      slitlim.default()
    Update a parameter
      slitlim.slitlim_dict[<slit_name>][<motor_name>][<item_name>]=<item_value>
    Save current parameters to the CONTROLLERS/slitlim.yml file
      slitlim.savetoyml()
    """

    def __init__(self,ctrl_name,config,*args,**kwargs):
        '''
        The parameters are kept in slitlim_dict.
        '''
        self._config=config

        if 'slitlim_dict' in config:
            self.slitlim_dict=config['slitlim_dict']
        else:
            self.default()
            self.savetoyml()

    def savetoyml(self):
        '''
        Save current ID01 slitlim parameters to yml file
        '''
        self._config['slitlim_dict']=self.slitlim_dict
        self._config.save()


    def default(self):
        '''
        Set slit parameters to default values
        '''

        # default slit configuration
        self.slitlim_dict={}

        # The dictionary can be defined in <object>.yml

        #slit motors for limit searach
        # for a new slitlim function

        slitlim_dict={}

        #p1, slittype 3 2016-01-27
        par={}
        par['p1f']={'search':"lim+",'dial_pos':'0.0','offset':'-5.74900','dial_lim':'(-12.3, 0)'} # mechanical problem close to lim-
        par['p1b']={'search':"lim+",'dial_pos':'0.0','offset':'7.50400','dial_lim':'(-11.5, 0)'} # mechanical problem close to lim-

        par['p1u']={'search':"lim-",'dial_pos':'0.0','offset':'-7.31335','dial_lim':'(0, 12.2)'}
        par['p1d']={'search':"lim-",'dial_pos':'0.0','offset':'7.15970','dial_lim':'(0, 12.0)'}
        slitlim_dict['p1']=par


        #p2, slittype 1 2015-04-12
        par={}

        par['p2f']={'search':"lim+",'dial_pos':'0.0','offset':'17.49450','dial_lim':'(-inf, +inf)'}
        par['p2b']={'search':"lim+",'dial_pos':'0.0','offset':'10.934000','dial_lim':'(-inf, +inf)'}
        par['p2u']={'search':"lim+",'dial_pos':'0.0','offset':'9.718000','dial_lim':'(-inf, +inf)'}
        par['p2d']={'search':"lim+",'dial_pos':'0.0','offset':'12.605500','dial_lim':'(-inf, +inf)'}

        slitlim_dict['p2']=par

        #s1, slittype 1 2014-11-05
        par={}

        par['s1f']={'search':"lim+",'dial_pos':'0.0','offset':'14.26200','dial_lim':'(-31.0, 0.0)'}
        par['s1b']={'search':"lim+",'dial_pos':'0.0','offset':'15.96800','dial_lim':'(-31.0, 0.0)'}
        par['s1u']={'search':"lim+",'dial_pos':'0.0','offset':'9.73500','dial_lim':'(-24.0, 0.0)'}
        par['s1d']={'search':"lim+",'dial_pos':'0.0','offset':'13.11150','dial_lim':'(-24.0, 0.0)'}

        slitlim_dict['s1']=par

        #s2, slittype 1 2014-11-05
        par={}

        par['s2f']={'search':"lim+",'dial_pos':'0.0','offset':'43.77550','dial_lim':'(-91.0, 0.0)'}
        par['s2b']={'search':"lim+",'dial_pos':'0.0','offset':'46.21250','dial_lim':'(-91.0, 0.0)'}
        par['s2u']={'search':"lim+",'dial_pos':'0.0','offset':'42.70000','dial_lim':'(-91.0, 0.0)'}
        par['s2d']={'search':"lim+",'dial_pos':'0.0','offset':'47.46750','dial_lim':'(-91.0, 0.0)'}

        slitlim_dict['s2']=par

        #s3, slittype 1, 2014-11-05 Peyronnard
        par={}

        par['s3f']={'search':"lim+",'dial_pos':'0.0','offset':'8.24800','dial_lim':'(-inf, +inf)'}
        par['s3b']={'search':"lim+",'dial_pos':'0.0','offset':'10.75660','dial_lim':'(-inf, +inf)'}
        par['s3u']={'search':"lim+",'dial_pos':'0.0','offset':'2.08340','dial_lim':'(-inf, +inf)'}
        par['s3d']={'search':"lim+",'dial_pos':'0.0','offset':'7.86040','dial_lim':'(-inf, +inf)'}

        slitlim_dict['s3']=par

        #s4, slittype 4 2014-11-19 Peyronnard
        par={}

        par['s4f']={'search':"lim-",'dial_pos':'0.0','offset':'3.425','dial_lim':'(-inf, +inf)'}
        par['s4b']={'search':"lim+",'dial_pos':'0.0','offset':'5.9728','dial_lim':'(-inf, +inf)'}
        par['s4u']={'search':"lim+",'dial_pos':'0.0','offset':'4.8232','dial_lim':'(-inf, +inf)'}
        par['s4d']={'search':"lim-",'dial_pos':'0.0','offset':'7.4896','dial_lim':'(-inf, +inf)'}
 
        slitlim_dict['s4']=par

        #s5, slittype 1
        par={}

        par['s5apos']={'search':"lim+",'dial_pos':'0.0','offset':'1.345985','dial_lim':'(-inf, +inf)'}
        par['s5aneg']={'search':"lim+",'dial_pos':'0.0','offset':'1.540876','dial_lim':'(-inf, +inf)'}
        par['s5bpos']={'search':"lim+",'dial_pos':'0.0','offset':'1.062044','dial_lim':'(-inf, +inf)'}
        par['s5bneg']={'search':"lim+",'dial_pos':'0.0','offset':'1.062044','dial_lim':'(-inf, +inf)'}

        slitlim_dict['s5']=par

        #s6, slittype 2
        par={}

        par['s6ag']={'search':"lim+",'dial_pos':'0.0','offset':'5.4895','dial_lim':'(0.2, -6)'}
        par['s6ao']={'search':"lim+",'dial_pos':'0.0','offset':'7.5','dial_lim':'(0.2, -16)'}
        par['s6bg']={'search':"lim+",'dial_pos':'0.0','offset':'5.000','dial_lim':'(0.2, -6)'}
        par['s6bo']={'search':"lim+",'dial_pos':'0.0','offset':'7.5','dial_lim':'(0.2, -16)'}

        slitlim_dict['s6']=par

        #s7, slittype 2
        par={}

        par['s7ag']={'search':"lim+",'dial_pos':'0.0','offset':'5.161342','dial_lim':'(0.2, -6)'}
        par['s7ao']={'search':"lim+",'dial_pos':'0.0','offset':'7.5','dial_lim':'(0.2, -16)'}
        par['s7bg']={'search':"lim+",'dial_pos':'0.0','offset':'5.414005','dial_lim':'(0.2, -6)'}
        par['s7bo']={'search':"lim+",'dial_pos':'0.0','offset':'7.5','dial_lim':'(0.2, -16)'}

        slitlim_dict['s7']=par

        #s8, slittype 1, Huber nose slit
        # 2017-12-13
        par={}

        par['s8apos']={'search':"lim+",'dial_pos':'0.0','offset':'1.26571','dial_lim':'(-inf, +inf)'}
        par['s8aneg']={'search':"lim+",'dial_pos':'0.0','offset':'1.55071','dial_lim':'(-inf, +inf)'}
        par['s8bpos']={'search':"lim+",'dial_pos':'0.0','offset':'1.61643','dial_lim':'(-inf, +inf)'}
        par['s8bneg']={'search':"lim+",'dial_pos':'0.0','offset':'1.61643','dial_lim':'(-inf, +inf)'}

        slitlim_dict['s8']=par

        self.slitlim_dict=slitlim_dict

    def __info__(self):
        self.show()

    def __info__(self,slit_name=None):

        slit_names=[]
        if slit_name is None:
            for slit_name in self.slitlim_dict:
                slit_names.append(slit_name)
        else: 
            slit_names.append(slit_name)
        
        info=""
        for slit_name in slit_names:
            spaces=""
            info+="{}{}:\n".format(spaces,slit_name)
            slit_motors=self.slitlim_dict[slit_name]
            for motor_name in slit_motors:
                spaces="    "
                info+="{}{}:\n".format(spaces,motor_name)
                motor_items=slit_motors[motor_name]
                for item_name in motor_items:
                    spaces="        "
                    motor_item=motor_items[item_name]
                    info+="{}{}:{}\n".format(spaces,item_name,motor_item)
                
        info+="\n"
        info+="# Updating/defining slitlim.slitlim_dict parameters\n"
        info+="#   All values are strings\n"
        info+="# Set all parameters to default values:\n"
        info+="#   slitlim.default()\n"
        info+="# Update a parameter\n"
        info+="#   slitlim.slitlim_dict[<slit_name>][<motor_name>][<item_name>]=<item_value>\n"
        info+="# Save current parameters to the CONTROLLERS/slitlim.yml file\n"
        info+="#   slitlim.savetoyml()\n"
        return(info)

    def show(self,slit_name=None):
        print(self.__info__(slit_name))


    def search(self,slitname=None,changedial=False,setoffset=False,setdiallim=False):
        '''
        Goes to the search position (lim+, lim- or home) and sets the dial and user position to the values
        in slitlim_dict.
        changedial: if set, changes the dial positions to the tabulated values without changing the offsets after the search
        setoffset: if set, sets the offsets after the search
        '''

        _log.debug("search BEGIN")

        if slitname is None or slitname not in self.slitlim_dict:
            print("USAGE: slitlim.search(slitname,changedial=False,setoffset=False,setdiallim=False)")
            print("    slitname: ",end='')
            sep=""
            for key in self.slitlim_dict: 
                print("{}'{}'".format(sep,key),end='')
                sep=","
            print()
            return

        # get slitlim_dict[slitname[
        par=self.slitlim_dict[slitname]

        # get the motors of slitname
        motors=()
        for motor_name in par:
            motors+=(static.get_config().get(motor_name),)

        for num in range(0,len(motors)):
            mot=motors[num]
            mot_name=mot.name

            _log.debug("    {}".format(mot_name))
            mot_par=par[mot_name]
            mot_search=mot_par.get('search')

            if mot_search is not None:
                _log.debug("      chg_dial({},{})".format(mot_name,mot_search))
                chg_dial(mot,mot_search)

        # wait for end of movements
        for num in range(0,len(motors)):
            mot=motors[num]
            mot.wait_move()

        # change and set positions
        for num in range(0,len(motors)):
            mot=motors[num]
            mot_name=mot.name
            mot_par=par[mot_name]

            if changedial:
                mot_dial_pos=float(mot_par.get('dial_pos'))
                if mot_dial_pos is not None:
                    _log.debug("      chg_dial({},{})".format(mot_name,mot_dial_pos))
                    chg_dial(mot,mot_dial_pos)

                if setdiallim:
                    mot_dial_lim=mot_par['dial_lim']
                    mot_dial_lim=eval(mot_dial_lim.replace('inf','float("inf")'))
                    _log.debug("      {} dial_lim={}".format(mot_name,mot_dial_lim))
                    mot.dial_limits=mot_dial_lim

            if setoffset:
                current_offset=float(mot.offset)
                _log.debug("      {} offset: {}".format(mot_name,current_offset))

                mot_offset=float(mot_par.get('offset'))
                if mot_offset is not None:
                    _log.debug("      {} new offset: {}".format(mot_name,mot_offset))
                    mot.offset=mot_offset

        _log.debug("search END")
        

