# -*- coding: utf-8 -*-

# ID01 BLISS git repo
#       authors:
#       Roberto Homs-Regojo  
#       Documentation >> Steven Leake, leake@esrf.fr
#
#       Created at: 04. Apr 10:30:00 CET 2022
#
#----------------------------------------------------------------------
# Description: 
#    lookupscan which goes back to the start position at the end was 
#    requested by EB 
#
#     
#
# FUNCTIONS:
#    lookupscan_returntostart
#----------------------------------------------------------------------
import typeguard
from typing import Optional

from bliss.common.utils import rounder, shorten_signature, typeguardTypeError_to_hint
from bliss.common.scans import lookupscan
from bliss.common.cleanup import cleanup, axis as cleanup_axis


from bliss.common.types import (
    _int,
    _float,
    _countable,
    _countables,
    _scannable_start_stop_list,
    _position_list,
    _scannable_position_list,
    _scannable_position_list_list,
)

@typeguardTypeError_to_hint
@shorten_signature(hidden_kwargs=["title", "name", "scan_type", "return_scan"])
@typeguard.typechecked
def lookupscan_return2start(
    motor_pos_tuple_list: _scannable_position_list_list,
    count_time,
    *counter_args: _countables,
    scan_type: str = "lookupscan",
    name: str = "lookupscan",
    title: Optional[str] = None,
    save: bool = True,
    save_images: Optional[bool] = None,
    sleep_time: Optional[_float] = None,
    run: bool = True,
    return_scan: bool = True,
    scan_info: Optional[dict] = None,
    scan_params: Optional[dict] = None,
):

    axislist=[]
    for mot,pos in motor_pos_tuple_list:
        axislist.append(mot)

    with cleanup(*axislist, restore_list=(cleanup_axis.POS,)):
        return lookupscan(
            motor_pos_tuple_list,
            count_time,
            *counter_args,
            scan_type=scan_type,
            name=name,
            title=title,
            save=save,
            save_images=save_images,
            sleep_time=sleep_time,
            run=run,
            return_scan=return_scan,
            scan_info=scan_info,
            scan_params=scan_params,
            )
