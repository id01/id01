============
ID01 project
============

[![build status](https://gitlab.esrf.fr/ID01/id01/badges/master/build.svg)](http://ID01.gitlab-pages.esrf.fr/ID01)
[![coverage report](https://gitlab.esrf.fr/ID01/id01/badges/master/coverage.svg)](http://ID01.gitlab-pages.esrf.fr/id01/htmlcov)

ID01 software & configuration

Latest documentation from master can be found [here](http://ID01.gitlab-pages.esrf.fr/id01)
